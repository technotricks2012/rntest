import React from 'react';
import {Text, RecyclerViewBackedScrollViewBase} from 'react-native';
import {StackNavigator, DrawerNavigator} from 'react-navigation';
import LoginScreen from '../Screen/Login';
import Auth from '../Screen/Auth';
import LogInfo from '../Screen/LogInfo';


// import SignupScreen from '../Containers/SignupScreen'
// import ForgottenPasswordScreen from '../Containers/ForgottenPasswordScreen'
import Dashboard from '../Screen/Home';
import Admin from '../Screen/Admin';
import Mode from '../Screen/Mode';
import RoadDefect from '../Screen/RoadDefect';


import {getLocal} from '../db/DbManager';


import {createDrawerNavigator} from 'react-navigation-drawer';


import {createAppContainer} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';
import transitionConfig from '../modules/transitionConfig'


// drawer stack
const DrawerStack1 = createDrawerNavigator(
  {

    // RoadDefect: {
    //   screen: RoadDefect,
    // },
    Dashboard: {
    
      screen: Dashboard,
    },
    Admin: {
      screen: Admin,

      
    },
 
    Mode: {screen: Mode},
    
    LogInfo: {screen: LogInfo},
  },
  {
    // drawerBackgroundColor: 'black',
    // initialRouteName: 'Admin',

    headerMode: 'none',

    transitionConfig: transitionConfig,
    drawerWidth: 200,
    drawerType: 'back',
    // swipeEdgeWidth:200,
    // gestureHandlerProps?: React.ComponentProps<typeof PanGestureHandler>;
  },
);

const DrawerStack2 = createDrawerNavigator(
  {

    RoadDefect: {
      screen: RoadDefect,
    },
 
    Admin: {
      screen: Admin,

      
    },
 
    Mode: {screen: Mode},
    
    LogInfo: {screen: LogInfo},
  },
  {
    // drawerBackgroundColor: 'black',
    // initialRouteName: 'Admin',

    headerMode: 'none',

    transitionConfig: transitionConfig,
    drawerWidth: 200,
    drawerType: 'back',
    // swipeEdgeWidth:200,
    // gestureHandlerProps?: React.ComponentProps<typeof PanGestureHandler>;
  },
);

const DrawerNavigation1 = createStackNavigator(
  {
    DrawerStack: {screen: DrawerStack1},
  },
  {
    headerMode: 'none',
  },
);


const DrawerNavigation2 = createStackNavigator(
  {
    DrawerStack: {screen: DrawerStack2},
  },
  {
    headerMode: 'none',
  },
);

// login stack
const LoginStack = createStackNavigator(
  {
    loginScreen: {screen: LoginScreen},
    //   signupScreen: { screen: SignupScreen },
    //   forgottenPasswordScreen: { screen: ForgottenPasswordScreen, navigationOptions: { title: 'Forgot Password' } }
  },
  {
    headerMode: 'none',
  },
);


// Manifest of possible screens
const PrimaryNav = createStackNavigator(
  {
    auth:{screen:Auth},
    loginStack: {screen: LoginStack},
    drawerStack1: {screen: DrawerNavigation1},
    drawerStack2: {screen: DrawerNavigation2},

  },

  {
    // Default config for all screens
    headerMode: 'none',
    // title: 'Main',
    initialRouteName: 'auth',
  },

  // {
  //   // initialRouteName: 'Menu',
  //   defaultNavigationOptions: {
  //     // headerStyle: {
  //     //   backgroundColor: '#f4511e',
  //     // },
  //     // headerTintColor: '#fff',
  //     // headerTitleStyle: {
  //     //   fontWeight: 'bold',
  //     // },
  //     header: null,
  //   },
  // },
);

export default createAppContainer(PrimaryNav);
