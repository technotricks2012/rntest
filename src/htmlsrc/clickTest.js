// const

export default './clickEvent.html'

var html = `
 
<!DOCTYPE html>
<html>
<body>

<p>Click the "Try it" button to create a BUTTON element with a "Click me" text.</p>

<button onclick="myFunction()">Click Me</button>

<script>
function myFunction() {
  
  window.postMessage("Sending data from WebView");
  alert('hi')

}
</script>

</body>
</html>

`;