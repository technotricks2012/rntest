import {Model,Q} from '@nozbe/watermelondb';
import {field} from '@nozbe/watermelondb/decorators';
import database from '../config';
export const TBL_Magnetometer = 'Magnetometer';
import {getBatchID} from './Batch';

export default class Magnetometer extends Model {
  static table = TBL_Magnetometer;

  @field('batch_id')
  batch_id;
  @field('timestamp')
  timestamp;
  @field('x')
  x;
  @field('y')
  y;
  @field('z')
  z;
  @field('mode')
  mode;
  @field('predictionMode')
  predictionMode;
  @field('journeyID')
  journeyID;
}

export async function addMagnetometer(batchId,mode,predictionMode,journeyID,magnetometerData) {
  // return await database.action(async () => {

  const newPost = await database.collections
    .get(TBL_Magnetometer)
    .create(tbl => {
      tbl.timestamp = magnetometerData.timestamp.toString();
      tbl.x = magnetometerData.x.toString();
      tbl.y = magnetometerData.y.toString();
      tbl.z = magnetometerData.z.toString();
      tbl.mode =mode.toString();
      tbl.predictionMode =predictionMode.toString();

      tbl.journeyID = journeyID.toString();
      tbl.batch_id = batchId.toString();
    });
  console.log(`Inserted new Magnetometer sensor data` + newPost);
  return newPost;
  // });
}



export async function getMagnetometer(batchID) {
  const newPost = await database.collections
    .get(TBL_Magnetometer)
    .query(Q.where('batch_id', batchID))
    .fetch();
  return newPost;
}


export async function getAllMagnetometer() {
  const newPost = await database.collections
    .get(TBL_Magnetometer)
    .query()
    .fetch();
  return newPost;
}


export async function deleteMagnetometer(batchID) {
  const newPost = await database.collections
    .get(TBL_Magnetometer)
    .query(Q.where('batch_id', batchID))
    .destroyAllPermanently();
  return newPost;
}
