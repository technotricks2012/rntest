import {Model, Q} from '@nozbe/watermelondb';
import {field} from '@nozbe/watermelondb/decorators';
export const TBL_LightSensor = 'LightSensor';
import database from '../config';
import {getBatchID} from './Batch';

var initLight = -1;

export default class LightSensor extends Model {
  static table = TBL_LightSensor;

  @field('batch_id')
  batch_id;
  @field('timestamp')
  timestamp;
  @field('light')
  light;
  @field('mode')
  mode;
  @field('predictionMode')
  predictionMode;
  @field('journeyID')
  journeyID;
}

export async function addLightSensor(
  batchId,
  mode,
  predictionMode,
  journeyID,
  lightSensorData,
  timestamp,
) {
  // if (lightSensorData.light == initLight) return;
  // initLight = lightSensorData.light;

  const newPost = await database.collections
    .get(TBL_LightSensor)
    .create(tbl => {
      tbl.timestamp = timestamp.toString();
      tbl.light = lightSensorData.light.toString();
      tbl.mode = mode.toString();
      tbl.predictionMode = predictionMode.toString();
      tbl.journeyID = journeyID.toString();
      tbl.batch_id = batchId.toString();
    });
  console.log(`Inserted new LightSensor sensor data` + newPost);

  return newPost;
}

export async function getLightSensor(batchID) {
  const newPost = await database.collections
    .get(TBL_LightSensor)
    .query(Q.where('batch_id', batchID))
    .fetch();
  return newPost;
}


export async function getAllLightSensor() {
  const newPost = await database.collections
    .get(TBL_LightSensor)
    .query()
    .fetch();
  return newPost;
}

export async function deleteLightSensor(batchID) {
  const newPost = await database.collections
    .get(TBL_LightSensor)
    .query(Q.where('batch_id', batchID))
    .destroyAllPermanently();
  return newPost;
}
