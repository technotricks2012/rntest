import {Model, Q} from '@nozbe/watermelondb';
import {field} from '@nozbe/watermelondb/decorators';
export const TBL_Thermometer = 'Thermometer';
import database from '../config';
import {getBatchID} from './Batch';

export default class Thermometer extends Model {
  static table = TBL_Thermometer;

  @field('batch_id')
  batch_id;
  @field('timestamp')
  timestamp;
  @field('temp')
  temp;
  @field('mode')
  mode;
  @field('predictionMode')
  predictionMode;
  @field('journeyID')
  journeyID;
}

export async function addThermometer(batchId,mode,predictionMode,journeyID,thermometerData,timestamp) {
  const newPost = await database.collections
    .get(TBL_Thermometer)
    .create(tbl => {
      tbl.timestamp = timestamp.toString();
      tbl.temp = thermometerData.temp.toString();
      tbl.mode = mode.toString();
      tbl.predictionMode = predictionMode.toString();

      tbl.journeyID = journeyID.toString();
      tbl.batch_id = batchId.toString();
    });
  console.log(`Inserted new Thermometer sensor data` + newPost);

  return newPost;
}

export async function getThermometer(batchID) {
  const newPost = await database.collections
    .get(TBL_Thermometer)
    .query(Q.where('batch_id', batchID))
    .fetch();
  return newPost;
}


export async function getAllThermometer() {
  const newPost = await database.collections
    .get(TBL_Thermometer)
    .query()
    .fetch();
  return newPost;
}




export async function deleteThermometer(batchID) {
  const newPost = await database.collections
    .get(TBL_Thermometer)
    .query(Q.where('batch_id', batchID))
    .destroyAllPermanently()
  return newPost;
}