import {Model, Q} from '@nozbe/watermelondb';
import {field, action} from '@nozbe/watermelondb/decorators';
import database from '../config';
import {getBatchID} from './Batch';

export const TBL_Location = 'Location';

export default class Location extends Model {
  static table = TBL_Location;

  @field('batch_id')
  batch_id;
  @field('timestamp')
  timestamp;
  @field('latitude')
  latitude;
  @field('longitude')
  longitude;
  @field('altitude')
  altitude;
  @field('mode')
  mode;
  @field('predictionMode')
  predictionMode;
  @field('journeyID')
  journeyID;
}

export async function addLocation(batchId,mode,predictionMode,journeyID,locationData) {
  
  const newPost = await database.collections.get(TBL_Location).create(tbl => {
    tbl.timestamp = locationData.timestamp.toString();
    tbl.latitude = locationData.latitude.toString();
    tbl.longitude = locationData.longitude.toString();
    tbl.altitude = locationData.altitude.toString();
    tbl.mode = mode.toString();
    tbl.predictionMode = predictionMode.toString();

    tbl.journeyID = journeyID.toString();
    tbl.batch_id = batchId;
  });
  console.log(`Inserted new Location  data` + newPost);

  return newPost;
}

export async function getLocation(batchID) {
  const newPost = await database.collections
    .get(TBL_Location)
    .query(Q.where('batch_id', batchID))
    .fetch();
  return newPost;
}



export async function getAllLocation() {
  const newPost = await database.collections
    .get(TBL_Location)
    .query()
    .fetch();
  return newPost;
}
export async function deleteLocation(batchID) {
  const newPost = await database.collections
    .get(TBL_Location)
    .query(Q.where('batch_id', batchID))
    .destroyAllPermanently();
  return newPost;
}
