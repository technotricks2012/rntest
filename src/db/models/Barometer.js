import {Model,Q} from '@nozbe/watermelondb';
import {field} from '@nozbe/watermelondb/decorators';
import database from '../config';
import {getBatchID} from './Batch';

export const TBL_Barometer = 'Barometer';

export default class Barometer extends Model {
  static table = TBL_Barometer;

  @field('batch_id')
  batch_id;
  @field('timestamp')
  timestamp;
  @field('pressure')
  pressure;
  @field('mode')
  mode;
  @field('predictionMode')
  predictionMode;
  @field('journeyID')
  journeyID;
}

export async function addBarometer(batchId,mode,predictionMode,journeyID,barometer, timestamp) {
  const newPost = await database.collections.get(TBL_Barometer).create(tbl => {
    tbl.timestamp = timestamp.toString();
    tbl.pressure = barometer.pressure.toString();

    tbl.mode = mode.toString();
    tbl.predictionMode = predictionMode.toString();

    tbl.journeyID = journeyID.toString();
    tbl.batch_id = batchId.toString();
  });
  console.log(`Inserted new Barometer sensor data` + newPost);
  return newPost.id;
}

export async function getBarometer(batchID) {
  const newPost = await database.collections
    .get(TBL_Barometer)
    .query(Q.where('batch_id', batchID))
    .fetch();
  return newPost;
}


export async function getAllBarometer() {
  const newPost = await database.collections
    .get(TBL_Barometer)
    .query()
    .fetch();
  return newPost;
}

export async function deleteBarometer(batchID) {
  const newPost = await database.collections
    .get(TBL_Barometer)
    .query(Q.where('batch_id', batchID))
    .destroyAllPermanently()
  return newPost;
}
