import {Model, Q} from '@nozbe/watermelondb';
import {field} from '@nozbe/watermelondb/decorators';
export const TBL_Orientation = 'Orientation';
import database from '../config';
import {getBatchID} from './Batch';

export default class Orientation extends Model {
  static table = TBL_Orientation;

  @field('batch_id')
  batch_id;
  @field('timestamp')
  timestamp;
  @field('azimuth')
  azimuth;
  @field('pitch')
  pitch;
  @field('roll')
  roll;
  @field('mode')
  mode;
  @field('predictionMode')
  predictionMode;
  @field('journeyID')
  journeyID;
}

export async function addOrientation(batchId,mode,predictionMode,journeyID,orientationData,timestamp) {
  const newPost = await database.collections
    .get(TBL_Orientation)
    .create(tbl => {
      tbl.timestamp = timestamp.toString();
      tbl.azimuth = orientationData.azimuth.toString();
      tbl.pitch = orientationData.pitch.toString();
      tbl.roll = orientationData.roll.toString();
      tbl.mode = mode.toString();
      tbl.predictionMode = predictionMode.toString();

      tbl.journeyID = journeyID.toString();
      tbl.batch_id = batchId.toString();
    });
  console.log(`Inserted new Orientation sensor data` + newPost);

  return newPost;
}

export async function getOrientation(batchID) {
  const newPost = await database.collections
    .get(TBL_Orientation)
    .query(Q.where('batch_id', batchID))
    .fetch();
  return newPost;
}


export async function getAllOrientation() {
  const newPost = await database.collections
    .get(TBL_Orientation)
    .query()
    .fetch();
  return newPost;
}



export async function deleteOrientation(batchID) {
  const newPost = await database.collections
    .get(TBL_Orientation)
    .query(Q.where('batch_id', batchID))
    .destroyAllPermanently()
  return newPost;
}