import {Model, Q} from '@nozbe/watermelondb';
import {field, lazy} from '@nozbe/watermelondb/decorators';
import database from '../config';

export const TBL_Batch = 'Batch';

import {NEW, UPLOADED, UPLOADING} from '../DbManager';
import {getCurrentTime} from '../../utils/Utils';


export default class Batch extends Model {
  static table = TBL_Batch;
  @field('timestamp')
  timestamp;
  @field('status')
  status;
}


export async function getAllBatch() {

  const newPost = await database.collections
    .get(TBL_Batch).query()
    .fetch();

  return newPost;
}


export async function getBatchID() {
  // return await database.action(async () => {
  const batchCollection = await database.collections.get(TBL_Batch);

  // try to find post by id
  const batch = await batchCollection.query(Q.where('status', NEW)).fetch();
  const currentTime = getCurrentTime();


  if (batch.length > 0) {
    return batch.pop().id;
  } else {
    const newBatch = await batchCollection.create(post => {
      post.status = NEW;
      post.timestamp = currentTime;
    });
    return newBatch.id;
  }
  // });
}

export async function updateBatchStatus(statusVal) {
  const batchCollection = await database.collections.get(TBL_Batch);
  const batch = await batchCollection.query(Q.where('status', NEW)).fetch();

  if (batch.length > 0) {
    const batchId = batch.pop().id;

    const post = await batchCollection.find(batchId);
    const currentTime = getCurrentTime();

    await post.update(post => {
      post.status = statusVal;
      post.timestamp = currentTime;
    });

    return post.id;
  }
  return null;
}

export async function updateBatchStatusWithBatchID(batchId,statusVal) {
  const batchCollection = await database.collections.get(TBL_Batch);
  const batch = await batchCollection.find(batchId);
  const currentTime = getCurrentTime();
  batch.update(item => {
    item.status = statusVal;
    item.timestamp = currentTime;
  });
  return batch;
}
