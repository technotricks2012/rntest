import {Model, Q} from '@nozbe/watermelondb';
import {field} from '@nozbe/watermelondb/decorators';
export const TBL_Pedometer = 'Pedometer';
import database from '../config';
import {getBatchID} from './Batch';

var initNumberOfSteps = -1
export default class Pedometer extends Model {
  static table = TBL_Pedometer;

  @field('batch_id')
  batch_id;
  @field('timestamp')
  timestamp;
  @field('distance')
  distance;
  @field('numberOfSteps')
  numberOfSteps;
  @field('mode')
  mode;
  @field('predictionMode')
  predictionMode;
  @field('journeyID')
  journeyID;
}

export async function addPedometer(batchId,mode,predictionMode,journeyID,pedometerData,timestamp) {
  
  // if(pedometerData.numberOfSteps == initNumberOfSteps)
  //     return

  
  // initNumberOfSteps = pedometerData.numberOfSteps 
  const newPost = await database.collections
    .get(TBL_Pedometer)
    .create(tbl => {
      
      tbl.timestamp = timestamp.toString();
      tbl.distance = pedometerData.distance.toString();
      tbl.numberOfSteps = pedometerData.numberOfSteps.toString();
      tbl.mode = mode.toString();
      tbl.predictionMode = predictionMode.toString();

      tbl.journeyID = journeyID.toString();
      tbl.batch_id = batchId.toString();
    });
  console.log(`Inserted new Pedometer sensor data` + newPost);

  return newPost;
}

export async function getPedometer(batchID) {
  const newPost = await database.collections
    .get(TBL_Pedometer)
    .query(Q.where('batch_id', batchID))
    .fetch();
  return newPost;
}


export async function getAllPedometer() {
  const newPost = await database.collections
    .get(TBL_Pedometer)
    .query()
    .fetch();
  return newPost;
}


export async function deletePedometer(batchID) {
  const newPost = await database.collections
    .get(TBL_Pedometer)
    .query(Q.where('batch_id', batchID))
    .destroyAllPermanently()
  return newPost;
}