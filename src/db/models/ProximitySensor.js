import {Model, Q} from '@nozbe/watermelondb';
import {field} from '@nozbe/watermelondb/decorators';
export const TBL_ProximitySensor = 'ProximitySensor';
import database from '../config';
import {getBatchID} from './Batch';

var initProximitySensorValue = -1;

export default class ProximitySensor extends Model {
  static table = TBL_ProximitySensor;

  @field('batch_id')
  batch_id;
  @field('timestamp')
  timestamp;
  @field('isNear')
  isNear;
  @field('value')
  value;
  @field('maxRange')
  maxRange;
  @field('mode')
  mode;
  @field('predictionMode')
  predictionMode;
  @field('journeyID')
  journeyID;
}

export async function addProximitySensor(
  batchId,
  mode,
  predictionMode,
  journeyID,
  proximitySensorData,
  timestamp,
) {
  // if (proximitySensorData.value == initProximitySensorValue) return;

  // initProximitySensorValue = proximitySensorData.value;

  const newPost = await database.collections
    .get(TBL_ProximitySensor)
    .create(tbl => {
      tbl.timestamp = timestamp.toString();
      tbl.isNear = proximitySensorData.isNear.toString();
      tbl.value = proximitySensorData.value.toString();
      tbl.maxRange = proximitySensorData.maxRange.toString();
      tbl.mode = mode.toString();
      tbl.predictionMode = predictionMode.toString();

      tbl.journeyID = journeyID.toString();
      tbl.batch_id = batchId.toString();
    });
  console.log(`Inserted new ProximitySensor sensor data` + newPost);

  return newPost;
}

export async function getProximitySensor(batchID) {
  const newPost = await database.collections
    .get(TBL_ProximitySensor)
    .query(Q.where('batch_id', batchID))
    .fetch();
  return newPost;
}



export async function getAllProximitySensor() {
  const newPost = await database.collections
    .get(TBL_ProximitySensor)
    .query()
    .fetch();
  return newPost;
}

export async function deleteProximitySensor(batchID) {
  const newPost = await database.collections
    .get(TBL_ProximitySensor)
    .query(Q.where('batch_id', batchID))
    .destroyAllPermanently();
  return newPost;
}
