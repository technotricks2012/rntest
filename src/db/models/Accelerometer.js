import {Model, Q} from '@nozbe/watermelondb';
import {field} from '@nozbe/watermelondb/decorators';
export const TBL_Accelerometer = 'Accelerometer';
import database from '../config';
import {getBatchID} from './Batch';

export default class Accelerometer extends Model {
  static table = TBL_Accelerometer;

  @field('batch_id')
  batch_id;
  @field('timestamp')
  timestamp;
  @field('x')
  x;
  @field('y')
  y;
  @field('z')
  z;
  @field('mode')
  mode;
  @field('predictionMode')
  predictionMode;
  @field('journeyID')
  journeyID;
}

export async function addAccelerometer(batchId,mode,predictionMode,journeyID,accelerometerData) {
  const newPost = await database.collections
    .get(TBL_Accelerometer)
    .create(tbl => {
      
      tbl.timestamp = accelerometerData.timestamp.toString();
      tbl.x = accelerometerData.x.toString();
      tbl.y = accelerometerData.y.toString();
      tbl.z = accelerometerData.z.toString();
      tbl.mode = mode.toString();
      tbl.predictionMode = predictionMode.toString();
      tbl.journeyID = journeyID.toString();

      tbl.batch_id = batchId.toString();
    });
  console.log(`Inserted new Accelerometer sensor data` + newPost);
  
  return newPost;
}



export async function getAllAccelerometer() {
  const newPost = await database.collections
    .get(TBL_Accelerometer)
    .query()
    .fetch();
  return newPost;
}
export async function getAccelerometer(batchID) {
  const newPost = await database.collections
    .get(TBL_Accelerometer)
    .query(Q.where('batch_id', batchID))
    .fetch();
  return newPost;
}



export async function deleteAccelerometer(batchID) {
  const newPost = await database.collections
    .get(TBL_Accelerometer)
    .query(Q.where('batch_id', batchID))
    .destroyAllPermanently()
  return newPost;
}


export async function getAccelerometerCount(batchID) {
  const newPost = await database.collections
    .get(TBL_Accelerometer)
    .query(Q.where('batch_id', batchID))
    .fetchCount()
  return newPost;
}