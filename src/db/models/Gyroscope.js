import {Model,Q} from '@nozbe/watermelondb';
import {field} from '@nozbe/watermelondb/decorators';
import database from '../config';
import {getBatchID} from './Batch';

export const TBL_Gyroscope = 'Gyroscope';

export default class Gyroscope extends Model {
  static table = TBL_Gyroscope;

  @field('batch_id')
  batch_id;
  @field('timestamp')
  timestamp;
  @field('x')
  x;
  @field('y')
  y;
  @field('z')
  z;
  @field('mode')
  mode;
  @field('predictionMode')
  predictionMode;
  @field('journeyID')
  journeyID;
}

export async function addGyroscope(batchId,mode,predictionMode,journeyID,gyroscopeData) {

  const newPost = await database.collections.get(TBL_Gyroscope).create(tbl => {
    tbl.timestamp = gyroscopeData.timestamp.toString();
    tbl.x = gyroscopeData.x.toString();
    tbl.y = gyroscopeData.y.toString();
    tbl.z = gyroscopeData.z.toString();
    tbl.mode = mode.toString();
    tbl.predictionMode = predictionMode.toString();
    tbl.journeyID = journeyID.toString();
    tbl.batch_id = batchId.toString();
  });
  console.log(`Inserted new Gyroscope sensor data` + newPost);
  return newPost.id;
}


export async function getGyroscope(batchID) {
  const newPost = await database.collections
    .get(TBL_Gyroscope)
    .query(Q.where('batch_id', batchID))
    .fetch();
  return newPost;
}


export async function getAllGyroscope() {
  const newPost = await database.collections
    .get(TBL_Gyroscope)
    .query()
    .fetch();
  return newPost;
}


export async function deleteGyroscope(batchID) {
  const newPost = await database.collections
    .get(TBL_Gyroscope)
    .query(Q.where('batch_id', batchID))
    .destroyAllPermanently();
  return newPost;
}