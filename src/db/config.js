import {Database} from '@nozbe/watermelondb';
import SQLiteAdapter from '@nozbe/watermelondb/adapters/sqlite';

import {mySchema} from './schema';
import Batch from './models/Batch';

import Accelerometer from './models/Accelerometer';
import Barometer from './models/Barometer';
import Gyroscope from './models/Gyroscope';
import Magnetometer from './models/Magnetometer';

import ProximitySensor from './models/ProximitySensor';
import Orientation from './models/Orientation';
import Thermometer from './models/Thermometer';
import LightSensor from './models/LightSensor';
import Pedometer from './models/Pedometer';


import Location from './models/Location';

const adapter = new SQLiteAdapter({
  dbName: 'SensorDB',
  schema: mySchema,
});

const database = new Database({
  adapter,
  modelClasses: [
    Batch,
    Accelerometer,
    Barometer,
    Gyroscope,
    Magnetometer,
    Location,
    Orientation,
    ProximitySensor,
    LightSensor,
    Thermometer,
    Pedometer
  ],
  actionsEnabled: true,
});

export default database;
