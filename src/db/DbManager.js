import database from './config';
import {TBL_Location} from './models/Location';
import {
  TRAVEL_MODE,
  JOURNEY_ID,
  TRAVEL_MODE_TIME,
  USER_NAME,
  SERVICE_START_TIME,
  ORIENTATION,
  PROXIMITY,
  STEPCOUNTER,
  THERMOMETER,
  LIGHTSENSOR,
  ACCELEROMETER,
  GYROSCOPE,
  MAGNETOMETER,
  BAROMETER,
  LOCATION,
  SENSORE_UPLOAD_INTERVAL,
  USER_ACTIVITY_MODE,
} from '../constants/IKeyConstants';

import {NativeModules} from 'react-native';


import {getDeviceInfo} from '../lib/DeviceInfo';
export const NEW = 'NEW';
export const UPLOADING = 'UPLOADING';
export const UPLOADED = 'UPLOADED';
import Accelerometer, {
  addAccelerometer,
  getAccelerometer,
  getAllAccelerometer,
  deleteAccelerometer,
} from '../db/models/Accelerometer';
import {
  addMagnetometer,
  getMagnetometer,
  getAllMagnetometer,
  deleteMagnetometer,
} from '../db/models/Magnetometer';
import {
  addGyroscope,
  getGyroscope,
  getAllGyroscope,
  deleteGyroscope,
} from '../db/models/Gyroscope';
import {
  addBarometer,
  getBarometer,
  getAllBarometer,
  deleteBarometer,
} from '../db/models/Barometer';

import {
  addOrientation,
  getOrientation,
  getAllOrientation,
  deleteOrientation,
} from '../db/models/Orientation';

import {
  addThermometer,
  getThermometer,
  getAllThermometer,
  deleteThermometer,
} from '../db/models/Thermometer';

import {
  addLightSensor,
  getLightSensor,
  getAllLightSensor,
  deleteLightSensor,
} from '../db/models/LightSensor';

import {
  addProximitySensor,
  getProximitySensor,
  getAllProximitySensor,
  deleteProximitySensor,
} from '../db/models/ProximitySensor';

import {
  addPedometer,
  getPedometer,
  getAllPedometer,
  deletePedometer,
} from '../db/models/Pedometer';

import {
  addLocation,
  getLocation,
  getAllLocation,
  deleteLocation,
} from '../db/models/Location';
import {
  getBatchID,
  updateBatchStatusWithBatchID,
  updateBatchStatus,
  getAllBatch,
} from './models/Batch';

import {getCurrentTime} from '../utils/Utils';

// export async function addLocation(time, lat, lng) {
//   return await database.action(async () => {
//     const newPost = await database.collections
//       .get(TBL_Location)
//       .create(location => {
//         location.timestamp = time;
//         location.lat = lat;
//         location.lng = lng;
//       });
//     console.log(newPost);
//   });
// }

export async function listLocation() {
  const count = await database.collections
    .get(TBL_Location)
    .query()
    .fetchCount();
  console.log(count);
}

export async function addSensorDataToDB(sensorData) {
  await database.action(async () => {
    
    const batchId = await getBatchID();
    
    const modeValue = await getLocal(TRAVEL_MODE);
    
    
    const predictionMode = await getLocal(USER_ACTIVITY_MODE);//? await getLocal(USER_ACTIVITY_MODE):"UNKNOWN";
    
    
    const journeyID = await getLocal(JOURNEY_ID);

    const mode = modeValue ? modeValue : 'Idle';
    
    
    
    if (
      typeof sensorData.accelerometer !== 'undefined' &&
      sensorData.accelerometer.SuccessCode == 200
    ) {
      await addAccelerometer(
        batchId,
        mode,
        predictionMode,
        journeyID,
        sensorData.accelerometer.Data,
      );
    }

    if (
      typeof sensorData.magnetometer !== 'undefined' &&
      sensorData.magnetometer.SuccessCode == 200
    ) {
      await addMagnetometer(
        batchId,
        mode,
        predictionMode,
        journeyID,
        sensorData.magnetometer.Data,
      );
    }

    if (
      typeof sensorData.gyroscope !== 'undefined' &&
      sensorData.gyroscope.SuccessCode == 200
    ) {
      await addGyroscope(
        batchId,
        mode,
        predictionMode,
        journeyID,
        sensorData.gyroscope.Data,
      );
    }

    if (
      typeof sensorData.barometer !== 'undefined' &&
      sensorData.barometer.SuccessCode == 200
    )
      await addBarometer(
        batchId,
        mode,
        predictionMode,
        journeyID,
        sensorData.barometer.Data,
        sensorData.timestamp,
      );

    if (
      typeof sensorData.orientation !== 'undefined' &&
      sensorData.orientation.SuccessCode == 200
    )
      await addOrientation(
        batchId,
        mode,
        predictionMode,
        journeyID,
        sensorData.orientation.Data,
        sensorData.timestamp,
      );

    if (
      typeof sensorData.proximity !== 'undefined' &&
      sensorData.proximity.SuccessCode == 200
    )
      await addProximitySensor(
        batchId,
        mode,
        predictionMode,
        journeyID,
        sensorData.proximity.Data,
        sensorData.timestamp,
      );

    if (
      typeof sensorData.thermometer !== 'undefined' &&
      sensorData.thermometer.SuccessCode == 200
    )
      await addThermometer(
        batchId,
        mode,
        predictionMode,
        journeyID,
        sensorData.thermometer.Data,
        sensorData.timestamp,
      );

    if (
      typeof sensorData.lightSensor !== 'undefined' &&
      sensorData.lightSensor.SuccessCode == 200
    )
      await addLightSensor(
        batchId,
        mode,
        predictionMode,
        journeyID,
        sensorData.lightSensor.Data,
        sensorData.timestamp,
      );

    if (
      typeof sensorData.pedometer !== 'undefined' &&
      sensorData.pedometer.SuccessCode == 200
    )
      await addPedometer(
        batchId,
        mode,
        predictionMode,
        journeyID,
        sensorData.pedometer.Data,
        sensorData.timestamp,
      );
  });
}

export async function addPredictedModeToDB(userActivityData) {

  if (
    typeof userActivityData.useractivity === 'undefined' &&
    userActivityData.useractivity.SuccessCode !== 200
  ) return;

  var predictionMode = 'NIL';

  const modeDataValues = userActivityData.useractivity.Data;
  
  if (modeDataValues.isWalking) predictionMode = 'Walking';
  else if (modeDataValues.isStationary) predictionMode = 'Stationary';
  else if (modeDataValues.isCycling) predictionMode = 'Cycling';
  else if (modeDataValues.isRunning) predictionMode = 'Running';
  else if (modeDataValues.unknown) predictionMode = 'unknown';
  else if (modeDataValues.isAutomotive) predictionMode = 'Automotive';
  else if (typeof modeDataValues.DetectedType !== 'undefined' && modeDataValues.DetectedType !='') 
  predictionMode = modeDataValues.DetectedType;

  if (predictionMode != 'NIL')
    await setLocal(USER_ACTIVITY_MODE, predictionMode);
}

export async function addLocationDataToDB(locationData) {
  if (locationData.SuccessCode != 200) return;

  await database.action(async () => {
    const batchId = await getBatchID();

    const modeValue = await getLocal(TRAVEL_MODE);
    const journeyID = await getLocal(JOURNEY_ID);

    const predictionMode = await getLocal(USER_ACTIVITY_MODE);

    const mode = modeValue ? modeValue : 'Idle';

    await addLocation(
      batchId,
      mode,
      predictionMode,
      journeyID,
      locationData.Data,
    );
  });
}

async function convertToJSON(
  username,
  deviceInfoValue,
  batchID,
  accelerometerList,
  barometerList,
  gyrometerList,
  magnetometerList,
  orientationList,
  thermometerList,
  lightList,
  proximityList,
  geoLocationList,
  pedometerList,
) {
  //Accelerometer
  var accArray = [];
  accArray = accelerometerList.map(item => ({
    // batch_id: item.batch_id,
    x: item.x,
    y: item.y,
    z: item.z,
    timestamp: item.timestamp,
    mode: item.mode,
    predictionMode: item.predictionMode,
    journeyID: item.journeyID,
  }));

  //Gyrometer
  var gyroArray = [];
  gyroArray = gyrometerList.map(item => ({
    // batch_id: item.batch_id,
    x: item.x,
    y: item.y,
    z: item.z,
    timestamp: item.timestamp,
    mode: item.mode,
    predictionMode: item.predictionMode,
    journeyID: item.journeyID,
  }));

  //Magnetometer
  var magnetoArray = [];
  magnetoArray = magnetometerList.map(item => ({
    // batch_id: item.batch_id,
    x: item.x,
    y: item.y,
    z: item.z,
    timestamp: item.timestamp,
    mode: item.mode,
    predictionMode: item.predictionMode,
    journeyID: item.journeyID,
  }));

  //Barometer
  var barArray = [];
  barArray = barometerList.map(item => ({
    // batch_id: item.batch_id,
    pressure: item.pressure,
    timestamp: item.timestamp,
    mode: item.mode,
    predictionMode: item.predictionMode,
    journeyID: item.journeyID,
  }));

  //Orientation
  var orientationArray = [];
  orientationArray = orientationList.map(item => ({
    // batch_id: item.batch_id,
    azimuth: item.azimuth,
    pitch: item.pitch,
    roll: item.roll,
    timestamp: item.timestamp,
    mode: item.mode,
    predictionMode: item.predictionMode,
    journeyID: item.journeyID,
  }));

  //Thermometer
  var thermometerArray = [];
  thermometerArray = thermometerList.map(item => ({
    // batch_id: item.batch_id,
    temp: item.temp,
    timestamp: item.timestamp,
    mode: item.mode,
    predictionMode: item.predictionMode,
    journeyID: item.journeyID,
  }));

  //LightSensor
  var lightSensorArray = [];
  lightSensorArray = lightList.map(item => ({
    // batch_id: item.batch_id,
    light: item.light,
    timestamp: item.timestamp,
    mode: item.mode,
    predictionMode: item.predictionMode,
    journeyID: item.journeyID,
  }));

  //Proximity Sensor
  var proximityArray = [];
  proximityArray = proximityList.map(item => ({
    // batch_id: item.batch_id,
    isNear: item.isNear,
    value: item.value,
    maxRange: item.maxRange,
    timestamp: item.timestamp,
    mode: item.mode,
    predictionMode: item.predictionMode,
    journeyID: item.journeyID,
  }));

  //LocationList
  var locationArray = [];
  locationArray = geoLocationList.map(item => ({
    // batch_id: item.batch_id,
    latitude: item.latitude,
    longitude: item.longitude,
    altitude: item.altitude,
    timestamp: item.timestamp,
    mode: item.mode,
    predictionMode: item.predictionMode,
    journeyID: item.journeyID,
  }));

  //LocationList
  var pedometerArray = [];
  pedometerArray = pedometerList.map(item => ({
    // batch_id: item.batch_id,
    steps: item.numberOfSteps,
    distance: item.distance,
    timestamp: item.timestamp,
    mode: item.mode,
    predictionMode: item.predictionMode,
    journeyID: item.journeyID,
  }));

  var sensorData = {
    userID: username,
    timestamp: new Date().getTime(), //Math.round((new Date()).getTime() / 1000),
    batchID: batchID,
    accelerometer: accArray,
    barometer: barArray,
    magnetometer: magnetoArray,
    gyroscope: gyroArray,
    orientation: orientationArray,
    thermometer: thermometerArray,
    lightSensor: lightSensorArray,
    proximitySensor: proximityArray,
    location: locationArray,
    pedometer: pedometerArray,
    deviceInfo: deviceInfoValue,
  };
  return sensorData;
}
export async function getAllDataFromDB() {
  return await database.action(async action => {
    const batchID = await updateBatchStatus(UPLOADING);
    if (batchID == 0 || batchID == null) return 'NIL';

    const accelerometerList = await getAccelerometer(batchID);
    const barometerList = await getBarometer(batchID);
    const gyrometerList = await getGyroscope(batchID);
    const magnetometerList = await getMagnetometer(batchID);
    const geoLocationList = await getLocation(batchID);

    const orientationList = await getOrientation(batchID);
    const thermometerList = await getThermometer(batchID);
    const lightList = await getLightSensor(batchID);
    const proximityList = await getProximitySensor(batchID);
    const pedometerList = await getPedometer(batchID);

    const deviceInfoValue = await getDeviceInfo();

    const username = await getLocal(USER_NAME);

    const sensordata = await convertToJSON(
      username,
      deviceInfoValue,
      batchID,
      accelerometerList,
      barometerList,
      gyrometerList,
      magnetometerList,
      orientationList,
      thermometerList,
      lightList,
      proximityList,
      geoLocationList,
      pedometerList,
    );
    return sensordata;
  });
}

export async function addGeoLocationData(locationData) {
  await addLocation(locationData);
}

export async function getSensorStatus() {
  return {
    orientation: await getLocal(ORIENTATION),
    proximity: await getLocal(PROXIMITY),
    pedometer: await getLocal(STEPCOUNTER),
    thermometer: await getLocal(THERMOMETER),
    lightsensor: await getLocal(LIGHTSENSOR),
    accelerometer: await getLocal(ACCELEROMETER),
    gyroscope: await getLocal(GYROSCOPE),
    magnetometer: await getLocal(MAGNETOMETER),
    barometer: await getLocal(BAROMETER),
    location: await getLocal(LOCATION),
  };
}

export async function deleteBatchFromDB(batchId) {
  return await database.action(async action => {
    if (batchId == 0 || batchId == null) return 'NIL';

    await updateBatchStatusWithBatchID(batchId, UPLOADED);

    const accelerometerList = await deleteAccelerometer(batchId);
    const barometerList = await deleteBarometer(batchId);
    const gyrometerList = await deleteGyroscope(batchId);
    const magnetometerList = await deleteMagnetometer(batchId);
    const geoLocationList = await deleteLocation(batchId);

    const orientationList = await deleteOrientation(batchId);
    const thermometerList = await deleteThermometer(batchId);
    const proximitySensorList = await deleteProximitySensor(batchId);
    const lightList = await deleteLightSensor(batchId);
    const pedometerList = await deletePedometer(batchId);

    return 'DONE';
  });
}

// export async function getCountFromDB(batchId) {

//     const accelerometerCount = await getAccelerometerCount(batchId);
//     const barometerList = await deleteBarometer(batchId);
//     const gyrometerList = await deleteGyroscope(batchId);
//     const magnetometerList = await deleteMagnetometer(batchId);
//     const geoLocationList = await deleteLocation(batchId);

//     const orientationList = await deleteOrientation(batchId);
//     const thermometerList = await deleteThermometer(batchId);
//     const proximitySensorList = await deleteProximitySensor(batchId);
//     const lightList = await deleteLightSensor(batchId);
//     const pedometerList = await deletePedometer(batchId);

// }

export async function setServerInterVal(type, batchID) {
  const uploadIntervalValue = await getLocal(SENSORE_UPLOAD_INTERVAL);

  const uploadIterval = uploadIntervalValue
    ? JSON.parse(uploadIntervalValue)
    : [];

  uploadIterval.push({mode: type, BatchId: batchID, time: getCurrentTime()});
  await setLocal(SENSORE_UPLOAD_INTERVAL, JSON.stringify(uploadIterval));
}

export async function setLocal(key, value) {

  return await NativeModules.StoreModule.saveValue(key,value);
  // return await database.adapter.setLocal(key, value);
}

export async function getLocal(key) {

  var value = await NativeModules.StoreModule.getValue(key);
  
console.log("Store == Key"+key+" Value= "+value)
  return value;
  // return await database.adapter.getLocal(key);
}

export async function removeLocal(key) {
  await database.adapter.removeLocal(key);
}

function mapToArray(dataFromDB) {
  var dataArray = [];
  dataArray = dataFromDB.map(item => item._raw);
  return dataArray;
}

export async function getDBLog() {
  return await database.action(async action => {
    const allBatches = await getAllBatch();
    const accelerometerList = await getAllAccelerometer();
    const barometerList = await getAllBarometer();
    const gyrometerList = await getAllGyroscope();
    const magnetometerList = await getAllMagnetometer();
    const geoLocationList = await getAllLocation();
    const orientationList = await getAllOrientation();
    const thermometerList = await getAllThermometer();
    const lightList = await getAllLightSensor();
    const proximityList = await getAllProximitySensor();
    const pedometerList = await getAllPedometer();
    const deviceInfoValue = await getDeviceInfo();

    var localDataInfo = {
      USER_NAME: await getLocal(USER_NAME),
      TRAVEL_MODE: await getLocal(TRAVEL_MODE),
      JOURNEY_ID: await getLocal(JOURNEY_ID),
      TRAVEL_MODE_TIME: await getLocal(TRAVEL_MODE_TIME),
      SERVICE_START_TIME: await getLocal(SERVICE_START_TIME),
      ORIENTATION: await getLocal(ORIENTATION),
      PROXIMITY: await getLocal(PROXIMITY),
      STEPCOUNTER: await getLocal(STEPCOUNTER),
      THERMOMETER: await getLocal(THERMOMETER),
      LIGHTSENSOR: await getLocal(LIGHTSENSOR),
      ACCELEROMETER: await getLocal(ACCELEROMETER),
      GYROSCOPE: await getLocal(GYROSCOPE),
      MAGNETOMETER: await getLocal(MAGNETOMETER),
      BAROMETER: await getLocal(BAROMETER),
      LOCATION: await getLocal(LOCATION),
      UPLOAD_INTERVAL: JSON.parse(await getLocal(SENSORE_UPLOAD_INTERVAL)),
      USER_ACTIVITY_MODE: await getLocal(USER_ACTIVITY_MODE),
    };
    var logData = {
      timestamp: new Date().getTime(),
      dateFormat: getCurrentTime(),
      // batchData: mapToArray(allBatches),
      // accelerometerData: mapToArray(accelerometerList),
      // barometerData: mapToArray(barometerList),
      // gyrometerData: mapToArray(gyrometerList),
      // magnetometerData: mapToArray(magnetometerList),
      // geoLocationData: mapToArray(geoLocationList),
      // orientationData: mapToArray(orientationList),
      // thermometerData: mapToArray(thermometerList),
      // lightData: mapToArray(lightList),
      // proximityData: mapToArray(proximityList),
      // pedometerData: mapToArray(pedometerList),
      // accelerometerData: mapToArray(accelerometerList),
      deviceData: deviceInfoValue,
      localDBInfo: localDataInfo,
    };
    return logData;
  });
}
