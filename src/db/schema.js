import {appSchema, tableSchema} from '@nozbe/watermelondb';
import {TBL_Location} from './models/Location';
import {TBL_Accelerometer} from './models/Accelerometer';
import {TBL_Barometer} from './models/Barometer';
import {TBL_Gyroscope} from './models/Gyroscope';
import {TBL_Magnetometer} from './models/Magnetometer';
import {TBL_Orientation} from './models/Orientation';
import {TBL_ProximitySensor} from './models/ProximitySensor';
import {TBL_Thermometer} from './models/Thermometer';
import {TBL_LightSensor} from './models/LightSensor';
import {TBL_Pedometer} from './models/Pedometer';

import {TBL_Batch} from './models/Batch';

export const mySchema = appSchema({
  version: 1,
  tables: [
    tableSchema({
      name: TBL_Batch,
      columns: [
        {name: 'timestamp', type: 'string'},
        {name: 'status', type: 'string'},
      ],
    }),
    tableSchema({
      name: TBL_Location,
      columns: [
        {name: 'batch_id', type: 'string', isIndexed: true},
        {name: 'timestamp', type: 'string'},
        {name: 'latitude', type: 'string'},
        {name: 'longitude', type: 'string'},
        {name: 'altitude', type: 'string'},
        {name: 'mode', type: 'string'},
        {name: 'predictionMode', type: 'string'},
        {name: 'journeyID', type: 'string'},

      ],
    }),
    tableSchema({
      name: TBL_Accelerometer,
      columns: [
        {name: 'batch_id', type: 'string', isIndexed: true},
        {name: 'timestamp', type: 'string'},
        {name: 'x', type: 'string'},
        {name: 'y', type: 'string'},
        {name: 'z', type: 'string'},
        {name: 'mode', type: 'string'},
        {name: 'predictionMode', type: 'string'},
        {name: 'journeyID', type: 'string'},


      ],
    }),

    tableSchema({
      name: TBL_Barometer,
      columns: [
        {name: 'batch_id', type: 'string', isIndexed: true},
        {name: 'timestamp', type: 'string'},
        {name: 'pressure', type: 'string'},
        {name: 'mode', type: 'string'},
        {name: 'predictionMode', type: 'string'},
        {name: 'journeyID', type: 'string'},


      ],
    }),

    tableSchema({
      name: TBL_Gyroscope,
      columns: [
        {name: 'batch_id', type: 'string', isIndexed: true},
        {name: 'timestamp', type: 'string'},
        {name: 'x', type: 'string'},
        {name: 'y', type: 'string'},
        {name: 'z', type: 'string'},
        {name: 'mode', type: 'string'},
        {name: 'predictionMode', type: 'string'},
        {name: 'journeyID', type: 'string'},


      ],
    }),

    tableSchema({
      name: TBL_Magnetometer,
      columns: [
        {name: 'batch_id', type: 'string', isIndexed: true},
        {name: 'timestamp', type: 'string'},
        {name: 'x', type: 'string'},
        {name: 'y', type: 'string'},
        {name: 'z', type: 'string'},
        {name: 'mode', type: 'string'},
        {name: 'predictionMode', type: 'string'},
        {name: 'journeyID', type: 'string'},


      ],
    }),

    tableSchema({
      name: TBL_Orientation,
      columns: [
        {name: 'batch_id', type: 'string', isIndexed: true},
        {name: 'timestamp', type: 'string'},
        {name: 'azimuth', type: 'string'},
        {name: 'pitch', type: 'string'},
        {name: 'roll', type: 'string'},
        {name: 'mode', type: 'string'},
        {name: 'predictionMode', type: 'string'},
        {name: 'journeyID', type: 'string'},


      ],
    }),

    tableSchema({
      name: TBL_Thermometer,
      columns: [
        {name: 'batch_id', type: 'string', isIndexed: true},
        {name: 'timestamp', type: 'string'},
        {name: 'temp', type: 'string'},
        {name: 'mode', type: 'string'},
        {name: 'predictionMode', type: 'string'},
        {name: 'journeyID', type: 'string'},


      ],
    }),

    tableSchema({
      name: TBL_LightSensor,
      columns: [
        {name: 'batch_id', type: 'string', isIndexed: true},
        {name: 'timestamp', type: 'string'},
        {name: 'light', type: 'string'},
        {name: 'mode', type: 'string'},
        {name: 'predictionMode', type: 'string'},
        {name: 'journeyID', type: 'string'},


      ],
    }),

    tableSchema({
      name: TBL_ProximitySensor,
      columns: [
        {name: 'batch_id', type: 'string', isIndexed: true},
        {name: 'timestamp', type: 'string'},
        {name: 'isNear', type: 'string'},
        {name: 'value', type: 'string'},
        {name: 'maxRange', type: 'string'},
        {name: 'mode', type: 'string'},
        {name: 'predictionMode', type: 'string'},
        {name: 'journeyID', type: 'string'},


      ],
    }),
    tableSchema({
      name: TBL_Pedometer,
      columns: [
        {name: 'batch_id', type: 'string', isIndexed: true},
        {name: 'timestamp', type: 'string'},
        {name: 'distance', type: 'string'},
        {name: 'numberOfSteps', type: 'string'},
        {name: 'mode', type: 'string'},
        {name: 'predictionMode', type: 'string'},
        {name: 'journeyID', type: 'string'},



      ],
    }),
  ],
});
