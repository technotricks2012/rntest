import React, {Component} from 'react';
import {
  View,
  Text,
  StyleSheet,
  SafeAreaView,
  ActivityIndicator,
} from 'react-native';
import WebView from 'react-native-webview';


import {startServiceTimer, stopServiceTimer} from '../../utils/ServiceManager';

import {TimeInterval} from '../../constants/ITimeConstant';
import {Header, Icon} from 'react-native-elements';
import { USER_NAME } from '../../constants/IKeyConstants';
import { getDeviceInfo } from '../../lib/DeviceInfo';
import { getLocal } from '../../db/DbManager';


class WebLink extends Component {
  _isMounted = false;
  webview = null;

  userData = null

  static navigationOptions = {
    drawerLabel: 'Home',
    // drawerIcon: ({tintColor}) => (
    //   <Icon
    //     name="home"
    //     onPress={() => {
    //       console.log('HI');
    //     }}
    //     color="grey"
    //     size={20}
    //   />
    // ),
  };

  constructor() {
    super();

    this.state = {
      ready: false,
      data: {x: null, y: null, z: null, timestamp: null},
      canada: '',
      isLoading: true,
    };
  }

  async componentDidMount() {
    let userID = await getLocal(USER_NAME);
    let deviceID = await getDeviceInfo();

    this.userData = {
        successCode: '200',
        userData: {
          userID:userID,
          deviceID:deviceID.deviceID
        }
      }
    await startServiceTimer(TimeInterval);
  }

  render() {
    const {isLoading} = this.state;

    return (
      <SafeAreaView style={styles.screenContainer}>
        <View style={styles.screenContainer}>
          <WebView
            ref={ref => (this.webview = ref)}
            style={styles.webView}
            onLoadStart={console.log('Webview=> Loading Start')}
            javaScriptEnabled={true}
            onNavigationStateChange={navState => {
              this.setState({isLoading: navState.loading});
            }}
            injectedJavaScript={`
            window.postMessage ('${JSON.stringify(this.userData)}', '*')
          `}
            source={{
              // uri: 'http://192.168.43.213:3000',

              // uri: 'http://ms-web-app-static.s3-website-ap-southeast-1.amazonaws.com',
              uri:'https://d1zalmttwow7ja.cloudfront.net/',
            }}
          />

          <View style={styles.menuIcon}>
            <Header
              style={styles.menuIcon}
              containerStyle={{height: 100, backgroundColor: 'transparent'}}
              placement="left"
              leftContainerStyle={{marginBottom: 35}}
              leftComponent={{
                icon: 'menu',
                color: '#fff',
                size: 30,

                onPress: () => {
                  this.props.navigation.openDrawer();
                },
              }}
            />
          </View>

          {isLoading && (
            <ActivityIndicator
              style={styles.spinner}
              size="large"
              color="#0000ff"
            />
          )}
        </View>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  screenContainer: {
    flex: 1,
    backgroundColor: '#234523',
  },

  webView: {
    flex: 1,
  },
  menuIcon: {
    position: 'absolute',
  },
  spinner: {
    alignItems: 'center',
    justifyContent: 'center',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    position: 'absolute',
  },
});

export default WebLink;
