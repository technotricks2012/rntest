

import React, {Component} from 'react';

import {
  View

} from 'react-native';
import {getLocal,setLocal} from '../../db/DbManager';
import { USER_NAME ,IS_ROAD_INSPECTOR} from '../../constants/IKeyConstants';


export default class Auth extends Component {
  constructor(props) {
    super(props);

  }

  componentDidMount() {
    this._bootstrapAsync();
  }

  // Fetch the token from storage then navigate to our appropriate place
  _bootstrapAsync = async () => {
    const userToken = await getLocal(USER_NAME);
    

    const isRoadInspector = (await getLocal(IS_ROAD_INSPECTOR) == "true");

    const isNotLogin = (userToken === null);
    console.log("userToken "+userToken)
    

    this.props.navigation.navigate((isNotLogin) ? 'loginStack' : (isRoadInspector)?'drawerStack2':'drawerStack1');
  };



  render() {
    return (
      <View>

      </View>
    );
  }
}
