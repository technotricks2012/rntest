import React, {Component} from 'react';
import {View, Text, StyleSheet} from 'react-native';
import FirebaseDB from '../firebase/FirebaseDB';
import {connect} from 'react-redux';

import {setAccelerometer} from '../store/AccelerometerStore'

import {
  accelerometer,
  setUpdateIntervalForType,
  SensorTypes,
} from 'react-native-sensors';
setUpdateIntervalForType(SensorTypes.accelerometer, 1000);

class Accelerometer extends Component {
  _isMounted = false;
  constructor() {
    super();
    this.state = {
      ready: false,
      data: {x: null, y: null, z: null, timestamp: null},
      accelerometer:''
    };
  }
  static navigationOptions = {
    title: 'Accelerometer',
  };
  componentDidMount() {
    this._isMounted = true;
    accelerometer.subscribe(({x, y, z, timestamp}) => {
      if (this._isMounted) {
        FirebaseDB.setAccelerometer({x, y, z, timestamp})
        this.props.setAccelerometer(x, y, z, timestamp);
        this.setState({
          ready: true,
          data: {
            x: x,
            y: y,
            z: z,
            timestamp: timestamp,
          },
        });
      }
    });
  }

  

  componentWillUnmount() {
    this._isMounted = false;
  }

  render() {
    console.log(this.props.accelerometer);
    return (
      <View style={styles.container}>
        {!this.state.ready && (
          <Text style={styles.big}>Reading Sensor Data.</Text>
        )}

        {this.state.ready && (
          <View>
            <Text style={styles.big}>{`X: ${this.state.data.x}`}</Text>
            <Text style={styles.big}>{`Y: ${this.state.data.y}`}</Text>
            <Text style={styles.big}>{`Z: ${this.state.data.z}`}</Text>
            <Text style={styles.big}>{`Timestamp: ${
              this.state.data.timestamp
            }`}</Text>
          </View>
        )}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  big: {
    fontSize: 15,
  },
});

// export default Accelerometer;

function mapStateToProps(state) {
  return {
    accelerometer: state.accelerometer
  };
}
export default connect(mapStateToProps,{setAccelerometer})(Accelerometer);
