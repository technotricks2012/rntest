import React, {Component} from 'react';

import {
  StyleSheet,
  View,
  TouchableOpacity,
  Text,
  Image,
  KeyboardAvoidingView,
  SafeAreaView,
  PermissionsAndroid, Platform
} from 'react-native';


import {w, h, totalSize} from '../../utils/Dimensions';
import GetStarted from './GetStarted';
import {red} from 'ansi-colors';
import RNDisableBatteryOptimizationsAndroid from 'react-native-disable-battery-optimizations-android';

const companyLogo = require('../../assets/companylogo.png');
const email = require('../../assets/email.png');
const password = require('../../assets/password.png');
import LinearGradient from 'react-native-linear-gradient';
import {Button} from 'react-native-elements';

import Background from './Components/Background';
import LoginForm from './Components/LoginForm';
import Logo from './Components/Logo';

export default class LoginScreen extends Component {
  constructor(props) {
    super(props);
  }

  async requestLocationPermission() {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
        {
          title: 'Location Permission',
          message: 'This app needs access to your location',
        },
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        console.log('You can use the location');
      } else {
        console.log('Location permission denied');
      }
    } catch (err) {
      console.warn(err);
    }
  }

  componentDidMount() {
    

    if (Platform.OS === 'android') {
      this.requestLocationPermission();
      
      RNDisableBatteryOptimizationsAndroid.isBatteryOptimizationEnabled().then(
        isEnabled => {
          if (isEnabled) {
            // RNDisableBatteryOptimizationsAndroid.openBatteryModal();
            RNDisableBatteryOptimizationsAndroid.enableBackgroundServicesDialogue();
          }
        },
      );
    }
  

   
  }
  

 

  render() {
    return (
      <Background>
        <Logo />
        <LoginForm />
      </Background>
    );
  }
}

const styles = StyleSheet.create({
  screenContainer: {
    width: w(70),
    height: h(50),
    backgroundColor: 'white',
    alignItems: 'center',

    position: 'absolute',
  },
  input: {
    height: 40,
    backgroundColor: 'rgba(225,225,225,0.2)',
    marginBottom: 10,
    padding: 10,
    color: '#fff',
  },
  buttonContainer: {
    backgroundColor: '#2980b6',
    paddingVertical: 15,
  },
  buttonText: {
    color: '#fff',
    textAlign: 'center',
    fontWeight: '700',
  },
});
