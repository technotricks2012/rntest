import React, {Component} from 'react';
import PropTypes from 'prop-types';
import Dimensions from 'Dimensions';
import {
  StyleSheet,
  View,
  TextInput,
  Text,
  Image,
  TouchableOpacity,
} from 'react-native';
import closeIcon from '../../../images/icon_close.png';

export default class UserInput extends Component {
  constructor(props) {
    super(props);
    this.state = {
      text: '',
      isClear: false,
    };
    // this.showPass = this.showPass.bind(this);
  }
  clear() {
    this.setState({text: '', isClear: false});
    this.onChangeText('');
  }

  onChangeText(text) {
    this.setState({isClear: text.length > 0 ? true : false, text: text});

    this.props.onChangeText(text);
    // this.setState({ text:text })
  }
  getInputValue = () => this.state.text;



  render() {
    return (
      <View style={styles.continer}>
      <View style={styles.inputWrapper}>
        <Image source={this.props.source} style={styles.inlineImg} />
        <TextInput
          style={styles.input}
          placeholder={this.props.placeholder}
          secureTextEntry={this.props.secureTextEntry}
          autoCorrect={this.props.autoCorrect}
          autoCapitalize={this.props.autoCapitalize}
          returnKeyType={this.props.returnKeyType}
          placeholderTextColor="#818181"
          underlineColorAndroid="transparent"
          value={ this.state.text}
         onChangeText={text => this.onChangeText(text)}
        />
        <View style={styles.line} />

        {this.state.isClear && (
          <TouchableOpacity
            activeOpacity={0.7}
            style={styles.touch}
            onPress={() => {
              this.clear();
            }}>
            <Image source={closeIcon} style={styles.closeIcon} />
          </TouchableOpacity>
        )}
      </View>
      <Text style={styles.errorText}>{this.props.errorMsg}</Text>
      </View>
    );
  }
}

UserInput.propTypes = {
  source: PropTypes.number.isRequired,
  placeholder: PropTypes.string.isRequired,
  secureTextEntry: PropTypes.bool,
  autoCorrect: PropTypes.bool,
  autoCapitalize: PropTypes.string,
  returnKeyType: PropTypes.string,
  errorMsg: PropTypes.string,

};

const DEVICE_WIDTH = Dimensions.get('window').width;
const DEVICE_HEIGHT = Dimensions.get('window').height;

const styles = StyleSheet.create({
  input: {
    backgroundColor: 'transparent',
    height: 50,
    width: '100%',
    marginHorizontal: 20,
    paddingLeft: 50,
    paddingRight: 40,
    color: '#818181',
  },
  continer: {
    alignItems: 'flex-start',
    height: 60,
    width: '100%',

  },
  errorText: {
    fontFamily: 'SanFranciscoDisplay-Semibold',
    fontSize: 12,
    color: 'red',

  },
  inputWrapper: {
    // flex: 1,
    alignItems: 'center',
    height: 50,
    width: '100%',
    backgroundColor: '#d8d8d8',
    borderRadius: 5,
    borderWidth: 1,
    borderColor: '#cfcfcf',
    borderBottomWidth: 0,
    shadowColor: '#56565656',
    shadowOffset: {width: 0, height: 1},
    shadowOpacity: 0.8,
    shadowRadius: 1,
    elevation: 1,
  },
  inlineImg: {
    position: 'absolute',
    resizeMode: 'contain',
    zIndex: 99,
    width: 26,
    height: 26,
    left: 10,
    top: 10,
  },

  touch: {
    position: 'absolute',
    zIndex: 99,
    right: 10,
    top: 13,
  },
  closeIcon: {
    width: 20,
    height: 20,
  },
  line: {
    position: 'absolute',
    zIndex: 100,
    width: 1,
    height: 50,
    left: 40,
    backgroundColor: 'rgba(151, 151, 151, 0.4)',
  },
});
