import React, {Component} from 'react';

import {ValidateEmail} from '../../../lib/Validation';
import {getLocal, setLocal} from '../../../db/DbManager';

import {
  View,
  StyleSheet,
  KeyboardAvoidingView,
  Image,
  Text,
  Switch,
  TouchableOpacity,
} from 'react-native';

import {w, h, totalSize} from '../../../utils/Dimensions';
import {Button} from 'react-native-elements';
import Biometrics from 'react-native-biometrics';

import usernameIcon from '../../../images/icon_user.png';
import passwordIcon from '../../../images/icon_password.png';
import fingerprintIcon from '../../../images/icon_fingerprint.png';
import {withNavigation} from 'react-navigation';

import UserInput from './UserInput';
import {USER_NAME,IS_ROAD_INSPECTOR} from '../../../constants/IKeyConstants';
var defaultPassword = 'password';

class LoginForm extends Component {
  constructor(props) {
    super(props);

    this.state = {
      username: '',
      password: '',
      usernameError: '',
      passwordError: '',
      isBiometricSupport: false,
    };
    state = {switchValue: false};
  }

  toggleSwitch = value => {
    this.setState({switchValue: value});  
    (async () => {
      await setLocal(IS_ROAD_INSPECTOR, ""+value);
    })();
  };

  componentDidMount() {
    Biometrics.isSensorAvailable().then(biometryType => {
      if (biometryType === Biometrics.TouchID) {
        this.setState({
          isBiometricSupport: true,
        });
      }
    });
  }

  forgotPass() {}
  async fingerPrintLogin() {
    let username = this.state.username;

    console.log('fingerPrintLogin' + username);

    this.setState({usernameError: '', passwordError: ''});

    if (username == '')
      this.setState({usernameError: 'Please enter the email id'});
    else if (!ValidateEmail(username)) {
      this.setState({usernameError: 'Invalid email'});
    } else {
      Biometrics.isSensorAvailable().then(biometryType => {
        if (biometryType === Biometrics.TouchID) {
          console.log('TouchID is supported');

          Biometrics.simplePrompt('Confirm fingerprint')
            .then(() => {
              (async () => {
                await setLocal(USER_NAME, username);
              })();

              this.props.navigation.navigate((isRoadInspector)?'drawerStack2':'drawerStack1');

              
              return users;
            })
            .catch(() => {
              console.log('fingerprint failed or prompt was cancelled');
            });
        } else if (biometryType === Biometrics.FaceID) {
          console.log('FaceID is supported');
        } else {
          console.log('Biometrics not supported');
        }
      });
    }
  }

  async onLoginPress() {
    let username = this.state.username;
    let password = this.state.password;

    this.setState({usernameError: '', passwordError: ''});

    if (username == '')
      this.setState({usernameError: 'Please enter the email id'});
    else if (!ValidateEmail(username)) {
      this.setState({usernameError: 'Invalid email'});
    } else if (password == '') {
      this.setState({passwordError: 'Please enter your password'});
    } else if (password !== defaultPassword) {
      this.setState({passwordError: 'Invalid password'});
    } else {
      await setLocal(USER_NAME, username);

      const isRoadInspector = this.state.switchValue
      this.props.navigation.navigate((isRoadInspector)?'drawerStack2':'drawerStack1');
    }
  }
  render() {
    const {isBiometricSupport} = this.state;

    return (
      <KeyboardAvoidingView behavior="padding" style={styles.screenContainer}>
        <Text style={styles.textTitle}>Login To Your Account</Text>
        {/* <View style={{flex: 0.05}} /> */}
        <View style={{height: 15}} />

        <UserInput
          source={usernameIcon}
          placeholder="Username"
          autoCapitalize={'none'}
          returnKeyType={'done'}
          errorMsg={this.state.usernameError}
          autoCorrect={false}
          onChangeText={text => {
            this.setState({
              username: text,
              usernameError: '',
            });
          }}
        />
        {/* <View style={{flex: 0.1}} /> */}
        <View style={{height: 15}} />

        <UserInput
          source={passwordIcon}
          secureTextEntry={true}
          placeholder="Password"
          errorMsg={this.state.passwordError}
          returnKeyType={'done'}
          autoCapitalize={'none'}
          autoCorrect={false}
          onChangeText={text => {
            this.setState({
              password: text,
              passwordError: '',
            });
          }}
        />

        <View
          style={{
            width: '100%',
            flexDirection: 'row',
            justifyContent: 'center',
          }}>
          <Text style={{textAlign: 'center', alignSelf: 'center'}}>
            Road Inspector?
          </Text>

          <Switch
            onValueChange={this.toggleSwitch}
            value={this.state.switchValue}
          />
        </View>
        <Button
          buttonStyle={styles.buttonStyle}
          title="Log In"
          titleStyle={styles.buttonTextStyle}
          onPress={() => this.onLoginPress()}
        />
        <View style={{height: 15}} />

        <TouchableOpacity activeOpacity={0.7} onPress={this.forgotPass}>
          <Text style={styles.textForgotPasswordTitle}>Forgot Password?</Text>
        </TouchableOpacity>

        <View style={{height: 15}} />

        {isBiometricSupport && (
          <TouchableOpacity
            activeOpacity={0.7}
            onPress={() => {
              this.fingerPrintLogin();
            }}>
            <View style={styles.fingerPrintCircle}>
              <Image source={fingerprintIcon} style={styles.fingerPrint} />
            </View>
          </TouchableOpacity>
        )}
      </KeyboardAvoidingView>
    );
  }
}

const styles = StyleSheet.create({
  screenContainer: {
    width: w(90),
    height: h(60),
    marginTop: h(30),
    marginLeft: w(5),
    backgroundColor: 'white',
    alignItems: 'center',
    position: 'absolute',
    // flex :3,
    // flexDirection:'column',
    padding: 20,
    borderRadius: 5,
    borderWidth: 1,
    borderColor: '#ddd',
    borderBottomWidth: 0,
    shadowColor: '#000',
    shadowOffset: {width: 0, height: 2},
    shadowOpacity: 0.8,
    shadowRadius: 2,
    elevation: 1,
  },

  input: {
    height: 40,
    backgroundColor: 'rgba(225,225,225,0.2)',
    marginBottom: 10,
    padding: 10,
    color: '#fff',
  },
  buttonContainer: {
    backgroundColor: '#2980b6',
    paddingVertical: 15,
  },
  buttonText: {
    color: '#fff',
    textAlign: 'center',
    fontWeight: '700',
  },
  btnEye: {
    position: 'absolute',
    top: 55,
    right: 28,
  },
  iconEye: {
    width: 25,
    height: 25,
    tintColor: 'rgba(0,0,0,0.2)',
  },
  textTitle: {
    fontFamily: 'SanFranciscoDisplay-Semibold',
    fontSize: 18,
    color: '#1ebebf',
    // flex:1
  },
  textForgotPasswordTitle: {
    fontFamily: 'SanFranciscoDisplay-Semibold',
    fontSize: 18,
    color: '#818181',
    // flex:1
  },
  padingTopStyle: {
    // marginTop:20,
    // flex:1
  },
  buttonLogin: {
    fontFamily: 'SanFranciscoDisplay-Semibold',
    fontSize: 18,
    color: '#1ebebf',
    // flex:1
  },
  buttonStyle: {
    marginTop: 10,
    padding: 10,
    backgroundColor: '#1ebebf',
    paddingStart: 50,
    paddingRight: 50,
    borderRadius: 5,
    justifyContent: 'center',
    alignItems: 'center',
  },

  buttonTextStyle: {
    color: 'white',
    fontSize: 18,
    textAlign: 'left',
    fontFamily: 'SanFranciscoDisplay-Semibold',
  },
  fingerPrintCircle: {
    width: 70,
    height: 70,
    borderRadius: 70 / 2,
    backgroundColor: '#f2f2f2',
    alignItems: 'center',
    justifyContent: 'center',
  },
  fingerPrint: {
    width: 40,
    height: 40,
    resizeMode: 'contain',
  },
});

export default withNavigation(LoginForm);
