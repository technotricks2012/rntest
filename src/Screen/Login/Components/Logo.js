import React, {Component} from 'react';

import {
  View,
  StyleSheet,
  KeyboardAvoidingView,
  Text,
  TextInput,
  Image,
} from 'react-native';
import {w, h, totalSize} from '../../../utils/Dimensions';

import logoImg from '../../../images/login_logo.png';

export default class LogoImage extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <View style={styles.container}>
        <Image source={logoImg} style={styles.image} />
      </View>
    );

    <Image style={styles.logo} source={LoginLogo} />;
  }
}

const styles = StyleSheet.create({

  image: {
    width: w(30),
    resizeMode: 'contain',
  },
  container: {
    position: 'absolute',
    backgroundColor: 'white',
    padding:5,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 20,
    borderWidth: 1,
    borderColor: '#ddd',
    borderBottomWidth: 0,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.8,
    shadowRadius: 2,
    elevation: 1,
    marginTop: h(5),   
    marginLeft: (w(50)/2)+30,
    justifyContent:'center'
  },

});
