import React, {Component} from 'react';

import {View, StyleSheet, SafeAreaView,StatusBar} from 'react-native';
import {w, h, totalSize} from '../../../utils/Dimensions';
import LinearGradient from 'react-native-linear-gradient';


class Background extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <SafeAreaView style={styles.screenContainer}>
        <LinearGradient
          colors={['#1a61d8', '#1fc7bd']}
          start={{x: 0.0, y: 1.0}}
          end={{x: 1.0, y: 1.0}}
          style={styles.tophalf}></LinearGradient>
        <View style={styles.bottomhalf} />
        {this.props.children}
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  screenContainer: {
    flex: 1,
    backgroundColor: '#234523',
  },

  tophalf: {
    flex: 1,
    width: w(100),
    height: h(50),
    marginTop: h(0),
    backgroundColor: 'blue',
  },
  bottomhalf: {
    flex: 1,
    width: w(100),
    height: h(50),
    alignItems: 'flex-start',
    backgroundColor: '#393939',
  },
  loginForm: {
    // marginTop:  w(100)/2,

    width: w(70),
    height: h(50),
    backgroundColor: 'white',
    alignItems: 'center',

    position: 'absolute',
  },
});

export default Background
