// import React, {Component} from 'react';
// import styles from './style';
// import {
//   Keyboard,
//   Text,
//   View,
//   TextInput,
//   TouchableWithoutFeedback,
//   Alert,
//   KeyboardAvoidingView,SafeAreaView
// } from 'react-native';
// import {Button} from 'react-native';

// const appId = '1047121222092614';

// import Biometrics from 'react-native-biometrics';

// export default class LoginScreen extends Component {
//   constructor() {
//     super();
//     this.state = {
//       username: '',
//       password: ''
//     };
//   }

//   render() {
//     return (

//       <KeyboardAvoidingView style={styles.containerView} behavior="padding">
//         <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
//           <View style={styles.loginScreenContainer}>
//             <View style={styles.loginFormView}>
//               <Text style={styles.logoText}>MobilitySensor</Text>
//               <TextInput
//                 placeholder="Username"
//                 placeholderColor="#c4c3cb"
//                 style={styles.loginFormTextInput}

//                 value={this.state.username}
//                 onChangeText={text => {
//                   this.setState({
//                     username: text
//                   });
//                 }}
//               />
//               <TextInput
//                 placeholder="Password"
//                 placeholderColor="#c4c3cb"
//                 style={styles.loginFormTextInput}
//                 secureTextEntry={true}
//                 value={this.state.password}
//                 onChangeText={text => {
//                   this.setState({
//                     password: text
//                   });
//                 }}

//               />
//               <Button
//                 buttonStyle={styles.loginButton}
//                 onPress={() => this.onLoginPress()}
//                 title="Login"
//               />
//               <Button
//                 buttonStyle={styles.fbLoginButton}
//                 onPress={() => this.onLoginWithTouchPress()}
//                 title="Login with TouchID"
//                 color="#3897f1"
//               />
//             </View>
//           </View>
//         </TouchableWithoutFeedback>
//       </KeyboardAvoidingView>

//     );
//   }

//   componentDidMount() {}

//   componentWillUnmount() {}

//   onLoginPress() {
//     let username = this.state.username
//     let password = this.state.password
//     this.props.navigation.navigate('Home');
//     // if((username == 'admin')&& (password == 'admin'))
//     //   this.props.navigation.navigate('Home');
//     //   else
//     //    alert('Invalid credentials...')
//   }

//   async onLoginWithTouchPress() {
//     Biometrics.simplePrompt('Confirm fingerprint')
//       .then(() => {
//         console.log('successful fingerprint provided');
//         this.props.navigation.navigate('Home');
//       })
//       .catch(() => {
//         console.log('fingerprint failed or prompt was cancelled');
//       });
//   }
// }

// import React, {Component} from 'react';

// import {
//   StyleSheet,
//   View,
//   TouchableOpacity,
//   Text,
//   Image,
//   KeyboardAvoidingView,
//   SafeAreaView,
// } from 'react-native';
// import {w, h, totalSize} from '../../utils/Dimensions';
// import GetStarted from './GetStarted';
// import {red} from 'ansi-colors';

// const companyLogo = require('../../assets/companylogo.png');
// const email = require('../../assets/email.png');
// const password = require('../../assets/password.png');
// import LinearGradient from 'react-native-linear-gradient';
// import {Button} from 'react-native-elements';

// import Background from './Components/Background';
// import LoginForm from './Components/LoginForm';
// import Logo from './Components/Logo';

// export default class LoginScreen extends Component {
//   constructor(props) {
//     super(props);

//   }
//   render() {
//     return (
//       <Background>
//         <Logo 
//         navigate={this.props.navigation.navigate} 
//         />
//         <LoginForm />
//       </Background>
//     );
//   }
// }

// const styles = StyleSheet.create({
//   screenContainer: {
//     width: w(70),
//     height: h(50),
//     backgroundColor: 'white',
//     alignItems: 'center',

//     position: 'absolute',
//   },
//   input: {
//     height: 40,
//     backgroundColor: 'rgba(225,225,225,0.2)',
//     marginBottom: 10,
//     padding: 10,
//     color: '#fff',
//   },
//   buttonContainer: {
//     backgroundColor: '#2980b6',
//     paddingVertical: 15,
//   },
//   buttonText: {
//     color: '#fff',
//     textAlign: 'center',
//     fontWeight: '700',
//   },
// });



import React, {Component} from 'react';
import styles from './style';
import {
  Keyboard,
  Text,
  View,
  TextInput,
  TouchableWithoutFeedback,
  Alert,
  Picker,
  KeyboardAvoidingView,
  SafeAreaView,
  Button,
} from 'react-native';

import {PermissionsAndroid, Platform} from 'react-native';



import Biometrics from 'react-native-biometrics';

export default class LoginScreen extends Component {
  state = {
    username: '',
    password: '',
    isBiometricSupport: false,
  };

  componentDidMount() {


    this.requestLocationPermission()
    Biometrics.isSensorAvailable().then(biometryType => {
      if (biometryType === Biometrics.TouchID) {
        this.setState({
          isBiometricSupport: true,
        });
      }
    });
  }

  render() {
    const {isBiometricSupport} = this.state;

    console.log('Start Trigger' + isBiometricSupport);

    return (
      <KeyboardAvoidingView style={styles.containerView} behavior="padding">
        <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
          <View style={styles.loginScreenContainer}>
            <View style={styles.loginFormView}>
              <Text style={styles.logoText}>MobilitySensor</Text>
              <TextInput
                placeholder="Username"
                placeholderColor="#c4c3cb"
                style={styles.loginFormTextInput}
                value={this.state.username}
                onChangeText={text => {
                  this.setState({
                    username: text,
                  });
                }}
              />
              <TextInput
                placeholder="Password"
                placeholderColor="#c4c3cb"
                style={styles.loginFormTextInput}
                secureTextEntry={true}
                value={this.state.password}
                onChangeText={text => {
                  this.setState({
                    password: text,
                  });
                }}
              />

              {/* <Picker
                selectedValue={this.state.language}
                style={{height: 50, width: 100}}
                onValueChange={(itemValue, itemIndex) =>
                  this.setState({language: itemValue})
                }>
                <Picker.Item label="Java" value="java" />
                <Picker.Item label="JavaScript" value="js" />
              </Picker> */}
              <Button
                buttonStyle={styles.loginButton}
                onPress={() => this.onLoginPress()}
                title="Login"
              />

              {isBiometricSupport && (
                <Button
                  buttonStyle={styles.fbLoginButton}
                  onPress={() => this.onLoginWithTouchPress()}
                  title="Login with TouchID"
                  color="#3897f1"
                />
              )}
            </View>
          </View>
        </TouchableWithoutFeedback>
      </KeyboardAvoidingView>
    );
  }

  async requestLocationPermission() {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
        {
          title: 'Location Permission',
          message: 'This app needs access to your location',
        },
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        console.log('You can use the location');
      } else {
        console.log('Location permission denied');
      }
    } catch (err) {
      console.warn(err);
    }
  }

  componentWillUnmount() {}

  onLoginPress() {
    let username = this.state.username;
    let password = this.state.password;
    // this.props.navigation.navigate('Home');
    this.props.navigation.navigate('drawerStack')
    // if((username == 'admin')&& (password == 'admin'))
    //   this.props.navigation.navigate('Home');
    //   else
    //    alert('Invalid credentials...')

    //  stopServiceTimer();
  }

  async onLoginWithTouchPress() {
    Biometrics.isSensorAvailable().then(biometryType => {
      if (biometryType === Biometrics.TouchID) {
        console.log('TouchID is supported');

        Biometrics.simplePrompt('Confirm fingerprint')
          .then(() => {
            console.log('successful fingerprint provided');
            this.props.navigation.navigate('drawerStack');
          })
          .catch(() => {
            console.log('fingerprint failed or prompt was cancelled');
          });
      } else if (biometryType === Biometrics.FaceID) {
        console.log('FaceID is supported');
      } else {
        console.log('Biometrics not supported');
      }
    });
  }
}

