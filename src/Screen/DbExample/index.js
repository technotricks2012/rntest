import * as React from 'react';
import {Appbar, Button, Avatar} from 'react-native-paper';
import {StyleSheet, StatusBar, View, Text, ScrollView, Platform,AppState} from 'react-native';

import database from '../../db/config'

import { addLocation,listLocation } from '../../db/DbManager'
import Location from '../../db/models/Location'


class DbExample extends React.Component {
  constructor(props) {
    super(props);
  }


  static navigationOptions = {
    title: 'Db Example',
  };



  render() {
    return (
      <View style={styles.continer}>
        <StatusBar backgroundColor="#456545" animated={true} />

        <View style={styles.continer}>
          <ScrollView>
          <Button
            style={styles.button}
            icon="camera"
            mode="contained"
            onPress={() =>  addLocation('2344','90.09','23.09')}>
            Add Loaction
          </Button>
          <Button
            style={styles.button}
            icon="camera"
            mode="contained"
            onPress={() => listLocation()}>
            List Location
          </Button>
        

          <Text>*****Locations*****</Text>

          <Text>X: </Text>
          <Text>Y: </Text>
          <Text>Z: </Text>
          <Text>Timestamp: </Text>


          </ScrollView>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  main: {
    flex: 1,
  },
  continer: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: '#64c4ed',
  },
  button: {
    alignContent: 'center',
    backgroundColor: '#32dbc6',

    marginTop: 20,
    height: 50,
    marginLeft: 0,
    marginRight: 0,
    justifyContent: 'center',
  },
  appbar: {
    backgroundColor: '#01024e',
    left: 0,
    right: 0,
    bottom: 0,
  },
});


export default DbExample;
