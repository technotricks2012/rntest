import * as React from 'react';
import {Appbar, Button, Avatar} from 'react-native-paper';
import {
  StyleSheet,
  StatusBar,
  View,
  Text,
  ScrollView,
  Platform,
  AppState,
} from 'react-native';

import {connect} from 'react-redux';
import {NativeModules} from 'react-native';

const {QuickServiceInterval} = NativeModules;
import {getSensorDataFromDB, deleteBatchFromDB} from '../db/DbManager';
import {uploadSensorData} from '../utils/UploadSensorData';
import {deleteAccelerometer} from '../db/models/Accelerometer';

// import PushNotification from 'react-native-push-notification';

class Menu extends React.Component {
  constructor(props) {
    super(props);
    this.handleAppStateChange = this.handleAppStateChange.bind(this);
    this.state = {
      seconds: 5,
      accelerometer: '',
      locationReducer: '',
    };
  }
  static navigationOptions = {
    title: 'Sensors',
  };

  componentDidMount() {
    AppState.addEventListener('change', this.handleAppStateChange);
    console.log('Print log ==+++', this.state.seconds);
  }

  componentWillUnmount() {
    AppState.removeEventListener('change', this.handleAppStateChange);
  }

  handleAppStateChange(appState) {
    if (appState === 'background') {
      let date = new Date(Date.now() + this.state.seconds * 1000);

      if (Platform.OS === 'ios') {
        date = date.toISOString();
      }

      console.log('Print log ==', this.state.seconds);
    }
  }
  _navTo(screenname) {
    console.log(screenname);
    this.props.navigation.navigate(screenname);
  }

  _notification() {
    // let date = new Date(Date.now() + (this.state.seconds * 1000));
    // if (Platform.OS === 'ios') {
    //   date = date.toISOString();
    // }
    // PushNotification.localNotificationSchedule({
    //   message: "My Notification Message",
    //   date,
    // });
  }

  async _uploadToServer() {
    const sensorData = await getSensorDataFromDB();
    console.log('getSensorDataFromDB ===<', JSON.stringify(sensorData));

    if (sensorData == 'NIL') return;
    let response = await uploadSensorData(sensorData);
    console.log('Response == ', JSON.stringify(response));

    if (response.statuscode == 200) {
      deleteBatchFromDB(response.batchId);
    }
  }

  render() {
    console.log(this.props);

    const {x, y, z, timestamp} = this.props.accelerometer;
    const {lat, lng} = this.props.locationReducer;

    return (
      <View style={styles.continer}>
        <StatusBar backgroundColor="#456545" animated={true} />
        {/* <Appbar.Header style={styles.appbar}>
          <Appbar.Content title="Sensors" />
        </Appbar.Header> */}
        <View style={styles.continer}>
          <ScrollView>
            <Button
              style={styles.button}
              icon="camera"
              mode="contained"
              onPress={() => this._navTo('GeoSensor')}>
              Geo Location
            </Button>
            <Button
              style={styles.button}
              icon="camera"
              mode="contained"
              onPress={() => this._navTo('Accelerometer')}>
              Accelerometer
            </Button>
            {
              <Button
                style={styles.button}
                icon="camera"
                mode="contained"
                onPress={() => this._navTo('WebLink')}>
                WebLink
              </Button>
            }

            {
              <Button
                style={styles.button}
                icon="camera"
                mode="contained"
                onPress={() => this._navTo('Login')}>
                Login
              </Button>
            }

            {
              <Button
                style={styles.button}
                icon="camera"
                mode="contained"
                onPress={() => this._notification()}>
                Notification
              </Button>
            }

            {
              <Button
                style={styles.button}
                icon="camera"
                mode="contained"
                onPress={() => this._navTo('MapView')}>
                Map
              </Button>
            }

            {
              <Button
                style={styles.button}
                icon="camera"
                mode="contained"
                onPress={() => this._navTo('Pedometer')}>
                Pedometer
              </Button>
            }

            {
              <Button
                style={styles.button}
                icon="camera"
                mode="contained"
                onPress={() => this._navTo('DbExample')}>
                DB Example
              </Button>
            }

            {
              <Button
                style={styles.button}
                icon="camera"
                mode="contained"
                onPress={() => QuickServiceInterval.startService(1000 * 1)}>
                Start Timer
              </Button>
            }

            {
              <Button
                style={styles.button}
                icon="camera"
                mode="contained"
                onPress={() => QuickServiceInterval.stopService()}>
                Stop Timer
              </Button>
            }

            {
              <Button
                style={styles.button}
                icon="camera"
                mode="contained"
                onPress={() => this._uploadToServer()}>
                Upload Sensor Data to Server
              </Button>
            }

            <Text>*****Accelerometer*****</Text>

            <Text>X: {x}</Text>
            <Text>Y: {y}</Text>
            <Text>Z: {z}</Text>
            <Text>Timestamp: {timestamp}</Text>

            <Text>*****Location*****</Text>
            <Text>Lat: {lat}</Text>
            <Text>Lng: {lng}</Text>
          </ScrollView>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  main: {
    flex: 1,
  },
  continer: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: '#64c4ed',
  },
  button: {
    alignContent: 'center',
    backgroundColor: '#32dbc6',

    marginTop: 20,
    height: 50,
    marginLeft: 0,
    marginRight: 0,
    justifyContent: 'center',
  },
  appbar: {
    backgroundColor: '#01024e',
    left: 0,
    right: 0,
    bottom: 0,
  },
});

function mapStateToProps(state) {
  return {
    accelerometer: state.accelerometer,
    locationReducer: state.locationReducer,
  };
}
export default connect(
  mapStateToProps,
  {},
)(Menu);
