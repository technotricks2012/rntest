import React, {Component} from 'react';
import {Button} from 'react-native-paper';
import FirebaseDB from '../../firebase/FirebaseDB';

import LocationBg from '../GeoLocation/BgTask';
import {NativeEventEmitter} from 'react-native';
import {DeviceEventEmitter} from 'react-native';

import {
  StyleSheet,
  StatusBar,
  View,
  Text,
  ScrollView,
  Platform,
  AppState,
} from 'react-native';
// import PedometerData from 'react-native-universal-pedometer';

const EventReminder = event => {
  console.log(event);
};

DeviceEventEmitter.addListener('EventReminder', EventReminder);

// // determine pedometer availability
// Pedometer.isStepCountingAvailable((error, isAvailable) => {

//   console.log('isStepCountingAvailable');

// });

// Pedometer.isDistanceAvailable((error, isAvailable) => {
//   console.log('isDistanceAvailable');
// });

// Pedometer.isFloorCountingAvailable((error, isAvailable) => {
//   console.log('isFloorCountingAvailable');
// });

// Pedometer.isCadenceAvailable((error, isAvailable) => {
//   console.log('isCadenceAvailable');
// });

// // start tracking from current time
// const now = new Date();
// Pedometer.startPedometerUpdatesFromDate(now.getTime(), pedometerData => {
//   console.log('startPedometerUpdatesFromDate');
//   console.log(pedometerData);
// });

// // query pedometer data from selected date to other selected date
// const startDate = new Date();
// startDate.setHours(0, 0, 0, 0);
// const endDate = new Date();
// Pedometer.queryPedometerDataBetweenDates(startDate.getTime(), endDate.getTime(), pedometerData => {
//   console.log('startPedometerUpdatesFromDate');
//   console.log(pedometerData);
// });

// // stop pedometer updates
// Pedometer.stopPedometerUpdates();

class Pedometer extends Component {
  _isMounted = false;
  constructor() {
    super();
    this.state = {
      ready: false,
      data: {x: null, y: null, z: null, timestamp: null},
      accelerometer: '',
      pedometerData: '',
    };
  }
  static navigationOptions = {
    title: 'Pedometer',
  };
  // componentDidMount() {
  //   this._isMounted = true;
  //   accelerometer.subscribe(({x, y, z, timestamp}) => {
  //     if (this._isMounted) {
  //       FirebaseDB.setAccelerometer({x, y, z, timestamp})
  //       this.props.setAccelerometer(x, y, z, timestamp);
  //       this.setState({
  //         ready: true,
  //         data: {
  //           x: x,
  //           y: y,
  //           z: z,
  //           timestamp: timestamp,
  //         },
  //       });
  //     }
  //   });
  // }

  componentWillUnmount() {
    this._isMounted = false;
  }

  render() {
    console.log(this.props.accelerometer);
    return (
      <View style={styles.continer}>
        <StatusBar backgroundColor="#456545" animated={true} />
        {/* <Appbar.Header style={styles.appbar}>
        <Appbar.Content title="Sensors" />
      </Appbar.Header> */}
        <View style={styles.continer}>
          <ScrollView>
            {/* {
              <Button
                style={styles.button}
                icon="camera"
                mode="contained"
                onPress={() => {
                  const now = new Date();
                  PedometerData.startPedometerUpdatesFromDate(
                    now.getTime(),
                    pedometerData => {
                      console.log('startPedometerUpdatesFromDate');
                      console.log(pedometerData);
                      this.setState({
                        pedometerData: pedometerData.numberOfSteps,
                      });

                      FirebaseDB.setData('Pedometer', {
                        startDate: pedometerData.startDate,
                        endDate: pedometerData.endDate,
                        numberOfSteps: pedometerData.numberOfSteps,
                        distance: pedometerData.distance,
                      });
                    },
                  );

                  PedometerData.isStepCountingAvailable(
                    (error, isAvailable) => {
                      console.log('isStepCountingAvailable');
                      FirebaseDB.setData('isStepCountingAvailable', true);
                    },
                  );
                }}>
                Notification
              </Button>
            } */}

            {
              <Button
                style={styles.button}
                icon="camera"
                mode="contained"
                onPress={() => {
                  LocationBg.readData(data => {
                    console.log(data);
                  });
                }}>
                Start
              </Button>
            }

            <Text>*****Accelerometer*****</Text>

            {/* <Text>PedometerData: {this.state.pedometerData}</Text> */}
          </ScrollView>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  main: {
    flex: 1,
  },
  continer: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: '#64c4ed',
  },
  button: {
    alignContent: 'center',
    backgroundColor: '#32dbc6',

    marginTop: 20,
    height: 50,
    marginLeft: 0,
    marginRight: 0,
    justifyContent: 'center',
  },
  appbar: {
    backgroundColor: '#01024e',
    left: 0,
    right: 0,
    bottom: 0,
  },
});

export default Pedometer;
