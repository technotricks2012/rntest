import { NativeModules } from 'react-native';

const { TimeInterval } = NativeModules;
export default TimeInterval;