// import {createStackNavigator, createAppContainer} from 'react-navigation';
// import { createDrawerNavigator, DrawerNavigator ,createAppContainer,createNavigator} from 'react-navigation'
import { createDrawerNavigator } from 'react-navigation-drawer';



import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';

import GeoSensor from './GeoLocation/GeoSensor';
import SwichExample from './SwichExample';
import Accelerometer from './Accelerometer';
import WebLink from './WebLink';
import Login from './Login';
import Menu from './Menu';
import MapView from './MapView';

import Pedometer from './Pedometer';

import DbExample from './DbExample';
import Home from './Home';
import Admin from './Admin';

const AppNavigator = createDrawerNavigator(
  {

    Login: Login,

    GeoSensor: GeoSensor,

    Menu: Menu,
    Home: Home,
    Admin:Admin,

    Accelerometer: Accelerometer,
    WebLink: WebLink,
    MapView: MapView,
    Pedometer: Pedometer,
    DbExample: DbExample,
    // AccelerometerSensor:AccelerometerSensor,
    SwichExample: SwichExample,
  },
  {
    // initialRouteName: 'Menu',
    defaultNavigationOptions: {
      // headerStyle: {
      //   backgroundColor: '#f4511e',
      // },
      // headerTintColor: '#fff',
      // headerTitleStyle: {
      //   fontWeight: 'bold',
      // },
      header: null,
    },
  },
);

export default createAppContainer(AppNavigator);
