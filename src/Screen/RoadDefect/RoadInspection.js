import {getLocal, setLocal} from '../../db/DbManager';
const POTHOLES = 'POTHOLES';
const SETTLEMENT = 'SETTLEMENT';
const DEPRESSION = 'DEPRESSION';
const ROADINSPECTIONSTATUS = 'ROADINSPECTIONSTATUS';

export class RoadInspection {
 constructor(isRunning, potholes, settlement, depression) {
    //   this.status = status;
    //   this.potholes = potholes;
    //   this.settlement = settlement;
    //   this.depression = depression;
    

   
  }

  async reset(){
        var _statusDB = await getLocal(ROADINSPECTIONSTATUS);
        const _status = _statusDB ? _statusDB : '';
        if ((_status == '')||(_status == 'FALSE')) {
           this.setStatus('FALSE');
           this.setPotholes("0");
           this.setSettlement("0");
           this.setDepression("0");
        }
  
  }

  setStatus(isRunning) {
    //   this.status = status
    setLocal(ROADINSPECTIONSTATUS, ""+isRunning);
  }

  setPotholes(potholesCount) {
    //   this.potholes = potholesCount
    setLocal(POTHOLES, ""+potholesCount);
  }

  setSettlement(settlementCount) {
    //   this.settlement = settlementCount
    setLocal(SETTLEMENT, ""+settlementCount);
  }
  setDepression(depressionCount) {
    //   this.depression = depressionCount
    setLocal(DEPRESSION, ""+depressionCount);
  }

  async toJson() {
    var _status = await getLocal(ROADINSPECTIONSTATUS);
    var _potholes = await getLocal(POTHOLES);
    var _settlement = await getLocal(SETTLEMENT);
    var _depression = await getLocal(DEPRESSION);

    return {
      isRunning: (_status == "TRUE")?true:false,
      potholes: parseInt(_potholes?_potholes:0),
      settlement: parseInt(_settlement?_settlement:0),
      depression: parseInt(_depression?_depression:0),
    };
  }
}
