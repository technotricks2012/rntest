import React, {Component} from 'react';
import {
  StyleSheet,
  SafeAreaView,
  ScrollView,
  View,
  PermissionsAndroid,
} from 'react-native';
import {withNavigation} from 'react-navigation';
import RNAndroidLocationEnabler from 'react-native-android-location-enabler';
import {startServiceTimer} from '../../utils/ServiceManager';
import {TimeInterval} from '../../constants/ITimeConstant';

import {Toast} from 'native-base';

import {w, h, totalSize} from '../../utils/Dimensions';
import {Button} from 'react-native-elements';

import debounce from 'lodash/debounce';

import Pulse from 'react-native-pulse';

import {SvgXml} from 'react-native-svg';

import {Text, Header} from 'react-native-elements';

import {CustomCountButton} from '../../components/CustomCountButton';

import selectedIcon from '../../images/icons/car.svg';
import {NativeModules, Platform} from 'react-native';

import {DeviceEventEmitter} from 'react-native';
import {RoadInspection} from './RoadInspection';

let items = [
  {title: 'Potholes', count: 0, isRunning: false},
  {title: 'Settlement', count: 0, isRunning: false},
  {title: 'Depression', count: 0, isRunning: false},
];
const RUNNING = 'RUNNING';
const FAIL = 'FAIL';
const UNKNOWN = 'UNKNOWN';

const LOCATION_SETTING = 'Location Settings Not enabled';
const LOCATION_PERMISSION = 'Location Permission Not enabled';

class RoadDefect extends Component {
  _isMounted = false;
  _status = UNKNOWN;
  timer = '';

  static navigationOptions = {
    drawerLabel: 'Road Inspection',
  };

  async reset() {
    this.roadInspectionData = new RoadInspection();
    await this.roadInspectionData.reset();
  }
  constructor() {
    super();
    this.state = {
      isRunning: false,
    };

    this.state = {
      isPotholesRunning: false,
      isSettlementRunning: false,
      isDepressionRunning: false,
    };

    this.state = {
      isRideButtonRunning: false,
    };

    this.state = {
      isPotholesDisiable: false,
      isSettlementDisiable: false,
      isDepressionDisiable: false,
    };

    this.state = {
      isSubButtonDisabled: false,
    };

    this.state = {
      modes: [],
    };

    this.state = {
      startTime: '',
      counterTime: '',
    };
  }

  async componentDidMount() {
    this._isMounted =true
    await startServiceTimer(TimeInterval);

    this.onRideButton = debounce(this.onRideButton.bind(this), 200);

    const {navigation} = this.props;
    this.focusListener = navigation.addListener('didFocus', () => {
      console.log('Test ==> componentDidMount');
    });

    await this.setData();

    if (Platform.OS === 'ios') {
      //TODO
    } else if (Platform.OS === 'android') {
      DeviceEventEmitter.addListener(
        'RoadDefect',
        this.RoadDefectLocationErrorObserver,
      );
    }
  }

  async setData() {
    await this.reset();
    const data = await this.roadInspectionData.toJson();

    console.log('Test ==> DATA = ' + JSON.stringify(data));

    const _isStart = data.isRunning;

    var _items = items;
    _items[0].count = data.potholes;
    _items[1].count = data.settlement;
    _items[2].count = data.depression;
    this.setState({modes: _items});
    this.setState({isRunning: _isStart});
  }

  RoadDefectLocationErrorObserver = async locationError => {
    if(this._isMounted){
    console.log('RoadDefect==>' + JSON.stringify(locationError));

    const error = locationError.errorResponse;
    if (error.isErroneous) {
      this._status = FAIL;

      const msg = error.error.detailMessage;
      if (msg == LOCATION_SETTING || msg == LOCATION_PERMISSION) {
        this.showErrorPopUp(msg);
      } else {
        Toast.show({
          text: 'Oops! Something went wrong.',
          buttonText: 'Okay',
          duration: 2000,
          type: 'danger',
        });
      }
    }
  }
  };

  showErrorPopUp(msg) {
    switch (msg) {
      case LOCATION_PERMISSION: {
        this.requestLocationPermission();
      }
      case LOCATION_SETTING: {
        this.requestLocationEnable();
      }
    }
  }

  componentWillUnmount() {
    this._isMounted =false
    this.focusListener.remove();

    if (Platform.OS === 'ios') {
      //TODO
    } else if (Platform.OS === 'android') {
      DeviceEventEmitter.removeListener('RoadDefect');
    }
  }

  onRideButton() {
    var _isRunning = this.state.isRunning;

    this._status = RUNNING;

    this.setState({isRideButtonRunning: true});

    if (_isRunning) {
      this.stopRide();
    } else {
      const values = items.map(function(item) {
        item.count = 0;
        return item;
      });
      this.setState({modes: [...values]});
      this.startRide();
    }

    setTimeout(() => {
      if (this._status != FAIL) {
        this.setState({isRideButtonRunning: false});
        const status = !_isRunning;
        this.setState({
          isRunning: status,
        });

        this.roadInspectionData.setStatus(status ? 'TRUE' : 'FALSE');
      }
    }, 1000);

    setTimeout(() => {
      this.setState({isRideButtonRunning: false});
    }, 3000);
  }

  startRide() {
    if (Platform.OS === 'ios') {
      //TODO
    } else if (Platform.OS === 'android') {
      NativeModules.RoadDefect.startTrip();
    }
  }

  stopRide() {
    if (Platform.OS === 'ios') {
      //TODO
    } else if (Platform.OS === 'android') {
      NativeModules.RoadDefect.stopTrip();
    }
  }

  updateTrip(type) {
    this._status = RUNNING;
    // this.setState({isSubButtonDisabled: true});

    switch (type) {
      case 'Potholes': {
        this.setState({isPotholesRunning: true});

        this.setState({isPotholesDisiable: false});
        this.setState({isSettlementDisiable: true});
        this.setState({isDepressionDisiable: true});

        break;
      }
      case 'Settlement': {
        this.setState({isSettlementRunning: true});

        this.setState({isPotholesDisiable: true});
        this.setState({isSettlementDisiable: false});
        this.setState({isDepressionDisiable: true});
        break;
      }
      case 'Depression': {
        this.setState({isDepressionRunning: true});

        this.setState({isPotholesDisiable: true});
        this.setState({isSettlementDisiable: true});
        this.setState({isDepressionDisiable: false});
        break;
      }
    }

    setTimeout(() => {
      // this.setState({isSubButtonDisabled: false});
      this.setState({isPotholesRunning: false});
      this.setState({isSettlementRunning: false});
      this.setState({isDepressionRunning: false});

      this.setState({isPotholesDisiable: false});
      this.setState({isSettlementDisiable: false});
      this.setState({isDepressionDisiable: false});
    }, 2000);

    setTimeout(() => {
      if (this._status != FAIL) {
        var modes = this.state.modes;
        switch (type) {
          case 'Potholes': {
            modes[0].count++;
            this.roadInspectionData.setPotholes(modes[0].count);
            break;
          }
          case 'Settlement': {
            modes[1].count++;
            this.roadInspectionData.setSettlement(modes[1].count);

            break;
          }
          case 'Depression': {
            modes[2].count++;
            this.roadInspectionData.setDepression(modes[2].count);
            break;
          }
        }
        this.setState({modes: modes});
      }
    }, 500);

    if (Platform.OS === 'ios') {
      //TODO
    } else if (Platform.OS === 'android') {
      NativeModules.RoadDefect.updateRoadDefect(type);
    }
  }

  async requestLocationPermission() {
    if (Platform.OS === 'android') {
      try {
        const granted = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
          {
            title: 'Location Permission',
            message: 'This app needs access to your location',
          },
        );
        if (granted === PermissionsAndroid.RESULTS.GRANTED) {
          console.log('You can use the location');
        } else {
          console.log('Location permission denied');
        }
      } catch (err) {
        console.warn(err);
      }
    }
  }

  requestLocationEnable() {
    if (Platform.OS === 'android') {
      RNAndroidLocationEnabler.promptForEnableLocationIfNeeded({
        interval: 10000,
        fastInterval: 5000,
      })
        .then(data => {})
        .catch(err => {});
    }
  }

  render() {
    const {isRunning, isRideButtonRunning, modes} = this.state;

    return (
      <SafeAreaView style={styles.container}>
        <Header
          ViewComponent={require('react-native-linear-gradient').default}
          linearGradientProps={{
            colors: ['#1a61d8', '#1fc7bd'],
            start: {x: 0, y: 0.5},
            end: {x: 1, y: 0.5},
          }}
          containerStyle={{height: 100}}
          placement="left"
          leftComponent={{
            icon: 'menu',
            color: '#fff',
            size: 30,
            onPress: () => {
              this.props.navigation.openDrawer();
            },
          }}
          centerComponent={{
            text: (
              <Text
                style={{
                  color: '#fff',
                  fontSize: 35,
                  fontFamily: 'SanFranciscoText-Bold',
                  fontWeight: 'bold',
                }}>
                Road Inspection
              </Text>
            ),
            style: {color: '#fff', size: 40},
          }}
        />

        <ScrollView>
          <View
            style={{
              justifyContent: 'flex-start',
              flex: 1,
              flexDirection: 'column',
              margin: 15,
              height: h(94) - 100,
            }}>
            <View
              style={{
                height: '25%',
                width: '100%',
                justifyContent: 'center',
              }}>
              <Text
                style={{
                  fontSize: 12,
                  textAlign: 'center',
                  paddingTop: 10,
                  color: 'black',
                }}>
                {!isRunning ? 'Press Start Ride' : 'On the road...'}
              </Text>
              <View
                style={{
                  height: '100%',
                  width: '100%',
                  justifyContent: 'center',
                  alignContent: 'center',
                }}>
                {isRunning && (
                  <Pulse
                    color="#1ebebf"
                    numPulses={2}
                    diameter={120}
                    speed={20}
                    duration={1000}
                  />
                )}

                <View
                  style={{
                    width: '100%',
                    flex: 1,
                    justifyContent: 'center',
                    alignItems: 'center',
                  }}>
                  <SvgXml
                    xml={selectedIcon}
                    width="50"
                    height="50"
                    style={{
                      width: 100,
                      height: 100,
                    }}
                    fill={isRunning ? 'white' : '#8FC0B6'}
                  />
                </View>
              </View>
            </View>

            <View
              style={{
                height: '75%',
                width: '100%',
                flex: 1,
                flexDirection: 'column',
              }}>
              <Button
                buttonStyle={styles.buttonRideStyle}
                title={!isRunning ? 'Start Ride' : 'End Ride'}
                titleStyle={styles.buttonTextStyle}
                onPress={() => this.onRideButton()}
                loading={isRideButtonRunning}
                disabled={isRideButtonRunning}
                disabledStyle={styles.buttonDisableRideStyle}
              />

              {isRunning && (
                <View>
                  <CustomCountButton
                    title={modes[0].title}
                    isRunning={this.state.isPotholesRunning}
                    isDisabled={this.state.isPotholesDisiable}
                    count={modes[0].count}
                    style={styles.buttonDefectStyle}
                    onPress={() => this.updateTrip(modes[0].title)}
                  />

                  <CustomCountButton
                    title={modes[1].title}
                    isRunning={this.state.isSettlementRunning}
                    isDisabled={this.state.isSettlementDisiable}
                    count={modes[1].count}
                    style={styles.buttonDefectStyle}
                    onPress={() => this.updateTrip(modes[1].title)}
                  />

                  <CustomCountButton
                    title={modes[2].title}
                    isRunning={this.state.isDepressionRunning}
                    isDisabled={this.state.isDepressionDisiable}
                    count={modes[2].count}
                    style={styles.buttonDefectStyle}
                    onPress={() => this.updateTrip(modes[2].title)}
                  />
                </View>
              )}
            </View>
          </View>
        </ScrollView>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },

  scroll: {
    marginTop: 20,
  },

  buttonRideStyle: {
    height: h(50) / 4,
    backgroundColor: '#1ebebf',
    borderRadius: 5,
    justifyContent: 'center',
    alignItems: 'center',
  },

  buttonDisableRideStyle: {
    height: h(50) / 4,
    backgroundColor: '#20ACAC',
    borderRadius: 5,
    justifyContent: 'center',
    alignItems: 'center',
  },

  buttonDefectStyle: {
    height: h(50) / 4,
    width: '100%',
    marginTop: 10,
  },

  buttonStyle: {
    marginTop: 15,
    padding: 10,
    backgroundColor: '#1ebebf',
    paddingStart: 50,
    paddingRight: 50,
    marginStart: 30,
    marginEnd: 30,
    marginTop: 20,
    borderRadius: 5,
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttonTextStyle: {
    color: 'white',
    fontSize: 18,
    textAlign: 'left',
    fontFamily: 'SanFranciscoDisplay-Semibold',
  },
});

export default withNavigation(RoadDefect);
