import React, {Component} from 'react';

import {View, StyleSheet, ScrollView, SafeAreaView, Button} from 'react-native';
import {connect} from 'react-redux';
import {Icon, Header, Text} from 'react-native-elements';
import {
  Table,
  TableWrapper,
  Row,
  Rows,
  Col,
} from 'react-native-table-component';

class Admin extends Component {
  _isMounted = false;

  constructor() {
    super();

    this.state = {
      proximity: '',
    };
  }

  render() {
    console.log(this.props);
    const {
      maxRange,
      value,
      isNear,
      timestamp,
    } = this.props.proximity;


    return (
      <Table borderStyle={{borderWidth: 2, borderColor: '#c8e1ff'}}>
      <Row
        data={['Proximity']}
        flexArr={[4]}
        style={{height: 40, backgroundColor: '#f1f8ff'}}
        textStyle={styles.textValue}
      />
      <TableWrapper style={{flexDirection: 'row'}}>
        <Col
          data={['MaxRange', 'Value', 'isNear', 'Time']}
          style={{flex: 1, backgroundColor: '#786757'}}
          heightArr={[28, 28]}
          textStyle={styles.textSubHead}
        />
        <Rows
          data={[[maxRange], [value], [isNear], [timestamp]]}
          flexArr={[3]}
          style={styles.row}
          textStyle={styles.textValue}
        />
      </TableWrapper>
    </Table>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  textValue: {textAlign: 'center', fontSize: 15},
  textHead: {textAlign: 'center', fontSize: 25},
  textSubHead: {textAlign: 'center', fontSize: 15},
  row: {height: 28},
});

function mapStateToProps(state) {
  return {
    proximity: state.proximity,
  };
}
export default connect(
  mapStateToProps,
  {},
)(Admin);
