import React, {Component} from 'react';

import {parseFloatUtil} from '../../../utils/Utils';

import {w,h} from '../../../utils/Dimensions';

import {View, StyleSheet, ScrollView, SafeAreaView, Button,Dimensions} from 'react-native';
import {connect} from 'react-redux';
import {Icon, Header, Text} from 'react-native-elements';
import {
  Table,
  TableWrapper,
  Row,
  Rows,
  Col,
} from 'react-native-table-component';

import MapView, {PROVIDER_GOOGLE, Marker} from 'react-native-maps';

const mapStyle= 

[
  {
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#242f3e"
      }
    ]
  },
  {
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#746855"
      }
    ]
  },
  {
    "elementType": "labels.text.stroke",
    "stylers": [
      {
        "color": "#242f3e"
      }
    ]
  },
  {
    "featureType": "administrative.locality",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#d59563"
      }
    ]
  },
  {
    "featureType": "poi",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#d59563"
      }
    ]
  },
  {
    "featureType": "poi.park",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#263c3f"
      }
    ]
  },
  {
    "featureType": "poi.park",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#6b9a76"
      }
    ]
  },
  {
    "featureType": "road",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#38414e"
      }
    ]
  },
  {
    "featureType": "road",
    "elementType": "geometry.stroke",
    "stylers": [
      {
        "color": "#212a37"
      }
    ]
  },
  {
    "featureType": "road",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#9ca5b3"
      }
    ]
  },
  {
    "featureType": "road.highway",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#746855"
      }
    ]
  },
  {
    "featureType": "road.highway",
    "elementType": "geometry.stroke",
    "stylers": [
      {
        "color": "#1f2835"
      }
    ]
  },
  {
    "featureType": "road.highway",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#f3d19c"
      }
    ]
  },
  {
    "featureType": "transit",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#2f3948"
      }
    ]
  },
  {
    "featureType": "transit.station",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#d59563"
      }
    ]
  },
  {
    "featureType": "water",
    "elementType": "geometry",
    "stylers": [
      {
        "color": "#17263c"
      }
    ]
  },
  {
    "featureType": "water",
    "elementType": "labels.text.fill",
    "stylers": [
      {
        "color": "#515c6d"
      }
    ]
  },
  {
    "featureType": "water",
    "elementType": "labels.text.stroke",
    "stylers": [
      {
        "color": "#17263c"
      }
    ]
  }
]
class Admin extends Component {
  _isMounted = false;

  constructor() {
    super();

    this.state = {
      location: '',
      latitudeDelta: 0.00522,
      longitudeDelta: Dimensions.get("window").width / Dimensions.get("window").height * 0.00522
    };
  }

  render() {
    console.log(this.props);
    const {latitude, longitude, altitude, timestamp} = this.props.location;

    const {longitudeDelta, latitudeDelta} = this.state;


    let lat = parseFloatUtil(latitude);
    let long = parseFloatUtil(longitude);

    return (
      <View style={{flexDirection: 'column', marginTop: 10}}>
        <View>
          <MapView
            zoomControlEnabled={false}
            zoomEnabled={false}
            zoomTapEnabled={false}
            pitchEnabled={false}
            rotateEnabled={false}
            scrollEnabled={false}
            style={{height: 200}}
            provider={PROVIDER_GOOGLE}
            customMapStyle={mapStyle}

                   
            region={{
              latitude: lat,
              longitude: long,
              latitudeDelta: latitudeDelta,
              longitudeDelta: longitudeDelta,
            }}
            initialRegion={{
              latitude: 1.352083,
              longitude: 103.819839,
              latitudeDelta: latitudeDelta,
              longitudeDelta: longitudeDelta,
            }}>
            <Marker
              coordinate={{
                latitude: lat,
                longitude: long,
              }}
              title="Current Location"
              description="Last known loaction"
            />
          </MapView>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  map: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
  },
  textValue: {textAlign: 'center', fontSize: 15},
  textHead: {textAlign: 'center', fontSize: 25},
  textSubHead: {textAlign: 'center', fontSize: 15},
  row: {height: 28},  
});

function mapStateToProps(state) {
  return {
    location: state.location,
  };
}
export default connect(mapStateToProps, {})(Admin);
