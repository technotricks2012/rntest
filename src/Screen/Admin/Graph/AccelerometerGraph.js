import React, {Component} from 'react';

import {View, StyleSheet, Text, SafeAreaView, Dimensions} from 'react-native';
import {connect} from 'react-redux';
import {w, h, totalSize} from '../../../utils/Dimensions';
import {Rect, Svg} from 'react-native-svg';
import {parseFloatUtil} from '../../../utils/Utils';

import {
  LineChart,
  AreaChart,
  Grid,
  YAxis,
  XAxis,
} from 'react-native-svg-charts';
import * as shape from 'd3-shape';
import * as scale from 'd3-scale';

class Admin extends Component {
  timeArr = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
  xArr = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
  yArr = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
  zArr = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0];

  constructor() {
    super();

    this.state = {
      accelerometer: '',
    };
  }

  count = 0;
  getLabelData(x, y, z, timestamp) {
    this.count++;
    this.timeArr.shift();
    this.xArr.shift();
    this.yArr.shift();
    this.zArr.shift();

    this.timeArr.push(this.count);

    this.xArr.push(parseFloatUtil(x));
    this.yArr.push(parseFloatUtil(y));
    this.zArr.push(parseFloatUtil(z));

    console.log('GRAPH X=>' + JSON.stringify(this.xArr));
    console.log('GRAPH Y=>' + JSON.stringify(this.yArr));
    console.log('GRAPH Z=>' + JSON.stringify(this.zArr));
    console.log('GRAPH Time=>' + JSON.stringify(this.timeArr));
  }


 

  render() {
    console.log(this.props);
    const {x, y, z, timestamp} = this.props.accelerometer;
    this.getLabelData(x, y, z, timestamp);

    const xInt = parseFloatUtil(x)
    const yInt = parseFloatUtil(y) 
    const zInt = parseFloatUtil(z)

    return (
      <View style={{flexDirection: 'column', marginTop: 10}}>
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'center',
          }}>
          <View style={[styles.boxStyle,{backgroundColor:'#e74c3c'}]}>
            <Text style={styles.textstyle}>x: {xInt.toFixed(2)}</Text>
          </View>

          <View style={[styles.boxStyle,{backgroundColor:'#2ecc71'}]}>
            <Text style={styles.textstyle}>y: {yInt.toFixed(2)}</Text>
          </View>

          <View style={[styles.boxStyle,{backgroundColor:'#2980b9'}]}>
            <Text style={styles.textstyle}>z: {zInt.toFixed(2)}</Text>
          </View>
          
        </View>

        <View style={{height: 200, flexDirection: 'row'}}>
          <YAxis
            data={[20, 0, -20]}
            svg={{
              fill: 'grey',

              fontSize: 13,
            }}
            numberOfTicks={3}
            formatLabel={value => value}
            contentInset={{top: 5, bottom: 5}}
          />
          <View style={{flex: 1}}>
            <LineChart
              numberOfTicks={3}
              yMin={-20}
              yMax={20}
              style={{flex: 1}}
              data={this.xArr}
              svg={{stroke: '#e74c3c', strokeWidth: 2}}
              contentInset={{top: 5, bottom: 5}}
              curve={shape.curveNatural}>
              <Grid />
            </LineChart>
            <LineChart
              contentInset={{top: 5, bottom: 5}}
              yMin={-20}
              yMax={20}
              numberOfTicks={3}
              style={StyleSheet.absoluteFill}
              data={this.yArr}
              svg={{stroke: '#2ecc71', strokeWidth: 2}}
              curve={shape.curveNatural}
            />
            <LineChart
              contentInset={{top: 5, bottom: 5}}
              yMin={-20}
              yMax={20}
              numberOfTicks={3}
              style={StyleSheet.absoluteFill}
              data={this.zArr}
              svg={{stroke: '#2980b9', strokeWidth: 2}}
              curve={shape.curveNatural}
            />
          </View>
        </View>

        <XAxis
          contentInset={{top: 5, bottom: 5, left: 20, right: 10}}
          data={this.timeArr}
          scale={scale.scaleTime}
          svg={{
            fill: 'grey',

            fontSize: 13,
          }}
          labelStyle={{color: 'black'}}
          formatLabel={value => value}
          xAccessor={({item}) => item}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  textstyle: {
    textAlign: 'center',
    fontSize: 12,
    fontFamily: 'SanFranciscoText-Medium',
    color: 'white',
  },
  boxStyle: {padding: 5,marginLeft:2,marginRight:2, borderRadius: 5},
});

function mapStateToProps(state) {
  return {
    accelerometer: state.accelerometer,
  };
}
export default connect(
  mapStateToProps,
  {},
)(Admin);
