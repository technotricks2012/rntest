
import AccelerometerGraph from './AccelerometerGraph';
import GyroscopeGraph from './GyroscopeGraph';
import GeoLocationGraph from './GeoLocationGraph'
import MagnetometerGraph from './MagnetometerGraph';
import BarometerGraph from './BarometerGraph';
import OrientationGraph from './OrientationGraph';
import LightSensorGraph from './LightSensorGraph';
import PedometerGraph from './PedometerGraph';
import Test from './Test';



import React from 'react';
import {TouchableOpacity, StyleSheet, Text, View} from 'react-native';

import {w, h, totalSize} from '../../../utils/Dimensions';
import {ACCELEROMETER} from '../../../constants/IKeyConstants';
export const Graph = props => {
  const {graphType} = props;

  return <View>{renderGraph(graphType)}</View>;

  function renderGraph(selectedMode) {
    switch (selectedMode) {
      case 'Test':
          return <Test />;
      case 'Accelerometer':
        return <AccelerometerGraph />;

      case 'Gyroscope':
        return <GyroscopeGraph />;

      case 'Magnetometer':
        return <MagnetometerGraph />;

      case 'Barometer':
        return <BarometerGraph />;

      case 'Location':
        return <GeoLocationGraph />;

      case 'Pedometer':
        return <PedometerGraph />;

      case 'Orientation':
        return <OrientationGraph />;

      case 'Lightsensor':
        return <LightSensorGraph />;
    }
  }
};

const styles = StyleSheet.create({
  container: {
    width: w(100) / 3,
    height: w(100) / 3,
    padding: 10,
  },

  innerContinerActive: {
    flex: 1,
    borderRadius: 5,
    borderWidth: 1,
    borderColor: '#ddd',
    borderBottomWidth: 0,
    shadowColor: '#000',
    shadowOffset: {width: 0, height: 2},
    shadowOpacity: 0.8,
    shadowRadius: 2,
    elevation: 3,
    justifyContent: 'center',
    alignItems: 'center',
  },

  button: {
    display: 'flex',
    height: 50,
    borderRadius: 5,
    justifyContent: 'center',
    alignItems: 'center',

    backgroundColor: '#2AC062',
    shadowColor: '#2AC062',
    shadowOpacity: 0.4,
    shadowOffset: {height: 10, width: 0},
    shadowRadius: 20,
  },

  textActive: {
    fontFamily: 'SanFranciscoText-Medium',
    fontSize: 12,
    alignContent: 'center',
  },
  image: {},
});
