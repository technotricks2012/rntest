import React, {Component} from 'react';

import {View, StyleSheet, Text, SafeAreaView, Dimensions} from 'react-native';
import {connect} from 'react-redux';
import {w, h, totalSize} from '../../../utils/Dimensions';
import {Rect, Svg} from 'react-native-svg';
import {parseFloatUtil} from '../../../utils/Utils';

import {
  LineChart,
  AreaChart,
  Grid,
  YAxis,
  XAxis,
} from 'react-native-svg-charts';
import * as shape from 'd3-shape';
import * as scale from 'd3-scale';

class Admin extends Component {
  timeArr = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
  xArr = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
  yArr = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
  zArr = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0];

  timeMax = 20;

  constructor() {
    super();

    this.state = {
      orientation: '',
    };
  }

  count = 0;
  getLabelData(x, y, z, timestamp) {
    this.count++;
    this.timeArr.shift();
    this.xArr.shift();
    this.yArr.shift();
    this.zArr.shift();

    this.timeArr.push(this.count);

    const x1 = parseFloatUtil(x);
    const y1 = parseFloatUtil(y);
    const z1 = parseFloatUtil(z);

    this.xArr.push(x1);
    this.yArr.push(y1);
    this.zArr.push(z1);

    if (Math.abs(x1) > this.timeMax) this.timeMax = Math.abs(x1) +100;
    if (Math.abs(y1) > this.timeMax) this.timeMax = Math.abs(y1) +100;
    if (Math.abs(z1) > this.timeMax) this.timeMax = Math.abs(z1) +100;

    console.log('GRAPH X=>' + JSON.stringify(this.xArr));
    console.log('GRAPH Y=>' + JSON.stringify(this.yArr));
    console.log('GRAPH Z=>' + JSON.stringify(this.zArr));
    console.log('GRAPH Time=>' + JSON.stringify(this.timeArr));
  }

  
  render() {
    console.log(this.props);
    const {
      roll,
      pitch,
      azimuth,
      timestamp,
    } = this.props.orientation;
    
    this.getLabelData( roll,
      pitch,
      azimuth,
      timestamp);

    const xInt = parseFloatUtil(roll);
    const yInt = parseFloatUtil(pitch);
    const zInt = parseFloatUtil(azimuth);

    return (
      <View style={{flexDirection: 'column', marginTop: 10}}>
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'center',
          }}>
          <View style={[styles.boxStyle, {backgroundColor: '#e74c3c'}]}>
            <Text style={styles.textstyle}>Roll: {xInt.toFixed(2)}</Text>
          </View>

          <View style={[styles.boxStyle, {backgroundColor: '#2ecc71'}]}>
            <Text style={styles.textstyle}>Pitch: {yInt.toFixed(2)}</Text>
          </View>

          <View style={[styles.boxStyle, {backgroundColor: '#2980b9'}]}>
            <Text style={styles.textstyle}>Azimuth: {zInt.toFixed(2)}</Text>
          </View>
        </View>

        <View style={{height: 200, flexDirection: 'row'}}>
          <YAxis
            data={[this.timeMax,0,-this.timeMax]}
            svg={{
              fill: 'grey',

              fontSize: 13,
            }}
            scale={scale.scaleLinear}
            yAccessor={({item}) => item}
            formatLabel={value => value}
            contentInset={{top: 5, bottom: 5}}
          />
          <View style={{flex: 1}}>
            <LineChart
              yMin={-this.timeMax}
              yMax={this.timeMax}
              style={{flex: 1}}
              data={this.xArr}
              svg={{stroke: '#e74c3c', strokeWidth: 2}}
              contentInset={{top: 5, bottom: 5}}
              curve={shape.curveNatural}>
              <Grid />
            </LineChart>
            <LineChart
              contentInset={{top: 5, bottom: 5}}
              yMin={-this.timeMax}
              yMax={this.timeMax}
              style={StyleSheet.absoluteFill}
              data={this.yArr}
              svg={{stroke: '#2ecc71', strokeWidth: 2}}
              curve={shape.curveNatural}
            />
            <LineChart
              contentInset={{top: 5, bottom: 5}}
              yMin={-this.timeMax}
              yMax={this.timeMax}
              style={StyleSheet.absoluteFill}
              data={this.zArr}
              svg={{stroke: '#2980b9', strokeWidth: 2}}
              curve={shape.curveNatural}
            />
          </View>
        </View>

        <XAxis
          contentInset={{top: 5, bottom: 5, left: 20, right: 10}}
          data={this.timeArr}
          scale={scale.scaleTime}
          xAccessor={({item}) => item}
          svg={{
            fill: 'grey',

            fontSize: 13,
          }}
          labelStyle={{color: 'black'}}
          formatLabel={value => value}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  textstyle: {
    textAlign: 'center',
    fontSize: 12,
    fontFamily: 'SanFranciscoText-Medium',
    color: 'white',
  },
  boxStyle: {padding: 5, marginLeft: 2, marginRight: 2, borderRadius: 5},
});

function mapStateToProps(state) {
  return {
    orientation: state.orientation,
  };
}
export default connect(
  mapStateToProps,
  {},
)(Admin);
