import React, {Component} from 'react';

import {View, StyleSheet, Text, SafeAreaView, Dimensions,Image} from 'react-native';
import {connect} from 'react-redux';
import {w, h, totalSize} from '../../../utils/Dimensions';
import {Rect, Svg} from 'react-native-svg';
import {parseFloatUtil} from '../../../utils/Utils';
import {SvgXml} from 'react-native-svg';
import walkIcon from '../../../images/icons/walk.svg';

import {
  LineChart,
  AreaChart,
  Grid,
  YAxis,
  XAxis,
} from 'react-native-svg-charts';
import * as shape from 'd3-shape';
import * as scale from 'd3-scale';
import Pulse from 'react-native-pulse';
import fingerprintIcon from '../../../images/icon_fingerprint.png';

class Admin extends Component {
  constructor() {
    super();

    this.state = {
      pedometer: '',
    };
  }

  render() {
    console.log(this.props);
    const {numberOfSteps, distance, timestamp} = this.props.pedometer;

    return (
      <View style={{flexDirection: 'column', marginTop: 10}}>
        <View
          style={styles.container}>
          {/* <Pulse
            color="#1fc7bd"
            numPulses={3}
            diameter={200}
            speed={20}
            duration={1000}
          />
           
          <Image style={styles.icon} source={fingerprintIcon} /> */}



            <View>
              <Pulse
                color="#1fc7bd"
                numPulses={3}
                diameter={150}
                speed={20}
                duration={1000}
              />
              <SvgXml
                xml={walkIcon}
                width="30"
                height="30"
                fill={'white'}
              />
            </View>
            <Text style={[styles.hinttext, {marginTop: 10}]}>
              Steps: {(numberOfSteps==0)?"--":numberOfSteps}
            </Text>
            <Text style={[styles.hinttext, {marginTop: 5}]}>
              Distance: {(distance==0)?"--":""+(distance/1000).toFixed(3)+" km"}
            </Text>

        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: 
  {
    marginTop: 20,
    marginLeft: w(100) / 4,
    width: w(100) / 2,
    height: 200,
    justifyContent: 'center',
    alignItems: 'center',
  },
  icon: {
    width: w(7),
    height: w(7),
    justifyContent: 'center',
    alignItems: 'center',
  },
  textstyle: {
    textAlign: 'center',
    fontSize: 12,
    fontFamily: 'SanFranciscoText-Medium',
    color: 'white',
  },
  hinttext: {
    color: '#393939',
    fontFamily: 'OpenSans-Semibold',
    fontSize: 14,
    textAlign: 'center',
    marginTop: 10,
  },
  boxStyle: {padding: 5, marginLeft: 2, marginRight: 2, borderRadius: 5},
});

function mapStateToProps(state) {
  return {
    pedometer: state.pedometer,
  };
}
export default connect(mapStateToProps, {})(Admin);
