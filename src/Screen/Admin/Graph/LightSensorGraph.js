import React, {Component} from 'react';

import {View, StyleSheet, Text, SafeAreaView, Dimensions} from 'react-native';
import {connect} from 'react-redux';
import {w, h, totalSize} from '../../../utils/Dimensions';
import {Rect, Svg} from 'react-native-svg';
import {parseFloatUtil} from '../../../utils/Utils';

import {
  LineChart,
  AreaChart,
  Grid,
  YAxis,
  XAxis,
} from 'react-native-svg-charts';
import * as shape from 'd3-shape';
import * as scale from 'd3-scale';

class Admin extends Component {
  timeArr = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
  lightArr = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0];
  timeMax = 20;

  constructor() {
    super();

    this.state = {
      lightSensor: '',
    };
  }

  count = 0;
  getLabelData(light, timestamp) {
    this.count++;
    this.timeArr.shift();
    this.lightArr.shift();
   

    this.timeArr.push(this.count);

    const light1 = parseFloatUtil(light);
    

    this.lightArr.push(light1);
   

    if (Math.abs(light1) > this.timeMax) this.timeMax = light1;

    console.log('GRAPH Light=>' + JSON.stringify(this.lightArr));
  }

  render() {
    console.log(this.props);
    const {light, timestamp} = this.props.lightSensor;
    this.getLabelData(light, timestamp);

    const ligntInt = parseFloatUtil(light);
   

    return (
      <View style={{flexDirection: 'column', marginTop: 10}}>
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'center',
          }}>
          <View style={[styles.boxStyle, {backgroundColor: '#e74c3c'}]}>
            <Text style={styles.textstyle}>Light: {ligntInt.toFixed(2)} lux</Text>
          </View>
        </View>

        <View style={{height: 200, flexDirection: 'row'}}>
          <YAxis
            data={[0,this.timeMax]}
            svg={{
              fill: 'grey',

              fontSize: 13,
            }}
            scale={scale.scaleLinear}
            yAccessor={({item}) => item}
            formatLabel={value => value}
            contentInset={{top: 5, bottom: 5}}
          />
          <View style={{flex: 1}}>
            <LineChart
              yMin={0}
              yMax={this.timeMax}
              style={{flex: 1}}
              data={this.lightArr}
              svg={{stroke: '#e74c3c', strokeWidth: 2}}
              contentInset={{top: 5, bottom: 5}}
              curve={shape.curveStep}>
              <Grid />
            </LineChart>
      
          </View>
        </View>

        <XAxis
          contentInset={{top: 5, bottom: 5, left: 20, right: 10}}
          data={this.timeArr}
          scale={scale.scaleTime}
          xAccessor={({item}) => item}
          svg={{
            fill: 'grey',

            fontSize: 13,
          }}
          labelStyle={{color: 'black'}}
          formatLabel={value => value}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  textstyle: {
    textAlign: 'center',
    fontSize: 12,
    fontFamily: 'SanFranciscoText-Medium',
    color: 'white',
  },
  boxStyle: {padding: 5, marginLeft: 2, marginRight: 2, borderRadius: 5},
});

function mapStateToProps(state) {
  return {
    lightSensor: state.lightSensor,
  };
}
export default connect(
  mapStateToProps,
  {},
)(Admin);
