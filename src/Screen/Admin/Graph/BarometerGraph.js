import React, {Component} from 'react';

import {View, StyleSheet, Text, SafeAreaView, Dimensions} from 'react-native';
import {connect} from 'react-redux';
import {w, h, totalSize} from '../../../utils/Dimensions';
import {parseFloatUtil} from '../../../utils/Utils';

import {Rect, Svg} from 'react-native-svg';



import {
  LineChart,
  AreaChart,
  Grid,
  YAxis,
  XAxis,
} from 'react-native-svg-charts';
import * as shape from 'd3-shape';
import * as scale from 'd3-scale';

class Admin extends Component {
  timeArr = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
  pressureArr = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
  timeMax = 20;

  constructor() {
    super();

    this.state = {
      barometer: '',
    };
  }

  count = 0;
  getLabelData(pressure, timestamp) {
    this.count++;
    this.timeArr.shift();
    this.pressureArr.shift();
   

    this.timeArr.push(this.count);

    const pressure1 = parseFloatUtil(pressure);
    

    this.pressureArr.push(pressure1);
   

    if (Math.abs(pressure1) > this.timeMax) this.timeMax = pressure1;

    console.log('GRAPH pressure=>' + JSON.stringify(this.pressureArr));
  }


  render() {
    console.log(this.props);
    const {pressure, timestamp} = this.props.barometer;
    this.getLabelData(pressure, timestamp);

    const pressureInt = parseFloatUtil(pressure);
   

    return (
      <View style={{flexDirection: 'column', marginTop: 10}}>
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'center',
          }}>
          <View style={[styles.boxStyle, {backgroundColor: '#e74c3c'}]}>
            <Text style={styles.textstyle}>Pressure: {pressureInt.toFixed(2)} hPa</Text>
          </View>
        </View>

        <View style={{height: 200, flexDirection: 'row'}}>
          <YAxis
            data={[0,this.timeMax]}
            svg={{
              fill: 'grey',

              fontSize: 13,
            }}
            scale={scale.scaleLinear}
            yAccessor={({item}) => item}
            formatLabel={value => value}
            contentInset={{top: 5, bottom: 5}}
          />
          <View style={{flex: 1}}>
            <LineChart
              yMin={0}
              yMax={this.timeMax}
              style={{flex: 1}}
              data={this.pressureArr}
              svg={{stroke: '#e74c3c', strokeWidth: 2}}
              contentInset={{top: 5, bottom: 5}}
              curve={shape.curveStep}>
              <Grid />
            </LineChart>
      
          </View>
        </View>

        <XAxis
          contentInset={{top: 5, bottom: 5, left: 20, right: 10}}
          data={this.timeArr}
          scale={scale.scaleTime}
          xAccessor={({item}) => item}
          svg={{
            fill: 'grey',

            fontSize: 13,
          }}
          labelStyle={{color: 'black'}}
          formatLabel={value => value}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  textstyle: {
    textAlign: 'center',
    fontSize: 12,
    fontFamily: 'SanFranciscoText-Medium',
    color: 'white',
  },
  boxStyle: {padding: 5, marginLeft: 2, marginRight: 2, borderRadius: 5},
});

function mapStateToProps(state) {
  return {
    barometer: state.barometer,
  };
}
export default connect(
  mapStateToProps,
  {},
)(Admin);
