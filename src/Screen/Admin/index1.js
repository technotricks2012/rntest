import React, {Component} from 'react';

import {View, StyleSheet, ScrollView, SafeAreaView, Button} from 'react-native';
import {connect} from 'react-redux';
import {Icon, Header, Text} from 'react-native-elements';
import {
  Table,
  TableWrapper,
  Row,
  Rows,
  Col,
} from 'react-native-table-component';
import {getDeviceInfo} from '../../lib/DeviceInfo';

class Admin extends Component {
  _isMounted = false;

  static navigationOptions = {
    drawerLabel: 'Admin',
    // drawerIcon: ({tintColor}) => (
    //   <Icon
    //     name="admin"
    //     onPress={() => {
    //       console.log('HI');
    //     }}
    //     color="grey"
    //     size={20}
    //   />
    // ),
  };

  constructor() {
    super();

    this.state = {
      allSesnorData:'',
      accelerometer: '',
      gyroscope: '',
      magnetometer: '',
      barometer: '',
      orientation: '',
      proximity: '',
      lightSensor: '',
      pedometer: '',
      allSesnorData:'',
      location: '',
    };
    this.state = {
      deviceInfo: '',
    };
  }

  async componentDidMount() {
    const deviceInfo = await getDeviceInfo();
    console.log('DEVCIE Start=>' + JSON.stringify(deviceInfo));
    this.setState({deviceInfo: deviceInfo});
  }

  render() {
    console.log(this.props);
    const {
      x: accelerometer_x,
      y: accelerometer_y,
      z: accelerometer_z,
      timestamp: accelerometerTimestamp,
    } = this.props.allSesnorData.data.accelerometer.Data;
    const {
      x: gyroscope_x,
      y: gyroscope_y,
      z: gyroscope_z,
      timestamp: gyroscopeTimestamp,
    } = this.props.allSesnorData.data.gyroscope.Data;

    const {
      x: magnetometer_x,
      y: magnetometer_y,
      z: magnetometer_z,
      timestamp: magnetometerTimestamp,
    } = this.props.allSesnorData.data.magnetometer.Data;

    const {pressure, timestamp: barometerTimestamp} = this.props.allSesnorData.data.barometer.Data;

    const {
      roll,
      pitch,
      azimuth,
      timestamp: orientationTimestamp,
    } = this.props.allSesnorData.data.orientation.Data;

    const {
      maxRange,
      value,
      isNear,
      timestamp: proximityTimestamp,
    } = this.props.allSesnorData.data.proximity.Data;

    const {light, timestamp: lightSensorTimestamp} = this.props.allSesnorData.data.lightSensor.Data;

    const {
      latitude,
      longitude,
      altitude,
      timestamp: locationTimestamp,
    } = this.props.location;

    const {
      numberOfSteps,
      distance,
      timestamp: pedometerTimestamp,
    } = this.props.allSesnorData.data.pedometer.Data;

    // const {mode} = 'WALK';

    // const {accSensorHead:[]}

    return (
      <SafeAreaView style={{flex: 1, backgroundColor: '#f3f3f3'}}>
        <Header
          placement="left"
          leftComponent={{
            icon: 'menu',
            color: '#fff',
            onPress: () => {
              this.props.navigation.openDrawer();
            },
          }}
          centerComponent={{
            text: (
              <Text
                style={{
                  color: '#fff',
                  fontSize: 40,
                 // fontFamily: 'SanFranciscoText',
                  fontWeight: 'bold',
                }}>
                Admin
              </Text>
            ),
            style: {color: '#fff', size: 40},
          }}
        />

        {/* <Button
          title="Press me"
          onPress={() => {
            this.props.navigation.openDrawer();
          }}
        /> */}
        <ScrollView>
          <Table borderStyle={{borderWidth: 2, borderColor: '#c8e1ff'}}>
            <Row
              data={['Device Info']}
              flexArr={[4]}
              style={{height: 40, backgroundColor: '#f1f8ff'}}
              textStyle={styles.textValue}
            />
            <TableWrapper style={{flexDirection: 'row'}}>
              <Col
                data={['Name', 'Model', 'Device ID']}
                style={{flex: 1, backgroundColor: '#786757'}}
                heightArr={[28, 28]}
                textStyle={styles.textSubHead}
              />
              <Rows
                data={[
                  [this.state.deviceInfo.deviceName],
                  [this.state.deviceInfo.model],
                  [this.state.deviceInfo.deviceID],
                ]}
                flexArr={[3]}
                style={styles.row}
                textStyle={styles.textValue}
              />
            </TableWrapper>
          </Table>

          <Table borderStyle={{borderWidth: 2, borderColor: '#c8e1ff'}}>
            <Row
              data={['Accelerometer']}
              flexArr={[4]}
              style={{height: 40, backgroundColor: '#f1f8ff'}}
              textStyle={styles.textValue}
            />
            <TableWrapper style={{flexDirection: 'row'}}>
              <Col
                data={['x', 'y', 'z', 'Time']}
                style={{flex: 1, backgroundColor: '#786757'}}
                heightArr={[28, 28]}
                textStyle={styles.textSubHead}
              />
              <Rows
                data={[
                  [accelerometer_x],
                  [accelerometer_y],
                  [accelerometer_z],
                  [accelerometerTimestamp],
                ]}
                flexArr={[3]}
                style={styles.row}
                textStyle={styles.textValue}
              />
            </TableWrapper>
          </Table>

          <Table borderStyle={{borderWidth: 2, borderColor: '#c8e1ff'}}>
            <Row
              data={['Gyroscope']}
              flexArr={[4]}
              style={{height: 40, backgroundColor: '#f1f8ff'}}
              textStyle={styles.textValue}
            />
            <TableWrapper style={{flexDirection: 'row'}}>
              <Col
                data={['x', 'y', 'z', 'Time']}
                style={{flex: 1, backgroundColor: '#786757'}}
                heightArr={[28, 28]}
                textStyle={styles.textSubHead}
              />
              <Rows
                data={[
                  [gyroscope_x],
                  [gyroscope_y],
                  [gyroscope_z],
                  [gyroscopeTimestamp],
                ]}
                flexArr={[3]}
                style={styles.row}
                textStyle={styles.textValue}
              />
            </TableWrapper>
          </Table>

          <Table borderStyle={{borderWidth: 2, borderColor: '#c8e1ff'}}>
            <Row
              data={['Magnetometer']}
              flexArr={[4]}
              style={{height: 40, backgroundColor: '#f1f8ff'}}
              textStyle={styles.textValue}
            />
            <TableWrapper style={{flexDirection: 'row'}}>
              <Col
                data={['x', 'y', 'z', 'Time']}
                style={{flex: 1, backgroundColor: '#786757'}}
                heightArr={[28, 28]}
                textStyle={styles.textSubHead}
              />
              <Rows
                data={[
                  [magnetometer_x],
                  [magnetometer_y],
                  [magnetometer_z],
                  [magnetometerTimestamp],
                ]}
                flexArr={[3]}
                style={styles.row}
                textStyle={styles.textValue}
              />
            </TableWrapper>
          </Table>

          <Table borderStyle={{borderWidth: 2, borderColor: '#c8e1ff'}}>
            <Row
              data={['Barometer']}
              flexArr={[4]}
              style={{height: 40, backgroundColor: '#f1f8ff'}}
              textStyle={styles.textValue}
            />
            <TableWrapper style={{flexDirection: 'row'}}>
              <Col
                data={['Pressure', 'Time']}
                style={{flex: 1, backgroundColor: '#786757'}}
                heightArr={[28, 28]}
                textStyle={styles.textSubHead}
              />
              <Rows
                data={[[pressure], [barometerTimestamp]]}
                flexArr={[3]}
                style={styles.row}
                textStyle={styles.textValue}
              />
            </TableWrapper>
          </Table>

          <Table borderStyle={{borderWidth: 2, borderColor: '#c8e1ff'}}>
            <Row
              data={['Pedometer']}
              flexArr={[4]}
              style={{height: 40, backgroundColor: '#f1f8ff'}}
              textStyle={styles.textValue}
            />
            <TableWrapper style={{flexDirection: 'row'}}>
              <Col
                data={['Steps', 'Distance', 'Time']}
                style={{flex: 1, backgroundColor: '#786757'}}
                heightArr={[28, 28]}
                textStyle={styles.textSubHead}
              />
              <Rows
                data={[[numberOfSteps], [distance], [pedometerTimestamp]]}
                flexArr={[3]}
                style={styles.row}
                textStyle={styles.textValue}
              />
            </TableWrapper>
          </Table>

          <Table borderStyle={{borderWidth: 2, borderColor: '#c8e1ff'}}>
            <Row
              data={['Orientation']}
              flexArr={[4]}
              style={{height: 40, backgroundColor: '#f1f8ff'}}
              textStyle={styles.textValue}
            />
            <TableWrapper style={{flexDirection: 'row'}}>
              <Col
                data={['Roll', 'Pitch', 'Azimuth', 'Time']}
                style={{flex: 1, backgroundColor: '#786757'}}
                heightArr={[28, 28]}
                textStyle={styles.textSubHead}
              />
              <Rows
                data={[[roll], [pitch], [azimuth], [orientationTimestamp]]}
                flexArr={[3]}
                style={styles.row}
                textStyle={styles.textValue}
              />
            </TableWrapper>
          </Table>

          <Table borderStyle={{borderWidth: 2, borderColor: '#c8e1ff'}}>
            <Row
              data={['Proximity']}
              flexArr={[4]}
              style={{height: 40, backgroundColor: '#f1f8ff'}}
              textStyle={styles.textValue}
            />
            <TableWrapper style={{flexDirection: 'row'}}>
              <Col
                data={['MaxRange', 'Value', 'isNear', 'Time']}
                style={{flex: 1, backgroundColor: '#786757'}}
                heightArr={[28, 28]}
                textStyle={styles.textSubHead}
              />
              <Rows
                data={[[maxRange], [value], [isNear], [proximityTimestamp]]}
                flexArr={[3]}
                style={styles.row}
                textStyle={styles.textValue}
              />
            </TableWrapper>
          </Table>

          <Table borderStyle={{borderWidth: 2, borderColor: '#c8e1ff'}}>
            <Row
              data={['LightSensor']}
              flexArr={[4]}
              style={{height: 40, backgroundColor: '#f1f8ff'}}
              textStyle={styles.textValue}
            />
            <TableWrapper style={{flexDirection: 'row'}}>
              <Col
                data={['Light', 'Time']}
                style={{flex: 1, backgroundColor: '#786757'}}
                heightArr={[28, 28]}
                textStyle={styles.textSubHead}
              />
              <Rows
                data={[[light], [lightSensorTimestamp]]}
                flexArr={[3]}
                style={styles.row}
                textStyle={styles.textValue}
              />
            </TableWrapper>
          </Table>

          <Table borderStyle={{borderWidth: 2, borderColor: '#c8e1ff'}}>
            <Row
              data={['Geo Location']}
              flexArr={[4]}
              style={{height: 40, backgroundColor: '#f1f8ff'}}
              textStyle={styles.textValue}
            />
            <TableWrapper style={{flexDirection: 'row'}}>
              <Col
                data={['Latitude', 'Longitude', 'Altitude', 'Time']}
                style={{flex: 1, backgroundColor: '#786757'}}
                heightArr={[28, 28]}
                textStyle={styles.textSubHead}
              />
              <Rows
                data={[
                  [latitude],
                  [longitude],
                  [altitude],
                  [locationTimestamp],
                ]}
                flexArr={[3]}
                style={styles.row}
                textStyle={styles.textValue}
              />
            </TableWrapper>
          </Table>

          {/* <Text>*****Accelerometer*****</Text>

          <Text>X: {accelerometer_x}</Text>
          <Text>Y: {accelerometer_y}</Text>
          <Text>Z: {accelerometer_z}</Text>
          <Text>Timestamp: {accelerometerTimestamp}</Text>

          <Text>*****Gyroscope*****</Text>
          <Text>X: {gyroscope_x}</Text>
          <Text>Y: {gyroscope_y}</Text>
          <Text>Z: {gyroscope_z}</Text>
          <Text>Timestamp: {gyroscopeTimestamp}</Text>

          <Text>*****Magnetometer*****</Text>
          <Text>X : {magnetometer_x}</Text>
          <Text>Y : {magnetometer_y}</Text>
          <Text>Z : {magnetometer_z}</Text>
          <Text>Timestamp : {magnetometerTimestamp}</Text>

          <Text>*****Barometer*****</Text>
          <Text>Pressure : {pressure}</Text>
          <Text>Timestamp : {barometerTimestamp}</Text>

          <Text>*****Pedometer*****</Text>
          <Text>Steps : {numberOfSteps}</Text>
          <Text>Distance : {distance}</Text>
          <Text>Timestamp: {pedometerTimestamp}</Text>

          <Text>*****Orientation*****</Text>
          <Text>Roll : {roll}</Text>
          <Text>Pitch : {pitch}</Text>
          <Text>Azimuth : {azimuth}</Text>
          <Text>Timestamp: {orientationTimestamp}</Text>

          <Text>*****Proximity*****</Text>
          <Text>MaxRange : {maxRange}</Text>
          <Text>Value : {value}</Text>
          <Text>isNear : {isNear}</Text>
          <Text>Timestamp: {proximityTimestamp}</Text>

          <Text>*****LightSensor*****</Text>
          <Text>Light : {light}</Text>
          <Text>Timestamp: {lightSensorTimestamp}</Text>

          <Text>*****Location*****</Text>
          <Text>Latitude : {latitude}</Text>
          <Text>Longitude : {longitude}</Text>
          <Text>Altitude : {altitude}</Text>

          <Text>Timestamp: {locationTimestamp}</Text> */}
        </ScrollView>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  textValue: {textAlign: 'center', fontSize: 15},
  textHead: {textAlign: 'center', fontSize: 25},
  textSubHead: {textAlign: 'center', fontSize: 15},
  row: {height: 28},
});

function mapStateToProps(state) {
  return {
    allSesnorData:state.allSesnorData,

    accelerometer: state.accelerometer,
    gyroscope: state.gyroscope,
    magnetometer: state.magnetometer,
    barometer: state.barometer,
    orientation: state.orientation,
    proximity: state.proximity,
    lightSensor: state.lightSensor,
    pedometer: state.pedometer,
    location: state.location,
  };
}
export default connect(
  mapStateToProps,
  {},
)(Admin);
