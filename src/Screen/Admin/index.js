import React, {Component} from 'react';

import {
  View,
  StyleSheet,
  ScrollView,
  SafeAreaView,
  FlatList,
  ActivityIndicator,
} from 'react-native';
import {connect} from 'react-redux';
import {Icon, Header, Text} from 'react-native-elements';

import {CustomModeButton} from '../../components/CustomModeButton';

import {Graph} from './Graph';

import accelerometerIcon from '../../images/icons/Accelerometer.svg';
import gyroscopeIcon from '../../images/icons/Gyroscope.svg';
import magenometerIcon from '../../images/icons/Magenometer.svg';

import barometerIcon from '../../images/icons/Barometer.svg';
import locationIcon from '../../images/icons/Location.svg';
import pedometerIcon from '../../images/icons/Pedometer.svg';

import lightIcon from '../../images/icons/Light.svg';
import orientationIcon from '../../images/icons/Orientation.svg';

import {getSensorStatus} from '../../db/DbManager';

var buttonState = [
  // {name: 'Test', iconPath: accelerometerIcon, isDisabled: false},

  {name: 'Accelerometer', iconPath: accelerometerIcon, isDisabled: false},
  {name: 'Gyroscope', iconPath: gyroscopeIcon, isDisabled: false},
  {name: 'Magnetometer', iconPath: magenometerIcon, isDisabled: false},
  {name: 'Barometer', iconPath: barometerIcon, isDisabled: false},
  {name: 'Location', iconPath: locationIcon, isDisabled: false},
  {name: 'Pedometer', iconPath: pedometerIcon, isDisabled: false},
  {name: 'Orientation', iconPath: orientationIcon, isDisabled: false},
  {name: 'Lightsensor', iconPath: lightIcon, isDisabled: false},
];

class Admin extends Component {
  _isMounted = false;

  static navigationOptions = {
    drawerLabel: 'Admin',
  };

  constructor() {
    super();
    this.state = {
      isLoading: true,
    };
  }

  async buttonRenderLogic() {
    const sensorStatus = await getSensorStatus();

    buttonState = buttonState.map(function(item) {
      let isDisabledState =
        sensorStatus[
          Object.keys(sensorStatus).find(
            key => key.toLowerCase() === item.name.toLowerCase(),
          )
        ];


      item.isDisabled =   (isDisabledState == "false")?true:false ;
      return item;
    });

    console.log('VVVV=>' + JSON.stringify(buttonState));

    this.setState({
      selectedItem: buttonState[0].name,
      isLoading: false,
    });
  }

  async componentDidMount() {
    await this.buttonRenderLogic();
  }

  onSelectButton(selectedItem) {
    console.log('Selected Item:=' + selectedItem);
    this.setState({
      selectedItem: selectedItem,
    });
  }

  renderGraph() {
    return (
      <View>
        <Graph graphType={this.state.selectedItem} />
      </View>
    );
  }
  renderButtons = ({item}) => (
    <CustomModeButton
      title={item.name}
      isDisabled={item.isDisabled}
      isSelected={this.state.selectedItem == item.name}
      svgPath={item.iconPath}
      onPress={() => this.onSelectButton(item.name)}
    />
  );

  render() {
    const {isLoading} = this.state;

    return (
      <SafeAreaView style={styles.container}>
        <Header
          ViewComponent={require('react-native-linear-gradient').default}
          linearGradientProps={{
            colors: ['#1a61d8', '#1fc7bd'],
            start: {x: 0, y: 0.5},
            end: {x: 1, y: 0.5},
          }}
          containerStyle={{height: 100}}
          placement="left"
          leftComponent={{
            icon: 'menu',
            color: '#fff',
            size: 30,
            onPress: () => {
              this.props.navigation.openDrawer();
            },
          }}
          centerComponent={{
            text: (
              <Text
                style={{
                  color: '#fff',
                  fontSize: 40,
                  fontFamily: 'SanFranciscoText-Bold',
                  fontWeight: 'bold',
                }}>
                Admin
              </Text>
            ),
            style: {color: '#fff', size: 40},
          }}
        />

        <ScrollView>
          {!isLoading && (
            <View>
              {this.renderGraph()}
              <FlatList
                data={buttonState}
                extraData={this.state}
                renderItem={this.renderButtons}
                keyExtractor={item => item.name}
                numColumns={3}
              />
            </View>
          )}

          {isLoading && (
            <ActivityIndicator
              size="large"
              color="#0000ff"
            />
          )}
        </ScrollView>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  textValue: {textAlign: 'center', fontSize: 15},
  textHead: {textAlign: 'center', fontSize: 25},
  textSubHead: {textAlign: 'center', fontSize: 15},
  row: {height: 28},
  spinner: {
    alignItems: 'center',
    justifyContent: 'center',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    position: 'absolute',
  },
});

export default Admin;
