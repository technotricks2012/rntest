import React, {Component} from 'react';
import {View, Dimensions, StyleSheet} from 'react-native';
import MapView, {PROVIDER_GOOGLE, Polyline,Marker} from 'react-native-maps';
import FirebaseDB from '../../firebase/FirebaseDB';
const {width, height} = Dimensions.get('window');

class MapViewScreen extends Component {
  _isMounted = false;
  intervalID = 0;

  constructor() {
    super();
    this.state = {
      region: {
        latitude: 1.306614,
        longitude: 103.849292,
        latitudeDelta: 0.01,
        longitudeDelta: 0.01,
      },
      coordinates: [],
      polylines: [],
      editing: null,
    };
  }
  static navigationOptions = {
    title: 'Map View',
  };
  componentDidMount() {
    this._isMounted = true;
    this.updateLocation();
  }

  updateLocation() {
    FirebaseDB.getGeoLocation(true).then(snapshot => {
      const locations = [];

      snapshot.forEach(function(childSnapshot) {
        var data = childSnapshot.val();
        console.log('value', data.latitude);

        const location = {latitude: data.latitude, longitude: data.longitude};

        locations.push(location);
      });

      this.setState({
        coordinates: locations,
      });
      console.log('Mapview Data', snapshot.val());
    });
  }

  componentWillUnmount() {
    this._isMounted = false;

    clearInterval(this.intervalID);
  }

  render() {
    return (
      <View style={styles.container}>
        <MapView
          style={styles.mapcontainer}
          showsUserLocation={true}
          showsMyLocationButton={false}
          zoomEnabled={true}
          initialRegion={this.state.region}>
          <Polyline
            coordinates={this.state.coordinates}
            strokeColor="#000"
            fillColor="rgba(255,0,0,0.5)"
            strokeWidth={3}
          />

            {this.state.coordinates.map(marker => (
            <Marker
              coordinate={marker}
            />
          ))}
        </MapView>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  mapcontainer: {
    flex: 1,
    width: width,
    height: height,
  },
});

export default MapViewScreen;
