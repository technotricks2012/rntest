import React, {Component} from 'react';
import {View, Text, StyleSheet} from 'react-native';
import {Button} from 'react-native-paper';

import Geolocation from '@react-native-community/geolocation';
import FirebaseDB from '../../firebase/FirebaseDB';
import {setLocation} from '../../store/LocationStore';
import {connect} from 'react-redux';
import {PermissionsAndroid, Platform} from 'react-native';

import LocationBg from './BgTask';
import {yieldExpression} from '@babel/types';

class GeoSensor extends Component {
  _isMounted = false;
  constructor() {
    super();
    this.state = {
      ready: false,
      initWhere: {lat: null, lng: null},
      currentWhere: {lat: null, lng: null},
      error: null,
      watchID: 0,
    };
  }
  static navigationOptions = {
    title: 'Geo Location',
  };

  async requestLocationPermission() {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
        {
          title: 'Location Permission',
          message: 'This app needs access to your location',
        },
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        console.log('You can use the location');
        this.loadLocation();
      } else {
        console.log('Location permission denied');
      }
    } catch (err) {
      console.warn(err);
    }
  }

  loadLocation() {
    Geolocation.getCurrentPosition(this.geoSuccess, this.geoFailure, {
      enableHighAccuracy: true,
      timeout: 100000,
      maximumAge: 1000,
    });
    this.watchID = Geolocation.watchPosition(
      position => {
        console.log('watchPosition location updated11');

        if (this._isMounted) {
          console.log('watchPosition location updated');
          FirebaseDB.setGeoLocation(position, false);

          this.props.setLocation(
            position.coords.latitude,
            position.coords.longitude,
          );

          this.setState({
            ready: true,
            currentWhere: {
              lat: position.coords.latitude,
              lng: position.coords.longitude,
            },
          });
        }
      },
      err => {
        console.log(`watchPosition location updated${err}`);
      },
      {
        enableHighAccuracy: true,
        timeout: 20000,
        maximumAge: 1000,
      },
    );
  }
  componentDidMount() {
    this._isMounted = true;

    if (Platform.OS == 'ios') {
      this.loadLocation();
    } else if (Platform.OS == 'android') {
      this.requestLocationPermission();
    }
  }

  componentWillUnmount = () => {
    Geolocation.clearWatch(this.watchID);
    this._isMounted = false;
  };

  geoSuccess = position => {
    console.log(position.coords.latitude);

    if (this._isMounted) {
      FirebaseDB.setGeoLocation(position, false);
      this.setState({
        ready: true,
        initWhere: {
          lat: position.coords.latitude,
          lng: position.coords.longitude,
        },
      });
    }
  };
  geoFailure = err => {
    if (this._isMounted) {
      this.setState({error: err.message});
    }
  };

  render() {
    return (
      <View style={styles.container}>
        {!this.state.ready && (
          <Text style={styles.big}>Using Geolocation in React Native.</Text>
        )}
        {this.state.error && <Text style={styles.big}>{this.state.error}</Text>}
        {this.state.ready && (
          <View>
            <Text
              style={
                styles.big
              }>{`Latitude: ${this.state.initWhere.lat}`}</Text>
            <Text
              style={
                styles.big
              }>{`Longitude: ${this.state.initWhere.lng}`}</Text>
          </View>
        )}

        {this.state.ready && (
          <View>
            <Text
              style={
                styles.big
              }>{`Current Latitude: ${this.state.currentWhere.lat}`}</Text>
            <Text
              style={
                styles.big
              }>{`Current Longitude: ${this.state.currentWhere.lng}`}</Text>
          </View>
        )}

        <View>
          <Button
            style={styles.button}
            icon="camera"
            mode="contained"
            onPress={() => LocationBg.startService()}>
            Start
          </Button>

          <Button
            style={styles.button}
            icon="camera"
            mode="contained"
            onPress={() => LocationBg.stopService()}>
            Stop
          </Button>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  big: {
    fontSize: 15,
  },
});

// export default GeoSensor;

function mapStateToProps(state) {
  return {
    locationReducer: state.locationReducer,
  };
}
export default connect(
  mapStateToProps,
  {setLocation},
)(GeoSensor);
