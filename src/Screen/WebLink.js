import React, {Component} from 'react';
import {View, Text, StyleSheet} from 'react-native';
import WebView from 'react-native-webview';


// const
var html = `
<!DOCTYPE html>
<html>
<body>

<p>Click the "Try it" button to create a BUTTON element with a "Click me" text.</p>

<button onclick="myFunction()">Click Me</button>

<script>
function myFunction() {
  
  document.addEventListener("click", function(){
    document.getElementById("demo").innerHTML = "Hello World!";
  });
  
  window.postMessage("Sending data from WebView");
  alert('hi')

}
</script>

</body>
</html>

`;

class WebLink extends Component {
  _isMounted = false;
  constructor() {
    super();
    this.state = {
      ready: false,
      data: {x: null, y: null, z: null, timestamp: null},
    };
  }
  static navigationOptions = {
    title: 'WebLink',
  };


   
  onMessage(data) {
    //Prints out data that was passed.
    console.log(data);
  }


  render() {

    const HTMLTemplate = html;

    return (


      <WebView
      source={{html: HTMLTemplate}}
      ref="webview"
      style={{marginTop: 0}}
        onMessage={this.onMessage}
      />
    );

    
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  big: {
    fontSize: 15,
  },
});

export default WebLink;
