import React, {Component} from 'react';

import {
  View,
  StyleSheet,
  ScrollView,
  SafeAreaView,
  ActivityIndicator,
} from 'react-native';
import {connect} from 'react-redux';

import {Icon, Header, Text, Button} from 'react-native-elements';

import JSONTree from 'react-native-json-tree';

import {getDBLog} from '../../db/DbManager';
import FirebaseDB from '../../firebase/FirebaseDB';

class LogInfo extends Component {
  dbLog = null;

  constructor() {
    super();

    this.state = {
      isDataAvailable: false,
      sensorData: '',
      isLoading: false,
      dbReportID: '',
      useractivity: '',
    };
  }

  async sync() {
    console.log('Sync Start ===>');

    this.setState({sensorData: '', isDataAvailable: false, isLoading: true});

    const data = await getDBLog();

    console.log('Sync Start Data Load ===>', JSON.stringify(data));

    this.setState({
      isDataAvailable: true,
      dbReportID: '',
      sensorData: data,
      isLoading: false,
    });
  }

  async ShareDBReport() {
    this.setState({isLoading: true});

    const refID = await FirebaseDB.setData('DBReport', this.state.sensorData);

    console.log('Report ID ===>', refID);

    this.setState({dbReportID: refID, isLoading: false});
  }

  render() {
    // const data1 = JSON.stringify(this.props.useractivity);

    const {
      isWalking,
      isAutomotive,
      isStationary,
      isCycling,
      isAvailable,
      isRunning,
      unknown,
      confidence,
      DetectedType
    } = this.props.useractivity;

    console.log('Report ID ===>', isWalking);

    // const data = JSON.stringify(data1);

    // const data = JSON.parse(`${data}`);

    return (
      <SafeAreaView style={styles.screenContainer}>
        <Header
          ViewComponent={require('react-native-linear-gradient').default}
          linearGradientProps={{
            colors: ['#1a61d8', '#1fc7bd'],
            start: {x: 0, y: 0.5},
            end: {x: 1, y: 0.5},
          }}
          containerStyle={{height: 100}}
          placement="left"
          leftComponent={{
            icon: 'menu',
            color: '#fff',
            size: 30,
            onPress: () => {
              this.props.navigation.openDrawer();
            },
          }}
          centerComponent={{
            text: (
              <Text
                style={{
                  color: '#fff',
                  fontSize: 40,
                  fontFamily: 'SanFranciscoText-Bold',
                  fontWeight: 'bold',
                }}>
                Dev Log
              </Text>
            ),
            style: {color: '#fff', size: 40},
          }}
        />
        <View style={{flexDirection: 'column'}}>
          <Text> Version:- 1.4.2</Text>
          <Button title="Sync" onPress={() => this.sync()}></Button>

          {this.state.isDataAvailable && (
            <Button
              title="Send DB Report"
              onPress={() => this.ShareDBReport()}></Button>
          )}
        </View>
        <ScrollView>
        
          {this.state.dbReportID != '' && (
            <Text> DB Report ID:-{this.state.dbReportID}</Text>
          )}

      <View style={{flex: 1, flexDirection: 'column'}}>
        
          <Text style={{width: '100%', height: 50, backgroundColor: isAvailable ? 'green' : 'red'}} > isAvailable => {confidence}</Text>
        <Text style={{width: '100%', height: 50, backgroundColor: isStationary ? 'green' : 'red'}} > isStationary </Text>
        <Text style={{width: '100%', height: 50, backgroundColor: isWalking ? 'green' : 'red'}} > Walking </Text>
        <Text style={{width: '100%', height: 50, backgroundColor: isAutomotive ? 'green' : 'red'}} > isAutomotive </Text>
        <Text style={{width: '100%', height: 50, backgroundColor: isCycling ? 'green' : 'red'}} > isCycling </Text>
        <Text style={{width: '100%', height: 50, backgroundColor: isRunning ? 'green' : 'red'}} > isRunning </Text>
        <Text style={{width: '100%', height: 50, backgroundColor: unknown ? 'green' : 'red'}} > unknown </Text>
        <Text style={{width: '100%', height: 50, backgroundColor: DetectedType ? 'green' : 'red'}} > {DetectedType} </Text>

      </View>

          {this.state.isDataAvailable ? (
            <JSONTree data={this.state.sensorData} />
          ) : (
            <View>
              <Text> ...</Text>
            </View>
          )}

          {this.state.isLoading && (
            <ActivityIndicator
              style={styles.spinner}
              size="large"
              color="#0000ff"
            />
          )}
        </ScrollView>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  screenContainer: {
    flex: 1,
    backgroundColor: 'white',
  },
  textValue: {textAlign: 'center', fontSize: 15},
  textHead: {textAlign: 'center', fontSize: 25},
  textSubHead: {textAlign: 'center', fontSize: 15},
  row: {height: 28},
  spinner: {
    alignItems: 'center',
    justifyContent: 'center',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    position: 'absolute',
  },
  separator: {
    marginVertical: 8,
    borderBottomColor: '#737373',
    borderBottomWidth: StyleSheet.hairlineWidth,
  },
});

function mapStateToProps(state) {
  return {
    useractivity: state.useractivity,
  };
}
export default connect(mapStateToProps, {})(LogInfo);
