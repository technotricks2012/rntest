import React, {Component} from 'react';
import {
  StyleSheet,
  SafeAreaView,
  ScrollView,
  Alert,
  FlatList,
  View,
} from 'react-native';
import {getLocal, setLocal} from '../../db/DbManager';
import {getRandomUniqueId} from '../../utils/Utils';
import {w, h, totalSize} from '../../utils/Dimensions';

import Pulse from 'react-native-pulse';

import moment from 'moment';
import {
  TRAVEL_MODE,
  JOURNEY_ID,
  TRAVEL_MODE_TIME,
  USER_NAME,
  SERVICE_START_TIME,
} from '../../constants/IKeyConstants';
import {SvgXml} from 'react-native-svg';

import {Icon, CheckBox, Text, Header} from 'react-native-elements';

import {CustomModeButton} from '../../components/CustomModeButton';

import bikeIcon from '../../images/icons/bike.svg';
import busIcon from '../../images/icons/bus.svg';
import carIcon from '../../images/icons/car.svg';

import lrtIcon from '../../images/icons/lrt.svg';
import idleIcon from '../../images/icons/people.svg';
import runIcon from '../../images/icons/run.svg';

import walkIcon from '../../images/icons/walk.svg';
import pmdIcon from '../../images/icons/pmd.svg';
import mrtIcon from '../../images/icons/mrt.svg';
import unknownIcon from '../../images/icons/unknown.svg';

const defaultMode = 'UNKNOWN';

const modes = [
  {title: 'UNKNOWN', iconPath: unknownIcon, state: 'idle'},
  {title: 'Idle', iconPath: idleIcon, state: 'idle'},
  {title: 'Walking', iconPath: walkIcon, state: 'idle'},
  {title: 'Running', iconPath: runIcon, state: 'idle'},
  {title: 'MRT', iconPath: mrtIcon, state: 'idle'},
  {title: 'LRT', iconPath: lrtIcon, state: 'idle'},
  {title: 'PMD', iconPath: pmdIcon, state: 'idle'},
  {title: 'Bus', iconPath: busIcon, state: 'idle'},
  {title: 'Cycle', iconPath: bikeIcon, state: 'idle'},
  {title: 'Car', iconPath: carIcon, state: 'idle'},
];

var selectedIcon = modes[0].iconPath;
class Mode extends Component {
  _isMounted = false;

  timer = '';
  static navigationOptions = {
    drawerLabel: 'Travel Mode',
    // drawerIcon: ({tintColor}) => (
    //   <Icon
    //     name="mode"
    //     onPress={() => {
    //       console.log('HI');
    //     }}
    //     color="grey"
    //     size={20}
    //   />
    // ),
  };

  constructor() {
    super();

    this.state = {
      selectedMode: modes[0].title,
      username: '',
    };

    this.state = {
      startTime: '',
      counterTime: '',
    };
  }

  async componentDidMount() {
    const mode = await getLocal(TRAVEL_MODE);

    let modeValue = mode ? mode : defaultMode;

    let selectedMode = modes.filter(item => item.title == modeValue)[0];
    this.onSelectMode(selectedMode, false);

    const user = await getLocal(USER_NAME);
    this.setState({username: user});

    const modeStartTime = await getLocal(TRAVEL_MODE_TIME);
    const startTime = modeStartTime
      ? modeStartTime
      : await getLocal(SERVICE_START_TIME);

    this.setState({startTime: startTime});
    this.startClockTimer();
  }

  startClockTimer() {
    this.timer = setInterval(() => {
      this.setState({counterTime: moment(this.state.startTime).fromNow()});

      console.log('NOW=:' + moment(this.state.startTime).fromNow());
    }, 1000);
  }

  componentWillUnmount() {
    clearInterval(this.timer);
  }

  onAlert(mode) {
    Alert.alert(
      'Travel Mode Alert',
      'Do you want to change the travel mode?',
      [
        {text: 'Yes', onPress: () => this.onSelectMode(mode, true)},
        {
          text: 'Cancel',
          onPress: () => console.log('Cancel Pressed'),
          style: 'cancel',
        },
      ],
      {cancelable: false},
    );
  }
  onSelectMode(mode, isjourneyID) {
    // console.log('MODE:=' + mode);
    this.setState({
      selectedMode: mode.title,
      JOURNEY_ID: '',
    });

    selectedIcon = mode.iconPath;
    if (isjourneyID) {
      const uniqueId = getRandomUniqueId();
      setLocal(JOURNEY_ID, uniqueId);

      let isoTime = new Date().toISOString();
      setLocal(TRAVEL_MODE_TIME, isoTime);
      this.setState({startTime: isoTime});
    }

    setLocal(TRAVEL_MODE, mode.title);
  }

  renderModes = ({item}) => (
    <CustomModeButton
      title={item.title}
      isSelected={this.state.selectedMode == item.title}
      svgPath={item.iconPath}
      onPress={() => this.onAlert(item)}
    />
  );

  render() {
    return (
      <SafeAreaView style={styles.container}>
        <Header
          ViewComponent={require('react-native-linear-gradient').default}
          linearGradientProps={{
            colors: ['#1a61d8', '#1fc7bd'],
            start: {x: 0, y: 0.5},
            end: {x: 1, y: 0.5},
          }}
          containerStyle={{height: 100}}
          placement="left"
          leftComponent={{
            icon: 'menu',
            color: '#fff',
            size: 30,
            onPress: () => {
              this.props.navigation.openDrawer();
            },
          }}
          centerComponent={{
            text: (
              <Text
                style={{
                  color: '#fff',
                  fontSize: 40,
                  fontFamily: 'SanFranciscoText-Bold',
                  fontWeight: 'bold',
                }}>
                Travel Mode
              </Text>
            ),
            style: {color: '#fff', size: 40},
          }}
        />

        <ScrollView>
          <Text style={styles.usernametext}>Hello {this.state.username}</Text>
          <Text style={styles.hinttext}>What are you doing now?</Text>

          <View
            style={{
              marginTop: 20,
              marginLeft: w(100) / 4,
              width: w(100) / 2,
              height: w(100) / 3,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <View>
              <Pulse
                color="#1a61d8"
                numPulses={3}
                diameter={150}
                speed={20}
                duration={1000}
              />
              <SvgXml
                xml={selectedIcon}
                width="50"
                height="50"
                fill={'white'}
              />
            </View>
            <Text style={[styles.hinttext, {marginTop: 20}]}>
              {this.state.counterTime}
            </Text>
          </View>

          <FlatList
            data={modes}
            extraData={this.state}
            renderItem={this.renderModes}
            keyExtractor={item => item.title}
            numColumns={3}
          />
        </ScrollView>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },

  scroll: {
    marginTop: 20,
  },
  usernametext: {
    color: '#393939',
    fontFamily: 'OpenSans-Semibold',
    fontSize: 24,
    textAlign: 'center',
    marginTop: 20,
  },
  hinttext: {
    color: '#393939',
    fontFamily: 'OpenSans-Semibold',
    fontSize: 14,
    textAlign: 'center',
    marginTop: 10,
  },
  slectModeText: {
    color: 'white',
    fontFamily: 'OpenSans-Semibold',
    fontSize: 16,
    textAlign: 'center',
    marginTop: 10,
  },
});

export default Mode;
