import forVertical from './forVertical'

export default () => ({
  transitionSpec: {
    duration: 0,
    useNativeDriver: true,
  },

  screenInterpolator: forVertical
})
