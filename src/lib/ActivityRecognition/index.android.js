const { DeviceEventEmitter, NativeModules ,AppRegistry} = require('react-native')
const { ActivityRecognition: ActivityRecognitionNative } = NativeModules


const ActivityRecognition = {
 

  start() {
    return new Promise((resolve, reject) => {
      console.log("Detected activity 0: ")
      NativeModules.ActivityDetection.startTracking();
    });
  },

}

function logAndReject(reject, errorMsg) {
  // Don't log this as an error, because the client should handle it using `catch`.
  console.log(`[ActivityRecognition] Error: ${errorMsg}`)
  reject(errorMsg)
}

module.exports = ActivityRecognition