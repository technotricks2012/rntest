import {Observable} from 'rxjs';
import {publish, refCount} from 'rxjs/operators';

import {NativeModules, DeviceEventEmitter} from 'react-native';

import {SensorManager} from 'NativeModules';

const listenerKeys = new Map([
  ['StepCounter', 'StepCounter'],
  ['Thermometer', 'Thermometer'],
  ['LightSensor', 'LightSensor'],
  ['Proximity', 'Proximity'],
  ['Orientation', 'Orientation'],
]);

function createSensorObservable(sensorType) {
  return Observable.create(function subscribe(observer) {
    this.isSensorAvailable = false;

    this.unsubscribeCallback = () => {
      if (!this.isSensorAvailable) return;

      stopSensor(sensorType)
    };

    DeviceEventEmitter.addListener(listenerKeys.get(sensorType), data => {
      this.isSensorAvailable = true;
      observer.next(data);
    });
    startSensor(sensorType)

    return this.unsubscribeCallback;
  }).pipe(makeSingleton());
}

// As we only have one sensor we need to share it between the different consumers
function makeSingleton() {
  return source =>
    source.pipe(
      publish(),
      refCount(),
    );
}

function startSensor(type) {
  switch (type) {
    case 'StepCounter':
      SensorManager.startStepCounter(1000);
      break;
    case 'Thermometer':
      SensorManager.startThermometer(1000);
      break;

    case 'LightSensor':
      SensorManager.startLightSensor(1000);
      break;
    case 'Proximity':
      SensorManager.startProximity(1000);
      break;
    case 'Orientation':
      SensorManager.startOrientation(1000);
      break;
  }
}

function stopSensor(type) {
  switch (type) {
    case 'StepCounter':
      SensorManager.stopStepCounter();
      break;
    case 'Thermometer':
      SensorManager.stopThermometer();
      break;

    case 'LightSensor':
      SensorManager.stopLightSensor();
      break;
    case 'Proximity':
      SensorManager.stopProximity();
      break;
    case 'Orientation':
      SensorManager.stopOrientation();
      break;
  }
}

const StepCounter = createSensorObservable('StepCounter');
const Thermometer = createSensorObservable('Thermometer');
const LightSensor = createSensorObservable('LightSensor');
const Proximity = createSensorObservable('Proximity');
const Orientation = createSensorObservable('Orientation');

export default {
  StepCounter,
  Thermometer,
  LightSensor,
  Proximity,
  Orientation,
};
