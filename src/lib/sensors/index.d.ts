// declare module 'sensors' {
//   import {Observable} from 'rxjs';

//   type Sensors = {
//     stepCounter: 'StepCounter';
//     thermometer: 'Thermometer';
//     lightSensor: 'LightSensor';
//     proximity: 'Proximity';
//     orientation: 'Orientation';
//   };

//   export const SensorTypes: Sensors;

//   interface OrientationData {
//     azimuth: number;
//     pitch: number;
//     roll: number;
//     timestamp: string;
//   }

//   interface ProximityData {
//     isNear: boolean;
//     value: number;
//     maxRange: number;
//   }
//   interface ThermometerData {
//     temp: number;
//   }

//   interface LightSensorData {
//     light: number;
//   }

//   interface StepCounterData {
//     steps: number;
//   }
//   type SensorsBase = {
//     stepCounter: Observable<StepCounterData>;
//     thermometer: Observable<ThermometerData>;
//     lightSensor: Observable<LightSensorData>;
//     proximity: Observable<ProximityData>;
//     orientation: Observable<OrientationData>;
//   };

//   export const {
//     stepCounter,
//     thermometer,
//     lightSensor,
//     proximity,
//     orientation,
//   }: SensorsBase;

//   const sensors: SensorsBase;

//   export default sensors;
// }
