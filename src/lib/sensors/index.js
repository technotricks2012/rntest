import sensors from "./sensors";
// export { setUpdateInterval as setUpdateIntervalForType } from "./src/rnsensors";

export const SensorTypes = {
    StepCounter: 'StepCounter',
    Thermometer: 'Thermometer',
    LightSensor: 'LightSensor',
    Proximity: 'Proximity',
    Orientation: 'Orientation'
};

export const { StepCounter, Thermometer, LightSensor, Proximity,Orientation } = sensors;
export default sensors;



