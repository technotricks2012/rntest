import DeviceInfo from 'react-native-device-info';
import { resolve } from 'uri-js';



export async function getUniqueId() {
 return  DeviceInfo.getUniqueId()
}

export async function getDeviceInfo() {
     return await Promise.all([
      DeviceInfo.getModel(),
      DeviceInfo.getPowerState(),
      DeviceInfo.getSystemVersion(),
      DeviceInfo.getHardware(),
      DeviceInfo.getUniqueId(),
      DeviceInfo.getUserAgent(),
      DeviceInfo.isEmulator(),
      DeviceInfo.getDeviceName(),
    ]).then(([modelValue, 
        powerDateValue, 
        osVersionValue, 
        hardwareValue,
        deviceIDValue,
        userAgentValue,
        isEmulatorValue,
        deviceNameValue,
    ]) => {
      var deviceInfo = {
        model: modelValue,
        powerState: powerDateValue,
        osVersion: osVersionValue,
        hardware: hardwareValue,
        deviceID:deviceIDValue,
        userAgent:userAgentValue,
        isEmulator:isEmulatorValue,
        deviceName:deviceNameValue
      };

    //   console.log("DeviceInfo =>"+JSON.stringify(deviceInfo))
  
    //    callback(deviceInfo);

    return deviceInfo
    });
  }