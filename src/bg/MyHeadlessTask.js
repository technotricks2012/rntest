import {FirebaseDB} from '../firebase/FirebaseDB';
import BackgroundFetch from 'react-native-background-fetch';

export default MyHeadlessTask = async event => {
  console.log('[BackgroundFetch HeadlessTask] start');

  const result = await InsertLog(true);

  FirebaseDB.setData('BGFETCH', true);

  BackgroundFetch.finish();
};
