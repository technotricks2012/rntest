import {
  AppRegistry,
  NativeModules,
  NativeEventEmitter,
  Platform,
} from 'react-native';
import {addSensorDataToDB, addLocationDataToDB,setLocal,addPredictedModeToDB} from '../db/DbManager';
import {
  getSensorData,
  getLocation,
  getStepsManager,
} from '../utils/SensorManager';


import  ActivityRecognition from '../lib/ActivityRecognition'


import {getDeviceInfo, getUniqueId} from '../lib/DeviceInfo';
import BackgroundFetch from 'react-native-background-fetch';
import NetInfo from "@react-native-community/netinfo";

import {getLocal} from '../../src/db/DbManager';
import {parseiOSData} from '../../src/utils/Utils';
import FirebaseDB from '../../src/firebase/FirebaseDB';
const timeout = ms => new Promise(res => setTimeout(res, ms));

import {
  addSensorDataToStore,
  addLocationDataToStore,
  addSensorDataToStoreV2,
  addTestData,
} from '../store/storeManager';
import {addLocationToStore} from '../store/LocationStore';

import {uploadAllData} from '../utils/UploadSensorData';
const QuickServiceIntervaliOSEvents = new NativeEventEmitter(
  NativeModules.QuickServiceInterval,
);

const RNMobilityKitiOSEvents = new NativeEventEmitter(
  NativeModules.RNMobilityKit,
);


// const RNMobilityKitAndroidEvents = new NativeEventEmitter(
//   NativeModules.ActivityDetection,
// );

import {TimeInterval} from '../constants/ITimeConstant';
import {from} from 'rxjs';
import {USER_NAME, IS_NETWORK} from '../constants/IKeyConstants';
import Location from '../db/models/Location';
import { addUserActitvityToStore } from '../store/ActivityDetectionStore';

const GeoLocationTask = async data => {
  console.log(`Receiving GeoLocation Data! data` + data);
  //FirebaseDB.setData(data);
  FirebaseDB.setGeoLocation(data, true);
};

var timeCount = 0;
var initNumberOfSteps = -1;
var locationCountDown = 0;
var stepChange = true;
var apiLock = true;
const QuickServiceInterval = async data => {
  console.log(`--QuickServiceInterval ID  ` + data);

  if (data.Event == 'Start') {
    timeCount = timeCount + TimeInterval;

    //Every time interval of Sec
    getSensorData(sensorData => {
      console.log(`--getSensorData ` + JSON.stringify(sensorData));

      addSensorDataToDB(sensorData);
      addSensorDataToStore(sensorData);

      // SetpChange logic
      if (sensorData.pedometer.SuccessCode == 200) {
        if (sensorData.pedometer.Data.numberOfSteps == initNumberOfSteps) {
          // stepChange = false;
        } else {
          locationCountDown = 0;
          stepChange = true;
          initNumberOfSteps = sensorData.pedometer.Data.numberOfSteps;
        }
      }
    });

    // if (stepChange) {
    //   // stepChange = false;
    //   locationCountDown = locationCountDown + TimeInterval;
    //   getLocation(locationData => {
    //     console.log(`--getLocation ` + JSON.stringify(locationData));
    //     addLocationDataToDB(locationData);
    //     addLocationDataToStore(locationData);
    //   });
    // }
    if (timeCount % (1 * 45) == 0) {
    getLocation(locationData => {
      console.log(`--getLocation ` + JSON.stringify(locationData));
      addLocationDataToDB(locationData);
      addLocationDataToStore(locationData);
    });
  }

    //loactioan flag turn off after 5 min Steps Trigger the location
    if (locationCountDown % (3 * 60) == 0) {
      stepChange = false;
    }

    // Every 1 Sec
    if (timeCount % (5 * 60) == 0) {
      stepChange = true;
      locationCountDown = 0;
    }

    //Only First Time
    if (timeCount <= 1) {
      (async () => {
        bgRun();
      })();
    }

    //Every 1 Min
    if (timeCount % (1 * 60) == 0) {
      if (apiLock) {
        
        apiLock = false;
        (async () => {
          await uploadAllData('1 Min');
          apiLock = true;
        })();
      }
    }
  } else {
    timeCount = 0;
  }
};



const RNMobilityKitServerCall = async data => {

  console.log(`--RNMobilityKitServerCall  `);

  (async () => {
    await uploadAllData('1 Min');
  })();
};

const RNMobilityKitLocation = async data => {

  var locationData = parseiOSData(data)

  console.log("--RNMobilityKitLocation ID  "+JSON.stringify(locationData));
    addLocationDataToDB(locationData);
    addLocationDataToStore(locationData);

};

const RNMobilityKitAccelerometer = async data => {

  var accelerometerData =  {accelerometer:parseiOSData(data)} 

  addSensorDataToDB(accelerometerData);
  addSensorDataToStore(accelerometerData);
  console.log(`--RNMobilityKitAccelerometer  `+JSON.stringify(JSON.parse(accelerometerData)));
};

const RNMobilityKitGyroscope = async data => {

  var gyroscopeData = {gyroscope:parseiOSData(data)} 
 
    addSensorDataToDB(gyroscopeData);
    addSensorDataToStore(gyroscopeData);
  console.log(`--RNMobilityKitGyroscope  `+JSON.stringify(gyroscopeData));
};

const RNMobilityKitMagnetoMeter = async data => {
  var magnetometerData =  {magnetometer:parseiOSData(data)} 
  addSensorDataToDB(magnetometerData);
  addSensorDataToStore(magnetometerData);
  console.log(`--RNMobilityKitMagnetoMeter  `+JSON.stringify(magnetometerData));
};

const RNMobilityKitBarometer = async data => {
  let json = parseiOSData(data)
  var barometerData =  {barometer:json,
    timestamp:json.Data.timestamp} 
  addSensorDataToDB(barometerData);
  addSensorDataToStore(barometerData);
  console.log(`--RNMobilityKitBarometer  `+JSON.stringify(JSON.parse(barometerData)));
};


const RNMobilityKitPedometer = async data => {

  let json = parseiOSData(data)
  var pedometerData =  {pedometer:data,
    timestamp:json.Data.timestamp} 

    addSensorDataToDB(pedometerData);
  addSensorDataToStore(pedometerData);
  console.log(`--RNMobilityKitPedometer  `+JSON.stringify(JSON.parse(pedometerData)));
};


const RNMobilityKitUserActivity = async data => {

  let json = parseiOSData(data)
  var userActivityData =  {useractivity:json} 

  
  addSensorDataToStore(userActivityData)

  addPredictedModeToDB(userActivityData)
   
  console.log(`--RNMobilityKitUserActivity  `+JSON.stringify(userActivityData));
};

const RNMobilityKitDeviceMotion = async data => {

  console.log(`--RNMobilityKitDeviceMotion  `+JSON.stringify(JSON.parse(data)));
};


const ActivityDetectionAndroidData = async data => {
  console.log("VALUE====>"+JSON.stringify(data))
  

  var userActivityData =  {useractivity:{
    SuccessCode: 200,
    Data:{
      DetectedType: data.DetectedType,
      confidence: data.Confidence,
      timestamp: ""
    }
  }} 

  console.log("VALUE====>"+JSON.stringify(userActivityData))

  addSensorDataToStore(userActivityData)

  addPredictedModeToDB(userActivityData)

  

}


const AllSensorObserver = async data => {
  // console.log("VALUE====>"+JSON.stringify(data))

  addSensorDataToStoreV2(data)
  
}

if (Platform.OS === 'ios') {
  AppRegistry.registerHeadlessTask('LocationBg', () => GeoLocationTask);

  // QuickServiceIntervaliOSEvents.addListener(
  //   'QuickServiceInterval',
  //   QuickServiceInterval,
  // );


  RNMobilityKitiOSEvents.addListener(
    'RNMobilityKitServerCall',
    RNMobilityKitServerCall,
  );

  RNMobilityKitiOSEvents.addListener(
    'RNMobilityKitLocation',
    RNMobilityKitLocation,
  );


  RNMobilityKitiOSEvents.addListener(
    'RNMobilityKitGyroscope',
    RNMobilityKitGyroscope,
  );


  RNMobilityKitiOSEvents.addListener(
    'RNMobilityKitAccelerometer',
    RNMobilityKitAccelerometer,
  );


  RNMobilityKitiOSEvents.addListener(
    'RNMobilityKitMagnetoMeter',
    RNMobilityKitMagnetoMeter,
  );


  RNMobilityKitiOSEvents.addListener(
    'RNMobilityKitDeviceMotion',
    RNMobilityKitDeviceMotion,
  );


  RNMobilityKitiOSEvents.addListener(
    'RNMobilityKitBarometer',
    RNMobilityKitBarometer,
  );


  RNMobilityKitiOSEvents.addListener(
    'RNMobilityKitPedometer',
    RNMobilityKitPedometer,
  );

  RNMobilityKitiOSEvents.addListener(
    'RNMobilityKitUserActivity',
    RNMobilityKitUserActivity,
  );

  

} else if (Platform.OS === 'android') {
  AppRegistry.registerHeadlessTask(
    'QuickServiceInterval',
    () => QuickServiceInterval,
  );

  AppRegistry.registerHeadlessTask(
    'ActivityDetection',
    () => ActivityDetectionAndroidData,
  );

  AppRegistry.registerHeadlessTask(
    'AllSensorObserver',
    () => AllSensorObserver,
  );

}

function bgRun() {
  console.log('RNBackgroundFetch  to init start');

  BackgroundFetch.configure(
    {
      minimumFetchInterval: 15, // <-- minutes (15 is minimum allowed)
      // Android options
      stopOnTerminate: false,
      enableHeadless: true,
      startOnBoot: true,
      forceReload: true,
      requiredNetworkType: BackgroundFetch.NETWORK_TYPE_ANY, // Default
      requiresCharging: false, // Default
      requiresDeviceIdle: false, // Default
      requiresBatteryNotLow: false, // Default
      requiresStorageNotLow: false, // Default
    },
    () => {
      console.log('RNBackgroundFetch  to start');

      // (async () => {
      //   await uploadAllData();

      // });

      BackgroundFetch.finish(BackgroundFetch.FETCH_RESULT_NEW_DATA);
    },
    error => {
      console.log('RNBackgroundFetch failed to start');
    },
  );

  // Optional: Query the authorization status.
  BackgroundFetch.status(status => {
    switch (status) {
      case BackgroundFetch.STATUS_RESTRICTED:
        console.log('RNBackgroundFetch restricted');
        break;
      case BackgroundFetch.STATUS_DENIED:
        console.log('RNBackgroundFetch denied');
        break;
      case BackgroundFetch.STATUS_AVAILABLE:
        console.log('RNBackgroundFetch is enabled');
        break;
    }
  });
}

NetInfo.addEventListener(state => {
   setLocal(IS_NETWORK,state.isConnected.toString())
});




export default BackgroundProcess = async event => {};
