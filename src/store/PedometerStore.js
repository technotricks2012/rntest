// import store from '../store';

export const Pedometer = 'Pedometer';

const initState = {
  numberOfSteps: '--',
  distance: '--',
  timestamp: '--',
  statusCode: '401',
};

export default (state = initState, action) => {
  switch (action.type) {
    case Pedometer:
      return {
        ...state,
        numberOfSteps: action.payload.numberOfSteps,
        distance: action.payload.distance,
        timestamp: action.payload.timestamp,
        statusCode: action.payload.statusCode,
      };
    default:
      return state;
  }
};

export const addPedometerToStore = (sensorData,timestamp) => {
  store.dispatch({
    type: Pedometer,
    payload: {
      numberOfSteps: sensorData.Data.numberOfSteps,
      distance: sensorData.Data.distance,
      timestamp: timestamp,
      statusCode: sensorData.SuccessCode,
    },
  });
};
