import {addAccelerometerToStore, Accelerometer} from './AccelerometerStore';
import {addGyroscopeToStore, GyroscopeStore} from './GyroscopeStore';
import {addBarometerToStore} from './BarometerStore';

import {addLightSensorToStore} from './LightSensorStore';
import {addMagnetometerToStore} from './MagnetometerStore';
import {addOrientationToStore} from './OrientationStore';
import {addProximityToStore} from './ProximityStore';
import {addPedometerToStore} from './PedometerStore';

import {addLocationToStore} from './LocationStore';
import {addAllSensorToStore} from './AllSensorStore';
import {addTestDataToStore} from './TestDataStore';
import {addUserActitvityToStore} from './ActivityDetectionStore';

export async function addTestData(data) {
  await addTestDataToStore(data);
}
export async function addSensorDataToStore(sensorData) {
  // await addAllSensorToStore(sensorData);

  if (
    typeof sensorData.accelerometer !== 'undefined' &&
    sensorData.accelerometer.SuccessCode == 200
  )
    await addAccelerometerToStore(sensorData.accelerometer);

  if (
    typeof sensorData.gyroscope !== 'undefined' &&
    sensorData.gyroscope.SuccessCode == 200
  )
    await addGyroscopeToStore(sensorData.gyroscope);

  if (
    typeof sensorData.magnetometer !== 'undefined' &&
    sensorData.magnetometer.SuccessCode == 200
  )
    await addMagnetometerToStore(sensorData.magnetometer);

  if (
    typeof sensorData.barometer !== 'undefined' &&
    sensorData.barometer.SuccessCode == 200
  )
    await addBarometerToStore(sensorData.barometer, sensorData.timestamp);

  if (
    typeof sensorData.lightSensor !== 'undefined' &&
    sensorData.lightSensor.SuccessCode == 200
  )
    await addLightSensorToStore(sensorData.lightSensor, sensorData.timestamp);

  if (
    typeof sensorData.orientation !== 'undefined' &&
    sensorData.orientation.SuccessCode == 200
  )
    await addOrientationToStore(sensorData.orientation, sensorData.timestamp);

  if (
    typeof sensorData.proximity !== 'undefined' &&
    sensorData.proximity.SuccessCode == 200
  )
    await addProximityToStore(sensorData.proximity, sensorData.timestamp);

  if (
    typeof sensorData.pedometer !== 'undefined' &&
    sensorData.pedometer.SuccessCode == 200
  )
    await addPedometerToStore(sensorData.pedometer, sensorData.timestamp);

  if (
    typeof sensorData.useractivity !== 'undefined' &&
    sensorData.useractivity.SuccessCode == 200
  )
    await addUserActitvityToStore(sensorData.useractivity);
}



export async function addSensorDataToStoreV2(sensorData) {
  

  if (
    typeof sensorData.accelerometer !== 'undefined' &&
    sensorData.accelerometer.hasResult
  )
    await addAccelerometerToStore(sensorData.accelerometer.result);

    if (
      typeof sensorData.magnetometer !== 'undefined' &&
      sensorData.magnetometer.hasResult
    )
      await addMagnetometerToStore(sensorData.magnetometer.result);

      
    /*
  if (
    typeof sensorData.gyroscope !== 'undefined' &&
    sensorData.gyroscope.SuccessCode == 200
  )
    await addGyroscopeToStore(sensorData.gyroscope);

  if (
    typeof sensorData.magnetometer !== 'undefined' &&
    sensorData.magnetometer.SuccessCode == 200
  )
    await addMagnetometerToStore(sensorData.magnetometer);

  if (
    typeof sensorData.barometer !== 'undefined' &&
    sensorData.barometer.SuccessCode == 200
  )
    await addBarometerToStore(sensorData.barometer, sensorData.timestamp);

  if (
    typeof sensorData.lightSensor !== 'undefined' &&
    sensorData.lightSensor.SuccessCode == 200
  )
    await addLightSensorToStore(sensorData.lightSensor, sensorData.timestamp);

  if (
    typeof sensorData.orientation !== 'undefined' &&
    sensorData.orientation.SuccessCode == 200
  )
    await addOrientationToStore(sensorData.orientation, sensorData.timestamp);

  if (
    typeof sensorData.proximity !== 'undefined' &&
    sensorData.proximity.SuccessCode == 200
  )
    await addProximityToStore(sensorData.proximity, sensorData.timestamp);

  if (
    typeof sensorData.pedometer !== 'undefined' &&
    sensorData.pedometer.SuccessCode == 200
  )
    await addPedometerToStore(sensorData.pedometer, sensorData.timestamp);

  if (
    typeof sensorData.useractivity !== 'undefined' &&
    sensorData.useractivity.SuccessCode == 200
  )
    await addUserActitvityToStore(sensorData.useractivity);*/
}


// const parentAction = () => (dispatch) => {
//   store.dispatch({
//     type: GyroscopeStore,
//     payload: {
//       x: sensorData.gyroscope.Data.x,
//       y: sensorData.gyroscope.Data.y,
//       z: sensorData.gyroscope.Data.z,
//       timestamp: sensorData.gyroscope.Data.timestamp,
//       statusCode:sensorData.gyroscope.SuccessCode,
//     },
//   })
//   dispatch(someOtherAction2())
//   dispatch(someOtherAction3())

//   return resolve()
// }

export async function addLocationDataToStore(locationData) {
  if (locationData.SuccessCode == 200) await addLocationToStore(locationData);
}
