// import store from '../store';

export const AllSesnor = 'AllSesnor';

const initState = {
  data: '--',
};

export default (state = initState, action) => {
  switch (action.type) {
    case AllSesnor:
      return {
        ...state,
        data: action.payload.data
      };
    default:
      return state;
  }
};


export const addAllSensorToStore = (sensorData) => {

  store.dispatch({
    type: AllSesnor,
    payload: {
      data: sensorData
    },
  });
};
 