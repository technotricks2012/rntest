import {combineReducers, createStore, applyMiddleware} from 'redux';
import { createLogger } from 'redux-logger'
import thunk from 'redux-thunk'

import LocationStore from './LocationStore';
import AccelerometerStore from './AccelerometerStore';
import GyroscopeStore from './GyroscopeStore';

import BarometerStore from './BarometerStore';
import ProximityStore from './ProximityStore';
import OrientationStore from './OrientationStore';
import MagnetometerStore from './MagnetometerStore';
import LightSensorStore from './LightSensorStore';
import PedometerStore from './PedometerStore';
import AllSensorStore from './AllSensorStore';

import ActivityDetectionStore from './ActivityDetectionStore';


import TestDataStore from './TestDataStore'


const middleware = [thunk]


// if (__DEV__) {
//   middleware.push(createLogger({
//     options: {
//       collapsed: true,
//       diff     : true,
//     },
//   }))
// }

const rootReducer = combineReducers({
  allSesnorData:AllSensorStore,
  accelerometer: AccelerometerStore,
  gyroscope: GyroscopeStore,
  magnetometer: MagnetometerStore,
  barometer: BarometerStore,
  orientation: OrientationStore,
  proximity: ProximityStore,
  lightSensor: LightSensorStore,
  location: LocationStore,
  pedometer:PedometerStore,
  useractivity:ActivityDetectionStore,
  data:TestDataStore
});

const configureStore = () => createStore(rootReducer,{},applyMiddleware(...middleware));
export default store = configureStore();

/*
Example

HOME.js
export const REDUCER_NAME = 'home'



import { HomeReducer, HomeConstants } from '../screens/Home'
import { ItemReducer, ItemConstants } from '../screens/Item'


export default combineReducers({
  [HomeConstants.REDUCER_NAME]: HomeReducer,
  [ItemConstants.REDUCER_NAME]: ItemReducer,
})
*/
