// import store from '../store';

export const TestData = 'TestData';

const initState = {
  data: '--'
};

export default (state = initState , action) => {
  switch (action.type) {
    case TestData:
      
      return {
        ...state,
        data: action.payload.data,
      };
    default:
      return state;
  }
};

export const addTestDataToStore = (testData) => {

  store.dispatch({
    type: TestData,
    payload: {
      data: testData,
    },
  });
};
