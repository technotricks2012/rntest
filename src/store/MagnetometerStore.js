// import store from '../store';

export const Magnetometer = 'Magnetometer';

const initState = {
  x: '--',
  y: '--',
  z: '--',
  timestamp: '--',
  statusCode: '401',
};

export default (state = initState, action) => {
  switch (action.type) {
    case Magnetometer:
      return {
        ...state,
        x: action.payload.x,
        y: action.payload.y,
        z: action.payload.z,
        timestamp: action.payload.timestamp,
        statusCode: action.payload.statusCode,
      };
    default:
      return state;
  }
};

export const addMagnetometerToStore = (sensorData,) => {

  store.dispatch({
    type: Magnetometer,
    payload: {
      x: sensorData.x,
      y: sensorData.y,
      z: sensorData.z,
      timestamp: sensorData.timestamp,
      // statusCode:sensorData.SuccessCode,
    },
  });
};
