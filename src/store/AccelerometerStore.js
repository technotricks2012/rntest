// import store from '../store';

export const Accelerometer = 'Accelerometer';

const initState = {
  x: '--',
  y: '--',
  z: '--',
  timestamp: '--',
  statusCode: '401',

};

export default (state = initState , action) => {
  switch (action.type) {
    case Accelerometer:
      
      return {
        ...state,
        x: action.payload.x,
        y: action.payload.y,
        z: action.payload.z,
        timestamp: action.payload.timestamp,
        statusCode: action.payload.statusCode,
      };
    default:
      return state;
  }
};

export const setAccelerometer = (x, y, z, timestamp,statusCode) => {
  return {
    type: Accelerometer,
    payload: {x, y, z, timestamp,statusCode},
  };
};

export const addAccelerometerToStore = (sensorData) => {

  store.dispatch({
    type: Accelerometer,
    payload: {
      x: sensorData.x,
      y: sensorData.y,
      z: sensorData.z,
      timestamp: sensorData.timestamp,
      //statusCode:sensorData.SuccessCode,
    },
  });
};
