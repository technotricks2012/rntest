
export const LOCATION ='LOCATION'

const initState = {
  latitude: '--',
  longitude: '--',
  altitude: '--',
  timestamp: '--',
  statusCode: '401',
};


export default (state = initState, action) => {
  switch (action.type) {
    case LOCATION:
      return {...state, latitude: action.payload.latitude, longitude: action.payload.longitude,altitude: action.payload.altitude,
        timestamp: action.payload.timestamp};
    default:
      return state;
  }
};

export const setLocation= (lat,lng) =>{
  return{
      type: LOCATION,
      payload:{latitude,longitude}
  };
}


export const addLocationToStore = (locationData) => {
  store.dispatch({
    type: LOCATION,
    payload: {
      latitude: locationData.Data.latitude,
      longitude: locationData.Data.longitude,
      altitude: locationData.Data.altitude,
      timestamp: locationData.Data.timestamp,
      statusCode:locationData.SuccessCode,
    },
  });
};