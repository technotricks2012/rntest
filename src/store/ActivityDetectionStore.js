// import store from '../store';

export const ActivityDetection = 'ActivityDetection';

const initState = {
  isAvailable: '--',
  isStationary: '--',
  isWalking: '--',
  isCycling: '--',
  isRunning: '--',
  isAutomotive: '--',
  unknown: '--',
  confidence: '--',
  DetectedType: '--',//Android
  timestamp: '--',
  statusCode: '401',
};

export default (state = initState, action) => {
  switch (action.type) {
    case ActivityDetection:
      return {
        ...state,
        isAvailable: action.payload.isAvailable,
        isStationary: action.payload.isStationary,
        isWalking: action.payload.isWalking,
        isCycling: action.payload.isCycling,
        isRunning: action.payload.isRunning,
        isAutomotive: action.payload.isAutomotive,
        unknown: action.payload.unknown,
        DetectedType: action.payload.DetectedType,//Android
        confidence:action.payload.confidence,
        timestamp: action.payload.timestamp,
        statusCode: action.payload.statusCode,
      };
    default:
      return state;
  }
};

export const addUserActitvityToStore = data => {
  store.dispatch({
    type: ActivityDetection,
    payload: {
      isAvailable: data.Data.isAvailable,
      isStationary: data.Data.isStationary,
      isWalking: data.Data.isWalking,
      isCycling: data.Data.isCycling,
      isRunning: data.Data.isRunning,
      isAutomotive: data.Data.isAutomotive,
      DetectedType: data.Data.DetectedType,//Android
      confidence: data.Data.confidence,
      unknown: data.Data.unknown,
      timestamp: data.Data.timestamp,
      statusCode: data.SuccessCode,
    },
  });
};
