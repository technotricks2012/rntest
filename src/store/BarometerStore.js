// import store from '../store';

export const Barometer = 'MagnetoBarometermeter';

const initState = {
  pressure: '--',
  timestamp: '--',
  statusCode: '401',
};

export default (state = initState, action) => {
  switch (action.type) {
    case Barometer:
      return {
        ...state,
        pressure: action.payload.pressure,
        timestamp: action.payload.timestamp,
        statusCode: action.payload.statusCode,
      };
    default:
      return state;
  }
};

export const addBarometerToStore = (sensorData,timestamp) => {

  store.dispatch({
    type: Barometer,
    payload: {
      pressure: sensorData.Data.pressure,
      timestamp: timestamp,
      statusCode:sensorData.SuccessCode,
    },
  });
};
