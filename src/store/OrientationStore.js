// import store from '../store';

export const Orientation = 'Orientation';

const initState = {
  roll: '--',
  pitch: '--',
  azimuth: '--',
  timestamp: '--',
  statusCode: '401',
};

export default (state = initState, action) => {
  switch (action.type) {
    case Orientation:
      return {
        ...state,
        roll: action.payload.roll,
        pitch: action.payload.pitch,
        azimuth: action.payload.azimuth,
        timestamp: action.payload.timestamp,
        statusCode: action.payload.statusCode,
      };
    default:
      return state;
  }
};

export const addOrientationToStore = (sensorData,timestamp) => {
  store.dispatch({
    type: Orientation,
    payload: {
      roll: sensorData.Data.roll,
      pitch: sensorData.Data.pitch,
      azimuth: sensorData.Data.azimuth,
      timestamp: timestamp,
      statusCode: sensorData.SuccessCode,
    },
  });
};
