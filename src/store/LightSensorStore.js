// import store from '../store';

export const LightSensor = 'LightSensor';

const initState = {
  light: '--',
  timestamp: '--',
  statusCode: '401',
};

export default (state = initState, action) => {
  switch (action.type) {
    case LightSensor:
      return {
        ...state,
        light: action.payload.light,
        timestamp: action.payload.timestamp,
        statusCode: action.payload.statusCode,
      };
    default:
      return state;
  }
};

export const addLightSensorToStore = (sensorData,timestamp) => {
  store.dispatch({
    type: LightSensor,
    payload: {
      light: sensorData.Data.light,
      timestamp: timestamp,
      statusCode: sensorData.SuccessCode,
    },
  });
};
