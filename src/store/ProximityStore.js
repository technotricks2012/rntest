// import store from '../store';

export const Proximity = 'Proximity';

const initState = {
  maxRange: '--',
  value: '--',
  isNear: '--',
  timestamp: '--',
  statusCode: '401',
};

export default (state = initState, action) => {
  switch (action.type) {
    case Proximity:
      return {
        ...state,
        maxRange: action.payload.maxRange,
        value: action.payload.value,
        isNear: action.payload.isNear,
        timestamp: action.payload.timestamp,
        statusCode: action.payload.statusCode,
      };
    default:
      return state;
  }
};

export const addProximityToStore = (sensorData,timestamp) => {
  store.dispatch({
    type: Proximity,
    payload: {
      maxRange: sensorData.Data.maxRange,
      value: sensorData.Data.value,
      isNear: sensorData.Data.isNear,
      timestamp: timestamp,
      statusCode: sensorData.SuccessCode,
    },
  });
};
