// import store from '../store';

export const GyroscopeStore = 'GyroscopeStore';

const initState = {
  x: '--',
  y: '--',
  z: '--',
  timestamp: '--',
  statusCode: '401',
};

export default (state = initState, action) => {
  switch (action.type) {
    case GyroscopeStore:
      return {
        ...state,
        x: action.payload.x,
        y: action.payload.y,
        z: action.payload.z,
        timestamp: action.payload.timestamp,
        statusCode: action.payload.statusCode,
      };
    default:
      return state;
  }
};

// export const setGyroscope = (x, y, z, timestamp) => {
//   return {
//     type: GyroscopeStore,
//     payload: {x, y, z, timestamp},
//   };
// };

export const addGyroscopeToStore = (data) => {
  store.dispatch({
    type: GyroscopeStore,
    payload: {
      x: data.Data.x,
      y: data.Data.y,
      z: data.Data.z,
      timestamp: data.Data.timestamp,
      statusCode:data.SuccessCode,
    },
  });
};
