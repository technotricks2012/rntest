const initState = {
    helloText: 'Hello!',
    pressButton:false
  };
  
  export default (state = initState, action) => {
    switch (action.type) {
      case 'HELLO_ACTION':
          return {...state,pressButton:true,helloText:'You Pressed the button'};
      default:
        return state;
    }
  };