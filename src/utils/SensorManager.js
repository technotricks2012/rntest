import {
  accelerometer,
  gyroscope,
  magnetometer,
  barometer,
  setUpdateIntervalForType,
  SensorTypes,
} from 'react-native-sensors';
import DeviceInfo from 'react-native-device-info';

import {
  NativeModules,
  DeviceEventEmitter,
  NativeEventEmitter,
} from 'react-native';
import {getSteps} from '../lib/Pedometer';

import {SensorManager} from 'NativeModules';

import {setLocal} from '../db/DbManager';

import {of, zip, Observable, from} from 'rxjs';
import {map, take, timeout, timeoutWith, catchError} from 'rxjs/operators';

import {getStepDetection} from './StepDetection';
import FirebaseDB from '../firebase/FirebaseDB';
import Geolocation from '@react-native-community/geolocation';

import {
  Orientation,
  Proximity,
  Thermometer,
  LightSensor,
  StepCounter,
} from '../lib/sensors';
import {date} from '@nozbe/watermelondb/decorators';
import { ORIENTATION, PROXIMITY, STEPCOUNTER, THERMOMETER, LIGHTSENSOR, ACCELEROMETER, GYROSCOPE, MAGNETOMETER, BAROMETER, LOCATION } from '../constants/IKeyConstants';

const interval = 1000;
setUpdateIntervalForType(SensorTypes.accelerometer, interval);
setUpdateIntervalForType(SensorTypes.barometer, interval);
setUpdateIntervalForType(SensorTypes.gyroscope, interval);
setUpdateIntervalForType(SensorTypes.magnetometer, interval);

export async function stopListener() {
  accelerometer.stop();
  barometer.stop();
  gyroscope.stop();
  magnetometer.stop();
}

async function getOrientationObserver() {
  return Observable.create(function subscribe(observer) {
    Orientation
    .pipe(
      timeout(900),
      catchError(date => {
        // observer.next({SuccessCode: '408'});
        return Observable.of({SuccessCode: '408'});
      }),
    )
    .pipe(take(1)).subscribe(
      data => {
        setLocal(ORIENTATION, 'true');

        observer.next({SuccessCode: '200', Data: data});
      },
      err => {
        setLocal(ORIENTATION, 'false');
        observer.next({SuccessCode: '500'});
      },
    );
  });
}

async function getProximityObserver() {
  return Observable.create(function subscribe(observer) {
    Proximity
      .pipe(
        timeout(900),
        catchError(date => {
          // observer.next({SuccessCode: '408'});
          return Observable.of({SuccessCode: '408'});
        }),
      )
      .pipe(take(1))
      .subscribe(
        data => {
          setLocal(PROXIMITY, 'true');

          observer.next({SuccessCode: '200', Data: data});
        },
        err => {
          setLocal(PROXIMITY, 'false');
          observer.next({SuccessCode: '500'});
        },
      );
  });
}

async function getStepCounterObserver() {
  return Observable.create(function subscribe(observer) {
    getStepsManager(steps => {
      console.log('getSteps Info =>' + JSON.stringify(steps));

      if (steps.SuccessCode == '200') {
        setLocal(STEPCOUNTER, 'true');
        observer.next({SuccessCode: '200', Data: steps.Data});
      } else {
        setLocal(STEPCOUNTER, 'false');
        observer.next({SuccessCode: '500'});
      }
    });
  });
}

//Thermometer is not working for some devices//
async function getThermometerObserver() {
  return Observable.create(function subscribe(observer) {
    Thermometer.pipe(
      timeout(900),
      catchError(date => {
        // observer.next({SuccessCode: '408'});
        return Observable.of({SuccessCode: '408'});
      }),
    )
      .pipe(take(1))
      .subscribe(
        data => {
          setLocal(THERMOMETER, 'true');


          observer.next({SuccessCode: '200', Data: data});
        },
        err => {
          setLocal(THERMOMETER, 'false');

          observer.next({SuccessCode: '500'});
        },
      );
  });
}

async function getLightSensorObserver() {
  return Observable.create(function subscribe(observer) {
    LightSensor
    .pipe(
      timeout(900),
      catchError(date => {
        // observer.next({SuccessCode: '408'});
        return Observable.of({SuccessCode: '408'});
      }),
    )
    .pipe(take(1))
    .subscribe(
      data => {
        setLocal(LIGHTSENSOR, 'true');

        observer.next({SuccessCode: '200', Data: data});
      },
      err => {
        setLocal(LIGHTSENSOR, 'false');
        observer.next({SuccessCode: '500'});
      },
    );
  });
}

async function getAccelerometerObserver() {
  return Observable.create(function subscribe(observer) {
    accelerometer
    .pipe(
      timeout(900),
      catchError(date => {
        // observer.next({SuccessCode: '408'});
        return Observable.of({SuccessCode: '408'});
      }),
    )
    .pipe(take(1)).subscribe(
      ({x, y, z, timestamp}) => {
        setLocal(ACCELEROMETER, 'true');

        observer.next({SuccessCode: '200', Data: {x, y, z, timestamp}});
      },
      err => {
        setLocal(ACCELEROMETER, 'false');
        observer.next({SuccessCode: '500'});
      },
    );
  });
}

async function getGyroscopeObserver() {
  return Observable.create(function subscribe(observer) {
    gyroscope
    .pipe(
      timeout(900),
      catchError(date => {
        // observer.next({SuccessCode: '408'});
        return Observable.of({SuccessCode: '408'});
      }),
    )
    .pipe(take(1)).subscribe(
      ({x, y, z, timestamp}) => {
        setLocal(GYROSCOPE, 'true');
        data = {SuccessCode: '200', Data: {x, y, z, timestamp}};
        observer.next(data);
      },
      err => {
        setLocal(GYROSCOPE, 'false');
        observer.next({SuccessCode: '500'});
      },
    );
  });
}

async function getMagnetometerObserver() {
  return Observable.create(function subscribe(observer) {
    magnetometer
    .pipe(
      timeout(900),
      catchError(date => {
        // observer.next({SuccessCode: '408'});
        return Observable.of({SuccessCode: '408'});
      }),
    )
    .pipe(take(1)).subscribe(
      ({x, y, z, timestamp}) => {
        setLocal(MAGNETOMETER, 'true');

        observer.next({SuccessCode: '200', Data: {x, y, z, timestamp}});
      },
      err => {
        setLocal(MAGNETOMETER, 'false');
        observer.next({SuccessCode: '500'});
      },
    );
  });
}

async function getBarometerObserver() {
  return Observable.create(function subscribe(observer) {
    barometer
    .pipe(
      timeout(900),
      catchError(date => {
        // observer.next({SuccessCode: '408'});
        return Observable.of({SuccessCode: '408'});
      }),
    )
    .pipe(take(1)).subscribe(
      ({pressure}) => {
        setLocal(BAROMETER, 'true');

        observer.next({SuccessCode: '200', Data: {pressure}});
      },
      err => {
        setLocal(BAROMETER, 'false');
        observer.next({SuccessCode: '500'});
      },
    );
  });
}

export async function getDeviceInfo(callback) {
  Promise.all([
    DeviceInfo.getModel(),
    DeviceInfo.getPowerState(),
    DeviceInfo.getSystemVersion(),
    DeviceInfo.getHardware(),
  ]).then(([modelValue, powerDateValue, osVersionValue, harwareValue]) => {
    var deviceInfo = {
      model: modelValue,
      powerState: powerDateValue,
      osVersion: osVersionValue,
      harware: harwareValue,
    };

    return callback(deviceInfo);
  });
}

export async function getStepsManager(callback) {
  // debugger
  // setLocal(STEPCOUNTER, 'true');

  await getSteps(callback);
}

export async function getLocation(callback) {
  Geolocation.getCurrentPosition(
    position => {
      setLocal(LOCATION, 'true');
      var data = {SuccessCode: '200', Data: {
        latitude:position.coords.latitude,
        longitude:position.coords.longitude,
        altitude:position.coords.altitude,
        timestamp:position.timestamp
      }};
      return callback(data);
    },
    err => {
      setLocal(LOCATION, 'false');
      return callback({SuccessCode: '500'});
    },
    {
      enableHighAccuracy: true,
      timeout: 20000,
      maximumAge: 1000,
    },
  );
}

export async function getSensorData(callback) {
  const combinedStream = zip(
    await getAccelerometerObserver(),
    await getGyroscopeObserver(),
    await getMagnetometerObserver(),
    await getBarometerObserver(),
    await getOrientationObserver(),
    await getProximityObserver(),
    await getThermometerObserver(),
    await getLightSensorObserver(),
    await getStepCounterObserver(),

  ).pipe(
    map(
      ([
        accelerometerValue,
        gyroscopeValue,
        magnetometerValue,
        barometerValue,
        orientationValue,
        proximityValue,
        thermometerValue,
        lightSensorValue,
        stepCounterValue,
      ]) => ({
        timestamp:
          accelerometerValue.SuccessCode == 200
            ? accelerometerValue.Data.timestamp
            : new Date().getTime(),
        accelerometer: accelerometerValue,
        gyroscope: gyroscopeValue,
        magnetometer: magnetometerValue,
        barometer: barometerValue,
        orientation: orientationValue,
        proximity: proximityValue,
        thermometer: thermometerValue,
        lightSensor: lightSensorValue,
        pedometer: stepCounterValue,
      }),
    ),
  );

  

  combinedStream.pipe(take(1)).subscribe(data => {    
    return callback(data);
  });
}

/***
 *Sample format 
 *
{
  "timestamp": 1571129698230,
  "accelerometer": {
    "SuccessCode": "200",
    "Data": {
      "x": 0,
      "y": 9.776309967041016,
      "z": 0.8123490214347839,
      "timestamp": 1571129698230
    }
  },
  "gyroscope": {
    "SuccessCode": "200",
    "Data": {
      "x": 0,
      "y": 0,
      "z": 0,
      "timestamp": 1571129698231
    }
  },
  "magnetometer": {
    "SuccessCode": "200",
    "Data": {
      "x": 0,
      "y": 9.887660026550293,
      "z": -47.745201110839844,
      "timestamp": 1571129698232
    }
  },
  "barometer": {
    "SuccessCode": "200",
    "Data": {
      "pressure": 1013.25
    }
  }
}*/
