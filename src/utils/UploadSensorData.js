import {
  getAllDataFromDB,
  deleteBatchFromDB,
  setServerInterVal,
  getLocal,
} from '../db/DbManager';
const timeout = ms => new Promise(res => setTimeout(res, ms));
import moment from 'moment';
import { IS_NETWORK } from '../constants/IKeyConstants';

export async function uploadSensorData(request) {
  return fetch(
    //'https://42d6yglqtj.execute-api.ap-southeast-1.amazonaws.com/dev/savewaypoints',
    'https://l8szzjslve.execute-api.ap-southeast-1.amazonaws.com/msp-dev-stage/SaveWaypointsV1',
    {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(request),
    },
  )
    .then(response => response.json())
    .then(responseJson => {
      return responseJson;
    })
    .catch(error => {
      console.error(error);
    });
}


// console.log("Time==>"+moment().unix())

export async function uploadAllData(type) {
  
  const isNetworkConnection = await getLocal(IS_NETWORK)

  console.log(JSON.stringify)
  if (isNetworkConnection == "true") {
    const sensorData = await getAllDataFromDB();
    await setServerInterVal(type, sensorData.batchID); // Check The Server connection Test
    console.log(
      'getSensorDataFromDB ToUpload Server ===<',
      JSON.stringify(sensorData),
    );

    if (sensorData == 'NIL') return;
    let response = await uploadSensorData(sensorData);
    console.log('Response == ', JSON.stringify(response));

    if (response.statuscode == 200) {
      await deleteBatchFromDB(response.batchId);
    }
    return true;
  } else {
    return false;
  }
}
