const ACCEL_RING_SIZE = 50;
const VEL_RING_SIZE = 10;
const STEP_THRESHOLD = 16; // Default 4f
const STEP_DELAY_NS = 250000000; // Default 250000000

var accelRingCounter = 0;
var accelRingX = new Array(ACCEL_RING_SIZE);
var accelRingY = new Array(ACCEL_RING_SIZE);
var accelRingZ = new Array(ACCEL_RING_SIZE);

var velRingCounter = 0;
var velRing = new Array(VEL_RING_SIZE);

var lastStepTimeNs = 0;
var oldVelocityEstimate = 0;

// Logic 1
const GRAVITY_EARTH = 9.80665;
var mAccel = 0.0;
var mAccelCurrent;
var mAccelLast;

export async function getStepDetection(accelerometer, callback) {
  //Reset the values
  //   accelRingCounter = 0;

  var timeNs = accelerometer.timestamp;
  var currentAccel = new Array(3);

  currentAccel[0] = accelerometer.x;
  currentAccel[1] = accelerometer.y;
  currentAccel[2] = accelerometer.z;

  // First step is to update our guess of where the global z vector is.
  accelRingCounter++;
  accelRingX[accelRingCounter % ACCEL_RING_SIZE] = currentAccel[0];
  accelRingY[accelRingCounter % ACCEL_RING_SIZE] = currentAccel[1];
  accelRingZ[accelRingCounter % ACCEL_RING_SIZE] = currentAccel[2];

  //   console.log(`****`+accelRingX);

  var worldZ = new Array(3);

  worldZ[0] = sum(accelRingX) / Math.min(accelRingCounter, ACCEL_RING_SIZE);
  worldZ[1] = sum(accelRingY) / Math.min(accelRingCounter, ACCEL_RING_SIZE);
  worldZ[2] = sum(accelRingZ) / Math.min(accelRingCounter, ACCEL_RING_SIZE);

  var normalization_factor = norm(worldZ);

  worldZ[0] = worldZ[0] / normalization_factor;
  worldZ[1] = worldZ[1] / normalization_factor;
  worldZ[2] = worldZ[2] / normalization_factor;

  // Next step is to figure out the component of the current acceleration
  // in the direction of world_z and subtract gravity's contribution
  var currentZ = dot(worldZ, currentAccel) - normalization_factor;
  velRingCounter++;
  velRing[velRingCounter % VEL_RING_SIZE] = currentZ;

  var velocityEstimate = sum(velRing);

  if (
    velocityEstimate > STEP_THRESHOLD &&
    oldVelocityEstimate <= STEP_THRESHOLD &&
    timeNs - lastStepTimeNs > STEP_DELAY_NS
  ) {
    //   listener.step(timeNs);

    console.log('**********Step Changed********');

    lastStepTimeNs = timeNs;
  }
  // console.log(`oldVelocityEstimate`+velRing+`   velocityEstimate`+velocityEstimate);

  oldVelocityEstimate = velocityEstimate;

  //Logic 1
  var x = accelerometer.x;
  var y = accelerometer.y;
  var z = accelerometer.z;
  mAccelLast = mAccelCurrent;
  mAccelCurrent = Math.sqrt(x * x + y * y + z * z);
  var delta = mAccelCurrent - mAccelLast;
  mAccel = mAccel * 0.9 + delta;

  console.log(`mAccelCurrent`+mAccelCurrent);

  if (mAccel > 3) {
    callback(timeNs);
  }
}

function sum(array) {
  var retval = 0;
  for (i = 0; i < array.length; i++) {
    retval += array[i];
  }
  return retval;
}

function norm(array) {
  var retval = 0;
  for (i = 0; i < array.length; i++) {
    retval += array[i] * array[i];
  }
  return Math.sqrt(retval);
}

// Note: only works with 3D vectors.
function dot(a, b) {
  var retval = a[0] * b[0] + a[1] * b[1] + a[2] * b[2];
  return retval;
}
