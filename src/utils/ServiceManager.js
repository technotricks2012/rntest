import {NativeModules, Platform} from 'react-native';

import {getLocal, setLocal} from '../db/DbManager';
import {getRandomUniqueId} from '../utils/Utils';

import  {ActivityRecognition} from '../lib/ActivityRecognition'

import {TRAVEL_MODE, JOURNEY_ID,SERVICE_START_TIME, USER_ACTIVITY_MODE} from '../constants/IKeyConstants';

export async function startServiceTimer(interval) {
  if (Platform.OS === 'ios') {
    // NativeModules.QuickServiceInterval.startTimer(interval);
    NativeModules.RNMobilityKit.startSensor(5);
    NativeModules.RNMobilityKit.startUpdateLocation(5);


  } else if (Platform.OS === 'android') {
    NativeModules.QuickServiceInterval.startService(1000 * interval);
    NativeModules.ActivityDetection.startTracking();
  }

  await initialise();
}

export async function stopServiceTimer(interval) {
  if (Platform.OS === 'ios') {
    NativeModules.QuickServiceInterval.stopTimer();
  } else if (Platform.OS === 'android') {
    NativeModules.QuickServiceInterval.stopService();
  }
}

async function initialise() {
  console.log("initialise...")
  const modeValue = await getLocal(TRAVEL_MODE);

  const mode = modeValue ? modeValue : 'UNKNOWN';
  await setLocal(TRAVEL_MODE, mode);

  
  const predictionModeValue = await getLocal(USER_ACTIVITY_MODE);

  const predictionMode = predictionModeValue ? predictionModeValue : 'UNKNOWN';
  await setLocal(USER_ACTIVITY_MODE, predictionMode);
  
  const journeyIDValue = await getLocal(JOURNEY_ID);
  const journeyID = journeyIDValue ? journeyIDValue : getRandomUniqueId();
  await setLocal(JOURNEY_ID, journeyID);

  const serviceStartTime = await getLocal(SERVICE_START_TIME);
  const startTimestamp = serviceStartTime
    ? serviceStartTime
    : new Date().toUTCString()
    console.log("Start Time ="+startTimestamp)
  await setLocal(SERVICE_START_TIME, startTimestamp);

  
}
