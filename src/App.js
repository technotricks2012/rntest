/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
// import Menu from './Screen/Menu';
import Screen from './Screen';
// import FirebaseConfig from'./firebase/FirebaseConfig';

import AppNavigation from './Navigation/AppNavigation';

import {Root} from 'native-base';

//Running Background Listener for all Sensor Data
import BackgroundProcess from './bg/BackgroundProcess';

class App extends Component {
  constructor() {
    super();
  }
  render() {
    return (
      // <Screen/>
      <Root>
        <AppNavigation />
      </Root>
    );
  }
}

export default App;
