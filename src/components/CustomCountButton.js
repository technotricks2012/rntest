import React from 'react';
import {
  TouchableOpacity,
  StyleSheet,
  Text,
  View,
  ActivityIndicator,
} from 'react-native';

import {w, h, totalSize} from '../utils/Dimensions';

export const CustomCountButton = props => {
  const {style = {}, textStyle = {}, onPress, isDisabled, isRunning} = props;

  const isButtonDisabled = isDisabled == null ? false : isDisabled;
  const isButtonRunning = isRunning == null ? false : isRunning;

  var containerColor = 'white';
  var disableColor = '#999999';
  var textColor = '#3d3d3d';

  return (
    <TouchableOpacity
      disabled={isButtonRunning || isButtonDisabled}
      onPress={onPress}>
      <View
        style={[
          style,
          styles.container,
          {backgroundColor: isButtonDisabled ? disableColor : containerColor},
        ]}>
        <View
          style={{
            flex: 1,
            flexDirection: 'row',
          }}>
          <View
            style={{
              width: '30%',
              justifyContent: 'center',
              alignContent: 'center',
              backgroundColor: '#1a61d8',
            }}>
            <Text style={[styles.textCountStyle]}>{props.count}</Text>
          </View>

          <View
            style={{
              width: '50%',
              justifyContent: 'center',
              alignContent: 'center',
            }}>
            {isButtonRunning && (
              <Text style={[styles.textProcessingStyle]}>Processing</Text>
            )}

            {!isButtonRunning && (
              <Text style={[styles.textTitleStyle]}>{props.title}</Text>
            )}
          </View>

          <View
            style={{
              width: '20%',
              justifyContent: 'center',
              alignContent: 'center',
            }}>
            {isButtonRunning && (
              <ActivityIndicator
                style={styles.spinner}
                size="large"
                color="#1fc7bd"
              />
            )}

            {!isButtonRunning && <Text style={[styles.textPlusStyle]}>+</Text>}
          </View>
        </View>

        {/* <View
          style={[
            styles.innerContinerActive,
            {backgroundColor: isButtonDisabled ? disableColor : containerColor},
          ]}>
          <Text style={[styles.textBigStyle, {color: textColor}]}>
            {props.count}
          </Text>
          <Text style={[styles.textStyle, {color: textColor}]}>
            {props.title}
          </Text>
        </View> */}
      </View>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  container: {
    borderRadius: 5,
    borderColor: '#ddd',
    borderBottomWidth: 0,
    shadowColor: '#000',
    shadowOffset: {width: 0, height: 2},
    shadowOpacity: 0.8,
    shadowRadius: 2,
    elevation: 3,
  },

  innerContinerActive: {
    flex: 1,
    borderRadius: 5,
    borderWidth: 1,
    borderColor: '#ddd',
    borderBottomWidth: 0,
    shadowColor: '#000',
    shadowOffset: {width: 0, height: 2},
    shadowOpacity: 0.8,
    shadowRadius: 2,
    elevation: 3,
    justifyContent: 'center',
    alignItems: 'center',
  },

  button: {
    display: 'flex',
    height: 50,
    borderRadius: 5,
    justifyContent: 'center',
    alignItems: 'center',

    backgroundColor: '#2AC062',
    shadowColor: '#2AC062',
    shadowOpacity: 0.4,
    shadowOffset: {height: 10, width: 0},
    shadowRadius: 20,
  },
  spinner: {
    alignItems: 'center',
    justifyContent: 'center',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
  },
  textStyle: {
    fontFamily: 'SanFranciscoText-Medium',
    fontSize: 12,
    alignContent: 'center',
  },
  textTitleStyle: {
    fontFamily: 'SanFranciscoText-Medium',
    fontSize: 24,
    color: '#818181',
    textAlign: 'center',
  },
  textProcessingStyle: {
    fontFamily: 'SanFranciscoText-Bold',
    fontWeight: 'bold',
    fontSize: 18,
    color: '#1fc7bd',
    textAlign: 'center',
  },
  textCountStyle: {
    fontFamily: 'SanFranciscoText-Medium',
    fontSize: 24,
    color: 'white',
    textAlign: 'center',
  },

  textPlusStyle: {
    fontFamily: 'SanFranciscoText-Medium',
    fontSize: 40,
    color: '#818181',
    textAlign: 'center',
  },

  image: {},
});
