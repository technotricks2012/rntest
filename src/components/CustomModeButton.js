import React from 'react';
import {TouchableOpacity, StyleSheet, Text, View} from 'react-native';

import {w, h, totalSize} from '../utils/Dimensions';
import {SvgXml} from 'react-native-svg';

export const CustomModeButton = props => {
  const {style = {}, textStyle = {}, onPress, isDisabled, isSelected} = props;

  const isButtonDisabled = isDisabled == null ? false : isDisabled;

  var containerColor = isSelected ? '#1c62db' : 'white';
  var svgColor = isSelected ? 'white' : '#1ebebf';
  var textColor = isSelected ? 'white' : '#3d3d3d';

  if (isButtonDisabled) {
    containerColor = '#eeeeee';
    svgColor = '#818181';
    textColor = '#818181';
  }

  console.log('NAme:' + props.title + 'STATE:' + isButtonDisabled);

  return (
    <TouchableOpacity
      disabled={isButtonDisabled}
      onPress={onPress}
      style={styles.container}>
      <View
        style={[styles.innerContinerActive, {backgroundColor: containerColor}]}>
        <SvgXml xml={props.svgPath} width="50" height="50" fill={svgColor} />
        <Text style={[styles.textStyle, {color: textColor}]}>
          {props.title}
        </Text>
      </View>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  container: {
    width: w(100) / 3,
    height: w(100) / 3,
    padding: 10,
  },

  innerContinerActive: {
    flex: 1,
    borderRadius: 5,
    borderWidth: 1,
    borderColor: '#ddd',
    borderBottomWidth: 0,
    shadowColor: '#000',
    shadowOffset: {width: 0, height: 2},
    shadowOpacity: 0.8,
    shadowRadius: 2,
    elevation: 3,
    justifyContent: 'center',
    alignItems: 'center',
  },

  button: {
    display: 'flex',
    height: 50,
    borderRadius: 5,
    justifyContent: 'center',
    alignItems: 'center',

    backgroundColor: '#2AC062',
    shadowColor: '#2AC062',
    shadowOpacity: 0.4,
    shadowOffset: {height: 10, width: 0},
    shadowRadius: 20,
  },

  textStyle: {
    fontFamily: 'SanFranciscoText-Medium',
    fontSize: 12,
    alignContent: 'center',
  },
  image: {},
});
