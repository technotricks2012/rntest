import React from 'react';
import {
  TouchableOpacity,
  StyleSheet,
  Text,
  View,
  ActivityIndicator,
} from 'react-native';

import {w, h, totalSize} from '../utils/Dimensions';

export const CustomProcessingButton = props => {
  const {style = {}, textStyle = {}, onPress, isRunning} = props;

  const isButtonRunning = isRunning == null ? false : isRunning;

  var containerColor = 'white';
  var disableColor = '#999999';
  var textColor = '#3d3d3d';

  return (
    <TouchableOpacity disabled={isButtonRunning} onPress={onPress}>
      <View style={[style, styles.container]}>
        <View
          style={{
            flex: 1,
            flexDirection: 'row',
            backgroundColor: '#1fc7bd',
            borderRadius: 5,
            justifyContent: 'center',
            alignContent: 'center',
          }}>
          <View
            style={{
              justifyContent: 'center',
              alignContent: 'center',
            }}>
            <Text style={[styles.textCountStyle]}>
              {isButtonRunning ? 'Processing' : props.children}
            </Text>
          </View>
          {isButtonRunning && (
            <ActivityIndicator
              style={styles.spinner}
              size="large"
              color="#1a61d8"
            />
          )}
        </View>
      </View>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  container: {
    borderRadius: 5,
    borderColor: '#ddd',
    borderBottomWidth: 0,
    shadowColor: '#000',
    shadowOffset: {width: 0, height: 2},
    shadowOpacity: 0.8,
    shadowRadius: 2,
    elevation: 3,
  },

  innerContinerActive: {
    flex: 1,
    borderRadius: 5,
    borderWidth: 1,
    borderColor: '#ddd',
    borderBottomWidth: 0,
    shadowColor: '#000',
    shadowOffset: {width: 0, height: 2},
    shadowOpacity: 0.8,
    shadowRadius: 2,
    elevation: 3,
    justifyContent: 'center',
    alignItems: 'center',
  },

  button: {
    display: 'flex',
    height: 50,
    borderRadius: 5,
    justifyContent: 'center',
    alignItems: 'center',

    backgroundColor: '#2AC062',
    shadowColor: '#2AC062',
    shadowOpacity: 0.4,
    shadowOffset: {height: 10, width: 0},
    shadowRadius: 20,
  },
  spinner: {
    alignItems: 'center',
    justifyContent: 'center',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
  },
  textStyle: {
    fontFamily: 'SanFranciscoText-Medium',
    fontSize: 12,
    alignContent: 'center',
  },
  textTitleStyle: {
    fontFamily: 'SanFranciscoText-Medium',
    fontSize: 24,
    color: '#818181',
    textAlign: 'center',
  },
  textProcessingStyle: {
    fontFamily: 'SanFranciscoText-Bold',
    fontWeight: 'bold',
    fontSize: 18,
    color: '#1fc7bd',
    textAlign: 'center',
  },
  textCountStyle: {
    fontFamily: 'SanFranciscoText-Medium',
    fontSize: 24,
    color: 'white',
    textAlign: 'center',
  },

  textPlusStyle: {
    fontFamily: 'SanFranciscoText-Medium',
    fontSize: 40,
    color: '#818181',
    textAlign: 'center',
  },

  image: {},
});
