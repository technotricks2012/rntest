// import * as firebase from 'firebase';
// import '@firebase/database';
import { firebase } from '@react-native-firebase/database';


class FirebaseDB {
  static setGeoLocation(position,isBg) {
      firebase
        .database()
        .ref(`GeoLocation/${isBg?"BG":"FG"}`)
        .push(position)
        .then(function(docRef) {
          console.log('Document written with ID: ', docRef.id);
        })
        .catch(function(error) {
          console.error('Error adding document: ', error);
        });
    
  }


  // static getAllMovies = async(isBg) => {
  //   return firebase
  //   .database()
  //   .ref(`GeoLocation/${isBg?"BG":"FG"}`)
  //   .once('value');
  // }


  static getGeoLocation = async (isBg) => {
    const name = firebase
    .database()
    .ref(`GeoLocation/${isBg?"BG":"FG"}`);
  
    var data = await name.once('value')
      .then(snapshot => {
        

      //   snapshot.forEach(function (childSnapshot) {
      //     var data = childSnapshot.val();
      //     console.log("value", data);
      // });

        return snapshot;
      })
      .catch(e => {
        console.log("firebase error", e);
        return null;
      });
  
    return data;
  }
  // static getGeoLocation(isBg) {
  //   firebase
  //     .database()
  //     .ref(`GeoLocation/${isBg?"BG":"FG"}`)
  //     .once('value', function (snapshot) {
  //       console.log(snapshot.val())
  //   });
  
//}

  static setAccelerometer(data) {
    // let userMobilePath = "/user/" + userId + "/details";

    // return firebase.database().ref(userMobilePath).set({
    //     mobile: mobile
    // })
      firebase
        .database()
        .ref('Accelerometer/')
        .push({
          data: data,
        })
        .then(function(docRef) {
          console.log('Document written with ID: ', docRef.id);
        })
        .catch(function(error) {
          console.error('Error adding document: ', error);
        });
    
  }


  static async setData(table,data) {
    return firebase
      .database()
      .ref(table)
      .push(data)
      .then(function(docRef) {
        console.log('Document written with ID: ', docRef.key);
        return docRef.key
      })
      .catch(function(error) {
        console.error('Error adding document: ', error);
        return "-1"
      });
  
}

  // /**
  //  * Listen for changes to a users mobile number
  //  * @param userId
  //  * @param callback Users mobile number
  //  */
  // static listenUserMobile(userId, callback) {

  //     let userMobilePath = "/user/" + userId + "/details";

  //     firebase.database().ref(userMobilePath).on('value', (snapshot) => {

  //         var mobile = "";

  //         if (snapshot.val()) {
  //             mobile = snapshot.val().mobile
  //         }

  //         callback(mobile)
  //     });
  // }
}

module.exports = FirebaseDB;
