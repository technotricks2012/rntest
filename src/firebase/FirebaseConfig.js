import * as firebase from 'firebase';

const config = {
  apiKey: "AIzaSyA2-JgRKOeE3E8EtFusBk8x_-47BQGRdKs",
  authDomain: "rnsensor-aa40b.firebaseapp.com",
  databaseURL: "https://rnsensor-aa40b.firebaseio.com",
  projectId: "rnsensor-aa40b",
  storageBucket: "",
  messagingSenderId: "556457211847",
  appId: "1:556457211847:web:2ee9737da2e951d3"
};

export default !firebase.apps.length ? firebase.initializeApp(config) : firebase.app();