package com.lta.location;

import android.content.Context;
import android.location.LocationManager;

import static android.content.Context.LOCATION_SERVICE;

public class GPSCheckPoint {

    public static LocationManager locationManager;
    public static boolean gpsProviderEnable(Context context) {

        locationManager = (LocationManager)context.getSystemService(LOCATION_SERVICE);
        boolean isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        return isGPSEnabled;
    }

    public static boolean networkProviderEnable(Context context) {
        boolean isNetworkEnabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        return isNetworkEnabled;
    }
}
