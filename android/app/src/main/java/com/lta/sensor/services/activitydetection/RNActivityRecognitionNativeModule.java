package com.lta.sensor.services.activitydetection;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.facebook.react.bridge.NativeModule;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.Callback;
import com.google.android.gms.location.DetectedActivity;
import com.lta.sensor.IAppConstants;

import java.util.HashMap;
import java.util.Map;

public class RNActivityRecognitionNativeModule extends ReactContextBaseJavaModule {
    private static final String REACT_CLASS = "ActivityRecognition";
    private ReactApplicationContext mReactContext;
//    BroadcastReceiver broadcastReceiver;
    private String TAG = RNActivityRecognitionNativeModule.class.getSimpleName();

    @Override
    public String getName() {
        return IAppConstants.ActivityDetection;
    }

    public RNActivityRecognitionNativeModule(ReactApplicationContext reactContext) {
        super(reactContext);
        mReactContext = reactContext;

//        broadcastReceiver = new BroadcastReceiver() {
//            @Override
//            public void onReceive(Context context, Intent intent) {
//                if (intent.getAction().equals(Constants.BROADCAST_DETECTED_ACTIVITY)) {
//                    int type = intent.getIntExtra("type", -1);
//                    int confidence = intent.getIntExtra("confidence", 0);
//                    handleUserActivity(type, confidence);
//                }
//            }
//        };
    }

    @Override
    public Map<String, Object> getConstants() {
        final Map<String, Object> constants = new HashMap<>();

        // Export a few common activity types to allow easier mocking.
        constants.put("ANDROID_STILL", DetectedActivity.STILL);
        constants.put("ANDROID_WALKING", DetectedActivity.WALKING);
        constants.put("ANDROID_IN_VEHICLE", DetectedActivity.IN_VEHICLE);

        return constants;
    }


    @ReactMethod
    private void startTracking() {
        Log.i(TAG, "Detected activity 1: ");

        Intent intent = new Intent(mReactContext, BackgroundDetectedActivitiesService.class);
        mReactContext.startService(intent);
    }

    @ReactMethod
    private void stopTracking() {
        Intent intent = new Intent(mReactContext, BackgroundDetectedActivitiesService.class);
        mReactContext.stopService(intent);
    }

}