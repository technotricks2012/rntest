package com.lta.sensor;

import android.app.Application;

import com.facebook.react.PackageList;
import com.facebook.react.ReactApplication;
import com.facebook.stetho.Stetho;
import com.lta.mobilitysensingsdk.sdk.SDKManager;
import com.lta.sensor.services.activitydetection.RNActivityRecognitionNativeModule;
import com.lta.sensor.services.activitydetection.RNActivityRecognitionPackage;
import com.lta.sensor.services.roadDefect.RoadDefectPackage;
import com.lta.sensor.services.storehelper.StorePackage;
import com.reactnativecommunity.netinfo.NetInfoPackage;
import io.invertase.firebase.database.ReactNativeFirebaseDatabasePackage;
import com.transistorsoft.rnbackgroundfetch.RNBackgroundFetchPackage;
import com.reactlibrary.RNDisableBatteryOptimizationsPackage;
import com.airbnb.android.react.maps.MapsPackage;
import com.BV.LinearGradient.LinearGradientPackage;
import io.invertase.firebase.crashlytics.ReactNativeFirebaseCrashlyticsPackage;
import io.invertase.firebase.app.ReactNativeFirebaseAppPackage;
import com.masteratul.exceptionhandler.ReactNativeExceptionHandlerPackage;
import com.swmansion.rnscreens.RNScreensPackage;
import com.swmansion.gesturehandler.react.RNGestureHandlerPackage;
import com.swmansion.reanimated.ReanimatedPackage;
import com.learnium.RNDeviceInfo.RNDeviceInfo;
import com.horcrux.svg.SvgPackage;
import com.rnbiometrics.ReactNativeBiometricsPackage;
import com.lta.sensor.services.location.LocationPackage;
import com.lta.sensor.services.timer.TimeIntervalPackage;
import com.facebook.react.ReactNativeHost;
import com.facebook.react.ReactPackage;
import com.facebook.soloader.SoLoader;

import com.nozbe.watermelondb.WatermelonDBPackage;


import java.util.List;

public class MainApplication extends Application implements ReactApplication {

  private final ReactNativeHost mReactNativeHost = new ReactNativeHost(this) {
    @Override
    public boolean getUseDeveloperSupport() {
      return BuildConfig.DEBUG;
    }

    @Override
    protected List<ReactPackage> getPackages() {
      @SuppressWarnings("UnnecessaryLocalVariable")
      List<ReactPackage> packages = new PackageList(this).getPackages();
      // Packages that cannot be autolinked yet can be added manually here, for example:
       packages.add(new LocationPackage());
      packages.add(new  WatermelonDBPackage());
      packages.add(new TimeIntervalPackage());
      packages.add(new RoadDefectPackage());
      packages.add(new StorePackage());
      packages.add(new RNActivityRecognitionPackage());

      return packages;
    }

    @Override
    protected String getJSMainModuleName() {
      return "index";
    }
  };

  @Override
  public ReactNativeHost getReactNativeHost() {
    return mReactNativeHost;
  }

  @Override
  public void onCreate() {
    super.onCreate();
    SoLoader.init(this, /* native exopackage */ false);

    SDKManager.getINSTANCE().initSdk(getApplicationContext());


    Stetho.initialize(
            Stetho.newInitializerBuilder(this)
                    .enableDumpapp(Stetho.defaultDumperPluginsProvider(this))
//                    .enableWebKitInspector(RealmInspectorModulesProvider.builder(this).build())
                    .enableWebKitInspector(Stetho.defaultInspectorModulesProvider(this))
                    .build());


  }

}
