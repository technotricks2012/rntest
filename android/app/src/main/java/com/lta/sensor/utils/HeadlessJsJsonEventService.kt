package com.lta.sensor.utils

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log

import com.facebook.react.HeadlessJsTaskService
import com.facebook.react.jstasks.HeadlessJsTaskConfig

class HeadlessJsJsonEventService : HeadlessJsTaskService() {

    companion object {

        val TASKKEY = "TASK_KEY1"
        val DATA = "DATA1"

        fun sendEvent(context: Context, taskKey: String, jsonData: String) {

            val myIntent = Intent(context, HeadlessJsJsonEventService::class.java)

            val bundle = Bundle()
            bundle.putString(TASKKEY, taskKey)
            bundle.putSerializable(DATA, jsonData)
            myIntent.putExtras(bundle)
            context.startService(myIntent)
            HeadlessJsTaskService.acquireWakeLockNow(context)

        }

    }


    override fun getTaskConfig(intent: Intent?): HeadlessJsTaskConfig? {


        val bundle = intent?.extras

        val data = bundle?.getSerializable(DATA) as String

        val taskKey = bundle.getString(TASKKEY)

        val mapData = Utils.convertJsonStringToWritableMap(data)



        return HeadlessJsTaskConfig(
                taskKey,
                mapData,
                5000,
                true)
    }
}