package com.lta.sensor.services.storehelper;


import android.app.Activity;
import android.util.Log;

import com.facebook.react.bridge.Promise;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.lta.mobilitysensingsdk.sdk.SDKManager;
import com.lta.sensor.IAppConstants;
import com.lta.sensor.services.roadDefect.RoadDefectHelper;

import javax.annotation.Nonnull;


public class StoreModule extends ReactContextBaseJavaModule {

    private  ReactApplicationContext reactContext;
    private String ModuleName = "StoreModule";

    public StoreModule(@Nonnull ReactApplicationContext reactContext) {
        super(reactContext);
        this.reactContext = reactContext;

        RoadDefectHelper.getINSTANCE().initSdk(reactContext);

    }

    @Nonnull
    @Override
    public String getName() {
        return ModuleName;
    }

    @ReactMethod
    public void saveValue(String key, String value) {
        SDKManager.getINSTANCE().getStoreManager().saveValue(key,value);
    }

    @ReactMethod
    public void getValue(String key, Promise promise) {
        promise.resolve(SDKManager.getINSTANCE().getStoreManager().getValue(key));
    }
}
