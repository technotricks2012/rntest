package com.lta.sensor.services.roadDefect;


import android.app.Activity;
import android.content.Intent;
import android.util.Log;

import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.Callback;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.WritableMap;
import com.lta.mobilitysensingsdk.sdk.SDKManager;
import com.lta.sensor.IAppConstants;
import com.lta.sensor.services.timer.QuickService;
import com.lta.sensor.utils.Utils;

import java.util.Timer;
import java.util.TimerTask;

import javax.annotation.Nonnull;


public class RoadDefectModule extends ReactContextBaseJavaModule {

    private  ReactApplicationContext reactContext;


    public RoadDefectModule(@Nonnull ReactApplicationContext reactContext) {
        super(reactContext);
        this.reactContext = reactContext;

        RoadDefectHelper.getINSTANCE().initSdk(reactContext);

    }

    @Nonnull
    @Override
    public String getName() {
        return IAppConstants.RoadDefect;
    }



    @ReactMethod
    public void startTrip() {
        final Activity activity = getCurrentActivity();
        if (activity != null) {
            RoadDefectHelper.getINSTANCE().startTrip(activity);
        }
    }

    @ReactMethod
    public void stopTrip() {
        final Activity activity = getCurrentActivity();
        if (activity != null) {
            RoadDefectHelper.getINSTANCE().stopTrip();
        }
    }

    @ReactMethod
    public void updateRoadDefect(String type) {
        final Activity activity = getCurrentActivity();
        if (activity != null) {
            RoadDefectHelper.getINSTANCE().updateRoadDefect(type);
        }
    }


}
