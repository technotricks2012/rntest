package com.lta.sensor.utils

import android.content.Context
import android.content.SharedPreferences
import com.facebook.react.bridge.*
import com.facebook.react.modules.core.DeviceEventManagerModule
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject

object Utils {

    fun sendEvent(reactContext: ReactContext,
                  eventName: String,
                  params: WritableMap?) {
        reactContext
                .getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter::class.java)
                .emit(eventName, params)
    }

    fun sendEventJSON(reactContext: ReactContext,
                      eventName: String,
                      data: String) {

        val mapData = convertJsonStringToWritableMap(data)
        reactContext
                .getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter::class.java)
                .emit(eventName, mapData)
    }

    fun sendHeadlessEventJSON(reactContext: ReactContext,
                      eventName: String,
                      data: String) {

        HeadlessJsJsonEventService.sendEvent(reactContext,eventName, data)

    }


    fun mapData(data: Map<String, String>): WritableMap {
        val map = Arguments.createMap()
        for ((key, value) in data) {
            map.putString(key, value)
        }
        return map
    }

    private var PRIVATE_MODE = 0
    private val PREF = "MobilitySensing"

    private val PREF_INTERVAL = "MobilitySensing"

    fun getInterval(context: Context): Int {

        val sharedPref: SharedPreferences = context.getSharedPreferences(PREF, PRIVATE_MODE)
        return sharedPref.getInt(PREF_INTERVAL, -1);
    }

    fun setInterval(context: Context, interval: Int) {

        val sharedPref: SharedPreferences = context.getSharedPreferences(PREF, PRIVATE_MODE)
        val editor = sharedPref.edit()
        editor.putInt(PREF_INTERVAL, interval)
        editor.apply()
    }





    @Throws(JSONException::class)
    fun convertJsonStringToWritableMap(string: String): WritableMap {
        val jsonObject = JSONObject(string)

        val map = WritableNativeMap()

        val iterator = jsonObject.keys()
        while (iterator.hasNext()) {
            val key = iterator.next()
            val value = jsonObject.get(key)
            if (value is JSONObject) {
                map.putMap(key, convertJsonToMap(value))
            } else if (value is JSONArray) {
                map.putArray(key, convertJsonToArray(value))
            } else if (value is Boolean) {
                map.putBoolean(key, value)
            } else if (value is Int) {
                map.putInt(key, value)
            } else if (value is Double) {
                map.putDouble(key, value)
            } else if (value is String) {
                map.putString(key, value)
            } else {
                map.putString(key, value.toString())
            }
        }
        return map
    }


    @Throws(JSONException::class)
    private fun convertJsonToMap(jsonObject: JSONObject): WritableMap? {

        val map: WritableMap = WritableNativeMap()
        val iterator: Iterator<String> = jsonObject.keys()
        while (iterator.hasNext()) {
            val key = iterator.next()
            val value: Any = jsonObject.get(key)
            if (value is JSONObject) {
                map.putMap(key, convertJsonToMap(value as JSONObject))
            } else if (value is JSONArray) {
                map.putArray(key, convertJsonToArray(value as JSONArray))
            } else if (value is Boolean) {
                map.putBoolean(key, value)
            } else if (value is Int) {
                map.putInt(key, value)
            } else if (value is Double) {
                map.putDouble(key, value)
            } else if (value is String) {
                map.putString(key, value)
            } else {
                map.putString(key, value.toString())
            }
        }
        return map
    }

    @Throws(JSONException::class)
    private fun convertJsonToArray(jsonArray: JSONArray): WritableArray? {
        val array: WritableArray = WritableNativeArray()
        for (i in 0 until jsonArray.length()) {
            val value = jsonArray[i]
            if (value is JSONObject) {
                array.pushMap(Utils.convertJsonToMap(value))
            } else if (value is JSONArray) {
                array.pushArray(convertJsonToArray(value))
            } else if (value is Boolean) {
                array.pushBoolean(value)
            } else if (value is Int) {
                array.pushInt(value)
            } else if (value is Double) {
                array.pushDouble(value)
            } else if (value is String) {
                array.pushString(value)
            } else {
                array.pushString(value.toString())
            }
        }
        return array
    }


}
