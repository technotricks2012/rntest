package com.lta.sensor.services.roadDefect

import android.app.Activity
import android.util.Log
import com.facebook.react.bridge.ReactContext
import com.lta.mobilitysensingsdk.sdk.SDKManager
import com.lta.sensor.IAppConstants
import com.lta.sensor.utils.Utils
import io.reactivex.disposables.CompositeDisposable

class RoadDefectHelper {
    companion object {
        @JvmStatic
        val INSTANCE by lazy { RoadDefectHelper() }
    }

    private var context: ReactContext? = null

    fun initSdk(context: ReactContext) {
        this.context = context
    }

    private val disposeBag = CompositeDisposable()
    init {
        observeTripError()
    }

    fun startTrip(activity: Activity) {
        SDKManager.INSTANCE.roadDefect.startRoadDefectLocation()

    }

    fun stopTrip() {
        SDKManager.INSTANCE.roadDefect.stopRoadDefectLocation()
    }

    fun updateRoadDefect(type: String) {
        SDKManager.INSTANCE.roadDefect.updateRoadDefect(type)
    }

    private fun observeTripError() {
        disposeBag.clear()
        disposeBag.add(
                SDKManager.INSTANCE
                        .roadDefect
                        .errorObserver()
                        .map { data ->
                            context?.let {
                                Utils.sendEventJSON(it, IAppConstants.RoadDefect, data)
                            }
                        }
                        .subscribe()
        )

        disposeBag.add(
                SDKManager.INSTANCE
                        .roadDefect
                        .successObserver()
                        .map { data ->
                            Log.d("Location observeTripError", data)
                            context?.let {
                                Utils.sendEventJSON(it, IAppConstants.RoadDefect, data)
                            }
                        }
                        .subscribe()
        )
    }

}