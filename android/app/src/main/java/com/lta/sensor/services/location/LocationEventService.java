package com.lta.sensor.services.location;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;

import com.facebook.react.HeadlessJsTaskService;
import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.WritableMap;
import com.facebook.react.jstasks.HeadlessJsTaskConfig;
import com.lta.sensor.IAppConstants;


public class LocationEventService extends HeadlessJsTaskService {
    @Nullable
    protected HeadlessJsTaskConfig getTaskConfig(Intent intent) {
        Bundle extras = intent.getExtras();
        return new HeadlessJsTaskConfig(
                IAppConstants.LocationPackage,
                extras != null ? Arguments.fromBundle(extras) : null,
                5000,
                true);

    }

    private static WritableMap mapData() {
        WritableMap map = Arguments.createMap();
        map.putString("name", "Kishore");
        map.putString("age", "Maneesha");
        return map;
    }
}