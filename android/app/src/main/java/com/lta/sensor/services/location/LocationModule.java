package com.lta.sensor.services.location;

import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.util.Log;

import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.Callback;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.WritableMap;
import com.lta.sensor.IAppConstants;
import com.lta.sensor.utils.Utils;

import java.util.Timer;
import java.util.TimerTask;

import javax.annotation.Nonnull;


public class LocationModule extends ReactContextBaseJavaModule {

    private static ReactApplicationContext reactContext;
    public BackgroundService gpsService;
    public boolean mTracking = false;

    public LocationModule(@Nonnull ReactApplicationContext reactContext) {
        super(reactContext);
        this.reactContext = reactContext;

//        final Intent intent = new Intent(this.getReactApplicationContext(), BackgroundService.class);
//        this.getReactApplicationContext().startService(intent);
////        this.getApplication().startForegroundService(intent);
//        this.getReactApplicationContext().bindService(intent, serviceConnection, Context.BIND_AUTO_CREATE);
    }

    @Nonnull
    @Override
    public String getName() {
        return IAppConstants.LocationPackage;
    }

    @ReactMethod
    public void startService() {

//        gpsService.startTracking();
//        mTracking = true;

        //PendingIntent pause = PendingIntent.getService(getReactApplicationContext(), PAUSE, pauseIntent, 0);

       Intent intent= new Intent(this.reactContext, LocationService.class);
//       intent.putExtra("LOCATION",IAppConstants.LocationSTART);

       intent.setAction(IAppConstants.LocationSTART);
        this.reactContext.startService(intent);
    }

    @ReactMethod
    public void stopService() {
        Log.i("Location","Stop");

//        mTracking = false;
//        gpsService.stopTracking();

        this.reactContext.stopService(new Intent(this.reactContext, LocationService.class));

    }

    @ReactMethod
    public void readData( final Callback success) {

        new Timer().scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                Utils.INSTANCE.sendEvent(reactContext, "EventReminder", mapData());

//                Log.i("Location","CAll");
            }
        }, 0, 1000);
        success.invoke(mapData());
    }

    private static WritableMap mapData() {
        WritableMap map = Arguments.createMap();
        map.putString("name", "Kishore1");
        map.putString("age", "Maneesha1");
        return map;
    }

    private ServiceConnection serviceConnection = new ServiceConnection() {
        public void onServiceConnected(ComponentName className, IBinder service) {
            String name = className.getClassName();
            if (name.endsWith("BackgroundService")) {
                gpsService = ((BackgroundService.LocationServiceBinder) service).getService();
            }
        }

        public void onServiceDisconnected(ComponentName className) {
            if (className.getClassName().equals("BackgroundService")) {
                gpsService = null;
            }
        }
    };
}
