package com.lta.sensor.services.location;


import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.util.Log;
import android.widget.Toast;

import androidx.core.app.NotificationManagerCompat;
import androidx.core.content.ContextCompat;

import com.lta.sensor.IAppConstants;
import com.lta.sensor.services.timer.QuickService;
import com.lta.sensor.utils.Utils;

public class LocationNotificationBroadcastReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {

//        String action =intent.getAction();
//        Log.d("Here", "I am here"+action);
//
//
//        Intent intentLocation= new Intent(context, QuickService.class);
//
//        intentLocation.setAction(intent.getAction());
//
//
//        if(action.equals(IAppConstants.QuickServiceStop))
//         NotificationManagerCompat.from(context).cancel(LocationService.SERVICE_NOTIFICATION_ID);
//
//
//        context.startService(intentLocation);

       int interval = Utils.INSTANCE.getInterval(context);

        Log.d("BroadcastReceiver", " -> Item restart with the interval"+interval);

        if(interval >0){

            Intent quickStartIntent= new Intent(context, QuickService.class);
            quickStartIntent.putExtra("Interval",interval);
            quickStartIntent.setAction(IAppConstants.QuickServiceStart);


            if (Build.VERSION.SDK_INT>= Build.VERSION_CODES.O) {
                context.startForegroundService(quickStartIntent);
            }
            else {
                context.startService(quickStartIntent);
            }
       }



    }


    /* Start RunAfterBootService service directly and invoke the service every 10 seconds. */
    private void startServiceDirectly(Context context,Intent intent)
    {
        try {
            while (true) {
                String message = "BootDeviceReceiver onReceive start service directly.";

                Toast.makeText(context, message, Toast.LENGTH_LONG).show();

                // This intent is used to start background service. The same service will be invoked for each invoke in the loop.
//                Intent startServiceIntent = new Intent(context, QuickService.class);
//                startServiceIntent.putExtra("Interval",1000);
//                startServiceIntent.setAction(IAppConstants.QuickServiceStart);
//                context.startService(startServiceIntent);

                ComponentName comp = new ComponentName(context.getPackageName(), QuickService.class.getName());
                // Start the service, keeping the device awake while it is launching.
                ContextCompat.startForegroundService(context,intent.setComponent(comp));


                // Current thread will sleep one second.
                Thread.sleep(10000);
            }
        }catch(InterruptedException ex)
        {
            Toast.makeText(context, ex.getMessage(), Toast.LENGTH_LONG).show();
        }
    }
}