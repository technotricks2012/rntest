package com.lta.sensor.utils

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log

import com.facebook.react.HeadlessJsTaskService
import com.facebook.react.jstasks.HeadlessJsTaskConfig

class HeadlessJsEventService : HeadlessJsTaskService() {

    companion object{

        val TASKKEY  = "TASK_KEY"
        val DATA = "DATA"

        fun sendEvent(context: Context,taskKey:String, data: HashMap<String,String>){

            val myIntent = Intent(context, HeadlessJsEventService::class.java)

            val bundle = Bundle()
            bundle.putString(TASKKEY, taskKey)
            bundle.putSerializable(DATA,data)
            myIntent.putExtras(bundle)
            context.startService(myIntent)
            HeadlessJsTaskService.acquireWakeLockNow(context)

        }

    }


    override fun getTaskConfig(intent: Intent?): HeadlessJsTaskConfig? {


        val bundle = intent?.extras

        val data = bundle?.getSerializable(DATA) as HashMap<String, String>

        val taskKey = bundle?.getString(TASKKEY)

        val mapData = Utils.mapData(data)


        return HeadlessJsTaskConfig(
                taskKey,
                mapData,
                5000,
                true)
    }
}