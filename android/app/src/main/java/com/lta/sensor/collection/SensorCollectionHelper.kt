package com.lta.sensor.collection

import com.facebook.react.bridge.ReactContext
import com.lta.mobilitysensingsdk.sdk.SDKManager
import com.lta.sensor.IAppConstants
import com.lta.sensor.utils.Utils
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class SensorCollectionHelper {
    companion object {
        @JvmStatic
        val INSTANCE by lazy { SensorCollectionHelper() }
    }

    private var context: ReactContext? = null


    private val disposeBag = CompositeDisposable()

    init {
       init()
    }

    fun initSdk(context: ReactContext) {
        this.context = context
        init()
    }

    private fun init(){
        SDKManager.INSTANCE.observeData.startObserving()
        observeSensor()
    }

    private fun observeSensor() {
        disposeBag.clear()
        disposeBag.add(
                SDKManager.INSTANCE
                        .observeData
                        .errorObserver()
                        .map { data ->
                            context?.let {
                                Utils.sendHeadlessEventJSON(it, IAppConstants.AllSensorObserver, data)
                            }
                        }
                        .subscribeOn(Schedulers.io())
                        .subscribe()
        )

        disposeBag.add(
                SDKManager.INSTANCE
                        .observeData
                        .successObserver()
                        .map { data ->
                            context?.let {

                                Utils.sendHeadlessEventJSON(it, IAppConstants.AllSensorObserver, data)
                            }
                        }
                        .subscribeOn(Schedulers.io())
                        .subscribe()
        )
    }

}