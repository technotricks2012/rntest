package com.lta.sensor.services.location;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.widget.RemoteViews;

import androidx.core.app.NotificationCompat;

import com.lta.sensor.IAppConstants;
import com.lta.sensor.MainActivity;
import com.lta.sensor.R;
import com.lta.sensor.utils.HeadlessJsEventService;

import java.util.HashMap;


public class LocationService extends Service {

    private LocationListener[] mLocationListener;
    private LocationManager mLocationManager;

    public static final int SERVICE_NOTIFICATION_ID = 12345;
    private static final String CHANNEL_ID = "GeoLocation";

    private final String TAG = "LocationService";
    boolean isGPSEnabled = false;
    boolean isNetworkEnabled = false;


    private final int LOCATION_INTERVAL = 2000;//Milliseconds
    private final int LOCATION_DISTANCE = 5; // Meter

    private class LocationListener implements android.location.LocationListener {
        private Location lastLocation = null;
        private final String TAG = "LocationListener";
        private Location mLastLocation;

        public LocationListener(String provider) {
            mLastLocation = new Location(provider);
        }

        @Override
        public void onLocationChanged(Location location) {
            mLastLocation = location;
            Log.i(TAG, "LocationChanged: " + location);

            Context context = getApplicationContext();
//            Intent myIntent = new Intent(context, LocationEventService.class);
//            Log.i("Location", "Runnable");
//
//            myIntent.putExtra("latitude", location.getLatitude());
//            myIntent.putExtra("longitude", location.getLongitude());
//            myIntent.putExtra("timestamp", location.getTime());
//            myIntent.putExtra("altitude", location.getAltitude());
//
//
//            context.startService(myIntent);
//            HeadlessJsTaskService.acquireWakeLockNow(context);

            HashMap<String,String> data= new HashMap();

            data.put("latitude", ""+ location.getLatitude());
            data.put("longitude",""+ location.getLongitude());
            data.put("timestamp",""+ location.getTime());
            data.put("altitude", ""+ location.getAltitude());
//
             HeadlessJsEventService.Companion.sendEvent(context, IAppConstants.LocationPackage,data);
        }

        @Override
        public void onProviderDisabled(String provider) {
            Log.e(TAG, "onProviderDisabled: " + provider);
        }

        @Override
        public void onProviderEnabled(String provider) {
            Log.e(TAG, "onProviderEnabled: " + provider);
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
            Log.e(TAG, "onStatusChanged: " + status);
        }
    }


    private void createNotificationChannel() {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel(CHANNEL_ID, "GeoLocation", importance);
            channel.setDescription("CHANEL DESCRIPTION");
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        int NOTIFICATION_ID = (int) (System.currentTimeMillis() % 10000);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            startForeground(NOTIFICATION_ID, new Notification.Builder(this, CHANNEL_ID).build());
        }
    }

    @Override
    public void onDestroy() {
        stopLocationService();
    }


    private void startTracking() {
        try {
            if (mLocationManager == null) {
                mLocationManager = (LocationManager) getApplicationContext().getSystemService(Context.LOCATION_SERVICE);
            }

            isGPSEnabled = mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
            isNetworkEnabled = mLocationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);


            mLocationListener = new LocationListener[]{
                    new LocationListener(LocationManager.GPS_PROVIDER),
                    new LocationListener(LocationManager.NETWORK_PROVIDER)};


            if (isGPSEnabled)
                mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, LOCATION_INTERVAL, LOCATION_DISTANCE, mLocationListener[0]);

            if (isNetworkEnabled)
                mLocationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, LOCATION_INTERVAL, LOCATION_DISTANCE, mLocationListener[1]);


        } catch (java.lang.SecurityException ex) {
            Log.i(TAG, "fail to request location update, ignore", ex);
        } catch (IllegalArgumentException ex) {
            Log.d(TAG, "gps provider does not exist " + ex.getMessage());
        }


    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.e(TAG, "Action: onStartCommand");

        if (intent != null) {
            Log.e(TAG, "Action: " + intent.getAction());


            if (intent.getAction().equals(IAppConstants.LocationSTART)) {
                startLocationService();
            } else if (intent.getAction().equals(IAppConstants.LocationSTOP)) {
                stopLocationService();
            }
        }
        return START_STICKY;
    }

    private void startLocationService() {
        startTracking();

        createNotificationChannel();
        Intent notificationIntent = new Intent(this, MainActivity.class);
        PendingIntent contentIntent = PendingIntent.getActivity(this, 0, notificationIntent, PendingIntent.FLAG_CANCEL_CURRENT);
        Notification notification = new NotificationCompat.Builder(this, CHANNEL_ID)
//                .setContentTitle("Services")
//                .setContentText("Running...")
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentIntent(contentIntent)
                .setContent(getRemoteViews())
                .setOngoing(true)
                .build();
        startForeground(SERVICE_NOTIFICATION_ID, notification);
    }

    private void stopLocationService() {
        super.onDestroy();
        //this.handler.removeCallbacks(this.runnableCode);
        if (mLocationManager != null) {
            try {
                mLocationManager.removeUpdates(mLocationListener[0]);
                mLocationManager.removeUpdates(mLocationListener[1]);

            } catch (Exception ex) {
                Log.i(TAG, "fail to remove location listners, ignore", ex);
            }
        }
    }

    private RemoteViews getRemoteViews() {
        RemoteViews remoteViews = new RemoteViews(getPackageName(), R.layout.view_notificaton);


        //Start
        Intent startIntent = new Intent(this, LocationNotificationBroadcastReceiver.class);
        startIntent.setAction(IAppConstants.LocationSTART);

        PendingIntent startPendingIntent = PendingIntent.getBroadcast(this, 0,
                startIntent, 0);

        remoteViews.setOnClickPendingIntent(R.id.btnStartAction, startPendingIntent);

        //Stop
        Intent stopIntent = new Intent(this, LocationNotificationBroadcastReceiver.class);
        stopIntent.setAction(IAppConstants.LocationSTOP);

        PendingIntent stopPendingIntent = PendingIntent.getBroadcast(this, 0,
                stopIntent, 0);

        remoteViews.setOnClickPendingIntent(R.id.btnStopAction, stopPendingIntent);

        return remoteViews;
    }

}
