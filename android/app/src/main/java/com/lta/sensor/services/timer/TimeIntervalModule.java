package com.lta.sensor.services.timer;


import android.app.Activity;
import android.content.Intent;
import android.util.Log;

import com.facebook.react.bridge.Arguments;
import com.facebook.react.bridge.Callback;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.WritableMap;
import com.lta.mobilitysensingsdk.sdk.SDKManager;
import com.lta.sensor.IAppConstants;
import com.lta.sensor.collection.SensorCollectionHelper;
import com.lta.sensor.utils.Utils;

import java.util.Timer;
import java.util.TimerTask;

import javax.annotation.Nonnull;


public class TimeIntervalModule extends ReactContextBaseJavaModule {

    private static ReactApplicationContext reactContext;


    public TimeIntervalModule(@Nonnull ReactApplicationContext reactContext) {
        super(reactContext);
        this.reactContext = reactContext;




    }

    @Nonnull
    @Override
    public String getName() {
        return IAppConstants.QuickServiceInterval;
    }

    @ReactMethod
    public void startService(int interval) {

        Log.i("Time Interval", "Start");

//        Utils.INSTANCE.setInterval(this.reactContext,interval);
////
//        Intent intent= new Intent(this.reactContext, QuickService.class);
//        intent.putExtra("Interval",interval);
//        intent.setAction(IAppConstants.QuickServiceStart);
//
//        if (Build.VERSION.SDK_INT>= Build.VERSION_CODES.O) {
//            this.reactContext.startForegroundService(intent);
//        }
//        else {
//            this.reactContext.startService(intent);
//        }


        SDKManager.getINSTANCE().startService(this.reactContext);
        SensorCollectionHelper.getINSTANCE().initSdk(reactContext);

//        SensorCollectionHelper.getINSTANCE().start();
//
    }

    @ReactMethod
    public void stopService() {
        Log.i("Time Interval", "Stop");
//        this.reactContext.stopService(new Intent(this.reactContext, QuickService.class));

        Utils.INSTANCE.setInterval(this.reactContext, -1);
        Intent intent = new Intent(this.reactContext, QuickService.class);
//        intent.setAction(IAppConstants.QuickServiceStop);
        this.reactContext.stopService(intent);

    }

    @ReactMethod
    public void readData(final Callback success) {
        new Timer().scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                success.invoke(mapData());
            }
        }, 0, 1000);

    }

    @ReactMethod
    public void startRide() {
        final Activity activity = getCurrentActivity();
        if (activity == null) {


            return;
        }
    }

    @ReactMethod
    public void stopRide() {

    }

    private static WritableMap mapData() {
        WritableMap map = Arguments.createMap();
        map.putString("name", "Kishore11");
        map.putString("age", "Maneesha11");
        return map;
    }
}
