package com.lta.sensor.services.timer;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.location.LocationManager;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;
import android.widget.RemoteViews;
import android.widget.Toast;

import androidx.core.app.NotificationCompat;

import com.lta.sensor.IAppConstants;
import com.lta.sensor.MainActivity;
import com.lta.sensor.R;
import com.lta.sensor.services.location.LocationNotificationBroadcastReceiver;
import com.lta.sensor.utils.HeadlessJsEventService;

import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;


public class QuickService extends Service {

    private LocationManager mLocationManager;

    public static final int SERVICE_NOTIFICATION_ID = 54321;
    private static final String CHANNEL_ID = "GeoLocation";

    private final String TAG = "QuickService";


//    Timer myTimer;
//    TimerTask doThis;
//    int delay = 0;
//    int period = 1000;

    public  int notify = 10 * 1000;  //interval between two services(Here Service run every 5 Minute)
    private Handler mHandler = new Handler();   //run on another Thread to avoid crash
    private Timer mTimer = null;    //timer handling


    private void createNotificationChannel() {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel(CHANNEL_ID, "GeoLocation", importance);
            channel.setDescription("CHANEL DESCRIPTION");
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();

//        int NOTIFICATION_ID = (int) (System.currentTimeMillis() % 10000);
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//            startForeground(NOTIFICATION_ID, new Notification.Builder(this, CHANNEL_ID).build());
//        }

//        if (mTimer != null) // Cancel if already existed
//            mTimer.cancel();
//        else
//            mTimer = new Timer();   //recreate new
//        mTimer.scheduleAtFixedRate(new TimeDisplay(), 0, notify);



    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        stopTimerService();

    }

    class TimeDisplay extends TimerTask {
        @Override
        public void run() {
            // run on another thread
            mHandler.post(new Runnable() {
                @Override
                public void run() {
                   sendTrigger("Start");
                }
            });
        }
    }

    private void sendTrigger(String event){
        HashMap<String, String> data = new HashMap();

        data.put("Event", event);
        HeadlessJsEventService.Companion.sendEvent(getApplicationContext(), IAppConstants.QuickServiceInterval, data);

    }


    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {


        Log.e(TAG, "Action: onStartCommand");
        if (intent != null) {
            Log.e(TAG, "Action: " + intent.getAction());


            if (intent.getAction().equals(IAppConstants.QuickServiceStart)) {
                notify = intent.getExtras().getInt("Interval");
                startTimerService();
            } else if (intent.getAction().equals(IAppConstants.QuickServiceStop)) {
                stopTimerService();
            }
        }
//        return START_STICKY;
        return super.onStartCommand(intent, flags, startId);
    }

    private void startTimerService() {

        if (mTimer != null) // Cancel if already existed
            mTimer.cancel();

        mTimer = new Timer();   //recreate new
        if (mTimer != null) {
            mTimer.scheduleAtFixedRate(new TimeDisplay(), 0, notify);

            createNotificationChannel();
            Intent notificationIntent = new Intent(this, MainActivity.class);
            PendingIntent contentIntent = PendingIntent.getActivity(this, 0, notificationIntent, PendingIntent.FLAG_CANCEL_CURRENT);
            Notification notification = new NotificationCompat.Builder(this, CHANNEL_ID)
                .setContentTitle("Sensor Service")
                .setContentText("Running...")
                    .setSmallIcon(R.mipmap.ic_launcher)
//                    .setContentIntent(contentIntent)
//                    .setContent(getRemoteViews())
                    .setOngoing(true)
                    .build();
            startForeground(SERVICE_NOTIFICATION_ID, notification);
        }
    }

    private void stopTimerService() {
        super.onDestroy();
        sendTrigger("Stop");


        if (mTimer != null) // Cancel if already existed
            mTimer.cancel();
    }

    private RemoteViews getRemoteViews() {
        RemoteViews remoteViews = new RemoteViews(getPackageName(), R.layout.view_notificaton);


        //Start
        Intent startIntent = new Intent(this, LocationNotificationBroadcastReceiver.class);
        startIntent.setAction(IAppConstants.QuickServiceStart);

        PendingIntent startPendingIntent = PendingIntent.getBroadcast(this, 0,
                startIntent, 0);

        remoteViews.setOnClickPendingIntent(R.id.btnStartAction, startPendingIntent);

        //Stop
        Intent stopIntent = new Intent(this, LocationNotificationBroadcastReceiver.class);
        stopIntent.setAction(IAppConstants.QuickServiceStop);

        PendingIntent stopPendingIntent = PendingIntent.getBroadcast(this, 0,
                stopIntent, 0);

        remoteViews.setOnClickPendingIntent(R.id.btnStopAction, stopPendingIntent);

        return remoteViews;
    }

}
