package com.lta.sensor.services.activitydetection;


import android.app.IntentService;
import android.content.Intent;
import android.util.Log;

import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.google.android.gms.location.ActivityRecognitionResult;
import com.google.android.gms.location.DetectedActivity;
import com.lta.sensor.IAppConstants;
import com.lta.sensor.utils.HeadlessJsEventService;

import java.util.ArrayList;
import java.util.HashMap;

public class DetectedActivitiesIntentService extends IntentService {

    protected static final String TAG = DetectedActivitiesIntentService.class.getSimpleName();

    public DetectedActivitiesIntentService() {
        // Use the TAG to name the worker thread.
        super(TAG);
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @SuppressWarnings("unchecked")
    @Override
    protected void onHandleIntent(Intent intent) {
        ActivityRecognitionResult result = ActivityRecognitionResult.extractResult(intent);

        ArrayList<DetectedActivity> detectedActivities = (ArrayList) result.getProbableActivities();

        for (DetectedActivity activity : detectedActivities) {
            Log.i(TAG, "Detected activity: " + activity.getType() + ", " + activity.getConfidence());
            broadcastActivity(activity);
        }
    }

    private void broadcastActivity(DetectedActivity activity) {
//        Intent intent = new Intent(Constants.BROADCAST_DETECTED_ACTIVITY);
//        intent.putExtra("type", activity.getType());
//        intent.putExtra("confidence", activity.getConfidence());
//        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);


        handleUserActivity(activity.getType(), activity.getConfidence());
    }

    private void handleUserActivity(int type, int confidence) {
        String label = "UNKNOWN";

        switch (type) {
            case DetectedActivity.IN_VEHICLE: {
                label = "IN_VEHICLE";
                break;
            }
            case DetectedActivity.ON_BICYCLE: {
                label = "ON_BICYCLE";
                break;
            }
            case DetectedActivity.ON_FOOT: {
                label = "ON_FOOT";
                break;
            }
            case DetectedActivity.RUNNING: {
                label = "RUNNING";
                break;
            }
            case DetectedActivity.STILL: {
                label = "STILL";
                break;
            }
            case DetectedActivity.TILTING: {
                label = "TILTING";
                break;
            }
            case DetectedActivity.WALKING: {
                label = "WALKING";
                break;
            }
            case DetectedActivity.UNKNOWN: {
                label = "UNKNOWN";
                break;
            }
        }

        if (confidence > Constants.CONFIDENCE) {
            Log.e(TAG, "User activity: " + label + ", Confidence: " + confidence);
            HashMap<String, String> data = new HashMap();

            data.put("DetectedType", label);
            data.put("Confidence", confidence + "");
            HeadlessJsEventService.Companion.sendEvent(getApplicationContext(), IAppConstants.ActivityDetection, data);

        }
    }
}