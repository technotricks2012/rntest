package com.lta.network

import android.content.Context
import com.chuckerteam.chucker.api.ChuckerInterceptor
import com.lta.network.constants.Timeouts
import com.lta.network.endpoints.MspApiEndpoint
import com.lta.network.endpoints.MspApiEndpointImpl
import com.lta.network.services.MspApi
import com.lta.network.utils.RequestAuthorizer
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.converter.jackson.JacksonConverterFactory
import java.util.concurrent.TimeUnit

class SDKApi : Api {


    private var context: Context? = null

    private lateinit var retrofit: Retrofit

    //Services
    private lateinit var mspApiService: MspApi


    // Endpoints
    private lateinit var mspApiEndpoint: MspApiEndpoint

    // Auth
    private var publicKey = ""
    private var privateKey = ""
    private var requestAuthorizer: Interceptor? = null


    override val mspApi: MspApiEndpoint
        get() = mspApiEndpoint


    companion object {

        @JvmStatic
        val INSTANCE by lazy {
            SDKApi()
        }


    }

    init {
        init()
    }

    private fun init() {
        initRetrofit()
        initServices()
        initEndpoints()
    }


    override fun initContext(context: Context) {
        this.context = context
    }


    override fun init(publicKey: String, privateKey: String) {
        if (isInitializationRequired(publicKey = publicKey, privateKey = privateKey)) {
            initAuthorizer(publicKey = publicKey, privateKey = privateKey)
            init()
        }
    }

    private fun initAuthorizer(publicKey: String, privateKey: String) {
        this.publicKey = publicKey
        this.privateKey = privateKey

        requestAuthorizer = RequestAuthorizer(
                publicKey = publicKey,
                privateKey = privateKey
        )
    }

    private fun isInitializationRequired(publicKey: String, privateKey: String): Boolean {
        return ((this.publicKey != publicKey) || (this.privateKey != privateKey))
    }


    private fun initRetrofit() {
        retrofit = Retrofit.Builder()
                .baseUrl(BuildConfig.API_BASE_URL)
                .client(initOkHttpClient())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.createAsync())
//                .addConverterFactory(JacksonConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())

                .build()
    }


    private fun initOkHttpClient(): OkHttpClient {
        return OkHttpClient.Builder()
                .connectTimeout(Timeouts.CONNECT, TimeUnit.SECONDS)
                .readTimeout(Timeouts.READ, TimeUnit.SECONDS)
                .writeTimeout(Timeouts.WRITE, TimeUnit.SECONDS)
                .also(::addExtraClientConfig)
                .build()
    }

    private fun addExtraClientConfig(builder: OkHttpClient.Builder) {
        if (BuildConfig.API_DEBUG_MODE) {
          //  builder.addInterceptor(HttpLoggingInterceptor().also { it.level = HttpLoggingInterceptor.Level.BODY })
            context?.let {

                builder.addInterceptor(ChuckerInterceptor(it))
            }

        }

//        requestAuthorizer?.let { builder.addInterceptor(it) }
    }


    private fun initServices() {
        mspApiService = retrofit.create(MspApi::class.java)
    }

    private fun initEndpoints() {
        mspApiEndpoint = MspApiEndpointImpl(mspApiService)
    }

}