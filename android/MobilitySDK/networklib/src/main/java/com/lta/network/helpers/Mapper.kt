package com.lta.network.helpers

import android.os.SystemClock
import com.lta.datalib.exceptions.NoResultError
import com.lta.datalib.interfaces.sensor.IDeviceInfoResource
import com.lta.datalib.model.AllSensorData
import com.lta.datalib.model.CLocation
import com.lta.datalib.model.device.CPower
import com.lta.datalib.model.roadDefect.*
import com.lta.datalib.model.sensor.*
import com.lta.datalib.utils.Response
import com.lta.datalib.utils.TimeUtils
import com.lta.network.model.SaveWayPoints
import com.lta.network.requests.*
import com.lta.network.requests.Location
import com.lta.network.requests.saveWayPoints.*
import com.lta.network.responses.SaveRoadInspectionResponse
import com.lta.network.responses.SaveWayPointsResponse
import com.lta.network.responses.base.BaseDataResponse

//
//internal fun SaveWayPointsResponse.toSingleSaveWayPointsResponse(): Response<SaveWayPoints, Throwable> {
//    return this.toSingleItemResponse {
//        it
//    }
//}

//
//internal fun SaveRoadInspectionResponse.toSingleRoadInspectionResponse(): Response<SaveRoadInspection, Throwable> {
//    return this.toSingleItemResponse {
//        it
//    }
//}

internal fun <T, R> BaseDataResponse<T>.toSingleItemResponse(resultMapper: (T) -> R): Response<R, Throwable> {
    val hasDataItems = (this.hasData && (this.data?.results?.isNotEmpty() ?: false))
    this.hasData
    return when {
        this.isErroneous -> Response.error(this.message ?: "")
        hasDataItems -> Response.result(resultMapper(this.data!!.results[0]))
        else -> Response.error(NoResultError(""))
    }
}


internal fun <T, R> BaseDataResponse<T>.toResponse(resultMapper: (List<T>) -> R): Response<R, Throwable> {
    return if (this.isErroneous) {
        Response.error(this.message ?: "")
    } else {
        Response.result(resultMapper(this.data?.results ?: kotlin.collections.emptyList()))
    }
}


internal fun RoadDefectData.toSaveRoadInspectionRequest(): SaveRoadInspectionRequest =
        SaveRoadInspectionRequest(
                packageId = this.packageId,
                deviceID = this.deviceID,
                roadDefects = this.roadDefect?.map { it.toRoadDefect() }
        )


internal fun CRoadDefectEmbedded.toRoadDefect(): RoadDefect =
        RoadDefect(
                rideId = this.roadDefect.id.toString(),
                startRide = this.roadDefectInfos.toStartRide(),
                endRide = this.roadDefectInfos.toEndRide(),
                defects = this.defects.map { it.toCDefectEmbedded() }
        )


internal fun List<CRoadDefectInfoEmbedded>.toStartRide(): Ride =
        Ride(
                timestamp = this.find { it.roadDefectInfo.isStart == true }?.roadDefectInfo?.timestamp.toString(),
                locations = this.find { it.roadDefectInfo.isStart == true }?.location?.map { it.toLocation() }
        )

internal fun List<CRoadDefectInfoEmbedded>.toEndRide(): Ride =
        Ride(
                timestamp = this.find { it.roadDefectInfo.isStart == false }?.roadDefectInfo?.timestamp.toString(),
                locations = this.find { it.roadDefectInfo.isStart == false }?.location?.map { it.toLocation() }
        )


internal fun CDefectEmbedded.toCDefectEmbedded(): Defect =
        Defect(
                timestamp = this.defect.timestamp.toString(),
                defectID = this.defect.id.toString(),
                defectMode = this.defect.defectType,
                locations = this.locations.map { it.toLocation() }
        )


internal fun CDefectLocation.toLocation(): Location =
        Location(
                timestamp = this.timestamp.toString(),
                latitude = this.latitude.toString(),
                longitude = this.longitude.toString(),
                altitude = this.altitude.toString(),
                accuracy = this.accuracy.toString()
        )

internal fun CAccelerometer.toAccelerometer(): Accelerometer =
        Accelerometer(
                timestamp = this.timestamp.toString(),
                x = this.x.toString(),
                y = this.y.toString(),
                z = this.z.toString(),
                mode = this.mode,
                predictionMode = this.predictionMode,
                journeyID = this.journeyID
        )


internal fun CAccelerometerLinear.toAccelerometerLinear(): AccelerometerLinear =
        AccelerometerLinear(
                timestamp = this.timestamp.toString(),
                x = this.x.toString(),
                y = this.y.toString(),
                z = this.z.toString(),
                mode = this.mode,
                predictionMode = this.predictionMode,
                journeyID = this.journeyID
        )


internal fun CMagnetometer.toMagnetometer(): Magnetometer =
        Magnetometer(
                timestamp = this.timestamp.toString(),
                x = this.x.toString(),
                y = this.y.toString(),
                z = this.z.toString(),
                mode = this.mode,
                predictionMode = this.predictionMode,
                journeyID = this.journeyID
        )


internal fun CGyroscope.toGyroscope(): Gyroscope =
        Gyroscope(
                timestamp = this.timestamp.toString(),
                x = this.x.toString(),
                y = this.y.toString(),
                z = this.z.toString(),
                mode = this.mode,
                predictionMode = this.predictionMode,
                journeyID = this.journeyID
        )


internal fun COrientationAxes.toOrientationAxes(): OrientationAxes =
        OrientationAxes(
                timestamp = this.timestamp.toString(),
                x = this.x.toString(),
                y = this.y.toString(),
                z = this.z.toString(),
                mode = this.mode,
                predictionMode = this.predictionMode,
                journeyID = this.journeyID
        )


internal fun CRotationVector.toRotationVector(): RotationVector =
        RotationVector(
                timestamp = this.timestamp.toString(),
                x = this.x.toString(),
                y = this.y.toString(),
                z = this.z.toString(),
                rotationVector = this.rotationVector.toString(),
                mode = this.mode,
                predictionMode = this.predictionMode,
                journeyID = this.journeyID
        )


internal fun CBarometer.toBarometer(): Barometer =
        Barometer(
                timestamp = this.timestamp.toString(),
                pressure = this.pressure.toString(),
                mode = this.mode,
                predictionMode = this.predictionMode,
                journeyID = this.journeyID
        )


internal fun CPedometer.toPedometer(): Pedometer =
        Pedometer(
                timestamp = this.timestamp.toString(),
                steps = this.steps.toString(),
                distance = this.distance.toString(),
                mode = this.mode,
                predictionMode = this.predictionMode,
                journeyID = this.journeyID
        )


internal fun CLocation.toLocationV2(): com.lta.network.requests.saveWayPoints.Location =
        com.lta.network.requests.saveWayPoints.Location(
                timestamp = this.timestamp.toString(),
                latitude = this.latitude.toString(),
                longitude = this.longitude.toString(),
                altitude = this.altitude.toString(),
                accuracy = this.accuracy.toString(),
                mode = this.mode,
                predictionMode = this.predictionMode,
                journeyID = this.journeyID
        )

internal fun CPower.toPowerState(): PowerState =
        PowerState(
                lowPowerMode = this.lowPowerMode,
                batteryState = this.batteryState,
                batteryLevel = this.batteryLevel
        )


internal fun IDeviceInfoResource.toDeviceInfo(): DeviceInfo =
        DeviceInfo(
                model = this.getModel(),
                powerState = this.getPower()?.toPowerState(),
                osVersion = this.getSystemVersion().toString(),
                hardware = this.getHardware(),
                deviceID = this.getDeviceId(),
                userAgent = null,
                isEmulator = this.isEmulator(),
                deviceName = this.getDeviceName()
        )


internal fun AllSensorData.toSaveWayPointRequest(): SaveWayPointsRequest =
        SaveWayPointsRequest(
                userID = this.storeDb?.getUserId(),
                timestamp = TimeUtils.currentTime(),
                batchID = this.batchId.toString(),
                deviceInfo = this.deviceInfo?.toDeviceInfo(),
                accelerometer = this.accelerometer?.map { it.toAccelerometer() },//?.take(3),//TODO Remove the Take
                accelerometerLinear = this.accelerometerLinear?.map { it.toAccelerometerLinear() },//?.take(3),//TODO Remove the Take
                magnetometer = this.magnetometer?.map { it.toMagnetometer() },//?.take(3),//TODO Remove the Take
                gyroscope = this.gyroscope?.map { it.toGyroscope() },//?.take(3),//TODO Remove the Take
                orientationAxes = this.orientationAxes?.map { it.toOrientationAxes() },//?.take(3),//TODO Remove the Take
                rotationVector = this.rotationVector?.map { it.toRotationVector() },//?.take(3),//TODO Remove the Take
                barometer = this.barometer?.map { it.toBarometer() },//?.take(3),//TODO Remove the Take
                pedometer = this.pedometer?.map { it.toPedometer() },//?.take(3)//TODO Remove the Take
                location = this.location?.map { it.toLocationV2() }
        )
