package com.lta.network

import android.content.Context
import com.lta.network.endpoints.MspApiEndpoint

interface Api {

    val mspApi : MspApiEndpoint

    fun init(publicKey : String, privateKey : String)

    fun initContext(context : Context)


}