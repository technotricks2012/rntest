package com.lta.network.endpoints

import com.lta.network.requests.SaveRoadInspectionRequest
import com.lta.network.requests.saveWayPoints.SaveWayPointsRequest
import com.lta.network.responses.SaveRoadInspectionResponse
import com.lta.network.responses.SaveWayPointsResponse
import io.reactivex.Single

interface MspApiEndpoint :Endpoint{
    fun saveWayPoints(body : SaveWayPointsRequest) : Single<SaveWayPointsResponse>

    fun saveRoadInspection(body : SaveRoadInspectionRequest) : Single<SaveRoadInspectionResponse>

}