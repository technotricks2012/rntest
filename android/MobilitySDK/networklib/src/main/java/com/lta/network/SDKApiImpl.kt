package com.lta.network

import com.lta.datalib.extenstions.applyIOWorkSchedulers
import com.lta.datalib.extenstions.asSingle
import com.lta.datalib.extenstions.resultOrError
import com.lta.datalib.extenstions.successfulResponseOrError
import com.lta.datalib.interfaces.network.IMspApi
import com.lta.datalib.model.AllSensorData
import com.lta.datalib.model.roadDefect.RoadDefectData
import com.lta.datalib.utils.Response
import com.lta.network.helpers.toSaveRoadInspectionRequest
import com.lta.network.helpers.toSaveWayPointRequest
import io.reactivex.Single

class SDKApiImpl : IMspApi {
    override fun saveWayPoints(data: AllSensorData): Single<Response<String, Throwable>> {
        return SDKApi.INSTANCE.mspApi.saveWayPoints(data.toSaveWayPointRequest())
                .flatMap {
                    resultOrError {
                        it.batchId
                    }.asSingle()
                }
                .applyIOWorkSchedulers()

    }

    override fun saveRoadInspection(data: RoadDefectData): Single<Response<List<String>, Throwable>> {
        return SDKApi.INSTANCE.mspApi.saveRoadInspection(data.toSaveRoadInspectionRequest())
                .flatMap {
                    resultOrError {
                    it.rideIds
                    }.asSingle()
                }
                .applyIOWorkSchedulers()
    }

}