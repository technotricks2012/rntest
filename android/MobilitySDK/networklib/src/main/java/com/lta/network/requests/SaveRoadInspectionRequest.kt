package com.lta.network.requests

import com.google.gson.annotations.SerializedName


data class SaveRoadInspectionRequest(
        @SerializedName("packageId")
        val packageId: String?,
        @SerializedName("deviceID")
        val deviceID: String?,
        @SerializedName("roadDefects")
        val roadDefects: List<RoadDefect>?
)


data class RoadDefect(
        @SerializedName("rideId")
        val rideId: String?,
        @SerializedName("startRide")
        val startRide: Ride?,
        @SerializedName("endRide")
        val endRide: Ride?,
        @SerializedName("defects")
        val defects: List<Defect?>?
)

data class Ride(
        @SerializedName("timestamp")
        val timestamp: String?,
        @SerializedName("location")
        val locations: List<Location>?
)

data class Location(
        @SerializedName("timestamp")
        val timestamp: String?,
        @SerializedName("latitude")
        val latitude: String?,
        @SerializedName("longitude")
        val longitude: String?,
        @SerializedName("altitude")
        val altitude: String?,
        @SerializedName("accuracy")
        var accuracy: String?
)

data class Defect(
        @SerializedName("timestamp")
        val timestamp: String?,
        @SerializedName("defectID")
        val defectID: String?,
        @SerializedName("defectMode")
        val defectMode: String?,
        @SerializedName("locations")
        val locations: List<Location>?
)