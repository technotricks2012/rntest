package com.lta.network.requests.saveWayPoints

import com.google.gson.annotations.SerializedName


data class RotationVector(

        @SerializedName("x")
        val x: String?,

        @SerializedName("y")
        val y: String?,

        @SerializedName("z")
        val z: String?,

        @SerializedName("rotationVector")
        val rotationVector: String?,

        @SerializedName("timestamp")
        val timestamp: String?,

        @SerializedName("mode")
        val mode: String?,

        @SerializedName("predictionMode")
        val predictionMode: String?,

        @SerializedName("journeyID")
        val journeyID: String?
)