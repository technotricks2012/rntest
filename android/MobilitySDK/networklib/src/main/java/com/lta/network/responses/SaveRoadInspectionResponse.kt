package com.lta.network.responses

import com.google.gson.annotations.SerializedName
import com.lta.network.responses.base.BaseDataResponse

//
//class SaveRoadInspectionResponse : BaseDataResponse<SaveRoadInspection>()
//


data class SaveRoadInspectionResponse(
        @SerializedName(Properties.RIDE_IDs) var rideIds: List<String> = emptyList(),
        @SerializedName(Properties.STATUSCODE) var statuscode: String = "",
        @SerializedName(Properties.MESSAGE) var msg: String = ""
) {
    object Properties {
        const val RIDE_IDs = "rideIds"
        const val STATUSCODE = "statuscode"
        const val MESSAGE = "msg"
    }
}