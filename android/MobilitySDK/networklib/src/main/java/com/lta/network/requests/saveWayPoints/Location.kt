package com.lta.network.requests.saveWayPoints

import com.google.gson.annotations.SerializedName


data class Location(
        @SerializedName("latitude")
        val latitude: String?,
        @SerializedName("longitude")
        val longitude: String?,
        @SerializedName("altitude")
        val altitude: String?,
        @SerializedName("accuracy")
        var accuracy: String?,

        @SerializedName("timestamp")
        val timestamp: String?,

        @SerializedName("mode")
        val mode: String?,

        @SerializedName("predictionMode")
        val predictionMode: String?,

        @SerializedName("journeyID")
        val journeyID: String?


)