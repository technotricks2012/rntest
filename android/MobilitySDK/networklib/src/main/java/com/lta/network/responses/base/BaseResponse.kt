package com.lta.network.responses.base

import android.text.TextUtils
import com.fasterxml.jackson.annotation.JsonIgnore
import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonProperty
import com.lta.network.Statuses

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
abstract class BaseResponse(
        @JsonProperty(Properties.CODE) var code : Int = 0,
        @JsonProperty(Properties.STATUS) var status : String? = null,
        @JsonProperty(Properties.MESSAGE) var message : String? = null
) {


    @get:JsonIgnore
    val hasStatus : Boolean
        get() = !TextUtils.isEmpty(status)

    @get:JsonIgnore
    val isErroneous : Boolean
        get() = !Statuses.OK.equals(status, ignoreCase = true)


    object Properties {

        const val CODE = "statuscode"
        const val STATUS = "status"
        const val MESSAGE = "msg"

    }


}