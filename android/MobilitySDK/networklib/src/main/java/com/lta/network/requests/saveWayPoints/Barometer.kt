package com.lta.network.requests.saveWayPoints

import com.google.gson.annotations.SerializedName


data class Barometer(

        @SerializedName("pressure")
        val pressure: String?,

        @SerializedName("timestamp")
        val timestamp: String?,

        @SerializedName("mode")
        val mode: String?,

        @SerializedName("predictionMode")
        val predictionMode: String?,

        @SerializedName("journeyID")
        val journeyID: String?
)