package com.lta.network.constants

internal object Timeouts {

    const val CONNECT = 60L
    const val READ = 60L
    const val WRITE = 60L

}