package com.lta.network.requests.saveWayPoints

import com.google.gson.annotations.SerializedName


data class DeviceInfo(
        @SerializedName("model")
        val model: String?,
        @SerializedName("powerState")
        val powerState: PowerState?,
        @SerializedName("osVersion")
        val osVersion: String?,
        @SerializedName("hardware")
        val hardware: String?,
        @SerializedName("deviceID")
        val deviceID: String?,
        @SerializedName("userAgent")
        val userAgent: String?,
        @SerializedName("isEmulator")
        val isEmulator: Boolean?,
        @SerializedName("deviceName")
        val deviceName: String?
)