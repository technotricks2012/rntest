package com.lta.network.endpoints

import com.lta.network.requests.SaveRoadInspectionRequest
import com.lta.network.requests.saveWayPoints.SaveWayPointsRequest
import com.lta.network.responses.SaveRoadInspectionResponse
import com.lta.network.responses.SaveWayPointsResponse
import com.lta.network.services.MspApi
import io.reactivex.Single

internal class MspApiEndpointImpl(mspApi: MspApi) : AbstractEndpoint<MspApi>(mspApi), MspApiEndpoint {
    override fun saveWayPoints(body: SaveWayPointsRequest): Single<SaveWayPointsResponse> {
        return service.saveWayPoints(body.batchID ?: "", body)
    }

    override fun saveRoadInspection(body: SaveRoadInspectionRequest): Single<SaveRoadInspectionResponse> {
        val url = "https://1zmswretz9.execute-api.ap-southeast-1.amazonaws.com/MspDev/saveroadinspectiondatav1"
        return service.saveRoadInspectionData(url, body)
    }
}
