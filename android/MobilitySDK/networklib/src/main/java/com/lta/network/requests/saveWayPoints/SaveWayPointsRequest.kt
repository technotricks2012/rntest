package com.lta.network.requests.saveWayPoints

import com.google.gson.annotations.SerializedName


data class SaveWayPointsRequest(

        @SerializedName("userID")
        val userID: String?,

        @SerializedName("timestamp")
        val timestamp: Long?,

        @SerializedName("batchID")
        val batchID: String?,

        @SerializedName("deviceInfo")
        val deviceInfo: DeviceInfo?,

        @SerializedName("accelerometer")
        val accelerometer: List<Accelerometer?>?,

        @SerializedName("accelerometerLinear")
        val accelerometerLinear: List<AccelerometerLinear?>?,

        @SerializedName("magnetometer")
        val magnetometer: List<Magnetometer?>?,

        @SerializedName("gyroscope")
        val gyroscope: List<Gyroscope?>?,

        @SerializedName("orientationAxes")
        val orientationAxes: List<OrientationAxes?>?,

        @SerializedName("rotationVector")
        val rotationVector: List<RotationVector?>?,

        @SerializedName("barometer")
        val barometer: List<Barometer?>?,

        @SerializedName("pedometer")
        val pedometer: List<Pedometer?>?,

        @SerializedName("location")
        val location: List<Location?>?

)