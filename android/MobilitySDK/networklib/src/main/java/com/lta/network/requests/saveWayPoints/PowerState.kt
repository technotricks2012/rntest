package com.lta.network.requests.saveWayPoints

import com.google.gson.annotations.SerializedName


data class PowerState(
        @SerializedName("lowPowerMode")
        val lowPowerMode: Boolean?,
        @SerializedName("batteryLevel")
        val batteryLevel: Float?,
        @SerializedName("batteryState")
        val batteryState: String?
)
