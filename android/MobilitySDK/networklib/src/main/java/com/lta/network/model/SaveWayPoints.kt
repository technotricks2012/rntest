package com.lta.network.model


import com.google.gson.annotations.SerializedName


data class SaveWayPoints(
        @SerializedName(Properties.BATCHID) var batchId: String = "",
        @SerializedName(Properties.STATUSCODE) var statuscode: String = "",
        @SerializedName(Properties.MESSAGE) var msg: String = ""
) {
    object Properties {
        const val BATCHID = "batchId"
        const val STATUSCODE = "statuscode"
        const val MESSAGE = "msg"
    }
}