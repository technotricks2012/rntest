//package com.lta.network.model
//
//
//import com.google.gson.annotations.SerializedName
//
//
//data class SaveRoadInspection(
//        @SerializedName(Properties.RIDE_IDs) var rideIds: List<String> = emptyList()
//        @SerializedName(Properties.STATUSCODE) var statuscode: String = "",
//        @SerializedName(Properties.MESSAGE) var msg: String = ""
//) {
//    object Properties {
//        const val RIDE_IDs = "rideIds"
//        const val STATUSCODE = "statuscode"
//        const val MESSAGE = "msg"
//    }
//}