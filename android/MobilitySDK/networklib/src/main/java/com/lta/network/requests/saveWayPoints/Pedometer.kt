package com.lta.network.requests.saveWayPoints

import com.google.gson.annotations.SerializedName


data class Pedometer(

        @SerializedName("steps")
        val steps: String?,

        @SerializedName("distance")
        val distance: String?,

        @SerializedName("timestamp")
        val timestamp: String?,

        @SerializedName("mode")
        val mode: String?,

        @SerializedName("predictionMode")
        val predictionMode: String?,

        @SerializedName("journeyID")
        val journeyID: String?
)