package com.lta.network.services

import com.lta.network.requests.SaveRoadInspectionRequest
import com.lta.network.requests.saveWayPoints.SaveWayPointsRequest
import com.lta.network.responses.SaveRoadInspectionResponse
import com.lta.network.responses.SaveWayPointsResponse
import io.reactivex.Single
import retrofit2.http.Body
import retrofit2.http.Header
import retrofit2.http.POST
import retrofit2.http.Url


interface MspApi {

    @POST("SaveWaypointsV1")
    fun saveWayPoints(@Header("BatchID")batchId:String,@Body data: SaveWayPointsRequest): Single<SaveWayPointsResponse>

    @POST
    fun saveRoadInspectionData(@Url url:String, @Body body: SaveRoadInspectionRequest): Single<SaveRoadInspectionResponse>


}