package com.lta.network.responses

//class SaveWayPointsResponse : BaseDataResponse<SaveWayPoints>()


import com.google.gson.annotations.SerializedName


data class SaveWayPointsResponse(
        @SerializedName(Properties.BATCHID) var batchId: String = "",
        @SerializedName(Properties.STATUSCODE) var statuscode: String = "",
        @SerializedName(Properties.MESSAGE) var msg: String = ""
) {
    object Properties {
        const val BATCHID = "batchId"
        const val STATUSCODE = "statuscode"
        const val MESSAGE = "msg"
    }
}