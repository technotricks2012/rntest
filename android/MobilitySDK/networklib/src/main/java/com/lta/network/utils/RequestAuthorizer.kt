package com.lta.network.utils

import com.lta.network.endpoints.EndpointPaths
import okhttp3.HttpUrl
import okhttp3.Interceptor
import okhttp3.Request
import okhttp3.Response


internal class RequestAuthorizer(
        val publicKey : String,
        val privateKey : String
) : Interceptor {


    override fun intercept(chain : Interceptor.Chain) : Response {
        return chain.proceed(chain.request().authorize())
    }


    private fun Request.authorize() : Request {
        return this.newBuilder()
                .url(this.createAuthorizedUrl())
                .build()
    }


    private fun Request.createAuthorizedUrl() : HttpUrl {
        val timestampInMillis = System.currentTimeMillis()

        return this.url
                .newBuilder()
                .addQueryParameter(EndpointPaths.Params.API_KEY, publicKey)
                .addQueryParameter(EndpointPaths.Params.HASH, timestampInMillis.createRequestHash(publicKey, privateKey))
                .addQueryParameter(EndpointPaths.Params.TIMESTAMP, timestampInMillis.toString())
                .build()
    }


}