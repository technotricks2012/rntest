package com.lta.locationlib.helpers

import android.location.Location
import com.lta.datalib.model.CLocation


fun Location.toCLocation(): CLocation =
        CLocation(
                timestamp = this.time,
                latitude = this.latitude,
                longitude = this.longitude,
                altitude = this.altitude,
                accuracy = this.accuracy,
                isMock = this.isFromMockProvider
        )