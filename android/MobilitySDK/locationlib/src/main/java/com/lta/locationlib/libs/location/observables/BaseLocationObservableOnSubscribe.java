package com.lta.locationlib.libs.location.observables;

import com.google.android.gms.location.LocationServices;

public abstract class BaseLocationObservableOnSubscribe<T> extends BaseObservableOnSubscribe<T> {
    protected BaseLocationObservableOnSubscribe(ObservableContext ctx) {
        super(ctx, LocationServices.API);
    }
}
