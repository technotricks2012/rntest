package com.lta.locationlib.implementations

import com.lta.datalib.model.CLocation
import com.lta.datalib.utils.Response
import io.reactivex.Flowable
import io.reactivex.Single

interface LocationUpdateSource :Source{
    fun get() : Flowable<Response<CLocation, Throwable>>
    fun getList(batchId:Long) : Flowable<Response<List<CLocation>, Throwable>>
}