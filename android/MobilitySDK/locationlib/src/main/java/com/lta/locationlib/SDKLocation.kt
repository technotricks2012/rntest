package com.lta.locationlib

import android.content.Context
import com.lta.datalib.interfaces.SdkBase
import com.lta.datalib.interfaces.location.ILocation
import com.lta.locationlib.implementations.LocationUpdateImpl
import com.lta.locationlib.libs.location.ReactiveLocationProvider
import com.lta.locationlib.libs.location.ReactiveLocationProviderConfiguration

class SDKLocation : SdkBase {

    private lateinit var locationProvider: ReactiveLocationProvider


    // Source
    private lateinit var locationUpdateSource: ILocation
    var context: Context? = null

    companion object {

        @JvmStatic
        val INSTANCE by lazy {
            SDKLocation()
        }
    }

    init {
        init()
    }

    fun getLocation(): ILocation= locationUpdateSource

    fun getLocationProvider(): ReactiveLocationProvider = locationProvider


    private fun init() {
        initLocationProvider()
        initSource()
    }

    private fun initLocationProvider() {
        locationProvider = ReactiveLocationProvider(this.context, ReactiveLocationProviderConfiguration
                .builder()
                .setRetryOnConnectionSuspended(true)
                .build()
        )

    }

    private fun initSource() {
        locationUpdateSource = LocationUpdateImpl()
    }

    override fun initContext(context: Context) {
        this.context = context
        init()
    }


}