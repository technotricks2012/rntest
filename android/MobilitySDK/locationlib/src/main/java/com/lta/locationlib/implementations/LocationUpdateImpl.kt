package com.lta.locationlib.implementations

import android.Manifest
import android.content.IntentSender
import android.content.pm.PackageManager
import android.util.Log
import androidx.core.app.ActivityCompat
import com.google.android.gms.common.api.Status
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationSettingsRequest
import com.google.android.gms.location.LocationSettingsStatusCodes
import com.lta.datalib.exceptions.LocationPermissionError
import com.lta.datalib.extenstions.resultOrError
import com.lta.datalib.interfaces.location.ILocation
import com.lta.datalib.model.CLocation
import com.lta.datalib.utils.Response
import com.lta.locationlib.SDKLocation
import com.lta.locationlib.helpers.toCLocation
import io.reactivex.Observable

class LocationUpdateImpl : ILocation {
    override fun get(): Observable<Response<CLocation, Throwable>> {

        val locationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(1000)

        val context = SDKLocation.INSTANCE.context
        if (ActivityCompat.checkSelfPermission(context!!, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return Observable.error(LocationPermissionError("Location Permission Not enabled"))
        }

        val locationProvider = SDKLocation.INSTANCE.getLocationProvider()
        return locationProvider
                .checkLocationSettings(
                        LocationSettingsRequest.Builder()
                                .addLocationRequest(locationRequest)
                                .setAlwaysShow(true)
                                .build()
                ).flatMap {

                    if (it.status.statusCode == LocationSettingsStatusCodes.RESOLUTION_REQUIRED) {
                        Observable.error(LocationPermissionError("Location Settings Not enabled"))
                    } else {


                        locationProvider.getUpdatedLocation(locationRequest).map { location ->
                            resultOrError { location.toCLocation() }
                        }.doOnError {
                            Response.error<CLocation>(it)
                        }
                    }

                }

    }

    override fun getList(batchId: Long): Observable<Response<List<CLocation>, Throwable>> {

        val locationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(1000)

        val context = SDKLocation.INSTANCE.context
        if (ActivityCompat.checkSelfPermission(context!!, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return Observable.error(LocationPermissionError("Location Permission Not enabled"))
        }

        val locationProvider = SDKLocation.INSTANCE.getLocationProvider()
        return locationProvider
                .checkLocationSettings(
                        LocationSettingsRequest.Builder()
                                .addLocationRequest(locationRequest)
                                .setAlwaysShow(true)
                                .build()
                ).flatMap {

                    if (it.status.statusCode == LocationSettingsStatusCodes.RESOLUTION_REQUIRED) {
                        Observable.error(LocationPermissionError("Location Settings Not enabled"))
                    } else {

                        locationProvider.getUpdatedLocation(locationRequest)
                                .buffer(5)
                                .map { location ->
                                    resultOrError { location.map { it.toCLocation() } }
                                }.doOnError {
                                    Response.error<CLocation>(it)
                                }
                    }

                }
    }

}
