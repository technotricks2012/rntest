package com.lta.locationlib.libs.location.observables.activity;

import com.google.android.gms.location.ActivityRecognition;
import com.lta.locationlib.libs.location.observables.BaseObservableOnSubscribe;
import com.lta.locationlib.libs.location.observables.ObservableContext;

abstract class BaseActivityObservableOnSubscribe<T> extends BaseObservableOnSubscribe<T> {
    BaseActivityObservableOnSubscribe(ObservableContext ctx) {
        super(ctx, ActivityRecognition.API);
    }
}
