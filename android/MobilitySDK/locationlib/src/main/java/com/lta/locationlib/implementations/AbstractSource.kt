package com.lta.locationlib.implementations

/**
 *
 */
abstract class AbstractSource<T : Any>(val service: T) :
        Source