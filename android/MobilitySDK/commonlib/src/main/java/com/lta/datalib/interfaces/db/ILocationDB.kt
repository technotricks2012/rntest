package com.lta.datalib.interfaces.db

import com.lta.datalib.model.CLocation
import com.lta.datalib.utils.Response
import io.reactivex.Single

interface ILocationDB{
     fun getList(batchId:Long) : Single<Response<List<CLocation>, Throwable>>
     fun getLast() : Single<Response<CLocation, Throwable>>
     fun save(data: CLocation) : Single<Response<CLocation, Throwable>>
     fun save(data: List<CLocation>) : Single<Response<List<CLocation>, Throwable>>
}