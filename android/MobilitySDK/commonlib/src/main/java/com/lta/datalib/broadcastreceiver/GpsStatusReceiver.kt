package com.lta.datalib.broadcastreceiver

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.location.LocationManager
import io.reactivex.ObservableEmitter

open class GpsStatusReceiver : BroadcastReceiver() {
    override fun onReceive(context: Context?, intent: Intent?) {
        if (intent?.action == "android.location.PROVIDERS_CHANGED") {

        }
    }

}



private var mSubscriber: ObservableEmitter<Boolean>? = null
private val mGpsChangeReceiver = object : GpsStatusReceiver() {
    override fun onReceive(context: Context?, intent: Intent?) {
        if (mSubscriber == null) {
            return
        }
        if (intent?.action == LocationManager.PROVIDERS_CHANGED_ACTION) {
            val locationManager = context?.getSystemService(Context.LOCATION_SERVICE) as LocationManager
            val isGpsEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)
            val isNetworkEnabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)

            mSubscriber?.onNext(isGpsEnabled && isNetworkEnabled)
        }
    }
}