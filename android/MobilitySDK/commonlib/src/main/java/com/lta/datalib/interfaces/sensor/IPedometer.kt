package com.lta.datalib.interfaces.sensor

import com.lta.datalib.model.sensor.CPedometer
import com.lta.datalib.utils.Response
import io.reactivex.Flowable

interface IPedometer{
    fun getSensorData() : Flowable<Response<CPedometer,Throwable>>
    fun getSensorDatas(batchId:Long) : Flowable<Response<List<CPedometer>,Throwable>>
}