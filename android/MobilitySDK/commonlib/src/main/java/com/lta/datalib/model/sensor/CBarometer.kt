package com.lta.datalib.model.sensor

import com.lta.datalib.model.base.BaseModel

data class CBarometer(
        val timestamp: Long = -1,
        var pressure: Float

) : BaseModel() {
    var batch_id: Long = 0
    var mode: String = ""
    var predictionMode: String = ""
    var journeyID: String = ""

}