package com.lta.datalib.interfaces.sensor

import com.lta.datalib.model.sensor.CAccelerometerLinear
import com.lta.datalib.utils.Response
import io.reactivex.Flowable

interface IAccelerometerLinearResource{
    fun get() :  Flowable<Response<CAccelerometerLinear, Throwable>>
    fun getList(batchId:Long) :Flowable<Response<List<CAccelerometerLinear>, Throwable>>
}