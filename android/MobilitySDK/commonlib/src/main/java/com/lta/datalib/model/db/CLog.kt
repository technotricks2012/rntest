package com.lta.datalib.model.db

import com.lta.datalib.model.base.BaseModel

data class CLog(
        val id: Long? = null,
        val timestamp: Long = 0,
        val type: TYPE = TYPE.UNKNOWN,
        val key: String?,
        var description: String?

) : BaseModel() {
    enum class TYPE(val value: String) {
        ERROR(Properties.ERROR),
        DEBUG(Properties.DEBUG),
        SUCCESS(Properties.SUCCESS),
        UNKNOWN(Properties.UNKNOWN);

        object Properties {
            const val ERROR = "ERROR"
            const val SUCCESS = "SUCCESS"
            const val DEBUG = "DEBUG"
            const val UNKNOWN = "UNKNOWN"
        }

        object Converter {
            @JvmStatic
            fun toString(type: TYPE): String {
                return type.value
            }

            @JvmStatic
            fun toType(value: String): TYPE {
                return when (value) {
                    ERROR.value -> ERROR
                    SUCCESS.value -> SUCCESS
                    DEBUG.value -> DEBUG
                    else -> UNKNOWN

                }
            }
        }
    }
}