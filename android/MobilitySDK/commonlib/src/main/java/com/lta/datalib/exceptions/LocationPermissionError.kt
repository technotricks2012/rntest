package com.lta.datalib.exceptions

import com.lta.datalib.utils.ResponseError

class LocationPermissionError(message : String = "") : ResponseError(message)