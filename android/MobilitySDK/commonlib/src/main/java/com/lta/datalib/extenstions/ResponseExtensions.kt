package com.lta.datalib.extenstions

import com.lta.datalib.utils.Response


/**
 *
 */
inline fun <T> resultOrError(block : () -> T?) : Response<T, Throwable> {
    var result : T? = null
    var error : Throwable? = null

    try {
        result = block()
    } catch(throwable : Throwable) {
        error = throwable
    }

    return Response(
        result = result,
        error = error
    )
}

