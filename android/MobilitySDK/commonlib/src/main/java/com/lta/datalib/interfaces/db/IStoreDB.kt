package com.lta.datalib.interfaces.db

import com.lta.datalib.model.sensor.CAccelerometer
import com.lta.datalib.utils.Response
import io.reactivex.Flowable
import io.reactivex.Observable
import io.reactivex.Single

interface IStoreDB{
     fun save(key:String,value:String)
     fun get(key:String) : Response<String, Throwable>


     fun getJourneyId():String
     fun getUserId():String
     fun getMode():String
     fun getPredictionMode():String
     fun setIsStart()
     fun getIsStart():Boolean

}