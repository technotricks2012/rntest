package com.lta.datalib.model.roadDefect

import com.lta.datalib.model.base.BaseModel


data class CRoadDefect(
        val id: Long? = null,
        val status: Status? = Status.UNKNOWN
) : BaseModel(){
    enum class Status(val value: String) {
        START("START"),
        STOP("STOP"),
        UPLOADED("UPLOADED"),
        UPLOADING("UPLOADING"),
        FAIL("FAIL"),
        UNKNOWN("UNKNOWN");
        object Converter {
            @JvmStatic
            fun toString(type: Status): String {
                return type.value
            }

            @JvmStatic
            fun toType(value: String): Status {
                return when (value) {
                    START.value -> START
                    STOP.value -> STOP
                    UPLOADING.value -> UPLOADING
                    UPLOADED.value -> UPLOADED
                    FAIL.value -> FAIL
                    else -> UNKNOWN

                }
            }
        }

    }
}






