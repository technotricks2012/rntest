package com.lta.datalib.model.roadDefect

import com.lta.datalib.model.base.BaseModel


data class CRoadDefectInfoEmbedded(
        var roadDefectInfo: CRoadDefectInfo,
        var location: List<CDefectLocation>):BaseModel()





