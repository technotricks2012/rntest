package com.lta.datalib.interfaces.db

import com.lta.datalib.model.sensor.COrientationAxes
import com.lta.datalib.utils.Response
import io.reactivex.Single

interface IOrientationAxesDB{
     fun getList(batchId:Long) : Single<Response<List<COrientationAxes>, Throwable>>
     fun getLast() : Single<Response<COrientationAxes, Throwable>>
     fun save(data: COrientationAxes) : Single<Response<COrientationAxes, Throwable>>
     fun save(data: List<COrientationAxes>) : Single<Response<List<COrientationAxes>, Throwable>>
}