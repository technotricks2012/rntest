package com.lta.datalib.model.roadDefect

import com.lta.datalib.model.base.BaseModel


data class CRoadDefectEmbedded(
      val roadDefect: CRoadDefect,
      val roadDefectInfos: List<CRoadDefectInfoEmbedded>,
      val defects: List<CDefectEmbedded>
):BaseModel()






