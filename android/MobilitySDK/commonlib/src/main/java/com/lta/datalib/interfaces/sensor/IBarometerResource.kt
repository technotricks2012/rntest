package com.lta.datalib.interfaces.sensor

import com.lta.datalib.model.sensor.CBarometer
import com.lta.datalib.utils.Response
import io.reactivex.Flowable

interface IBarometerResource{
    fun get() :  Flowable<Response<CBarometer, Throwable>>
    fun getList(batchId:Long) :Flowable<Response<List<CBarometer>, Throwable>>
}