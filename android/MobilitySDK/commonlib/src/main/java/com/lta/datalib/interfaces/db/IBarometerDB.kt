package com.lta.datalib.interfaces.db

import com.lta.datalib.model.sensor.CBarometer
import com.lta.datalib.utils.Response
import io.reactivex.Single

interface IBarometerDB{
     fun getList(batchId:Long) : Single<Response<List<CBarometer>, Throwable>>
     fun getLast() : Single<Response<CBarometer, Throwable>>
     fun save(data: CBarometer) : Single<Response<CBarometer, Throwable>>
     fun save(data: List<CBarometer>) : Single<Response<List<CBarometer>, Throwable>>
}