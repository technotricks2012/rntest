package com.lta.datalib.interfaces.db

import com.lta.datalib.model.sensor.CAccelerometer
import com.lta.datalib.model.sensor.CMagnetometer
import com.lta.datalib.utils.Response
import io.reactivex.Flowable
import io.reactivex.Observable
import io.reactivex.Single

interface IMagnetometerDB{
     fun getList(batchId:Long) : Single<Response<List<CMagnetometer>, Throwable>>
     fun getLast() : Single<Response<CMagnetometer, Throwable>>
     fun save(data: CMagnetometer) : Single<Response<CMagnetometer, Throwable>>
     fun save(data: List<CMagnetometer>) : Single<Response<List<CMagnetometer>, Throwable>>
}