package com.lta.datalib.interfaces.sensor

import com.lta.datalib.model.sensor.COrientationAxes
import com.lta.datalib.utils.Response
import io.reactivex.Flowable

interface IOrientationAxesResource{
    fun get() :  Flowable<Response<COrientationAxes, Throwable>>
    fun getList(batchId:Long) :Flowable<Response<List<COrientationAxes>, Throwable>>
}