package com.lta.datalib.interfaces.sensor

import com.lta.datalib.model.sensor.CPedometer
import com.lta.datalib.utils.Response
import io.reactivex.Flowable

interface IPedometerResource{
    fun get() :  Flowable<Response<CPedometer, Throwable>>
    fun getList(batchId:Long) :Flowable<Response<List<CPedometer>, Throwable>>
}