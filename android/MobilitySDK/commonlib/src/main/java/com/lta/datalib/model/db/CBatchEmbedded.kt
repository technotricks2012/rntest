package com.lta.datalib.model.db

import com.lta.datalib.model.CLocation
import com.lta.datalib.model.base.BaseModel
import com.lta.datalib.model.sensor.*

data class CBatchEmbedded(
        val batch: CBatch,
        val accelerometer: List<CAccelerometer>,
        val accelerometerLinear: List<CAccelerometerLinear>,
        val gyroscope: List<CGyroscope>,
        val magnetometer: List<CMagnetometer>,
        val location: List<CLocation>,
        val orientationAxes: List<COrientationAxes>,
        val rotationVector: List<CRotationVector>,
        val barometer: List<CBarometer>,
        val pedometer: List<CPedometer>
) : BaseModel()