package com.lta.datalib.model.roadDefect

import com.lta.datalib.model.base.BaseModel

data class CDefect(
        var id: Long? = null,
        val timestamp: Long = -1,
        val defectType: String

) : BaseModel()