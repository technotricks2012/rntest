package com.lta.datalib.utils

open class ResponseError(message : String = "") : RuntimeException(message)