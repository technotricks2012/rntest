package com.lta.datalib.model

import com.lta.datalib.interfaces.db.IStoreDB
import com.lta.datalib.interfaces.sensor.IDeviceInfoResource
import com.lta.datalib.model.base.BaseModel
import com.lta.datalib.model.sensor.*

data class AllSensorData(
        var batchId:Long,
        var deviceInfo: IDeviceInfoResource?,
        var storeDb: IStoreDB?,
        var accelerometer: List<CAccelerometer>?=null,
        var accelerometerLinear: List<CAccelerometerLinear>?=null,
        var magnetometer: List<CMagnetometer>?=null,
        var gyroscope: List<CGyroscope>?=null,
        var orientationAxes: List<COrientationAxes>?=null,
        var rotationVector: List<CRotationVector>?=null,
        var barometer: List<CBarometer>?=null,
        var pedometer: List<CPedometer>?=null,
        var location: List<CLocation>?=null



): BaseModel(){

//    var accelerometer: List<CAccelerometer>?=null
//    var magnetometerList: List<CAccelerometer>?=null

}