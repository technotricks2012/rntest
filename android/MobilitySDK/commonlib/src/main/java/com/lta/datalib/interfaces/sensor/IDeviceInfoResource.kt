package com.lta.datalib.interfaces.sensor

import com.lta.datalib.model.device.CPower


interface IDeviceInfoResource {
    fun getDeviceId(): String?
    fun getPackageId(): String?
    fun getModel(): String?
    fun getSystemVersion(): Int?
    fun getHardware():String?
    fun isEmulator():Boolean?
    fun getDeviceName(): String?
    fun getPower(): CPower?
}