package com.lta.datalib.interfaces.db

import com.lta.datalib.model.sensor.CAccelerometerLinear
import com.lta.datalib.utils.Response
import io.reactivex.Single

interface IAccelerometerLinearDB{
     fun getList(batchId:Long) : Single<Response<List<CAccelerometerLinear>, Throwable>>
     fun getLast() : Single<Response<CAccelerometerLinear, Throwable>>
     fun save(data: CAccelerometerLinear) : Single<Response<CAccelerometerLinear, Throwable>>
     fun save(data: List<CAccelerometerLinear>) : Single<Response<List<CAccelerometerLinear>, Throwable>>
}