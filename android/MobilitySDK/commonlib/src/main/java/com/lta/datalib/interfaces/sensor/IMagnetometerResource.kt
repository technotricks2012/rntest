package com.lta.datalib.interfaces.sensor

import com.lta.datalib.model.sensor.CAccelerometer
import com.lta.datalib.model.sensor.CMagnetometer
import com.lta.datalib.utils.Response
import io.reactivex.Flowable
import io.reactivex.Observable
import io.reactivex.Single

interface IMagnetometerResource{
    fun get() :  Flowable<Response<CMagnetometer, Throwable>>
    fun getList(batchId:Long) :Flowable<Response<List<CMagnetometer>, Throwable>>
}