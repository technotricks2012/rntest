package com.lta.datalib.interfaces.sensor

import com.lta.datalib.model.sensor.CGyroscope
import com.lta.datalib.utils.Response
import io.reactivex.Flowable

interface IGyroscopeResource{
    fun get() :  Flowable<Response<CGyroscope, Throwable>>
    fun getList(batchId:Long) :Flowable<Response<List<CGyroscope>, Throwable>>
}