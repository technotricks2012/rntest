//package com.lta.datalib.interfaces.sensor
//
//import com.lta.datalib.model.sensor.CAxis
//import com.lta.datalib.utils.Response
//import io.reactivex.Flowable
//import io.reactivex.Observable
//import io.reactivex.Single
//
//interface IAxisResource{
//    fun getAxis() : Flowable<Response<CAxis, Throwable>>
//    fun getAxisList(batchId:Long) : Flowable<Response<List<CAxis>, Throwable>>
//}