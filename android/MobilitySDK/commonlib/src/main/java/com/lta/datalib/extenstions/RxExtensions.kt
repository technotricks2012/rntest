package com.lta.datalib.extenstions

import com.lta.datalib.exceptions.NoResultError
import com.lta.datalib.utils.Response
import com.lta.datalib.utils.ResponseError
import io.reactivex.Flowable
import io.reactivex.Maybe
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers


/**
 *
 */


/**
 *
 */
fun <T> Observable<T>.subscribeOnIO() : Observable<T> {
    return this.subscribeOn(Schedulers.io())
}



/**
 *
 */
fun <T> T.asSingle() : Single<T> {
    return Single.just(this)
}


/**
 *
 */
fun <T> Single<T>.observeOnUI() : Single<T> {
    return this.observeOn(AndroidSchedulers.mainThread())
}


/**
 *
 */
fun <T> Single<T>.observeOnIO() : Single<T> {
    return this.observeOn(Schedulers.io())
}

/**
 *
 */
fun <T> Single<T>.subscribeOnIO() : Single<T> {
    return this.subscribeOn(Schedulers.io())
}


/**
 *
 */
fun <T> Single<Response<T, Throwable>>.withResult() : Maybe<Response<T, Throwable>> {
    return this.filter { it.hasResult }
}


fun <T> Single<Response<T, Throwable>>.resultOrError() : Single<T> {
    return this.flatMapOrError { Single.just(it) }
}


fun <T> Flowable<Response<T, Throwable>>.resultOrError() : Flowable<T> {
    return this.flatMapOrError { Flowable.just(it) }
}


inline fun <T, R> Flowable<Response<T, Throwable>>.flatMapOrError(crossinline mappingFunction : (T) -> Flowable<R>) : Flowable<R> {
    return this.flatMap {
        if(it.isErroneous) {
            Flowable.error(it.errorOrDefault())
        } else if(!it.hasResult) {
            Flowable.error(NoResultError("The Response Result is null."))
        } else {
            mappingFunction(it.result!!)
        }
    }
}


/**
 *
 */
inline fun <T, R> Observable<Response<T, Throwable>>.flatMapOrError(crossinline mappingFunction : (T) -> Observable<R>) : Observable<R> {
    return this.flatMap {
        if(it.isErroneous) {
            Observable.error(it.errorOrDefault())
        } else if(!it.hasResult) {
            Observable.error(NoResultError("The Response Result is null."))
        } else {
            mappingFunction(it.result!!)
        }
    }
}

inline fun <T, R> Single<Response<T, Throwable>>.flatMapOrError(crossinline mappingFunction : (T) -> Single<R>) : Single<R> {
    return this.flatMap {
        if(it.isErroneous) {
            Single.error(it.errorOrDefault())
        } else if(!it.hasResult) {
            Single.error(NoResultError("The Response Result is null."))
        } else {
            mappingFunction(it.result!!)
        }
    }
}


fun <T> Single<T>.applyIOWorkSchedulers() : Single<T> {
    return this.subscribeOnIO()
        .observeOnIO()
}


/**
 *
 */
fun <T> Single<Response<T, Throwable>>.successfulResponseOrError() : Single<Response<T, Throwable>> {
    return this.flatMapResponseOrErrorOut { Single.just(it) }
}

fun Disposable.disposedBy(disposableBag: CompositeDisposable?) {
    disposableBag?.add(this)
}


/**
 *
 */
inline fun <T, R> Single<Response<T, Throwable>>.flatMapResponseOrErrorOut(crossinline mappingFunction : (Response<T, Throwable>) -> Single<R>) : Single<R> {
    return this.flatMap {
        if(it.isErroneous) {
            Single.error(it.errorOrDefault())
        } else if(!it.hasResult) {
            Single.error(NoResultError("The Response Result is null."))
        } else {
            mappingFunction(it)
        }
    }
}


/**
 *
 */
fun Response<*, Throwable>.errorOrDefault() : Throwable {
    return (this.error ?: ResponseError())
}


