package com.lta.datalib.extenstions

import java.util.*

fun String.toRandom(length:Int = 15) : String = UUID.randomUUID().toString().substring(0,length)

