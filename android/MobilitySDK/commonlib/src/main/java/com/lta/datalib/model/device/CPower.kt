package com.lta.datalib.model.device



data class CPower(
        val lowPowerMode: Boolean,
        var batteryLevel: Float,
        var batteryState: String
)