package com.lta.datalib.interfaces.location

import com.lta.datalib.model.CLocation
import com.lta.datalib.utils.Response
import io.reactivex.Flowable
import io.reactivex.Observable

interface ILocation{
     fun get() : Observable<Response<CLocation, Throwable>>
     fun getList(batchId:Long) :Observable<Response<List<CLocation>, Throwable>>
}