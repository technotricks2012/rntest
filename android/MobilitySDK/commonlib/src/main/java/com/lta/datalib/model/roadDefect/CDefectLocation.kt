package com.lta.datalib.model.roadDefect

import com.lta.datalib.model.base.BaseModel


data class CDefectLocation(
        val timestamp: Long = -1,
        val latitude: Double,
        var longitude: Double,
        var altitude: Double,
        var accuracy: Float
) : BaseModel()