package com.lta.datalib.model.sensor

import com.lta.datalib.model.base.BaseModel

data class CRotationVector(
        val timestamp: Long = 0,
        var x: Float,
        var y: Float,
        var z: Float,
        var rotationVector:Float

): BaseModel(){
    var batch_id:Long = 0
    var mode: String =""
    var predictionMode: String =""
    var journeyID: String =""

}