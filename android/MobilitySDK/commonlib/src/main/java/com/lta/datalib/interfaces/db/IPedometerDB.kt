package com.lta.datalib.interfaces.db

import com.lta.datalib.model.sensor.CPedometer
import com.lta.datalib.utils.Response
import io.reactivex.Single

interface IPedometerDB{
     fun getList(batchId:Long) : Single<Response<List<CPedometer>, Throwable>>
     fun getLast() : Single<Response<CPedometer, Throwable>>
     fun save(data: CPedometer) : Single<Response<CPedometer, Throwable>>
     fun save(data: List<CPedometer>) : Single<Response<List<CPedometer>, Throwable>>
}