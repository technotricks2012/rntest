package com.lta.datalib.model.roadDefect

import com.lta.datalib.model.base.BaseModel

data class RoadDefectData(
        var packageId: String?,
        val deviceID: String?,
        var roadDefect: List<CRoadDefectEmbedded>?
): BaseModel()