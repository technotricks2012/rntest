package com.lta.datalib.interfaces.db

import com.lta.datalib.model.db.CBatch
import com.lta.datalib.model.db.CBatchEmbedded
import com.lta.datalib.utils.Response
import io.reactivex.Single


interface IBatchDB {
    fun getNewBatch() : Single<Response<CBatch, Throwable>>
    fun delete(batchId:Long):Single<Response<Boolean, Throwable>>
    fun updateStatus(batchId:Long,status: CBatch.Status):Single<Response<Boolean, Throwable>>
    fun getReadyOrFailBatch() : Single<Response<List<CBatchEmbedded>,Throwable>>

}