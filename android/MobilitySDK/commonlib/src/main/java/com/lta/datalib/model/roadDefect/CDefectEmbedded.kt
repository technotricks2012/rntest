package com.lta.datalib.model.roadDefect

import com.lta.datalib.model.base.BaseModel


data class CDefectEmbedded(
      val defect: CDefect,
      var locations: List<CDefectLocation>
):BaseModel()




