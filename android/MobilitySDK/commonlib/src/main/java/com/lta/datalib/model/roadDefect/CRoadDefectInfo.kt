package com.lta.datalib.model.roadDefect

import com.lta.datalib.model.base.BaseModel


data class CRoadDefectInfo(
        var id: Long? = null,
        val timestamp: Long = -1,
        val isStart: Boolean?=null

) : BaseModel()
