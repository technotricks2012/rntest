package com.lta.datalib.interfaces.db

import com.lta.datalib.model.roadDefect.*
import com.lta.datalib.utils.Response
import io.reactivex.Single

interface IRoadDefectDB{
     fun insertDefect(cDefect: CDefect,cDefectLocations: List<CDefectLocation>) : Single<Response<List<CRoadDefectEmbedded>,Throwable>>
     fun insertStartTimeWithLocation(timestamp: Long,stDefectLocations: List<CDefectLocation>) :  Single<Response<List<CRoadDefectEmbedded>,Throwable>>
     fun insertStopTimeWithLocation(timestamp: Long,stDefectLocations: List<CDefectLocation>) :  Single<Response<List<CRoadDefectEmbedded>,Throwable>>
     fun delete(ids: List<Long>) :  Single<Response<Boolean,Throwable>>
     fun getList() : Single<Response<List<CRoadDefectEmbedded>,Throwable>>
}