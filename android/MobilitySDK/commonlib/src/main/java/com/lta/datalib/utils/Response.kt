package com.lta.datalib.utils



/**
 *
 */
data class Response<R, E : Throwable>(
    val result : R? = null,
    val error : E? = null
) {


    /**
     *
     */
    val hasResult = (result != null)

    /**
     *
     */
    val isErroneous = (error != null)


    companion object {

        /**
         *
         */
        @JvmStatic fun <R> result(result : R?) : Response<R, Throwable> {
            return Response(result = result)
        }

        /**
         *
         */
        @JvmStatic fun <R> error(error : Throwable?) : Response<R, Throwable> {
            return Response(error = error)
        }

        /**
         *
         */
        @JvmStatic fun <R> error(errorMessage : String) : Response<R, Throwable> {
            return error(ResponseError(errorMessage))
        }

    }


}