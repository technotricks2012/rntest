package com.lta.datalib.model.db

import com.lta.datalib.model.base.BaseModel

data class CBatch(
        val id: Long? = null,
        val timestamp: Long = 0,
        val status: Status? = Status.UNKNOWN

) : BaseModel() {
    enum class Status(val value: String) {
        NEW(Properties.NEW),
        UPLOADED(Properties.UPLOADED),
        READY(Properties.READY),
        UPLOADING(Properties.UPLOADING),

        FAIL(Properties.FAIL),
        UNKNOWN(Properties.UNKNOWN);
        object Properties {
            const val NEW = "NEW"
            const val UPLOADING = "UPLOADING"
            const val UPLOADED = "UPLOADED"
            const val READY = "FAIL"
            const val FAIL = "FAIL"
            const val UNKNOWN = "UNKNOWN"
        }

        object Converter {
            @JvmStatic
            fun toString(type: Status): String {
                return type.value
            }

            @JvmStatic
            fun toType(value: String): Status {
                return when (value) {
                    NEW.value -> NEW
                    UPLOADED.value -> UPLOADED
                    READY.value -> READY
                    FAIL.value -> FAIL
                    UPLOADING.value -> UPLOADING
                    else -> UNKNOWN

                }
            }
        }
    }
}