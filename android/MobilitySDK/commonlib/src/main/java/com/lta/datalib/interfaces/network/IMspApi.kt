package com.lta.datalib.interfaces.network

import com.lta.datalib.model.AllSensorData
import com.lta.datalib.model.roadDefect.RoadDefectData
import com.lta.datalib.utils.Response
import io.reactivex.Single

interface IMspApi{
     fun saveWayPoints(data: AllSensorData) : Single<Response<String, Throwable>>
     fun saveRoadInspection(data: RoadDefectData) : Single<Response<List<String>, Throwable>>

}