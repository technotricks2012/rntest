package com.lta.datalib.exceptions

import com.lta.datalib.utils.ResponseError

class NoResultError(message : String = "") : ResponseError(message)