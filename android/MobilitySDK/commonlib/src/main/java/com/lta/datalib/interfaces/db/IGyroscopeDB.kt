package com.lta.datalib.interfaces.db

import com.lta.datalib.model.sensor.CGyroscope
import com.lta.datalib.utils.Response
import io.reactivex.Single

interface IGyroscopeDB {
    fun getList(batchId: Long): Single<Response<List<CGyroscope>, Throwable>>
    fun getLast(): Single<Response<CGyroscope, Throwable>>
    fun save(data: CGyroscope): Single<Response<CGyroscope, Throwable>>
    fun save(data: List<CGyroscope>): Single<Response<List<CGyroscope>, Throwable>>
}