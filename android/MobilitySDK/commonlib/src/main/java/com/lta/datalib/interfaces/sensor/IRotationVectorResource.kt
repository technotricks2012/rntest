package com.lta.datalib.interfaces.sensor

import com.lta.datalib.model.sensor.CRotationVector
import com.lta.datalib.utils.Response
import io.reactivex.Flowable

interface IRotationVectorResource{
    fun get() :  Flowable<Response<CRotationVector, Throwable>>
    fun getList(batchId:Long) :Flowable<Response<List<CRotationVector>, Throwable>>
}