package com.lta.datalib.interfaces.sensor

import com.lta.datalib.model.sensor.CAccelerometer
import com.lta.datalib.utils.Response
import io.reactivex.Flowable
import io.reactivex.Observable
import io.reactivex.Single

interface IAccelerometerResource{
    fun get() :  Flowable<Response<CAccelerometer, Throwable>>
    fun getList(batchId:Long) :Flowable<Response<List<CAccelerometer>, Throwable>>
}