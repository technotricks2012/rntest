package com.lta.datalib.interfaces.db

import com.lta.datalib.model.sensor.CAccelerometer
import com.lta.datalib.utils.Response
import io.reactivex.Flowable
import io.reactivex.Observable
import io.reactivex.Single

interface IAccelerometerDB{
     fun getList(batchId:Long) : Single<Response<List<CAccelerometer>, Throwable>>
     fun getLast() : Single<Response<CAccelerometer, Throwable>>
     fun save(data: CAccelerometer) : Single<Response<CAccelerometer, Throwable>>
     fun save(data: List<CAccelerometer>) : Single<Response<List<CAccelerometer>, Throwable>>
}