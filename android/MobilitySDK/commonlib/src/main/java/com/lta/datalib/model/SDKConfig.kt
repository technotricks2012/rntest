package com.lta.datalib.model

data class SDKConfig(
    val id : Long=-1,
    val secret: String,
     val appId: String
){

    private constructor(builder: Builder) : this(secret = builder.secret,appId = builder.appId)
    companion object {
        inline fun build(block: Builder.() -> Unit) = Builder().apply(block).build()
    }

    class Builder {
        lateinit var secret: String
//            private set
        lateinit var appId: String
//            private set

//        fun setSecretKey(secret: String) = apply { this.secret = secret }
//        fun setAppId(appId: String) = apply { this.appId = appId }
        fun build() = SDKConfig(this)
    }
}