package com.lta.datalib.interfaces.sensor

import com.lta.datalib.model.sensor.CBarometer
import com.lta.datalib.utils.Response
import io.reactivex.Flowable

interface IBarometer{
    fun getBarometer() : Flowable<Response<CBarometer, Throwable>>
    fun getBarometerList(batchId:Long) : Flowable<Response<List<CBarometer>, Throwable>>
}