package com.lta.datalib.interfaces.db

import com.lta.datalib.model.db.CLog
import com.lta.datalib.utils.Response
import io.reactivex.Single


interface ILogDB {
    fun save(log:CLog):Single<Response<CLog, Throwable>>
    fun getList() : Single<Response<List<CLog>,Throwable>>
}