package com.lta.datalib.qualifiers


import javax.inject.Qualifier

@Qualifier
@Retention(AnnotationRetention.RUNTIME)
annotation class Source(val value : Type) {


    enum class Type {

        CACHE,
        DATABASE,
        SERVER

    }


}