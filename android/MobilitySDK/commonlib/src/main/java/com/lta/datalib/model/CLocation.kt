package com.lta.datalib.model

import com.lta.datalib.model.base.BaseModel


data class CLocation(
        val timestamp: Long = 0,
        var latitude: Double,
        var longitude: Double,
        var altitude: Double,
        var accuracy: Float,
        var isMock:Boolean
): BaseModel(){
    var batch_id:Long = 0
    var mode: String =""
    var predictionMode: String =""
    var journeyID: String =""

}