package com.lta.datalib.model.sensor

import com.lta.datalib.model.base.BaseModel

data class CPedometer(
        val timestamp: Long = -1,
        var steps: Float,
        var distance: Float
) : BaseModel() {
    var batch_id: Long = 0
    var mode: String = ""
    var predictionMode: String = ""
    var journeyID: String = ""

}