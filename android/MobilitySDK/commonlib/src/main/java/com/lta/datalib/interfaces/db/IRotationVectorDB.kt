package com.lta.datalib.interfaces.db

import com.lta.datalib.model.sensor.CRotationVector
import com.lta.datalib.utils.Response
import io.reactivex.Single

interface IRotationVectorDB{
     fun getList(batchId:Long) : Single<Response<List<CRotationVector>, Throwable>>
     fun getLast() : Single<Response<CRotationVector, Throwable>>
     fun save(data: CRotationVector) : Single<Response<CRotationVector, Throwable>>
     fun save(data: List<CRotationVector>) : Single<Response<List<CRotationVector>, Throwable>>
}