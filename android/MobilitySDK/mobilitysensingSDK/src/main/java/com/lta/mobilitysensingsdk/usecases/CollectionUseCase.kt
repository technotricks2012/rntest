package com.lta.mobilitysensingsdk.usecases

import android.util.Log
import com.lta.datalib.base.BaseUseCase
import com.lta.datalib.extenstions.disposedBy
import com.lta.datalib.extenstions.flatMapOrError
import com.lta.datalib.extenstions.resultOrError
import com.lta.datalib.interfaces.db.*
import com.lta.datalib.interfaces.network.IMspApi
import com.lta.datalib.interfaces.sensor.IDeviceInfoResource
import com.lta.datalib.model.AllSensorData
import com.lta.datalib.model.CLocation
import com.lta.datalib.model.db.CBatch
import com.lta.datalib.model.db.CBatchEmbedded
import com.lta.datalib.model.db.CLog
import com.lta.datalib.model.sensor.*
import com.lta.datalib.utils.Response
import com.lta.datalib.utils.TimeUtils
import com.lta.datalib.utils.toPrettyJson
import com.lta.mobilitysensingsdk.mapper.toAllSensorDataTransaction
import io.reactivex.Flowable
import io.reactivex.Observable
import io.reactivex.Observer
import io.reactivex.Single
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import java.util.concurrent.TimeUnit

class CollectionUseCase(
        private  val disposeBag: CompositeDisposable?,
        private val networkSource: IMspApi,
        private val deviceInfo: IDeviceInfoResource,
        private val batchDB: IBatchDB,
        private val storeDb: IStoreDB,
        private val logStoreData: ILogDB
) : BaseUseCase.WithoutParams<Response<String, Throwable>>() {


    override fun onExecute(): Observable<Response<String, Throwable>> {

        var init = 1L

        return batchDB.getReadyOrFailBatch().toObservable()
                .subscribeOn(defaultBackgroundScheduler())
                .observeOn(defaultObserverScheduler())
                .flatMapOrError { items: List<CBatchEmbedded> ->
                    init = 1L
                    Observable.fromIterable(
                            items.filter {
                                it.accelerometer.count() > 0 ||
                                        it.accelerometerLinear.count() > 0 ||
                                        it.gyroscope.count() > 0 ||
                                        it.magnetometer.count() > 0 ||
                                        it.barometer.count() > 0 ||
                                        it.location.count() > 0 ||
                                        it.rotationVector.count() > 0 ||
                                        it.pedometer.count() > 0 ||
                                        it.orientationAxes.count() > 0
                            }
                                    .map { item ->
                                        item.toAllSensorDataTransaction(deviceInfo, storeDb)
                                    })
                }
//                .buffer(5)
//                .delay {
//                    init+=5
//                    Observable.interval(init,TimeUnit.SECONDS)
//                }
//                .flatMap {
//                    Log.d("CollectionUseCase", "Buffer")
//
//                    Observable.fromIterable(it)
//                }
                .delay {
                    init += 1
                    Observable.interval(init, TimeUnit.SECONDS)
                }
                .flatMap { allSensorData ->
                    dbUpdate(allSensorData.batchId, CBatch.Status.UPLOADING)
                    apiCall(allSensorData)
                }
                .doOnError {
                    logStoreData.save(
                            CLog(
                                    timestamp = TimeUtils.currentTime(),
                                    type = CLog.TYPE.ERROR,
                                    key = "CollectionUseCase",
                                    description = it.toPrettyJson())).subscribe({},{}).disposedBy(disposeBag)
                }


    }


    private fun apiCall(data: AllSensorData): Observable<Response<String, Throwable>> {
        Log.d("CollectionUseCase", "API Call ${data.batchId}")

        return networkSource.saveWayPoints(data).toObservable()
                .doOnNext {
                    batchDB.delete(data.batchId).subscribe({},{}).disposedBy(disposeBag)
                }
                .doOnError {
//                    dbUpdate(data.batchId.toString(), "FAIL")
                    dbUpdate(data.batchId, CBatch.Status.FAIL)
                }
                .onErrorResumeNext(Observable.just(resultOrError { "-1" }))
    }

    private fun dbUpdate(id: Long, status: CBatch.Status) {
        Log.d("CollectionUseCase", "DB Call ${status.value}<= $id")
        batchDB.updateStatus(id, status).subscribe({},{}).disposedBy(disposeBag)
    }
}

