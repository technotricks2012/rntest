package com.lta.mobilitysensingsdk.usecases

import com.lta.datalib.base.BaseUseCase
import com.lta.datalib.extenstions.disposedBy
import com.lta.datalib.extenstions.flatMapOrError
import com.lta.datalib.interfaces.db.IAccelerometerDB
import com.lta.datalib.interfaces.db.IAccelerometerLinearDB
import com.lta.datalib.interfaces.db.ILogDB
import com.lta.datalib.interfaces.sensor.IAccelerometerLinearResource
import com.lta.datalib.interfaces.sensor.IAccelerometerResource
import com.lta.datalib.model.db.CLog
import com.lta.datalib.model.sensor.CAccelerometer
import com.lta.datalib.model.sensor.CAccelerometerLinear
import com.lta.datalib.utils.Response
import com.lta.datalib.utils.TimeUtils
import com.lta.datalib.utils.toPrettyJson
import io.reactivex.Observable
import io.reactivex.disposables.Disposable
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.launch

class AccelerometerLinearUseCase(
        private val accSensorData: IAccelerometerLinearResource,
        private val accStoreData: IAccelerometerLinearDB,
        private val logStoreData: ILogDB
) : BaseUseCase.WithoutParams<Response<List<CAccelerometerLinear>, Throwable>>() {

    override fun onExecute(): Observable<Response<List<CAccelerometerLinear>, Throwable>> {
        return accSensorData.getList(-1)
                .subscribeOn(defaultBackgroundScheduler())
                .observeOn(defaultObserverScheduler())
                .flatMapOrError {

                    accStoreData.save(it).toFlowable()

                }
                .doOnError{
                    logStoreData.save(
                            CLog(
                                    timestamp = TimeUtils.currentTime(),
                                    type = CLog.TYPE.ERROR,
                                    key = "AccelerometerLinearUseCase",
                                    description = it.toPrettyJson())).subscribe()
                }
                .toObservable()
    }
}