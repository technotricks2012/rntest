package com.lta.mobilitysensingsdk.manager

import com.lta.datalib.extenstions.disposedBy
import com.lta.datalib.model.sensor.CAccelerometer
import com.lta.datalib.model.sensor.CMagnetometer
import com.lta.datalib.utils.Response
import com.lta.datalib.utils.toPrettyJson
import com.lta.locationlib.implementations.LocationUpdateImpl
import com.lta.mobilitysensingsdk.models.DataObserverTuple
import com.lta.mobilitysensingsdk.sdk.SDKManager
import com.lta.storelib.implementations.StoreAccelerometerImpl
import com.lta.storelib.implementations.StoreMagnetometerImpl
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.functions.BiFunction
import io.reactivex.subjects.PublishSubject
import java.util.concurrent.TimeUnit


class ObserverManager {

    companion object {
        @JvmStatic
        val INSTANCE by lazy { ObserverManager() }
    }

    private val errorTrigger by lazy {
        PublishSubject.create<String>()
    }
    private val successTrigger by lazy {
        PublishSubject.create<String>()
    }

    private val disposeBag = CompositeDisposable()

    private val locationImpl by lazy { LocationUpdateImpl() }

//    private val dbModuleAccelerometer by lazy { StoreAccelerometerImpl(SDKManager.INSTANCE.context) }

//    private val dbModuleMagnetometer by lazy { StoreMagnetometerImpl(SDKManager.INSTANCE.context) }



    fun startObserving() {

        SDKManager.INSTANCE.context?.let {
            clearObserver()

            Observable.combineLatest(
                    StoreAccelerometerImpl.getInstance(it).getLast().toObservable(),
                    StoreMagnetometerImpl.getInstance(it).getLast().toObservable(),
                    BiFunction<Response<CAccelerometer, Throwable>, Response<CMagnetometer, Throwable>, DataObserverTuple>
                    { t1, t2 ->
                        DataObserverTuple(t1, t2)
                    })
                    .delay(1, TimeUnit.SECONDS)
                    .repeatUntil {
                        false
                    }
                    .subscribe({ data ->
                        successTrigger.onNext(data.toPrettyJson())
                    }, { error ->
                        errorTrigger.onNext(error.toPrettyJson())
                    })
                    .disposedBy(disposeBag)

        }




    }

    fun errorObserver(): Observable<String> {
        return errorTrigger.hide()
    }

    fun successObserver(): Observable<String> {
        return successTrigger.hide()
    }

    private fun clearObserver() {
        disposeBag.clear()
    }


}