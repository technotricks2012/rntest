package com.lta.mobilitysensingsdk.usecases

import com.lta.datalib.extenstions.disposedBy
import com.lta.datalib.model.CLocation
import com.lta.datalib.model.roadDefect.CDefect
import com.lta.datalib.model.roadDefect.RoadDefectData
import com.lta.datalib.utils.Response
import com.lta.datalib.utils.TimeUtils
import com.lta.datalib.utils.toPrettyJson
import com.lta.locationlib.implementations.LocationUpdateImpl
import com.lta.mobilitysensingsdk.models.RoadDefectCRoadDefectEmbeddedTuple
import com.lta.mobilitysensingsdk.models.RoadDefectTuple
import com.lta.mobilitysensingsdk.sdk.SDKManager
import com.lta.mobilitysensingsdk.usecases.RoadDefectUseCase.DEFECT_STATUS.*
import com.lta.network.SDKApiImpl
import com.lta.sensor.implementations.DeviceInfoImpl
import com.lta.storelib.helpers.toCDefectLocationTransaction
import com.lta.storelib.implementations.StoreRoadDefectImpl
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.functions.BiFunction
import io.reactivex.subjects.PublishSubject


class RoadDefectUseCase {

    companion object {
        @JvmStatic
        val INSTANCE by lazy { RoadDefectUseCase() }
    }

    private val errorTrigger by lazy {
        PublishSubject.create<String>()
    }

    private val successTrigger by lazy {
        PublishSubject.create<String>()
    }


    private var status: DEFECT_STATUS = UNKNOWN


    private val disposeBag = CompositeDisposable()

    private val locationImpl by lazy { LocationUpdateImpl() }

    private val dbModule by lazy { StoreRoadDefectImpl(SDKManager.INSTANCE.context) }

    private val apiModule by lazy { SDKApiImpl() }

    private val deviceInfo by lazy { DeviceInfoImpl() }


    fun startRoadDefectLocation() {
        status = START
        runLocation()
    }

    fun stopRoadDefectLocation() {
        status = STOP
        runLocation()
    }


    fun updateRoadDefect(type: String) {
        status = HIGH_FREQUENCY
        runLocation(type)
    }

    private fun runLocation(type: String? = null) {
        //  clearObserver()


        Observable.combineLatest(
                locationImpl.get(),
                Observable.just(status),
                BiFunction<Response<CLocation, Throwable>, DEFECT_STATUS, RoadDefectTuple>
                { t1, t2 ->
                    RoadDefectTuple(t1, t2)
                })
                .doOnNext {
                    if (it.location.isErroneous) {
                        sendError(it.location.error)
                    }
                }
                .take(5)
                .buffer(5)
                .flatMap {
                    val locations = it.map {
                        it.location
                    }.map {
                        it.result
                    }.map {
                        it?.toCDefectLocationTransaction()!!
                    }

                    when (val statusType = it.getOrNull(0)?.type) {
                        START -> {
                            dbModule.insertStartTimeWithLocation(
                                    timestamp = TimeUtils.currentTime(),
                                    stRoadInfoLocations = locations)
                                    .doAfterSuccess { data ->
                                        if (data.isErroneous) {
                                            Observable.error<Throwable>(data.error)
                                        }
                                    }
                                    .map {
                                        RoadDefectCRoadDefectEmbeddedTuple(it, statusType)
                                    }
                                    .toObservable()
                        }
                        HIGH_FREQUENCY -> {

                            dbModule.insertDefect(CDefect(
                                    timestamp = TimeUtils.currentTime(),
                                    defectType = type ?: ""),
                                    cDefectLocations = locations)
                                    .doAfterSuccess { data ->
                                        if (data.isErroneous) {
                                            Observable.error<Throwable>(data.error)
                                        }
                                    }
                                    .map {
                                        RoadDefectCRoadDefectEmbeddedTuple(it, statusType)
                                    }
                                    .toObservable()
                        }
                        STOP -> {
                            dbModule.insertStopTimeWithLocation(
                                    timestamp = TimeUtils.currentTime(),
                                    stRoadInfoLocations = locations)
                                    .doAfterSuccess { data ->
                                        if (data.isErroneous) {
                                            Observable.error<Throwable>(data.error)
                                        }
                                    }
                                    .map {
                                        RoadDefectCRoadDefectEmbeddedTuple(it, statusType)
                                    }
                                    .toObservable()
                        }
                        else -> {
                            Observable.error(Throwable("INVALID TYPE"))
                        }
                    }

                }
                .doOnNext {

                    if (it.locations.hasResult) {

                        when (val statusType = it.type) {
                            STOP -> {
                                apiModule.saveRoadInspection(
                                        data = RoadDefectData(
                                                packageId = deviceInfo.getPackageId(),
                                                deviceID = deviceInfo.getDeviceId(),
                                                roadDefect = it.locations.result))
                                        .flatMap {

                                            if (it.hasResult) {
                                                dbModule.delete(it.result?.map { it.toLong() }
                                                        ?: emptyList())

                                            } else {
                                                Single.error(it.error)
                                                //TODO Update STATUS to FAIL
                                            }
                                        }
                                        .map {
                                            it
                                        }
                                        .subscribe({

                                            it
                                        }, {
                                            sendError(it)
                                        }).disposedBy(disposeBag)
                            }
                        }
                    } else {
                        sendError(it.locations.error)
                    }
                }
                .subscribe({

                    it
                }, {

                    sendError(it)
                })
                .disposedBy(disposeBag)

    }

    private fun sendError(throwable: Throwable?) {
        val error = RoadDefectError(errorResponse = Response.error<String>(throwable),
                actionType = status.name)

        errorTrigger.onNext(error.toPrettyJson())
    }

    data class RoadDefectError<T>(val errorResponse: T, val actionType: String)

    fun errorObserver(): Observable<String> {
        return errorTrigger.hide()
    }

    fun successObserver(): Observable<String> {
        return successTrigger.hide()
    }

    private fun clearObserver() {
        disposeBag.clear()
    }

    enum class DEFECT_STATUS {
        START, STOP, HIGH_FREQUENCY, UNKNOWN
    }
}