package com.lta.mobilitysensingsdk.sdk

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations

class SDKConfig(builder: Builder) {


    private var mAppId: String? = null
    private var mSecret: String? = null
    private var mPlatformURL: String? = null


    init {
        this.mAppId = builder.appId
        this.mSecret = builder.secret
        this.mPlatformURL = builder.platformURL
    }

    class Builder(var appId: String?, var secret: String?) {


        var platformURL: String = ""

        fun baseURL(url: String): Builder {
            platformURL = url
            return this
        }

        fun build(): SDKConfig{
            return SDKConfig(this)
        }
    }



}