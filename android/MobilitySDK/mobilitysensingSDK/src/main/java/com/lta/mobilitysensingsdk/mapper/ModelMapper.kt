package com.lta.mobilitysensingsdk.mapper

import com.lta.datalib.interfaces.db.IStoreDB
import com.lta.datalib.interfaces.sensor.IDeviceInfoResource
import com.lta.datalib.model.AllSensorData
import com.lta.datalib.model.db.CBatchEmbedded


fun CBatchEmbedded.toAllSensorDataTransaction(deviceInfo: IDeviceInfoResource,
                                              storeDb: IStoreDB): AllSensorData {
    return AllSensorData(
            batchId = this.batch.id ?: -1,
            deviceInfo = deviceInfo,
            storeDb = storeDb,
            accelerometer = this.accelerometer,
            accelerometerLinear = this.accelerometerLinear,
            gyroscope = this.gyroscope,
            magnetometer = this.magnetometer,
            orientationAxes = this.orientationAxes,
            rotationVector = this.rotationVector,
            location = this.location,
            barometer = this.barometer,
            pedometer = this.pedometer
    )
}
