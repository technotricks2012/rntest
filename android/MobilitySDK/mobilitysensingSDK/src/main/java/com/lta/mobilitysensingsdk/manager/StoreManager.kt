package com.lta.mobilitysensingsdk.manager

import com.lta.datalib.interfaces.db.IStoreDB
import com.lta.datalib.utils.Response

class StoreManager(var store: IStoreDB) {
    fun saveValue(key: String, value: String) = store.save(key, value)
    fun getValue(key: String): String? = store.get(key).result

}