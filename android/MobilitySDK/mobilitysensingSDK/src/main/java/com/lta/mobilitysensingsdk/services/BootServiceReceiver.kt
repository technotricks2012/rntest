package com.lta.mobilitysensingsdk.services

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import com.lta.mobilitysensingsdk.sdk.SDKManager

class BootServiceReceiver : BroadcastReceiver() {
    override fun onReceive(context: Context, intent: Intent) {
        when (intent.action) {
            Intent.ACTION_BOOT_COMPLETED -> {
                if (SDKManager.INSTANCE.storeManager.store.getIsStart()){
//                    DataUploadService.INSTANCE.startJob(context)
                    SensorCollectionService.INSTANCE.startJob(context)
                }

            }
        }
    }
}