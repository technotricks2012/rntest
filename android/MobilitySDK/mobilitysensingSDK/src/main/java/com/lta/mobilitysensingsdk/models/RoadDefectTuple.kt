package com.lta.mobilitysensingsdk.models

import com.lta.datalib.model.CLocation
import com.lta.datalib.model.roadDefect.CRoadDefectEmbedded
import com.lta.datalib.utils.Response
import com.lta.mobilitysensingsdk.usecases.RoadDefectUseCase

class RoadDefectTuple (val location:Response<CLocation, Throwable>,val type: RoadDefectUseCase.DEFECT_STATUS)

class RoadDefectCRoadDefectEmbeddedTuple (val locations :Response<List<CRoadDefectEmbedded>, Throwable>, val type: RoadDefectUseCase.DEFECT_STATUS)



