package com.lta.mobilitysensingsdk.services.jobs

import android.app.job.JobParameters
import android.content.Context
import android.util.Log
import com.lta.datalib.extenstions.disposedBy
import com.lta.datalib.extenstions.isInternetOn
import com.lta.mobilitysensingsdk.services.jobs.base.BaseJob
import com.lta.mobilitysensingsdk.services.jobs.base.BaseJobService
import com.lta.mobilitysensingsdk.usecases.CollectionUseCase
import com.lta.network.SDKApiImpl
import com.lta.sensor.implementations.DeviceInfoImpl
import com.lta.storelib.implementations.*
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import java.util.concurrent.TimeUnit


class CollectSensorDataFromDBJob(context: Context) : BaseJob(context = context) {

    override fun runJob(jobService: BaseJobService?, params: JobParameters?, disposeBag: CompositeDisposable?): Boolean {
         return CollectionUseCase(
                 disposeBag = disposeBag,
                networkSource = SDKApiImpl(),
                deviceInfo = DeviceInfoImpl(),
                batchDB = BatchImpl(context),
                storeDb = DataStoreImpl(context),
                logStoreData = LogImpl(context)
        )
                .buildWithOutScheduler()
//                .repeatWhen {
//                    Observable.interval(1, TimeUnit.MINUTES)
//                }
                .subscribe({
                    if(it.result == "-1"){
                        Log.d("CollectionUseCase"," Fail 1")
                    }else{
                        Log.d("CollectionUseCase"," Success ${it.result}")

                    }
                }, {
                  //  it
                    Log.d("CollectionUseCase"," Fail 2")

                }).isDisposed

//        return true
    }

    companion object {
        val JOB_ID = 1002
    }
}