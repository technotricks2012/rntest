package com.lta.mobilitysensingsdk.sdk

import android.content.Context
import com.lta.mobilitysensingsdk.services.SensorCollectionService
import com.lta.mobilitysensingsdk.manager.ObserverManager
import com.lta.mobilitysensingsdk.manager.StoreManager
import com.lta.mobilitysensingsdk.services.DataUploadService
import com.lta.mobilitysensingsdk.usecases.RoadDefectUseCase

abstract class BaseSDKManager {

    abstract fun initSdk(context: Context)

    abstract val roadDefect:RoadDefectUseCase?

    abstract val dataUploadService: DataUploadService?
    abstract val sensorCollectionService: SensorCollectionService?
    abstract val observeData: ObserverManager?

    abstract val storeManager: StoreManager?


}
