package com.lta.mobilitysensingsdk.usecases

import android.util.Log
import com.lta.datalib.base.BaseUseCase
import com.lta.datalib.extenstions.flatMapOrError
import com.lta.datalib.interfaces.db.IAccelerometerDB
import com.lta.datalib.interfaces.db.ILogDB
import com.lta.datalib.interfaces.sensor.IAccelerometerResource
import com.lta.datalib.model.db.CLog
import com.lta.datalib.model.sensor.CAccelerometer
import com.lta.datalib.utils.Response
import com.lta.datalib.utils.TimeUtils
import com.lta.datalib.utils.toPrettyJson
import io.reactivex.Observable

class AccelerometerUseCase(
        private val accSensorData: IAccelerometerResource,
        private val accStoreData: IAccelerometerDB,
        private val logStoreData: ILogDB

) : BaseUseCase.WithoutParams<Response<List<CAccelerometer>, Throwable>>() {

    override fun onExecute(): Observable<Response<List<CAccelerometer>, Throwable>> {

        return accSensorData.getList(-1)
                .subscribeOn(defaultBackgroundScheduler())
                .observeOn(defaultObserverScheduler())
                .flatMapOrError {
                    accStoreData.save(it).toFlowable()
                }
                .doOnError{
                    logStoreData.save(
                            CLog(
                                    timestamp = TimeUtils.currentTime(),
                                    type = CLog.TYPE.ERROR,
                                    key = "AccelerometerUseCase",
                                    description = it.toPrettyJson())).subscribe()
                }
                .toObservable()
    }
}