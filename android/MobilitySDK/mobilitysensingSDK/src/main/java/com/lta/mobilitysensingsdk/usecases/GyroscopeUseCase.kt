package com.lta.mobilitysensingsdk.usecases

import com.lta.datalib.base.BaseUseCase
import com.lta.datalib.extenstions.flatMapOrError
import com.lta.datalib.interfaces.db.IGyroscopeDB
import com.lta.datalib.interfaces.db.ILogDB
import com.lta.datalib.interfaces.sensor.IGyroscopeResource
import com.lta.datalib.model.db.CLog
import com.lta.datalib.model.sensor.CGyroscope
import com.lta.datalib.utils.Response
import com.lta.datalib.utils.TimeUtils
import com.lta.datalib.utils.toPrettyJson
import io.reactivex.Observable

class GyroscopeUseCase(
        private val resource: IGyroscopeResource,
        private val store: IGyroscopeDB,
        private val logStoreData: ILogDB
) : BaseUseCase.WithoutParams<Response<List<CGyroscope>, Throwable>>() {

    override fun onExecute(): Observable<Response<List<CGyroscope>, Throwable>> {
        return resource.getList(-1)
                .subscribeOn(defaultBackgroundScheduler())
                .observeOn(defaultObserverScheduler())
                .flatMapOrError {
                    store.save(it).toFlowable()
                }
                .doOnError{
                    logStoreData.save(
                            CLog(
                                    timestamp = TimeUtils.currentTime(),
                                    type = CLog.TYPE.ERROR,
                                    key = "GyroscopeUseCase",
                                    description = it.toPrettyJson())).subscribe()
                }
                .toObservable()
    }
}