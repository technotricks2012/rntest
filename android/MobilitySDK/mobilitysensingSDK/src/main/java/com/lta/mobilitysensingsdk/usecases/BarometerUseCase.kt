package com.lta.mobilitysensingsdk.usecases

import com.lta.datalib.base.BaseUseCase
import com.lta.datalib.extenstions.flatMapOrError
import com.lta.datalib.interfaces.db.IBarometerDB
import com.lta.datalib.interfaces.db.ILogDB
import com.lta.datalib.interfaces.sensor.IBarometerResource
import com.lta.datalib.model.db.CLog
import com.lta.datalib.model.sensor.CBarometer
import com.lta.datalib.utils.Response
import com.lta.datalib.utils.TimeUtils
import com.lta.datalib.utils.toPrettyJson
import io.reactivex.Observable

class BarometerUseCase(
        private val resource: IBarometerResource,
        private val store: IBarometerDB,
        private val logStoreData: ILogDB

) : BaseUseCase.WithoutParams<Response<List<CBarometer>, Throwable>>() {

    override fun onExecute(): Observable<Response<List<CBarometer>, Throwable>> {
        return resource.getList(-1)
                .subscribeOn(defaultBackgroundScheduler())
                .observeOn(defaultObserverScheduler())
                .flatMapOrError {
                    store.save(it).toFlowable()
                }
                .doOnError{
                    logStoreData.save(
                            CLog(
                                    timestamp = TimeUtils.currentTime(),
                                    type = CLog.TYPE.ERROR,
                                    key = "BarometerUseCase",
                                    description = it.toPrettyJson())).subscribe()
                }
                .toObservable()
    }
}