package com.lta.mobilitysensingsdk.usecases

import com.lta.datalib.base.BaseUseCase
import com.lta.datalib.extenstions.flatMapOrError
import com.lta.datalib.interfaces.db.ILogDB
import com.lta.datalib.interfaces.db.IPedometerDB
import com.lta.datalib.interfaces.sensor.IPedometerResource
import com.lta.datalib.model.db.CLog
import com.lta.datalib.model.sensor.CPedometer
import com.lta.datalib.utils.Response
import com.lta.datalib.utils.TimeUtils
import com.lta.datalib.utils.toPrettyJson
import io.reactivex.Observable
import io.reactivex.disposables.Disposable

class PedometerUseCase(
        private val resource: IPedometerResource,
        private val store: IPedometerDB,
        private val logStoreData: ILogDB
) : BaseUseCase.WithoutParams<Response<List<CPedometer>, Throwable>>() {

    override fun onExecute(): Observable<Response<List<CPedometer>, Throwable>> {
        return resource.getList(-1)
                .subscribeOn(defaultBackgroundScheduler())
                .observeOn(defaultObserverScheduler())
                .flatMapOrError {
                    store.save(it).toFlowable()
                }
                .doOnError{
                    logStoreData.save(
                            CLog(
                                    timestamp = TimeUtils.currentTime(),
                                    type = CLog.TYPE.ERROR,
                                    key = "PedometerUseCase",
                                    description = it.toPrettyJson())).subscribe()
                }
                .toObservable()
    }
}