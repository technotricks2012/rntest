package com.lta.mobilitysensingsdk.services

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.Service
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.IBinder
import android.util.Log
import androidx.core.app.NotificationCompat
import com.lta.datalib.extenstions.isServiceRunning
import com.lta.mobilitysensingsdk.services.jobs.*
import io.reactivex.disposables.CompositeDisposable

class SensorCollectionService : Service() {
    private val CHANNEL_ID = "MSPAPP_Notification_CHANNEL"
    private val NOTIFICATION_ID = 900898
    private val disposeBag = CompositeDisposable()

    private fun createNotificationChannel() { // Create the NotificationChannel, but only on API 26+ because
        val notificationManager = getSystemService(NotificationManager::class.java)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val importance = NotificationManager.IMPORTANCE_DEFAULT
            val channel = NotificationChannel(CHANNEL_ID, "Mobility Sensing", importance)
            channel.description = "Sensor Notification 1"
            notificationManager.createNotificationChannel(channel)
        }

        val notification = NotificationCompat.Builder(this, CHANNEL_ID)
                .setContentTitle("Service")
                .setContentText("Running...")
                .setSmallIcon(applicationInfo.icon)
                .setOngoing(true)
                .build()
        startForeground(NOTIFICATION_ID, notification)
    }


    fun startJob(context: Context) {
        if (context.isServiceRunning(SensorCollectionService::class.java)) {
            return
        }

        val intent = Intent(context, SensorCollectionService::class.java)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            context.startForegroundService(intent)
        } else {
            context.startService(intent)
        }
    }


    fun stopJob() {
        stopSelf()
    }

    companion object {
        @JvmStatic
        val INSTANCE by lazy { SensorCollectionService() }

    }

    override fun onBind(intent: Intent?): IBinder? = null

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        startServices()
        return START_STICKY
    }

    private fun startServices() {
        createNotificationChannel()
        startSensorCollection(this)
    }

    override fun onDestroy() {
        super.onDestroy()
        disposeBag.clear()
    }

    private fun startSensorCollection(context: Context) {
        disposeBag.clear()
        Log.d("SensorCollectionService", "startSensorCollection")
        AccelerometerSensorToDBJob(this).runJob(disposeBag = disposeBag)
        AccelerometerSensorLinearToDBJob(this).runJob(disposeBag = disposeBag)
        GyroscopeSensorToDBJob(this).runJob(disposeBag = disposeBag)
        MagnetometerSensorToDBJob(this).runJob(disposeBag = disposeBag)
        OrientationAxesToDBJob(this).runJob(disposeBag = disposeBag)
        RotationVectorToDBJob(this).runJob(disposeBag = disposeBag)
        BarometerSensorToDBJob(this).runJob(disposeBag = disposeBag)
        PedometerSensorToDBJob(this).runJob(disposeBag = disposeBag)
        LocationHFToDBJob(this).runJob(disposeBag = disposeBag)
    }

}