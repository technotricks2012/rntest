package com.lta.mobilitysensingsdk.usecases

import com.lta.datalib.base.BaseUseCase
import com.lta.datalib.extenstions.flatMapOrError
import com.lta.datalib.interfaces.db.ILogDB
import com.lta.datalib.interfaces.db.IRotationVectorDB
import com.lta.datalib.interfaces.sensor.IRotationVectorResource
import com.lta.datalib.model.db.CLog
import com.lta.datalib.model.sensor.CRotationVector
import com.lta.datalib.utils.Response
import com.lta.datalib.utils.TimeUtils
import com.lta.datalib.utils.toPrettyJson
import io.reactivex.Observable

class RotationVectorUseCase(
        private val resource: IRotationVectorResource,
        private val store: IRotationVectorDB,
        private val logStoreData: ILogDB
) : BaseUseCase.WithoutParams<Response<List<CRotationVector>, Throwable>>() {

    override fun onExecute(): Observable<Response<List<CRotationVector>, Throwable>> {
        return resource.getList(-1)
                .subscribeOn(defaultBackgroundScheduler())
                .observeOn(defaultObserverScheduler())
                .flatMapOrError {
                    store.save(it).toFlowable()
                }
                .doOnError{
                    logStoreData.save(
                            CLog(
                                    timestamp = TimeUtils.currentTime(),
                                    type = CLog.TYPE.ERROR,
                                    key = "RotationVectorUseCase",
                                    description = it.toPrettyJson())).subscribe()
                }
                .toObservable()
    }
}