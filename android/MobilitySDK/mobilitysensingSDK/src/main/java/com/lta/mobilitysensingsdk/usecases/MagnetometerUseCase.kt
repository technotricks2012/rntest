package com.lta.mobilitysensingsdk.usecases

import com.lta.datalib.base.BaseUseCase
import com.lta.datalib.extenstions.disposedBy
import com.lta.datalib.extenstions.flatMapOrError
import com.lta.datalib.interfaces.db.IAccelerometerDB
import com.lta.datalib.interfaces.db.ILogDB
import com.lta.datalib.interfaces.db.IMagnetometerDB
import com.lta.datalib.interfaces.sensor.IAccelerometerResource
import com.lta.datalib.interfaces.sensor.IMagnetometerResource
import com.lta.datalib.model.db.CLog
import com.lta.datalib.model.sensor.CAccelerometer
import com.lta.datalib.model.sensor.CMagnetometer
import com.lta.datalib.utils.Response
import com.lta.datalib.utils.TimeUtils
import com.lta.datalib.utils.toPrettyJson
import io.reactivex.Observable
import io.reactivex.disposables.Disposable
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.launch

class MagnetometerUseCase(
        private val sensorSource: IMagnetometerResource,
        private val dbSource: IMagnetometerDB,
        private val logStoreData: ILogDB

) : BaseUseCase.WithoutParams<Response<List<CMagnetometer>, Throwable>>() {

    override fun onExecute(): Observable<Response<List<CMagnetometer>, Throwable>> {
        return sensorSource.getList(-1)
                .subscribeOn(defaultBackgroundScheduler())
                .observeOn(defaultObserverScheduler())
                .flatMapOrError {
                    dbSource.save(it).toFlowable()
                }
                .doOnError{
                    logStoreData.save(
                            CLog(
                                    timestamp = TimeUtils.currentTime(),
                                    type = CLog.TYPE.ERROR,
                                    key = "MagnetometerUseCase",
                                    description = it.toPrettyJson())).subscribe()
                }
                .toObservable()
    }
}