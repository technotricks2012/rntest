package com.lta.mobilitysensingsdk.usecases

import com.lta.datalib.base.BaseUseCase
import com.lta.datalib.extenstions.flatMapOrError
import com.lta.datalib.interfaces.db.ILogDB
import com.lta.datalib.interfaces.db.IOrientationAxesDB
import com.lta.datalib.interfaces.sensor.IOrientationAxesResource
import com.lta.datalib.model.db.CLog
import com.lta.datalib.model.sensor.COrientationAxes
import com.lta.datalib.utils.Response
import com.lta.datalib.utils.TimeUtils
import com.lta.datalib.utils.toPrettyJson
import io.reactivex.Observable

class OrientationAxesUseCase(
        private val resource: IOrientationAxesResource,
        private val store: IOrientationAxesDB,
        private val logStoreData: ILogDB
) : BaseUseCase.WithoutParams<Response<List<COrientationAxes>, Throwable>>() {

    override fun onExecute(): Observable<Response<List<COrientationAxes>, Throwable>> {
        return resource.getList(-1)
                .subscribeOn(defaultBackgroundScheduler())
                .observeOn(defaultObserverScheduler())
                .flatMapOrError {
                    store.save(it).toFlowable()
                }
                .doOnError{
                    logStoreData.save(
                            CLog(
                                    timestamp = TimeUtils.currentTime(),
                                    type = CLog.TYPE.ERROR,
                                    key = "OrientationAxesUseCase",
                                    description = it.toPrettyJson())).subscribe()
                }
                .toObservable()
    }
}