package com.lta.mobilitysensingsdk.models

import com.lta.datalib.model.sensor.CAccelerometer
import com.lta.datalib.model.sensor.CMagnetometer
import com.lta.datalib.utils.Response

class DataObserverTuple(
        val accelerometer: Response<CAccelerometer, Throwable>,
        val magnetometer: Response<CMagnetometer, Throwable>
)

