package com.lta.mobilitysensingsdk.services.jobs

import android.app.job.JobParameters
import android.content.Context
import com.lta.datalib.extenstions.disposedBy
import com.lta.mobilitysensingsdk.services.jobs.base.BaseJob
import com.lta.mobilitysensingsdk.services.jobs.base.BaseJobService
import com.lta.mobilitysensingsdk.usecases.PedometerUseCase
import com.lta.sensor.implementations.SensorPedometerImpl
import com.lta.storelib.implementations.LogImpl
import com.lta.storelib.implementations.StorePedometerImpl
import io.reactivex.disposables.CompositeDisposable

class PedometerSensorToDBJob(context: Context) : BaseJob(context = context) {


    override fun runJob(jobService: BaseJobService?, params: JobParameters?, disposeBag: CompositeDisposable?): Boolean {
        PedometerUseCase(
                SensorPedometerImpl(context),
                StorePedometerImpl(context),
                LogImpl(context)
        )
                .buildWithOutScheduler()
                .subscribe({
                    it
                }, {
                    it
                    //TODO Cache Error
                })
                .disposedBy(disposeBag)


        return true
    }

    companion object {
        val JOB_ID = 1010
    }
}