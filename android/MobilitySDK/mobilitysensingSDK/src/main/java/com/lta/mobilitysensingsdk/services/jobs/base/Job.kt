package com.lta.mobilitysensingsdk.services.jobs.base

import android.app.job.JobParameters
import android.content.Context
import io.reactivex.disposables.CompositeDisposable


interface Job {
    val context: Context
    fun runJob(jobService: BaseJobService? = null, params: JobParameters? = null, disposeBag: CompositeDisposable? = CompositeDisposable()): Boolean
}