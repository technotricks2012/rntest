package com.lta.mobilitysensingsdk.services.jobs.base

import android.content.Context

abstract class BaseJob (context : Context): Job {
    override val context =context.applicationContext

}
