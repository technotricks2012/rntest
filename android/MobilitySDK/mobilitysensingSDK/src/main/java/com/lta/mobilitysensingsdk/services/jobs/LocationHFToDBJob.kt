package com.lta.mobilitysensingsdk.services.jobs

import android.app.job.JobParameters
import android.content.Context
import com.lta.datalib.extenstions.disposedBy
import com.lta.locationlib.implementations.LocationUpdateImpl
import com.lta.mobilitysensingsdk.services.jobs.base.BaseJob
import com.lta.mobilitysensingsdk.services.jobs.base.BaseJobService
import com.lta.mobilitysensingsdk.usecases.LocationHFUseCase
import com.lta.storelib.implementations.LogImpl
import com.lta.storelib.implementations.StoreLocationImpl
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import java.util.concurrent.TimeUnit

class LocationHFToDBJob(context: Context) : BaseJob(context = context) {
    override fun runJob(jobService: BaseJobService?, params: JobParameters?, disposeBag: CompositeDisposable?): Boolean {
        LocationHFUseCase(
                LocationUpdateImpl(),
                StoreLocationImpl(context),
                LogImpl(context)
        )
                .buildWithOutScheduler()
                .repeatWhen {
                    Observable.interval(60 * 1000, TimeUnit.MILLISECONDS)
                }
                .subscribe({

                    it
                }, {

                    it
                })
                .disposedBy(disposeBag)


        return true
    }

    companion object {
        val JOB_ID = 1011
    }
}