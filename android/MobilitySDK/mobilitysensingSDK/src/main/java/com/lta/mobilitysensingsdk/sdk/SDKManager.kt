package com.lta.mobilitysensingsdk.sdk

import android.content.Context
import com.lta.locationlib.SDKLocation
import com.lta.mobilitysensingsdk.services.SensorCollectionService
import com.lta.mobilitysensingsdk.manager.ObserverManager
import com.lta.mobilitysensingsdk.manager.StoreManager
import com.lta.mobilitysensingsdk.services.DataUploadService
import com.lta.mobilitysensingsdk.usecases.RoadDefectUseCase
import com.lta.network.SDKApi
import com.lta.sensor.SDKSensor
import com.lta.storelib.implementations.DataStoreImpl

class SDKManager : BaseSDKManager() {

    var context: Context? = null


    override fun initSdk(context: Context) {
        this.context = context
        SDKSensor.INSTANCE.initContext(context)
        SDKApi.INSTANCE.initContext(context)
        SDKApi.INSTANCE.init(
                publicKey = "Temp",
                privateKey = "Temp"
        )
        SDKLocation.INSTANCE.initContext(context)
        ErrorHandler.initRxErrorHandler()
    }

    override val roadDefect: RoadDefectUseCase
        get() = RoadDefectUseCase.INSTANCE
    override val dataUploadService: DataUploadService?
        get() = DataUploadService.INSTANCE
    override val sensorCollectionService: SensorCollectionService?
        get() = SensorCollectionService.INSTANCE
    override val observeData: ObserverManager
        get() = ObserverManager.INSTANCE

    override val storeManager: StoreManager
        get() = StoreManager(DataStoreImpl(context))
    
    fun startService(context: Context) {
        storeManager.store.setIsStart()
        dataUploadService?.startJob(context)
        sensorCollectionService?.startJob(context)
    }

    fun stopService() {
        dataUploadService?.stopJob()
        sensorCollectionService?.stopJob()
    }

    companion object {
        @JvmStatic
        val INSTANCE by lazy { SDKManager() }
    }

}
