package com.lta.mobilitysensingsdk.usecases

import com.lta.datalib.base.BaseUseCase
import com.lta.datalib.extenstions.flatMapOrError
import com.lta.datalib.interfaces.db.IGyroscopeDB
import com.lta.datalib.interfaces.db.ILocationDB
import com.lta.datalib.interfaces.db.ILogDB
import com.lta.datalib.interfaces.location.ILocation
import com.lta.datalib.model.CLocation
import com.lta.datalib.model.db.CLog
import com.lta.datalib.utils.Response
import com.lta.datalib.utils.TimeUtils
import com.lta.datalib.utils.toPrettyJson
import io.reactivex.Observable

class LocationHFUseCase(
        private val locationSource: ILocation,
        private val store: ILocationDB,
        private val logStoreData: ILogDB
) : BaseUseCase.WithoutParams<Response<List<CLocation>, Throwable>>() {

    override fun onExecute(): Observable<Response<List<CLocation>, Throwable>> {
        return locationSource.getList(-1)
                .subscribeOn(defaultBackgroundScheduler())
                .observeOn(defaultObserverScheduler())
                .take(1)
                .flatMapOrError {
                    store.save(it).toObservable()
                }
                .doOnError {
                    logStoreData.save(
                            CLog(
                                    timestamp = TimeUtils.currentTime(),
                                    type = CLog.TYPE.ERROR,
                                    key = "LocationHFUseCase",
                                    description = it.toPrettyJson())).subscribe()
                }


    }
}