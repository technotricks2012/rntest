package com.lta.mobilitysensingsdk.services.jobs

import android.app.job.JobParameters
import android.content.Context
import android.util.Log
import com.lta.datalib.extenstions.disposedBy
import com.lta.mobilitysensingsdk.services.jobs.base.BaseJob
import com.lta.mobilitysensingsdk.services.jobs.base.BaseJobService
import com.lta.mobilitysensingsdk.usecases.AccelerometerUseCase
import com.lta.sensor.implementations.SensorAccelerometerImpl
import com.lta.storelib.implementations.LogImpl
import com.lta.storelib.implementations.StoreAccelerometerImpl
import io.reactivex.disposables.CompositeDisposable

class AccelerometerSensorToDBJob(context: Context) : BaseJob(context = context) {


    override fun runJob(jobService: BaseJobService?, params: JobParameters?, disposeBag: CompositeDisposable?): Boolean {
        System.identityHashCode(this)

       AccelerometerUseCase(
                SensorAccelerometerImpl(context),
                StoreAccelerometerImpl(context),
                LogImpl(context)
        )
                .buildWithOutScheduler()
                .subscribe({
                    it
                }, {
                    it
                    //TODO Cache Error
                })
                .disposedBy(disposeBag)


        return true
    }

    companion object {
        val JOB_ID = 1001
    }
}