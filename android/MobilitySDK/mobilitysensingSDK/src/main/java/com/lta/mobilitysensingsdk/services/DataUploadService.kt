package com.lta.mobilitysensingsdk.services

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.job.JobInfo
import android.app.job.JobParameters
import android.app.job.JobScheduler
import android.content.ComponentName
import android.content.Context
import android.os.Build
import androidx.core.app.NotificationCompat
import com.lta.mobilitysensingsdk.services.jobs.*
import com.lta.mobilitysensingsdk.services.jobs.base.BaseJobService
import io.reactivex.disposables.CompositeDisposable
import java.util.concurrent.TimeUnit


class DataUploadService : BaseJobService() {
    private val CHANNEL_ID = "MSPAPP_Notification_CHANNEL_Upload"
    private val NOTIFICATION_ID = 800989

    override fun onStartJob(params: JobParameters?): Boolean {
        createNotificationChannel()
        return when (params?.jobId) {
            CollectSensorDataFromDBJob.JOB_ID -> CollectSensorDataFromDBJob(this).runJob(this, params)
            else -> false
        }
    }

    private fun createNotificationChannel() { // Create the NotificationChannel, but only on API 26+ because
        val notificationManager = getSystemService(NotificationManager::class.java)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val importance = NotificationManager.IMPORTANCE_DEFAULT
            val channel = NotificationChannel(CHANNEL_ID, "Mobility Sensing", importance)
            channel.description = "Sensor Notification"
            notificationManager.createNotificationChannel(channel)
        }

        val notification = NotificationCompat.Builder(this, CHANNEL_ID)
                .setContentTitle("Upload Service")
                .setContentText("Uploading...")
                .setSmallIcon(applicationInfo.icon)
                .setOngoing(true)
                .build()
        startForeground(NOTIFICATION_ID, notification)
    }

    fun startJob(context: Context) {
        /**Upload Data**/
        val collectDataFromDBJob = JobInfo.Builder(CollectSensorDataFromDBJob.JOB_ID, ComponentName(context, DataUploadService::class.java))
                .setPeriodic(TimeUnit.MINUTES.toMillis(15))// Run the service with in he range of 15 min
                .setRequiredNetworkType(JobInfo.NETWORK_TYPE_ANY)// Start the service if the network is available
                .setPersisted(true)//Start after reboot the phone
                .build()
        (context.getSystemService(Context.JOB_SCHEDULER_SERVICE) as JobScheduler).schedule(collectDataFromDBJob)
    }

    fun stopJob() {
        stopSelf()
    }

    companion object {
        @JvmStatic
        val INSTANCE by lazy { DataUploadService() }
    }

}