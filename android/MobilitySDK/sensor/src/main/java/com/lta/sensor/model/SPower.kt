package com.lta.sensor.model



data class SPower(
        val lowPowerMode: Boolean,
        var batteryLevel: Float,
        var batteryState: String
)