package com.lta.sensor.implementations

import android.content.Context
import android.hardware.Sensor
import android.hardware.SensorManager
import com.lta.datalib.extenstions.resultOrError
import com.lta.datalib.interfaces.sensor.IRotationVectorResource
import com.lta.datalib.model.sensor.CRotationVector
import com.lta.datalib.utils.Response
import com.lta.sensor.constants.IConfig
import com.lta.sensor.libs.sensor.RxSensor
import com.lta.sensor.mapper.toRotationVectorTransaction
import io.reactivex.Flowable

class SensorRotationVectorImpl(private val context: Context) : IRotationVectorResource {
    override fun get(): Flowable<Response<CRotationVector, Throwable>> =
            RxSensor.sensorEvent(context, Sensor.TYPE_ROTATION_VECTOR)
                    .map {
                        resultOrError {it.toRotationVectorTransaction()}
                    }
                    .onErrorReturn { Response.error(it) }

    override fun getList(batchId: Long): Flowable<Response<List<CRotationVector>, Throwable>> =
            RxSensor.sensorEvent(context, Sensor.TYPE_ROTATION_VECTOR, SensorManager.SENSOR_STATUS_ACCURACY_LOW)
                    .buffer(IConfig.SENSOR_BUFFER_SIZE)
                    .map { events ->
                        resultOrError {events.map { it.toRotationVectorTransaction() }}
                    }
                    .onErrorReturn { Response.error(it) }

}