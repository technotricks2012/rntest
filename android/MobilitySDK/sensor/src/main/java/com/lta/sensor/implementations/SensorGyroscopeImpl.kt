package com.lta.sensor.implementations

import android.content.Context
import android.hardware.Sensor
import android.hardware.SensorManager
import com.lta.datalib.extenstions.resultOrError
import com.lta.datalib.interfaces.sensor.IGyroscopeResource
import com.lta.datalib.model.sensor.CGyroscope
import com.lta.datalib.utils.Response
import com.lta.sensor.constants.IConfig
import com.lta.sensor.libs.sensor.RxSensor
import com.lta.sensor.mapper.toGyroscopeTransaction
import io.reactivex.Flowable

class SensorGyroscopeImpl(private val context: Context) : IGyroscopeResource {
    override fun get(): Flowable<Response<CGyroscope, Throwable>> =
            RxSensor.sensorEvent(context, Sensor.TYPE_GYROSCOPE)
                    .map {
                        resultOrError {it.toGyroscopeTransaction()}
                    }
                    .onErrorReturn { Response.error(it) }


    override fun getList(batchId: Long): Flowable<Response<List<CGyroscope>, Throwable>> =
            RxSensor.sensorEvent(context, Sensor.TYPE_GYROSCOPE, SensorManager.SENSOR_STATUS_ACCURACY_LOW)
                    .buffer(IConfig.SENSOR_BUFFER_SIZE)
                    .map { events ->
                        resultOrError {
                            events.map { it.toGyroscopeTransaction() }
                        }
                    }
                    .onErrorReturn { Response.error(it) }

}