package com.lta.sensor.libs.sensor;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Build;
import android.util.Log;

import io.reactivex.BackpressureStrategy;
import io.reactivex.Flowable;
import io.reactivex.FlowableEmitter;
import io.reactivex.FlowableOnSubscribe;
import io.reactivex.annotations.NonNull;
import io.reactivex.functions.Cancellable;

public final class SensorEventFlowable {

    /**
     * Creates a Flowable that subscirbe to the source of data (sensor) and emits {@link RxSensorEvent}
     * items with a predefined frequency.
     *
     * @param sensorManager      A {@link SensorManager} object.
     * @param sensor             The {@link Sensor Sensor} to register to.
     * @param samplingPeriodUs   See {@link SensorManager#registerListener(SensorEventListener listener, Sensor sensor, int samplingPeriodUs, int maxReportLatencyUs)}
     * @param maxReportLatencyUs See {@link SensorManager#registerListener(SensorEventListener listener, Sensor sensor, int samplingPeriodUs, int maxReportLatencyUs)}
     * @return A Flowable that generates {@link RxSensorEvent} based on the provided parameters.
     */
    public static Flowable<RxSensorEvent> create(final SensorManager sensorManager, final Sensor sensor,
                                                 final int samplingPeriodUs, final int maxReportLatencyUs) {
        return Flowable.create(new FlowableOnSubscribe<RxSensorEvent>() {
            @Override
            public void subscribe(@NonNull FlowableEmitter<RxSensorEvent> e) throws Exception {
                final Listener listener = new Listener(e);
                e.setCancellable(new Cancellable() {
                    @Override
                    public void cancel() throws Exception {
                        Log.d("RxSensorEvent", "unregisterListener");
                        sensorManager.unregisterListener(listener);
                    }
                });

                sensorManager.unregisterListener(listener);
                if (Build.VERSION.SDK_INT < 19) {
                    Log.d("RxSensorEvent", "registerListener");
                    sensorManager.registerListener(listener, sensor, samplingPeriodUs);
                } else {
                    Log.d("RxSensorEvent", "registerListener 1");
                    sensorManager.registerListener(listener, sensor, samplingPeriodUs, maxReportLatencyUs);
                }
            }
        }, BackpressureStrategy.BUFFER);
    }

    /**
     * See {@link #create(SensorManager, Sensor, int, int)}. It used a predefined value for
     * maxReportLatencyUs equal to zero. The events will then be delivered as soon as they will be
     * available.
     */
    public static Flowable<RxSensorEvent> create(SensorManager sensorManager, Sensor sensor,
                                                 int samplingPeriodUs) {
        return create(sensorManager, sensor, samplingPeriodUs, 0);
    }

    static final class Listener implements SensorEventListener {
        private final FlowableEmitter<RxSensorEvent> emitter;

        private long sensorTimeReference = 0L;
        private long myTimeReference = 0L;

        Listener(FlowableEmitter<RxSensorEvent> emitter) {
            this.emitter = emitter;
        }

        @Override
        public void onSensorChanged(SensorEvent event) {

            if(sensorTimeReference == 0L && myTimeReference == 0L) {
                sensorTimeReference = event.timestamp;
                myTimeReference = System.currentTimeMillis();
            }
            // set event timestamp to current time in milliseconds
            event.timestamp = myTimeReference + Math.round((event.timestamp - sensorTimeReference) / 1000000.0);

            emitter.onNext(new RxSensorEvent(event));
        }

        @Override
        public void onAccuracyChanged(Sensor sensor, int accuracy) {
        }
    }
}