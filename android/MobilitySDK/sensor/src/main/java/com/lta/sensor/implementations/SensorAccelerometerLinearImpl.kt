package com.lta.sensor.implementations

import android.content.Context
import android.hardware.Sensor
import android.hardware.SensorManager
import com.lta.datalib.extenstions.resultOrError
import com.lta.datalib.interfaces.sensor.IAccelerometerLinearResource
import com.lta.datalib.model.sensor.CAccelerometerLinear
import com.lta.datalib.utils.Response
import com.lta.sensor.constants.IConfig
import com.lta.sensor.libs.sensor.RxSensor
import com.lta.sensor.libs.sensor.RxSensorFilter
import com.lta.sensor.mapper.toAccelerometerLinearTransaction
import io.reactivex.Flowable
import java.util.concurrent.TimeUnit

class SensorAccelerometerLinearImpl(private val context: Context) : IAccelerometerLinearResource {
    override fun get(): Flowable<Response<CAccelerometerLinear, Throwable>> =
            RxSensor.sensorEvent(context, Sensor.TYPE_LINEAR_ACCELERATION)
            .map {
                resultOrError { it.toAccelerometerLinearTransaction() }
            }
            .onErrorReturn { Response.error(it) }


    override fun getList(batchId: Long): Flowable<Response<List<CAccelerometerLinear>, Throwable>> =
            RxSensor.sensorEvent(context, Sensor.TYPE_LINEAR_ACCELERATION, SensorManager.SENSOR_STATUS_ACCURACY_LOW)
                    .buffer(IConfig.SENSOR_BUFFER_SIZE)
                    .map { events ->
                        resultOrError {
                            events.map { it.toAccelerometerLinearTransaction() }
                        }
                    }
                    .onErrorReturn { Response.error(it) }

}