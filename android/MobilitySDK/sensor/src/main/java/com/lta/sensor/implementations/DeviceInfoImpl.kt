package com.lta.sensor.implementations

import com.lta.datalib.interfaces.sensor.IDeviceInfoResource
import com.lta.datalib.model.device.CPower
import com.lta.sensor.libs.device.DeviceInfo
import com.lta.sensor.mapper.toCPowerTransaction
import com.lta.sensor.model.SPower

class DeviceInfoImpl : IDeviceInfoResource {
    override fun getDeviceId(): String? = DeviceInfo.getUniqueId()
    override fun getPackageId(): String? = DeviceInfo.getPackageId()
    override fun getModel(): String? = DeviceInfo.getModel()
    override fun getSystemVersion(): Int? = DeviceInfo.getSystemVersion()
    override fun getHardware(): String? =DeviceInfo.getHardware()
    override fun isEmulator(): Boolean? = DeviceInfo.isEmulator()
    override fun getDeviceName(): String? = DeviceInfo.getDeviceName()
    override fun getPower(): CPower? = DeviceInfo.getPower().toCPowerTransaction()

}