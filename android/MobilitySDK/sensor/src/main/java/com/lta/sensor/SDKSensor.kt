package com.lta.sensor

import android.content.Context
import com.lta.datalib.interfaces.SdkBase

class SDKSensor :SdkBase{

    var context: Context? = null

    companion object {

        @JvmStatic
        val INSTANCE by lazy {
            SDKSensor()
        }
    }

    init {
        init()
    }

    private fun init() {

    }

    override fun initContext(context: Context) {
        this.context = context
        init()

    }

}