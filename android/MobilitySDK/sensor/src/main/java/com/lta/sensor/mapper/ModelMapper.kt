package com.lta.sensor.mapper

import com.lta.datalib.model.device.CPower
import com.lta.datalib.model.sensor.*
import com.lta.sensor.libs.sensor.RxSensorEvent
import com.lta.sensor.model.SPower


internal fun RxSensorEvent.toAccelerometerTransaction(): CAccelerometer {
    return CAccelerometer(
            x = this.values[0],
            y = this.values[1],
            z = this.values[2],
            timestamp = this.timestamp
    )
}

fun RxSensorEvent.toRotationVectorTransaction(): CRotationVector {
    return CRotationVector(
            x = this.values[0],
            y = this.values[1],
            z = this.values[2],
            rotationVector = this.values[3],
            timestamp = this.timestamp
    )
}






fun RxSensorEvent.toOrientationAxesTransaction(): COrientationAxes {
    return COrientationAxes(
            x = this.values[0],
            y = this.values[1],
            z = this.values[2],
            timestamp = this.timestamp
    )
}


fun RxSensorEvent.toAccelerometerLinearTransaction(): CAccelerometerLinear {
    return CAccelerometerLinear(
            x = this.values[0],
            y = this.values[1],
            z = this.values[2],
            timestamp = this.timestamp
    )
}


fun RxSensorEvent.toGyroscopeTransaction(): CGyroscope {
    return CGyroscope(
            x = this.values[0],
            y = this.values[1],
            z = this.values[2],
            timestamp = this.timestamp
    )
}

fun RxSensorEvent.toMagnetometerTransaction(): CMagnetometer {
    return CMagnetometer(
            x = this.values[0],
            y = this.values[1],
            z = this.values[2],
            timestamp = this.timestamp
    )
}


fun RxSensorEvent.toBarometerTransaction(): CBarometer {
    return CBarometer(
            pressure = this.values[0],
            timestamp = this.timestamp
    )
}

fun RxSensorEvent.toPedometerTransaction(): CPedometer {
    return CPedometer(
            steps = this.values[0],
            distance = 0.0F,
            timestamp = this.timestamp
    )
}


fun SPower.toCPowerTransaction(): CPower {
    return CPower(
            lowPowerMode = this.lowPowerMode,
            batteryLevel = this.batteryLevel,
            batteryState = this.batteryState
    )
}
