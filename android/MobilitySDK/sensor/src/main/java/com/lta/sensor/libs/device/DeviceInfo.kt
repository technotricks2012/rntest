package com.lta.sensor.libs.device

import android.annotation.SuppressLint
import android.content.Context
import android.content.Context.BATTERY_SERVICE
import android.content.Intent
import android.content.IntentFilter
import android.os.BatteryManager
import android.os.Build
import android.os.PowerManager
import android.provider.Settings
import com.lta.sensor.SDKSensor
import com.lta.sensor.model.SPower


object DeviceInfo {
    @SuppressLint("HardwareIds")
    fun getUniqueId(): String? = Settings.Secure.getString(SDKSensor.INSTANCE.context?.contentResolver, Settings.Secure.ANDROID_ID)

    fun getPackageId(): String? = SDKSensor.INSTANCE.context?.packageName
    fun getModel(): String? = Build.MODEL
    fun getSystemVersion(): Int? = Build.VERSION.SDK_INT
    fun getDeviceName(): String? = Build.DEVICE

    fun getHardware(): String? = Build.HARDWARE
    fun isEmulator(): Boolean? = Build.FINGERPRINT.startsWith("generic")
            || Build.FINGERPRINT.startsWith("unknown")
            || Build.MODEL.contains("google_sdk")
            || Build.MODEL.contains("Emulator")
            || Build.MODEL.contains("Android SDK built for x86")
            || Build.BOARD == "QC_Reference_Phone" //bluestacks
            || Build.MANUFACTURER.contains("Genymotion")
            || Build.HOST.startsWith("Build") //MSI App Player
            || (Build.BRAND.startsWith("generic") && Build.DEVICE.startsWith("generic"))
            || "google_sdk" == Build.PRODUCT


    fun getPower(): SPower {
        var context = SDKSensor.INSTANCE.context

        val powerManager: PowerManager? = context?.getSystemService(Context.POWER_SERVICE) as PowerManager
        var powerSaveMode = false
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            powerSaveMode = powerManager?.isPowerSaveMode ?: false
        }

        val iFilter = IntentFilter(Intent.ACTION_BATTERY_CHANGED)
        val batteryStatus: Intent? = context?.registerReceiver(null, iFilter)

        val batteryLevel: Int = batteryStatus?.getIntExtra(BatteryManager.EXTRA_LEVEL, -1) ?: -1
        val batteryScale: Int = batteryStatus?.getIntExtra(BatteryManager.EXTRA_SCALE, -1) ?: -1
        val isPlugged: Int = batteryStatus?.getIntExtra(BatteryManager.EXTRA_PLUGGED, -1) ?: -1
        val status: Int = batteryStatus?.getIntExtra(BatteryManager.EXTRA_STATUS, -1) ?: -1

        val batteryPercentage = batteryLevel / batteryScale.toFloat()

        var batteryState = "unknown"

        when {
            isPlugged === 0 -> {
                batteryState = "unplugged"
            }
            status === BatteryManager.BATTERY_STATUS_CHARGING -> {
                batteryState = "charging"
            }
            status === BatteryManager.BATTERY_STATUS_FULL -> {
                batteryState = "full"
            }
        }

        return SPower(powerSaveMode, batteryPercentage, batteryState)

    }


}