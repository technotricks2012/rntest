//package com.lta.sensor.implementations
//
//import android.content.Context
//import android.hardware.Sensor
//import android.hardware.SensorManager
//import com.lta.datalib.interfaces.sensor.IAxisResource
//import com.lta.datalib.model.sensor.CAxis
//import com.lta.datalib.utils.Response
//import com.lta.sensor.libs.sensor.RxSensor
//import com.lta.sensor.mapper.toAxisTransaction
//import io.reactivex.Flowable
//
//class SensorAxisImpl(private val context: Context, private val type: Int) : IAxisResource {
//    override fun getAxis(): Flowable<Response<CAxis, Throwable>> =
//            RxSensor.sensorEvent(context, type, SensorManager.SENSOR_STATUS_ACCURACY_HIGH)
//                    .map {
//                        Response.result(it.toAxisTransaction())
//                    }
//                    .onErrorReturn { Response.error(it) }
//
//
//    override fun getAxisList(batchId: Long): Flowable<Response<List<CAxis>, Throwable>> =
//            RxSensor.sensorEvent(context, type, SensorManager.SENSOR_STATUS_ACCURACY_HIGH)
//                    .buffer(20)
//                    .map { events ->
//                        Response.result(events.map { it.toAxisTransaction() })
//                    }
//                    .onErrorReturn { Response.error(it) }
//}