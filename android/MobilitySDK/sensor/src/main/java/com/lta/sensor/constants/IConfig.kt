package com.lta.sensor.constants

object IConfig {
    const val SENSOR_BUFFER_SIZE: Int = 100
    const val SENSOR_TAKE_SIZE: Int = 1

}