package com.lta.sensor.implementations

import android.content.Context
import android.hardware.Sensor
import android.hardware.SensorManager
import com.lta.datalib.extenstions.resultOrError
import com.lta.datalib.interfaces.sensor.IPedometerResource
import com.lta.datalib.model.sensor.CPedometer
import com.lta.datalib.utils.Response
import com.lta.sensor.constants.IConfig
import com.lta.sensor.libs.sensor.RxSensor
import com.lta.sensor.mapper.toPedometerTransaction
import io.reactivex.Flowable

class SensorPedometerImpl(private val context: Context) : IPedometerResource {
    override fun get(): Flowable<Response<CPedometer, Throwable>> =
            RxSensor.sensorEvent(context, Sensor.TYPE_STEP_COUNTER)
                    .map {
                        resultOrError {it.toPedometerTransaction()}
                    }
                    .onErrorReturn { Response.error(it) }

    override fun getList(batchId: Long): Flowable<Response<List<CPedometer>, Throwable>> =
            RxSensor.sensorEvent(context, Sensor.TYPE_STEP_COUNTER, SensorManager.SENSOR_STATUS_ACCURACY_MEDIUM)
                    .buffer(IConfig.SENSOR_BUFFER_SIZE)
                    .map { events ->
                        resultOrError {events.map { it.toPedometerTransaction() }}
                    }
                    .onErrorReturn {
                        Response.error(it)
                    }


}