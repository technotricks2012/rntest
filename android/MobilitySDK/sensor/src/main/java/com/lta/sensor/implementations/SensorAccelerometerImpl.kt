package com.lta.sensor.implementations

import android.content.Context
import android.hardware.Sensor
import android.hardware.SensorManager
import com.lta.datalib.extenstions.resultOrError
import com.lta.datalib.interfaces.sensor.IAccelerometerResource
import com.lta.datalib.model.sensor.CAccelerometer
import com.lta.datalib.utils.Response
import com.lta.sensor.constants.IConfig
import com.lta.sensor.libs.sensor.RxSensor
import com.lta.sensor.mapper.toAccelerometerTransaction
import io.reactivex.BackpressureStrategy
import io.reactivex.Flowable
import io.reactivex.schedulers.Schedulers

class SensorAccelerometerImpl(private val context: Context) : IAccelerometerResource {
    override fun get(): Flowable<Response<CAccelerometer, Throwable>> =
            RxSensor.sensorEvent(context, Sensor.TYPE_ACCELEROMETER)
                    .map {
                        resultOrError {
                            it.toAccelerometerTransaction()
                        }
                    }
                    .onErrorReturn { Response.error(it) }


    override fun getList(batchId: Long): Flowable<Response<List<CAccelerometer>, Throwable>> =
            RxSensor.sensorEvent(context, Sensor.TYPE_ACCELEROMETER, SensorManager.SENSOR_STATUS_ACCURACY_LOW)
                    .buffer(IConfig.SENSOR_BUFFER_SIZE)
                    .map { events ->
                        resultOrError {
                            events.map { it.toAccelerometerTransaction() }
                        }
                    }
                    .onErrorReturn { Response.error(it) }

}