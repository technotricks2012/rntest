package com.lta.sensor.libs.sensor.internal;

import android.hardware.Sensor;

import androidx.annotation.RestrictTo;


import com.lta.sensor.libs.sensor.exceptions.SensorNotFoundException;

import static androidx.annotation.RestrictTo.Scope.LIBRARY_GROUP;

@RestrictTo(LIBRARY_GROUP)
public final class Preconditions {
    public static void checkNotNull(Object value, String message) {
        if (value == null) {
            throw new NullPointerException(message);
        }
    }

    public static boolean isCheckNull(Object value) {
        if (value == null) {
           return true;
        }
        return false;
    }

    public static void checkSensorExists(Sensor sensor) {
        if (sensor == null) {
            throw new SensorNotFoundException();
        }
    }

    public static boolean isSensorNotExists(Sensor sensor) {
        if (sensor == null) {
            return true;
        }
        return false;
    }


    private Preconditions() {
        throw new AssertionError("No instances.");
    }
}