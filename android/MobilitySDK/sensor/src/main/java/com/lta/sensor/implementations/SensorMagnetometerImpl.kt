package com.lta.sensor.implementations

import android.content.Context
import android.hardware.Sensor
import android.hardware.SensorManager
import com.lta.datalib.extenstions.resultOrError
import com.lta.datalib.interfaces.sensor.IMagnetometerResource
import com.lta.datalib.model.sensor.CMagnetometer
import com.lta.datalib.utils.Response
import com.lta.sensor.constants.IConfig
import com.lta.sensor.libs.sensor.RxSensor
import com.lta.sensor.mapper.toMagnetometerTransaction
import io.reactivex.Flowable

class SensorMagnetometerImpl(private val context: Context) : IMagnetometerResource {
    override fun get(): Flowable<Response<CMagnetometer, Throwable>> =
            RxSensor.sensorEvent(context, Sensor.TYPE_MAGNETIC_FIELD, SensorManager.SENSOR_STATUS_ACCURACY_HIGH)
            .map {
                resultOrError {it.toMagnetometerTransaction()}
            }
            .onErrorReturn { Response.error(it) }


    override fun getList(batchId: Long): Flowable<Response<List<CMagnetometer>, Throwable>> =
            RxSensor.sensorEvent(context, Sensor.TYPE_MAGNETIC_FIELD, SensorManager.SENSOR_STATUS_ACCURACY_LOW)
                    .buffer(IConfig.SENSOR_BUFFER_SIZE)
                    .map { events ->
                        resultOrError {
                            events.map { it.toMagnetometerTransaction() }

                        }
                    }
                    .onErrorReturn { Response.error(it) }

}