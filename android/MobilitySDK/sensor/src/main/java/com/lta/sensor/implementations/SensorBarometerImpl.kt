package com.lta.sensor.implementations

import android.content.Context
import android.hardware.Sensor
import android.hardware.SensorManager
import com.lta.datalib.extenstions.resultOrError
import com.lta.datalib.interfaces.sensor.IBarometerResource
import com.lta.datalib.model.sensor.CBarometer
import com.lta.datalib.utils.Response
import com.lta.sensor.constants.IConfig
import com.lta.sensor.libs.sensor.RxSensor
import com.lta.sensor.libs.sensor.RxSensorFilter
import com.lta.sensor.mapper.toBarometerTransaction
import io.reactivex.Flowable

class SensorBarometerImpl(private val context: Context) : IBarometerResource {
    override fun get(): Flowable<Response<CBarometer, Throwable>> =
            RxSensor.sensorEvent(context, Sensor.TYPE_PRESSURE)
                    .map {
                        resultOrError {  it.toBarometerTransaction()}
                    }
                    .onErrorReturn { Response.error(it) }

    override fun getList(batchId: Long): Flowable<Response<List<CBarometer>, Throwable>> =
            RxSensor.sensorEvent(context, Sensor.TYPE_PRESSURE, SensorManager.SENSOR_STATUS_ACCURACY_MEDIUM)
                    .buffer(IConfig.SENSOR_BUFFER_SIZE)
                    .map { events ->
                        resultOrError {events.map { it.toBarometerTransaction() }.takeLast(1)}
                    }
                    .onErrorReturn {
                        Response.error(it)
                    }

}