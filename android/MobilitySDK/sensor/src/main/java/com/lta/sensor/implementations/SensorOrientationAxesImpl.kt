package com.lta.sensor.implementations

import android.content.Context
import android.hardware.Sensor
import android.hardware.SensorManager
import com.lta.datalib.extenstions.resultOrError
import com.lta.datalib.interfaces.sensor.IOrientationAxesResource
import com.lta.datalib.model.sensor.COrientationAxes
import com.lta.datalib.utils.Response
import com.lta.sensor.constants.IConfig
import com.lta.sensor.libs.sensor.RxSensor
import com.lta.sensor.mapper.toOrientationAxesTransaction
import io.reactivex.Flowable

class SensorOrientationAxesImpl(private val context: Context) : IOrientationAxesResource {
    override fun get(): Flowable<Response<COrientationAxes, Throwable>> =
            RxSensor.orientationEventWithRemap(context, SensorManager.AXIS_X, SensorManager.AXIS_Z, SensorManager.SENSOR_DELAY_FASTEST)
                    .map {
                        resultOrError {it.toOrientationAxesTransaction()}
                    }
                    .onErrorReturn { Response.error(it) }


    override fun getList(batchId: Long): Flowable<Response<List<COrientationAxes>, Throwable>> =
            RxSensor.orientationEventWithRemap(context, SensorManager.AXIS_X, SensorManager.AXIS_Z, SensorManager.SENSOR_STATUS_ACCURACY_LOW)
                    .buffer(IConfig.SENSOR_BUFFER_SIZE)
                    .map { events ->
                        resultOrError {events.map { it.toOrientationAxesTransaction() }}
                    }
                    .onErrorReturn { Response.error(it) }


}
