package com.lta.storelib.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.lta.storelib.entity.STBarometer

@Dao
interface BarometerDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(chapter: STBarometer)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(chapter: List<STBarometer>)

    @Query("SELECT * FROM ${STBarometer.Properties.TABLE_NAME} WHERE ${STBarometer.Properties.BATCH_IDFK} = :batchId")
    fun queryList(batchId : Long) : List<STBarometer>

    @Query("SELECT * FROM ${STBarometer.Properties.TABLE_NAME} ORDER BY ${STBarometer.Properties.ID} DESC LIMIT 1")
    fun queryLast() : STBarometer

}