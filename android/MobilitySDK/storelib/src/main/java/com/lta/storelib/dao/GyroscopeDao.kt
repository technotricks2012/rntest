package com.lta.storelib.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.lta.storelib.entity.STGyroscope

@Dao
interface GyroscopeDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(chapter: STGyroscope)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(chapter: List<STGyroscope>)

    @Query("SELECT * FROM ${STGyroscope.Properties.TABLE_NAME} WHERE ${STGyroscope.Properties.BATCH_IDFK} = :batchId")
    fun queryList(batchId: Long): List<STGyroscope>

    @Query("SELECT * FROM ${STGyroscope.Properties.TABLE_NAME} ORDER BY ${STGyroscope.Properties.ID} DESC LIMIT 1")
    fun queryLast(): STGyroscope

}