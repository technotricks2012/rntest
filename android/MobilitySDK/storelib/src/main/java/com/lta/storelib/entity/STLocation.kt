package com.lta.storelib.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey

@Entity(tableName =  STLocation.Properties.TABLE_NAME, foreignKeys = [
    ForeignKey(
            entity = STBatch::class,
            parentColumns = arrayOf(STBatch.Properties.ID),
            childColumns = arrayOf(STLocation.Properties.BATCH_IDFK),
            onDelete = ForeignKey.CASCADE
    )]
)
data class STLocation(
    @ColumnInfo(name = Properties.ID)
    @PrimaryKey(autoGenerate = true) val id: Long? = null,
    @ColumnInfo(name = Properties.TIMESTAMP) val timestamp: Long = 0,
    @ColumnInfo(name = Properties.LATITUDE) val latitude: Double,
    @ColumnInfo(name = Properties.LONGITUDE) var longitude: Double,
    @ColumnInfo(name = Properties.ALTITUDE) var altitude: Double,
    @ColumnInfo(name = Properties.ACCURACY) var accuracy: Float,
    @ColumnInfo(name = Properties.IS_MOCK) var isMock: Boolean,
    @ColumnInfo(name = Properties.BATCH_IDFK) var batchIdFK: Long,
    @ColumnInfo(name = Properties.MODE) var mode: String,
    @ColumnInfo(name = Properties.PREDICTIONMode) var predictionMode: String,
    @ColumnInfo(name = Properties.JourneyID) var journeyID: String
){
    object Properties {
        const val TABLE_NAME = "Location"
        const val ID = "locationIdPK"
        const val BATCH_IDFK = "batchIdFK"
        const val TIMESTAMP = "timestamp"
        const val LATITUDE = "latitude"
        const val LONGITUDE = "longitude"
        const val ALTITUDE = "altitude"
        const val ACCURACY = "accuracy"
        const val IS_MOCK = "isMock"

        const val MODE = "mode"
        const val PREDICTIONMode = "predictionMode"
        const val JourneyID = "journeyID"
    }
}

