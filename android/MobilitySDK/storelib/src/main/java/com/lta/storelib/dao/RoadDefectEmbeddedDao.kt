package com.lta.storelib.dao

import androidx.room.*
import com.lta.datalib.exceptions.NoResultError
import com.lta.storelib.entity.roadDefect.*

@Dao
interface RoadDefectEmbeddedDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertStart(defect: STRoadDefect): Long

    @Update
    fun updateRoadDefect(defect: STRoadDefect): Int

    @Transaction
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertRoadDefect(defect: STRoadDefect): Long

    @Transaction
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertRoadDefectInfo(defectLocation: STRoadDefectInfo): Long

    @Transaction
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertDefect(defect: STDefect): Long

    @Transaction
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertDefectLocations(stDefectLocations: List<STDefectLocation>): List<Long>

    @Query("""SELECT * FROM ${STRoadDefect.Properties.TABLE_NAME} WHERE ${STRoadDefect.Properties.STATUS} = '${STRoadDefect.Status.Properties.START}'""")
    fun getRoadDefectWithStart(): List<STRoadDefect>

    @Query("""SELECT * FROM ${STRoadDefect.Properties.TABLE_NAME} WHERE ${STRoadDefect.Properties.STATUS} = '${STRoadDefect.Status.Properties.STOP}'""")
    fun getRoadDefectWithStop(): List<STRoadDefect>

    @Query("""SELECT * FROM ${STRoadDefectInfo.Properties.TABLE_NAME} WHERE ${STRoadDefectInfo.Properties.TYPE} = 'TRUE'""")
    fun getStartRoadDefectInfo(): List<STRoadDefectInfo>


    @Transaction
    fun getStartRoadDefectQuery(): STRoadDefect {
        return getRoadDefectWithStart().getOrNull(0)?.let {
            return@let it
        } ?: run {
            return@run STRoadDefect().apply {
                id = insertStart(this)
            }
        }
    }

    @Transaction
    fun getRoadDefectInfoQuery(): STRoadDefectInfo {
        return getStartRoadDefectInfo().getOrNull(0)?.let {
            return@let it
        } ?: run {
            return@run STRoadDefectInfo(isStart = true).apply {
                id = insertRoadDefectInfo(this)
            }
        }
    }

    @Transaction
    fun getStopRoadDefectQuery(): STRoadDefect? {

        return getRoadDefectWithStart().getOrNull(0).apply {
            this?.status = STRoadDefect.Status.STOP
        }?.apply {
            updateRoadDefect(this)
            getRoadDefectWithStop().getOrNull(0)
        }
    }

    @Transaction
    fun insertStartTransaction(timestamp: Long, stRoadInfoLocations: List<STDefectLocation>): List<STRoadDefectEmbedded> {
        val rideIDPK = getStartRoadDefectQuery().id ?: -1

        if (rideIDPK <= 0) throw NoResultError(message = "row record found")

        val roadDefectInfoId = STRoadDefectInfo(isStart = true).apply {
            rideID_FK = rideIDPK
            this.timestamp = timestamp
        }.apply {
            this.id = insertRoadDefectInfo(this)
        }.id ?: -1

        if (roadDefectInfoId <= 0) throw NoResultError(message = "row record found")

        stRoadInfoLocations.map {
            it.roadDefectInfo_FK = roadDefectInfoId
            it
        }.apply {
            val ids =insertDefectLocations(this)
        }

        return getDefects()
    }


    @Transaction
    fun insertStopTransaction(timestamp: Long, stRoadInfoLocations: List<STDefectLocation>): List<STRoadDefectEmbedded> {
        val rideIDPK = getStopRoadDefectQuery()?.id ?: -1

        if (rideIDPK < 1) throw NoResultError(message = "row record found")

        val roadDefectInfoId = STRoadDefectInfo(isStart = false).apply {
            rideID_FK = rideIDPK
            this.timestamp = timestamp
        }.apply {
            this.id = insertRoadDefectInfo(this)
        }.id ?: -1

        if (roadDefectInfoId <= 0) throw NoResultError(message = "row record found")

        stRoadInfoLocations.map {
            it.roadDefectInfo_FK = roadDefectInfoId
            it
        }.apply {
            val ids = insertDefectLocations(this)
        }

        return getDefects()
    }

    @Transaction
    fun insertDefectTransaction(stDefect: STDefect, stDefectLocations: List<STDefectLocation>): List<STRoadDefectEmbedded> {
        val rideIDPK = getStartRoadDefectQuery().id ?: -1

        if (rideIDPK <= 0) throw NoResultError(message = "row record found")

        stDefect.rideID_FK = rideIDPK
        val childId = insertDefect(stDefect)
         stDefect.id = childId

        if (childId <= 0) throw NoResultError(message = "row record found")

        stDefectLocations.map {
            it.defectID_FK = childId
            it
        }.apply {
            var ids = insertDefectLocations(this)
        }
        return getDefects()

    }

    @Query("SELECT * FROM ${STRoadDefect.Properties.TABLE_NAME} WHERE ${STRoadDefect.Properties.STATUS} = '${STRoadDefect.Status.Properties.STOP}'")
    fun getDefects(): List<STRoadDefectEmbedded>

    @Query("DELETE FROM ${STRoadDefect.Properties.TABLE_NAME} WHERE ${STRoadDefect.Properties.ID} in (:ids)")
    fun delete(ids: List<Long>)
}