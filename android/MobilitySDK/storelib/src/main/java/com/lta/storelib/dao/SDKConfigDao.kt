package com.lta.storelib.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.lta.storelib.entity.SDKConfig

@Dao
interface SDKConfigDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(chapter: SDKConfig)

    @Query("SELECT * FROM ${SDKConfig.Properties.TABLE_NAME} WHERE ${SDKConfig.Properties.ID} = :id")
    fun getConfig(id : Long) : SDKConfig?
}