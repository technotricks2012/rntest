package com.lta.storelib.implementations

import android.content.Context
import com.lta.datalib.extenstions.applyIOWorkSchedulers
import com.lta.datalib.extenstions.resultOrError
import com.lta.datalib.interfaces.db.IBatchDB
import com.lta.datalib.model.db.CBatch
import com.lta.datalib.model.db.CBatchEmbedded
import com.lta.datalib.utils.Response
import com.lta.storelib.datastore.batch.BatchDatabaseDataStore
import com.lta.storelib.entity.STBatch
import com.lta.storelib.helpers.toCBatchEmbeddedTransaction
import com.lta.storelib.helpers.toCBatchTransaction
import io.reactivex.Single

class BatchImpl(private val context: Context) : IBatchDB {
//    private val batchDatabaseDataStore = BatchDatabaseDataStore(context)

    override fun getNewBatch(): Single<Response<CBatch, Throwable>> =
            Single.fromCallable {
                resultOrError {
                    BatchDatabaseDataStore.getInstance(context).getNewBatch().result?.toCBatchTransaction()
                }
            }.applyIOWorkSchedulers()

    override fun delete(batchId: Long): Single<Response<Boolean, Throwable>> =
            Single.fromCallable {
                resultOrError {
                    BatchDatabaseDataStore.getInstance(context).delete(batchId).result
                }
            }.applyIOWorkSchedulers()

    override fun updateStatus(batchId: Long, status: CBatch.Status): Single<Response<Boolean, Throwable>> =
        Single.fromCallable {


            resultOrError {
                BatchDatabaseDataStore.getInstance(context).updateStatus(batchId,status = STBatch.Status.valueOf(status.value)).result
            }
        }.applyIOWorkSchedulers()

    override fun getReadyOrFailBatch(): Single<Response<List<CBatchEmbedded>, Throwable>> =
            Single.fromCallable {
                resultOrError {
                    BatchDatabaseDataStore.getInstance(context).getReadyOrFailBatch().result?.map { it.toCBatchEmbeddedTransaction() }
                }
            }.applyIOWorkSchedulers()


}