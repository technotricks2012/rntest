package com.lta.storelib.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.lta.storelib.entity.STLog

@Dao
interface LogDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(chapter: STLog)

    @Query("SELECT * FROM ${STLog.Properties.TABLE_NAME}")
    fun queryList(): List<STLog>
}