package com.lta.storelib.datastore.accelerometerLinear

import android.content.Context
import com.lta.datalib.extenstions.resultOrError
import com.lta.datalib.model.sensor.CAccelerometerLinear
import com.lta.datalib.utils.Response
import com.lta.storelib.Database
import com.lta.storelib.datastore.base.AbstractDataStore
import com.lta.storelib.helpers.toCAccelerometerLinearTransaction
import com.lta.storelib.helpers.toSTAccelerometerLinearTransaction

internal class AccelerometerLinearDatabaseDataStore(context: Context?) : AbstractDataStore(context = context), AccelerometerLinearDataStore {


    override  fun save(cAccelerometer: CAccelerometerLinear): Response<CAccelerometerLinear, Throwable> = saveItem(cAccelerometer)

    override  fun save(cAccelerometers: List<CAccelerometerLinear>): Response<List<CAccelerometerLinear>, Throwable> = saveItems(cAccelerometers)
    override  fun getList(batchId: Long): Response<List<CAccelerometerLinear>, Throwable> = getItems(batchId)
    override fun getLast(): Response<CAccelerometerLinear, Throwable> =
        resultOrError {
            return@resultOrError Database.getDatabase(context)
                    .accelerometerLinearDao()
                    .queryLast().toCAccelerometerLinearTransaction()
        }


    private  fun saveItem(cAccelerometer: CAccelerometerLinear): Response<CAccelerometerLinear, Throwable> =
        resultOrError {
            Database.getDatabase(context)
                    .accelerometerLinearDao()
                    .insert(cAccelerometer.toSTAccelerometerLinearTransaction())
            return@resultOrError cAccelerometer
        }


    private  fun saveItems(cAccelerometer: List<CAccelerometerLinear>): Response<List<CAccelerometerLinear>, Throwable> =
        resultOrError {
            Database.getDatabase(context)
                    .accelerometerLinearDao()
                    .insert(cAccelerometer.map { it.toSTAccelerometerLinearTransaction() })
            return@resultOrError cAccelerometer
        }


    private  fun getItems(batchId: Long): Response<List<CAccelerometerLinear>, Throwable> =
        resultOrError {
            return@resultOrError Database.getDatabase(context)
                .accelerometerLinearDao()
                .queryList(batchId).map {
                        it.toCAccelerometerLinearTransaction()
                    }
        }
    }
