package com.lta.storelib.helpers

open class SynchronizedSingletonHolder<out T, in A>(creator : (A) -> T) {


    private var creator : ((A) -> T)? = creator
    @Volatile private var instance : T? = null


    fun getInstance(argument : A) : T {
        val firstInstance = instance

        if(firstInstance != null) {
            return firstInstance
        }

        return synchronized(this) {
            val secondInstance = instance

            if(secondInstance != null) {
                secondInstance
            } else {
                val createdInstance = creator!!(argument)
                instance = createdInstance
                creator = null

                createdInstance
            }
        }
    }


}