package com.lta.storelib.datastore.gyroscope

import android.content.Context
import com.lta.datalib.extenstions.resultOrError
import com.lta.datalib.model.sensor.CGyroscope
import com.lta.datalib.utils.Response
import com.lta.storelib.Database
import com.lta.storelib.datastore.base.AbstractDataStore
import com.lta.storelib.helpers.toCGyroscopeTransaction
import com.lta.storelib.helpers.toSTGyroscopeTransaction

internal class GyroscopeDatabaseDataStore(context: Context?) : AbstractDataStore(context = context), GyroscopeDataStore {

    override fun save(sensorData: CGyroscope): Response<CGyroscope, Throwable> = saveItem(sensorData)

    override fun save(sensorDatas: List<CGyroscope>): Response<List<CGyroscope>, Throwable> = saveItems(sensorDatas)
    override fun getList(batchId: Long): Response<List<CGyroscope>, Throwable> = getItems(batchId)
    override fun getLast(): Response<CGyroscope, Throwable> =
            resultOrError {
                return@resultOrError Database.getDatabase(context)
                        .gyroscopeDao()
                        .queryLast().toCGyroscopeTransaction()
            }

    private fun saveItem(sensorData: CGyroscope): Response<CGyroscope, Throwable> =
            resultOrError {
                Database.getDatabase(context)
                        .gyroscopeDao()
                        .insert(sensorData.toSTGyroscopeTransaction())
                return@resultOrError sensorData
            }

    private fun saveItems(sensorDatas: List<CGyroscope>): Response<List<CGyroscope>, Throwable> =
            resultOrError {
                Database.getDatabase(context)
                        .gyroscopeDao()
                        .insert(sensorDatas.map { it.toSTGyroscopeTransaction() })
                return@resultOrError sensorDatas
            }
    
    private fun getItems(batchId: Long): Response<List<CGyroscope>, Throwable> =
            resultOrError {
                return@resultOrError Database.getDatabase(context)
                        .gyroscopeDao()
                        .queryList(batchId).map {
                            it.toCGyroscopeTransaction()
                        }
            }
}
