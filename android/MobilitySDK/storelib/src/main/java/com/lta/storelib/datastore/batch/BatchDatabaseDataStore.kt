package com.lta.storelib.datastore.batch

import android.content.Context
import android.util.Log
import androidx.sqlite.db.SimpleSQLiteQuery
import com.lta.datalib.exceptions.NoResultError
import com.lta.datalib.extenstions.resultOrError
import com.lta.datalib.helper.SingletonHolder
import com.lta.datalib.utils.Response
import com.lta.storelib.Database
import com.lta.storelib.datastore.base.AbstractDataStore
import com.lta.storelib.entity.STBatch
import com.lta.storelib.entity.STBatchEmbedded
import kotlinx.coroutines.*

internal class BatchDatabaseDataStore(
        context: Context?
) : AbstractDataStore(context = context), BatchDataStore {

    companion object : SingletonHolder<BatchDatabaseDataStore, Context>(::BatchDatabaseDataStore)

    var batchCount: Long = 0
    override fun getNewBatch(): Response<STBatch, Throwable> {
        batchCount++
        return resultOrError {
            Log.d("BATCH Count", "Batch $batchCount")
            val pair = Database.getDatabase(context)
                    .batchDao()
                    .getNewBatch(batchCount)
            if (pair?.second == true) {
                batchCount = 0
            }
            pair?.first
        }

    }

    override fun getReadyOrFailBatch(): Response<List<STBatchEmbedded>, Throwable> =
            resultOrError {
                Database.getDatabase(context)
                        .batchDao()
                        .getBatchEmbedded()
            }

    override fun delete(batchId: Long): Response<Boolean, Throwable> =
            resultOrError {
                Database.getDatabase(context)
                        .batchDao()
                        .delete(batchId)
                true
            }

    override fun updateStatus(batchId: Long, status: STBatch.Status): Response<Boolean, Throwable> =
            resultOrError {
                Database.getDatabase(context)
                        .batchDao()
                        .updateStatus(batchId, status = status.value)
                true
            }

    override fun getCount(): Response<Long, Throwable> = resultOrError {
        Database.getDatabase(context)
                .batchDao()
                .getCount(SimpleSQLiteQuery("select IFNULL(sum(seq),0) AS Count from sqlite_sequence WHERE name in ('Accelerometer')"))
    }
}