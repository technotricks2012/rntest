package com.lta.storelib.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.lta.storelib.entity.SDKConfig
import com.lta.storelib.entity.STAccelerometer
import io.reactivex.Flowable

@Dao
interface AccelerometerDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(chapter: STAccelerometer)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(chapter: List<STAccelerometer>)

    @Query("SELECT * FROM ${STAccelerometer.Properties.TABLE_NAME} WHERE ${STAccelerometer.Properties.BATCH_IDFK} = :batchId")
    fun queryList(batchId : Long) : List<STAccelerometer>

    @Query("SELECT * FROM ${STAccelerometer.Properties.TABLE_NAME} ORDER BY ${STAccelerometer.Properties.ID} DESC LIMIT 1")
    fun queryLast() : STAccelerometer

}