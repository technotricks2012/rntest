package com.lta.storelib.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.lta.storelib.entity.SDKConfig
import com.lta.storelib.entity.STAccelerometer
import com.lta.storelib.entity.STMagnetometer
import io.reactivex.Flowable

@Dao
interface MagnetometerDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(chapter: STMagnetometer)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(chapter: List<STMagnetometer>)

    @Query("SELECT * FROM ${STMagnetometer.Properties.TABLE_NAME} WHERE ${STMagnetometer.Properties.BATCH_IDFK} = :batchId")
    fun queryList(batchId : Long) : List<STMagnetometer>

    @Query("SELECT * FROM ${STMagnetometer.Properties.TABLE_NAME} ORDER BY ${STMagnetometer.Properties.ID} DESC LIMIT 1")
    fun queryLast() : STMagnetometer

}