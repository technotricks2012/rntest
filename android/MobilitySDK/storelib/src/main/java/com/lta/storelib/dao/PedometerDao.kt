package com.lta.storelib.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.lta.storelib.entity.STPedometer

@Dao
interface PedometerDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(chapter: STPedometer)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(chapter: List<STPedometer>)

    @Query("SELECT * FROM ${STPedometer.Properties.TABLE_NAME} WHERE ${STPedometer.Properties.BATCH_IDFK} = :batchId")
    fun queryList(batchId : Long) : List<STPedometer>

    @Query("SELECT * FROM ${STPedometer.Properties.TABLE_NAME} ORDER BY ${STPedometer.Properties.ID} DESC LIMIT 1")
    fun queryLast() : STPedometer

}