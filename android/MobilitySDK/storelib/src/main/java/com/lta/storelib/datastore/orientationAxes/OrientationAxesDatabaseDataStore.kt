package com.lta.storelib.datastore.orientationAxes

import android.content.Context
import com.lta.datalib.extenstions.resultOrError
import com.lta.datalib.model.sensor.COrientationAxes
import com.lta.datalib.utils.Response
import com.lta.storelib.Database
import com.lta.storelib.datastore.base.AbstractDataStore
import com.lta.storelib.helpers.toCOrientationAxesTransaction
import com.lta.storelib.helpers.toSTOrientationAxesTransaction

internal class OrientationAxesDatabaseDataStore(context: Context?) : AbstractDataStore(context = context), OrientationAxesDataStore {


    override  fun save(cSensor: COrientationAxes): Response<COrientationAxes, Throwable> = saveItem(cSensor)

    override  fun save(cSensors: List<COrientationAxes>): Response<List<COrientationAxes>, Throwable> = saveItems(cSensors)
    override  fun getList(batchId: Long): Response<List<COrientationAxes>, Throwable> = getItems(batchId)
    override fun getLast(): Response<COrientationAxes, Throwable> =
        resultOrError {
            return@resultOrError Database.getDatabase(context)
                    .orientationAxesDao()
                    .queryLast().toCOrientationAxesTransaction()
        }


    private  fun saveItem(cSensor: COrientationAxes): Response<COrientationAxes, Throwable> =
        resultOrError {
            Database.getDatabase(context)
                    .orientationAxesDao()
                    .insert(cSensor.toSTOrientationAxesTransaction())
            return@resultOrError cSensor
        }


    private  fun saveItems(cSensors: List<COrientationAxes>): Response<List<COrientationAxes>, Throwable> =
        resultOrError {
            Database.getDatabase(context)
                    .orientationAxesDao()
                    .insert(cSensors.map { it.toSTOrientationAxesTransaction() })
            return@resultOrError cSensors
        }


    private  fun getItems(batchId: Long): Response<List<COrientationAxes>, Throwable> =
        resultOrError {
            return@resultOrError Database.getDatabase(context)
                .orientationAxesDao()
                .queryList(batchId).map {
                        it.toCOrientationAxesTransaction()
                    }
        }
    }
