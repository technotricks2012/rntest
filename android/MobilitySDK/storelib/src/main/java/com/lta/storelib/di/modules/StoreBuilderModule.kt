//package com.lta.storelib.di.modules
//
//import android.content.Context
//import com.lta.datalib.qualifiers.Source
//import com.lta.storelib.datastore.DataStoreFactory
//import com.lta.storelib.datastore.sdkconfig.SDKConfigConfigDataStore
//import com.lta.storelib.datastore.sdkconfig.SDKConfigDataStoreFactory
//import dagger.Module
//import dagger.Provides
//import javax.inject.Singleton
//
//@Module
//public abstract class StoreBuilderModule {
//    @Source(Source.Type.DATABASE)
//    @Provides
//    @Singleton
//    fun provideDatabaseDataStore(context: Context): SDKConfigConfigDataStore {
//        return SDKConfigDataStoreFactory(context).create(DataStoreFactory.Type.DATABASE)
//    }
//}