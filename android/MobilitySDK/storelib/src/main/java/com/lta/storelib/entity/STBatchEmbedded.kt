package com.lta.storelib.entity

import androidx.room.*

data class STBatchEmbedded(
        @Embedded
        val batch: STBatch,
        @Relation(parentColumn = STBatch.Properties.ID, entityColumn = STAccelerometer.Properties.BATCH_IDFK, entity = STAccelerometer::class)
        val accelerometer: List<STAccelerometer>,
        @Relation(parentColumn = STBatch.Properties.ID, entityColumn = STAccelerometerLinear.Properties.BATCH_IDFK, entity = STAccelerometerLinear::class)
        val accelerometerLinear: List<STAccelerometerLinear>,
        @Relation(parentColumn = STBatch.Properties.ID, entityColumn = STGyroscope.Properties.BATCH_IDFK, entity = STGyroscope::class)
        val gyroscope: List<STGyroscope>,
        @Relation(parentColumn = STBatch.Properties.ID, entityColumn = STMagnetometer.Properties.BATCH_IDFK, entity = STMagnetometer::class)
        val magnetometer: List<STMagnetometer>,
        @Relation(parentColumn = STBatch.Properties.ID, entityColumn = STLocation.Properties.BATCH_IDFK, entity = STLocation::class)
        val location: List<STLocation>,
        @Relation(parentColumn = STBatch.Properties.ID, entityColumn = STOrientationAxes.Properties.BATCH_IDFK, entity = STOrientationAxes::class)
        val orientationAxes: List<STOrientationAxes>,
        @Relation(parentColumn = STBatch.Properties.ID, entityColumn = STRotationVector.Properties.BATCH_IDFK, entity = STRotationVector::class)
        val rotationVector: List<STRotationVector>,
        @Relation(parentColumn = STBatch.Properties.ID, entityColumn = STBarometer.Properties.BATCH_IDFK, entity = STBarometer::class)
        val barometer: List<STBarometer>,
        @Relation(parentColumn = STBatch.Properties.ID, entityColumn = STPedometer.Properties.BATCH_IDFK, entity = STPedometer::class)
        val pedometer: List<STPedometer>
)
