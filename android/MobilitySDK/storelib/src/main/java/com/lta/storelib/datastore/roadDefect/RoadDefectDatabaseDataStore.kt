package com.lta.storelib.datastore.roadDefect

import android.content.Context
import com.lta.datalib.extenstions.resultOrError
import com.lta.datalib.model.roadDefect.CDefect
import com.lta.datalib.model.roadDefect.CRoadDefect
import com.lta.datalib.model.roadDefect.CRoadDefectEmbedded
import com.lta.datalib.utils.Response
import com.lta.storelib.Database
import com.lta.storelib.datastore.base.AbstractDataStore
import com.lta.storelib.entity.roadDefect.STDefect
import com.lta.storelib.entity.roadDefect.STDefectLocation
import com.lta.storelib.entity.roadDefect.STRoadDefect
import com.lta.storelib.helpers.toCDefectTransaction
import com.lta.storelib.helpers.toCRoadDefectEmbeddedTransaction
import com.lta.storelib.helpers.toCRoadDefectTransaction

internal class RoadDefectDatabaseDataStore(context: Context?) : AbstractDataStore(context = context), RoadDefectDataStore {

    override fun insertDefect(stDefect: STDefect, stDefectLocations: List<STDefectLocation>): Response<List<CRoadDefectEmbedded>, Throwable> =
            resultOrError {
                return@resultOrError Database.getDatabase(context)
                        .roadDefect()
                        .insertDefectTransaction(stDefect, stDefectLocations)
                        .map { it.toCRoadDefectEmbeddedTransaction() }

            }

    override fun insertStartTimeWithLocation(timestamp: Long, stRoadInfoLocations: List<STDefectLocation>): Response<List<CRoadDefectEmbedded>, Throwable> {
        return resultOrError {
            Database.getDatabase(context)
                    .roadDefect()
                    .insertStartTransaction(timestamp, stRoadInfoLocations)
                    .map { it.toCRoadDefectEmbeddedTransaction() }
        }
    }

    override fun insertStopTimeWithLocation(timestamp: Long, stRoadInfoLocations: List<STDefectLocation>): Response<List<CRoadDefectEmbedded>, Throwable> {
        return resultOrError {
            Database.getDatabase(context)
                    .roadDefect()
                    .insertStopTransaction(timestamp, stRoadInfoLocations)
                    .map { it.toCRoadDefectEmbeddedTransaction() }
        }
    }

    override fun delete(ids: List<Long>): Response<Boolean, Throwable> {
        return resultOrError {
            Database.getDatabase(context)
                    .roadDefect()
                    .delete(ids)
            true
        }
    }

    override fun getList(): Response<List<CRoadDefectEmbedded>, Throwable> =
            resultOrError {
                return@resultOrError Database.getDatabase(context)
                        .roadDefect()
                        .getDefects().map {
                            it.toCRoadDefectEmbeddedTransaction()
                        }
            }

}
