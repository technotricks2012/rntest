package com.lta.storelib.datastore.magnetometer

import com.lta.datalib.model.sensor.CMagnetometer
import com.lta.datalib.utils.Response
import com.lta.storelib.datastore.base.DataStore

interface MagnetometerDataStore :DataStore{

     fun save(stAccelerometer: CMagnetometer) : Response<CMagnetometer,Throwable>
     fun save(stAccelerometer: List<CMagnetometer>) : Response<List<CMagnetometer>,Throwable>
     fun getList(batchId:Long) : Response<List<CMagnetometer>,Throwable>
     fun getLast() : Response<CMagnetometer,Throwable>



}