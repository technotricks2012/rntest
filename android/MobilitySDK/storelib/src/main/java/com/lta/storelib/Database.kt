package com.lta.storelib

import android.content.Context
import androidx.room.Room
import androidx.room.RoomDatabase
import com.lta.storelib.dao.*
import com.lta.storelib.entity.*
import com.lta.storelib.entity.roadDefect.*

@androidx.room.Database(
        entities = [
            SDKConfig::class,
            STLog::class,

            STBatch::class,
            STAccelerometer::class,
            STAccelerometerLinear::class,
            STGyroscope::class,
            STOrientationAxes::class,
            STMagnetometer::class,
            STRotationVector::class,
            STBarometer::class,
            STPedometer::class,
            STLocation::class,

            STRoadDefect::class,
            STRoadDefectInfo::class,
            STDefect::class,
            STDefectLocation::class
        ],
        version = Constants.DATABASE_VERSION,
        exportSchema = false
)

abstract class Database : RoomDatabase() {

    abstract fun sdkConfigTable(): SDKConfigDao

    abstract fun batchDao(): BatchDao
    abstract fun accelerometerDao(): AccelerometerDao
    abstract fun accelerometerLinearDao(): AccelerometerLinearDao
    abstract fun gyroscopeDao(): GyroscopeDao
    abstract fun orientationAxesDao(): OrientationAxesDao
    abstract fun magnetometerDao(): MagnetometerDao
    abstract fun rotationVectorDao(): RotationVectorDao
    abstract fun barometerDao(): BarometerDao
    abstract fun pedometerDao(): PedometerDao
    abstract fun locationDao(): LocationDao

    abstract fun logDao(): LogDao




    abstract fun roadDefect(): RoadDefectEmbeddedDao

    companion object {
        @Volatile
        private var INSTANCE: Database? = null

        fun getDatabase(context: Context): Database {
            val tempInstance = INSTANCE
            if (tempInstance != null) {
                return tempInstance
            }
            synchronized(this) {
                val instance = Room.databaseBuilder(
                        context.applicationContext,
                        Database::class.java,
                        Constants.DATABASE_NAME
                ).build()
                INSTANCE = instance
                return instance
            }
        }
    }
}