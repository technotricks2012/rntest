package com.lta.storelib.datastore.rotationVector

import com.lta.datalib.model.sensor.CRotationVector
import com.lta.datalib.utils.Response
import com.lta.storelib.datastore.base.DataStore

interface RotationVectorDataStore : DataStore {
    fun save(stAccelerometer: CRotationVector): Response<CRotationVector, Throwable>
    fun save(stAccelerometer: List<CRotationVector>): Response<List<CRotationVector>, Throwable>
    fun getList(batchId: Long): Response<List<CRotationVector>, Throwable>
    fun getLast(): Response<CRotationVector, Throwable>
}