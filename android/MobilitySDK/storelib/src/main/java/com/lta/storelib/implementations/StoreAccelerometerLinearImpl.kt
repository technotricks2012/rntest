package com.lta.storelib.implementations

import android.content.Context
import android.util.Log
import com.lta.datalib.extenstions.applyIOWorkSchedulers
import com.lta.datalib.interfaces.db.IAccelerometerLinearDB
import com.lta.datalib.model.sensor.CAccelerometerLinear
import com.lta.datalib.utils.Response
import com.lta.storelib.datastore.accelerometerLinear.AccelerometerLinearDatabaseDataStore
import com.lta.storelib.datastore.batch.BatchDatabaseDataStore
import io.reactivex.Single

class StoreAccelerometerLinearImpl(private val context: Context) : IAccelerometerLinearDB {
    private val accelerometerDatabaseDataStore = AccelerometerLinearDatabaseDataStore(context)
//    private val batchDatabaseDataStore = BatchDatabaseDataStore(context)
    private val storeDb = DataStoreImpl(context)



    override fun getList(batchId: Long): Single<Response<List<CAccelerometerLinear>, Throwable>> =
            Single.fromCallable {
                accelerometerDatabaseDataStore.getList(batchId)
            }.applyIOWorkSchedulers()


    override fun getLast(): Single<Response<CAccelerometerLinear, Throwable>> =
            Single.fromCallable {
                accelerometerDatabaseDataStore.getLast()
            }.applyIOWorkSchedulers()

    override fun save(data: CAccelerometerLinear): Single<Response<CAccelerometerLinear, Throwable>> =
            Single.fromCallable {
//                val batch = batchDatabaseDataStore.getNewBatch()
                val batch = BatchDatabaseDataStore.getInstance(context).getNewBatch()

                val journeyID = storeDb.getJourneyId()
                var mode = storeDb.getMode()
                var predictionMode = storeDb.getPredictionMode()
                if (batch.hasResult) {
                    data.batch_id = batch.result?.id ?: -1
                    data.journeyID = journeyID
                    data.predictionMode = predictionMode
                    data.mode = mode
                } else
                    data.batch_id = -1
                accelerometerDatabaseDataStore.save(data)
            }.applyIOWorkSchedulers()


    override fun save(data: List<CAccelerometerLinear>): Single<Response<List<CAccelerometerLinear>, Throwable>> =
            Single.fromCallable {
                Log.d("Accelerometers Linear DB Size", data.size.toString())

//                val batch = batchDatabaseDataStore.getNewBatch()
                val batch = BatchDatabaseDataStore.getInstance(context).getNewBatch()

                if (batch.hasResult) {
                    val batchId = batch.result?.id ?: -1
                    val journeyID = storeDb.getJourneyId()
                    var mode = storeDb.getMode()
                    var predictionMode = storeDb.getPredictionMode()
                    data.map {
                        it.batch_id = batchId
                        it.journeyID = journeyID
                        it.predictionMode = predictionMode
                        it.mode = mode
                    }
                } else
                    data.map {
                        it.batch_id = -1
                    }
                accelerometerDatabaseDataStore.save(data)
            }.applyIOWorkSchedulers()


}