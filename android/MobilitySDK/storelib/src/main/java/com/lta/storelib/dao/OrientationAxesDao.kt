package com.lta.storelib.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.lta.storelib.entity.STOrientationAxes

@Dao
interface OrientationAxesDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(chapter: STOrientationAxes)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(chapter: List<STOrientationAxes>)

    @Query("SELECT * FROM ${STOrientationAxes.Properties.TABLE_NAME} WHERE ${STOrientationAxes.Properties.BATCH_IDFK} = :batchId")
    fun queryList(batchId : Long) : List<STOrientationAxes>

    @Query("SELECT * FROM ${STOrientationAxes.Properties.TABLE_NAME} ORDER BY ${STOrientationAxes.Properties.ID} DESC LIMIT 1")
    fun queryLast() : STOrientationAxes

}