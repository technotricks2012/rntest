//package com.lta.storelib.datastore.sdkconfig
//
//import android.content.Context
//import com.lta.storelib.datastore.AbstractDataStoreFactory
//import com.lta.storelib.datastore.DataStoreFactory
//
//class SDKConfigDataStoreFactory (context: Context):AbstractDataStoreFactory<SDKConfigConfigDataStore>(context){
//
//
//    override fun create(type: DataStoreFactory.Type): SDKConfigConfigDataStore {
//        return when (type){
//            DataStoreFactory.Type.DATABASE -> SDKConfigDatabaseDataStore(internalContext)
//            DataStoreFactory.Type.CACHE ->  SDKConfigNoDataStore(internalContext)
//            DataStoreFactory.Type.SERVER ->  SDKConfigNoDataStore(internalContext)
//        }
//    }
//
//}