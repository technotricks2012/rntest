package com.lta.storelib.entity.roadDefect

import androidx.room.*
 data class STDefectEmbedded(
    @Embedded
    val defect: STDefect,
    @Relation(parentColumn = STDefect.Properties.ID,entityColumn = STDefectLocation.Properties.FK_ID,entity = STDefectLocation::class)
    var locations: List<STDefectLocation>
)










