package com.lta.storelib.cache

import com.lta.storelib.entity.STBatch
import javax.inject.Singleton

@Singleton
class BatchCache {

    private  var currentBatchCache: STBatch?=null
    fun get() = currentBatchCache

    fun set(currentBatch: STBatch) {
        currentBatchCache = currentBatch
    }

    fun has() = (currentBatchCache != null)

    fun clear() {
        currentBatchCache = null
    }

    companion object {
        val instance: BatchCache by lazy {
            BatchCache()
        }
    }
}