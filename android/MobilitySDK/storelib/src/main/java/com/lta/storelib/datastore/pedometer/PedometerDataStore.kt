package com.lta.storelib.datastore.pedometer

import com.lta.datalib.model.sensor.CPedometer
import com.lta.datalib.utils.Response
import com.lta.storelib.datastore.base.DataStore

interface PedometerDataStore : DataStore {
    fun save(stAccelerometer: CPedometer): Response<CPedometer, Throwable>
    fun save(stAccelerometer: List<CPedometer>): Response<List<CPedometer>, Throwable>
    fun getList(batchId: Long): Response<List<CPedometer>, Throwable>
    fun getLast(): Response<CPedometer, Throwable>
}