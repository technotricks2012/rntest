package com.lta.storelib.datastore

import android.content.Context
import com.lta.storelib.datastore.base.DataStore


abstract class AbstractDataStoreFactory<T : DataStore> internal constructor(context : Context) : DataStoreFactory<T> {


    protected val internalContext = context.applicationContext


}