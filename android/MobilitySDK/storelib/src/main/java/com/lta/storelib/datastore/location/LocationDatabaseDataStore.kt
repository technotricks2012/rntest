package com.lta.storelib.datastore.location

import android.content.Context
import com.lta.datalib.extenstions.resultOrError
import com.lta.datalib.model.CLocation
import com.lta.datalib.utils.Response
import com.lta.storelib.Database
import com.lta.storelib.datastore.base.AbstractDataStore
import com.lta.storelib.helpers.toCLocationTransaction
import com.lta.storelib.helpers.toSTLocationTransaction

internal class LocationDatabaseDataStore(context: Context?) : AbstractDataStore(context = context), LocationDataStore {

    override fun save(sensorData: CLocation): Response<CLocation, Throwable> = saveItem(sensorData)

    override fun save(sensorDatas: List<CLocation>): Response<List<CLocation>, Throwable> = saveItems(sensorDatas)
    override fun getList(batchId: Long): Response<List<CLocation>, Throwable> = getItems(batchId)
    override fun getLast(): Response<CLocation, Throwable> =
            resultOrError {
                return@resultOrError Database.getDatabase(context)
                        .locationDao()
                        .queryLast().toCLocationTransaction()
            }

    private fun saveItem(sensorData: CLocation): Response<CLocation, Throwable> =
            resultOrError {
                Database.getDatabase(context)
                        .locationDao()
                        .insert(sensorData.toSTLocationTransaction())
                return@resultOrError sensorData
            }

    private fun saveItems(sensorDatas: List<CLocation>): Response<List<CLocation>, Throwable> =
            resultOrError {
                Database.getDatabase(context)
                        .locationDao()
                        .insert(sensorDatas.map { it.toSTLocationTransaction() })
                return@resultOrError sensorDatas
            }
    
    private fun getItems(batchId: Long): Response<List<CLocation>, Throwable> =
            resultOrError {
                return@resultOrError Database.getDatabase(context)
                        .locationDao()
                        .queryList(batchId).map {
                            it.toCLocationTransaction()
                        }
            }
}
