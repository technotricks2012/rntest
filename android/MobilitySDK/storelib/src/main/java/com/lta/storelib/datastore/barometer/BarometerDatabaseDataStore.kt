package com.lta.storelib.datastore.barometer

import android.content.Context
import com.lta.datalib.extenstions.resultOrError
import com.lta.datalib.model.sensor.CBarometer
import com.lta.datalib.utils.Response
import com.lta.storelib.Database
import com.lta.storelib.datastore.base.AbstractDataStore
import com.lta.storelib.helpers.toCBarometerTransaction
import com.lta.storelib.helpers.toSTBarometerTransaction

internal class BarometerDatabaseDataStore(context: Context?) : AbstractDataStore(context = context), BarometerDataStore {

    override fun save(sensorData: CBarometer): Response<CBarometer, Throwable> = saveItem(sensorData)

    override fun save(sensorDatas: List<CBarometer>): Response<List<CBarometer>, Throwable> = saveItems(sensorDatas)
    override fun getList(batchId: Long): Response<List<CBarometer>, Throwable> = getItems(batchId)
    override fun getLast(): Response<CBarometer, Throwable> =
            resultOrError {
                return@resultOrError Database.getDatabase(context)
                        .barometerDao()
                        .queryLast().toCBarometerTransaction()
            }

    private fun saveItem(sensorData: CBarometer): Response<CBarometer, Throwable> =
            resultOrError {
                Database.getDatabase(context)
                        .barometerDao()
                        .insert(sensorData.toSTBarometerTransaction())
                return@resultOrError sensorData
            }

    private fun saveItems(sensorDatas: List<CBarometer>): Response<List<CBarometer>, Throwable> =
            resultOrError {
                Database.getDatabase(context)
                        .barometerDao()
                        .insert(sensorDatas.map { it.toSTBarometerTransaction() })
                return@resultOrError sensorDatas
            }
    
    private fun getItems(batchId: Long): Response<List<CBarometer>, Throwable> =
            resultOrError {
                return@resultOrError Database.getDatabase(context)
                        .barometerDao()
                        .queryList(batchId).map {
                            it.toCBarometerTransaction()
                        }
            }
}
