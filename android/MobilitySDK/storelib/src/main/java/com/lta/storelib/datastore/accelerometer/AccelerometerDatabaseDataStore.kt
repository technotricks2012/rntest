package com.lta.storelib.datastore.accelerometer

import android.content.Context
import com.lta.datalib.extenstions.resultOrError
import com.lta.datalib.model.sensor.CAccelerometer
import com.lta.datalib.utils.Response
import com.lta.storelib.Database
import com.lta.storelib.datastore.base.AbstractDataStore
import com.lta.storelib.helpers.toCAccelerometerTransaction
import com.lta.storelib.helpers.toSTAccelerometerTransaction

internal class AccelerometerDatabaseDataStore(context: Context?) : AbstractDataStore(context = context), AccelerometerDataStore {


    override  fun save(cAccelerometer: CAccelerometer): Response<CAccelerometer, Throwable> = saveItem(cAccelerometer)

    override  fun save(cAccelerometers: List<CAccelerometer>): Response<List<CAccelerometer>, Throwable> = saveItems(cAccelerometers)
    override  fun getList(batchId: Long): Response<List<CAccelerometer>, Throwable> = getItems(batchId)
    override fun getLast(): Response<CAccelerometer, Throwable> =
        resultOrError {
            return@resultOrError Database.getDatabase(context)
                    .accelerometerDao()
                    .queryLast().toCAccelerometerTransaction()
        }


    private  fun saveItem(cAccelerometer: CAccelerometer): Response<CAccelerometer, Throwable> =
        resultOrError {
            Database.getDatabase(context)
                    .accelerometerDao()
                    .insert(cAccelerometer.toSTAccelerometerTransaction())
            return@resultOrError cAccelerometer
        }


    private  fun saveItems(cAccelerometer: List<CAccelerometer>): Response<List<CAccelerometer>, Throwable> =
        resultOrError {
            Database.getDatabase(context)
                    .accelerometerDao()
                    .insert(cAccelerometer.map { it.toSTAccelerometerTransaction() })
            return@resultOrError cAccelerometer
        }


    private  fun getItems(batchId: Long): Response<List<CAccelerometer>, Throwable> =
        resultOrError {
            return@resultOrError Database.getDatabase(context)
                .accelerometerDao()
                .queryList(batchId).map {
                        it.toCAccelerometerTransaction()
                    }
        }
    }
