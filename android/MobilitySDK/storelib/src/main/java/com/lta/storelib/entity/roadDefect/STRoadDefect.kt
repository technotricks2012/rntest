package com.lta.storelib.entity.roadDefect

import androidx.room.*


@Entity(tableName = STRoadDefect.Properties.TABLE_NAME)
data class STRoadDefect(
        @ColumnInfo(name = Properties.ID)
        @PrimaryKey(autoGenerate = true)
        var id: Long? = null
) {

    @ColumnInfo(name = Properties.STATUS,defaultValue = Status.Properties.START)
    @TypeConverters(Status.STRoadDefectConverter::class)
    var status: Status = Status.START

    object Properties {
        const val TABLE_NAME = "RoadDefect"
        const val ID = "rideIDPK"
        const val STATUS = "status"

    }

    enum class Status(val value: String) {
        START(Properties.START),
        STOP(Properties.STOP),
        UPLOADED(Properties.UPLOADED),
        UPLOADING(Properties.UPLOADING),
        FAIL(Properties.FAIL),
        UNKNOWN(Properties.UNKNOWN);

        object Properties {
            const val START = "START"
            const val STOP = "STOP"
            const val UPLOADED = "UPLOADED"
            const val UPLOADING = "UPLOADING"
            const val FAIL = "FAIL"
            const val UNKNOWN = "UNKNOWN"

        }

        object STRoadDefectConverter {
            @TypeConverter
            @JvmStatic
            fun toString(type: Status): String {
                return type.value
            }

            @TypeConverter
            @JvmStatic
            fun toType(value: String): Status {
                return when (value) {
                    START.value -> START
                    STOP.value -> STOP
                    UPLOADING.value -> UPLOADING
                    UPLOADED.value -> UPLOADED
                    FAIL.value -> FAIL
                    else -> UNKNOWN

                }
            }
        }
    }

}






