package com.lta.storelib.entity.roadDefect

import androidx.room.*

data class STRoadDefectEmbedded(
        @Embedded
        val roadDefect: STRoadDefect,
        @Relation(parentColumn = STRoadDefect.Properties.ID, entityColumn = STRoadDefectInfo.Properties.FK_ID, entity = STRoadDefectInfo::class)
        val roadDefectInfos: List<STRoadDefectInfoEmbedded>,
        @Relation(parentColumn = STRoadDefect.Properties.ID, entityColumn = STDefect.Properties.FK_ID, entity = STDefect::class)
        var defects: List<STDefectEmbedded>
)










