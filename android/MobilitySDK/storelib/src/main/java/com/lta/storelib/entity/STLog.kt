package com.lta.storelib.entity

import androidx.room.*

@Entity(tableName = STLog.Properties.TABLE_NAME)
data class STLog(
    @ColumnInfo(name = Properties.ID)
    @PrimaryKey(autoGenerate = true) var id: Long? = null,
    @ColumnInfo(name = Properties.TIMESTAMP) val timestamp: Long = 0,
    @ColumnInfo(name = Properties.KEY) val key: String? = null,
    @ColumnInfo(name = Properties.DESCRIPTION) var description: String? = null


){
    @ColumnInfo(name = Properties.TYPE)
    @TypeConverters(TYPE.Converter::class)
    var type: TYPE = TYPE.DEBUG

    object Properties {
        const val TABLE_NAME = "Log"
        const val ID = "logIdPK"
        const val KEY = "key"
        const val DESCRIPTION = "description"
        const val TIMESTAMP = "timestamp"
        const val TYPE = "type"
    }

    enum class TYPE(val value: String) {
        ERROR(Properties.ERROR),
        DEBUG(Properties.DEBUG),
        SUCCESS(Properties.SUCCESS),
        UNKNOWN(Properties.UNKNOWN);


        object Properties {
            const val ERROR = "ERROR"
            const val SUCCESS = "SUCCESS"
            const val DEBUG = "DEBUG"
            const val UNKNOWN = "UNKNOWN"
        }

        object Converter{
            @TypeConverter
            @JvmStatic
            fun toString(type: TYPE): String {
                return type.value
            }

            @TypeConverter
            @JvmStatic
            fun toType(value: String): TYPE {
                return when (value) {
                    ERROR.value -> ERROR
                    SUCCESS.value -> SUCCESS
                    DEBUG.value -> DEBUG
                    else -> UNKNOWN

                }
            }
        }
    }
}

