package com.lta.storelib.implementations

import android.content.Context
import android.util.Log
import com.lta.datalib.extenstions.applyIOWorkSchedulers
import com.lta.datalib.helper.SingletonHolder
import com.lta.datalib.interfaces.db.IAccelerometerDB
import com.lta.datalib.model.sensor.CAccelerometer
import com.lta.datalib.utils.Response
import com.lta.storelib.datastore.accelerometer.AccelerometerDatabaseDataStore
import com.lta.storelib.datastore.batch.BatchDatabaseDataStore
import io.reactivex.Single

class StoreAccelerometerImpl(private val context: Context) : IAccelerometerDB {
    private val accelerometerDatabaseDataStore = AccelerometerDatabaseDataStore(context)
//    private val batchDatabaseDataStore = BatchDatabaseDataStore(context)

    private val storeDb = DataStoreImpl(context)

    companion object : SingletonHolder<StoreAccelerometerImpl, Context>(::StoreAccelerometerImpl)



    override fun getList(batchId: Long): Single<Response<List<CAccelerometer>, Throwable>> =
            Single.fromCallable {

                accelerometerDatabaseDataStore.getList(batchId)
            }.applyIOWorkSchedulers()


    override fun getLast(): Single<Response<CAccelerometer, Throwable>> =
            Single.fromCallable {
                accelerometerDatabaseDataStore.getLast()
            }.applyIOWorkSchedulers()

    override fun save(data: CAccelerometer): Single<Response<CAccelerometer, Throwable>> =
            Single.fromCallable {
//                val batch = batchDatabaseDataStore.getNewBatch()
                val batch = BatchDatabaseDataStore.getInstance(context).getNewBatch()

                val journeyID = storeDb.getJourneyId()
                var mode = storeDb.getMode()
                var predictionMode = storeDb.getPredictionMode()
                if (batch.hasResult) {
                    data.batch_id = batch.result?.id ?: -1
                    data.journeyID = journeyID
                    data.predictionMode = predictionMode
                    data.mode = mode
                } else
                    data.batch_id = -1
                accelerometerDatabaseDataStore.save(data)
            }.applyIOWorkSchedulers()


    override fun save(data: List<CAccelerometer>): Single<Response<List<CAccelerometer>, Throwable>> =
            Single.fromCallable {

                Log.d("Accelerometers DB Size", "Object ${System.identityHashCode(this).toString()}- Size ="+data.size.toString())

//                val batch = batchDatabaseDataStore.getNewBatch()
                val batch = BatchDatabaseDataStore.getInstance(context!!).getNewBatch()

                if (batch.hasResult) {
                    val batchId = batch.result?.id ?: -1
                    val journeyID = storeDb.getJourneyId()
                    val mode = storeDb.getMode()
                    val predictionMode = storeDb.getPredictionMode()
                    data.map {
                        it.batch_id = batchId
                        it.journeyID = journeyID
                        it.predictionMode = predictionMode
                        it.mode = mode
                    }
                } else
                    data.map {
                        it.batch_id = -1
                    }
                accelerometerDatabaseDataStore.save(data)
            }.applyIOWorkSchedulers()


}