package com.lta.storelib.implementations

import android.content.Context
import android.util.Log
import com.lta.datalib.extenstions.applyIOWorkSchedulers
import com.lta.datalib.helper.SingletonHolder
import com.lta.datalib.interfaces.db.IMagnetometerDB
import com.lta.datalib.model.sensor.CMagnetometer
import com.lta.datalib.utils.Response
import com.lta.storelib.datastore.batch.BatchDatabaseDataStore
import com.lta.storelib.datastore.magnetometer.MagnetometerDatabaseDataStore
import io.reactivex.Single

class StoreMagnetometerImpl(private val context: Context) : IMagnetometerDB {
    private val magnetometerDatabaseDataStore = MagnetometerDatabaseDataStore(context)
//    private val batchDatabaseDataStore = BatchDatabaseDataStore(context)
    private val storeDb = DataStoreImpl(context)
    companion object : SingletonHolder<StoreMagnetometerImpl, Context>(::StoreMagnetometerImpl)


    override fun getList(batchId: Long): Single<Response<List<CMagnetometer>, Throwable>> {
        return Single.fromCallable {
            magnetometerDatabaseDataStore.getList(batchId)
        }.applyIOWorkSchedulers()
    }

    override fun getLast(): Single<Response<CMagnetometer, Throwable>> =
             Single.fromCallable {
                magnetometerDatabaseDataStore.getLast()
            }.applyIOWorkSchedulers()

    override fun save(data: CMagnetometer): Single<Response<CMagnetometer, Throwable>> {
        return Single.fromCallable {
//            val batch = batchDatabaseDataStore.getNewBatch()
            val batch = BatchDatabaseDataStore.getInstance(context).getNewBatch()

            val journeyID = storeDb.getJourneyId()
            val mode = storeDb.getMode()
            val predictionMode = storeDb.getPredictionMode()
            if (batch.hasResult) {
                data.batch_id = batch.result?.id ?: -1
                data.journeyID = journeyID
                data.predictionMode = predictionMode
                data.mode = mode
            } else
                data.batch_id = -1
            magnetometerDatabaseDataStore.save(data)
        }.applyIOWorkSchedulers()
    }


    override fun save(data: List<CMagnetometer>): Single<Response<List<CMagnetometer>, Throwable>> {

        return Single.fromCallable {
            Log.d("Magnetometer DB Size", data.size.toString())
//            val batch = batchDatabaseDataStore.getNewBatch()
            val batch = BatchDatabaseDataStore.getInstance(context).getNewBatch()

            val journeyID = storeDb.getJourneyId()
            val mode = storeDb.getMode()
            val predictionMode = storeDb.getPredictionMode()

            if (batch.hasResult) {
                val batchId = batch.result?.id ?: -1
                data.map {
                    it.batch_id = batchId
                    it.journeyID = journeyID
                    it.predictionMode = predictionMode
                    it.mode = mode
                }
            } else
                data.map {
                    it.batch_id = -1
                }
            magnetometerDatabaseDataStore.save(data)
        }.applyIOWorkSchedulers()

    }

}