package com.lta.storelib.helpers

import com.lta.datalib.model.CLocation
import com.lta.datalib.model.db.CBatch
import com.lta.datalib.model.db.CBatchEmbedded
import com.lta.datalib.model.db.CLog
import com.lta.datalib.model.roadDefect.*
import com.lta.datalib.model.sensor.*
import com.lta.storelib.entity.*
import com.lta.storelib.entity.roadDefect.*

fun STRoadDefect.toCRoadDefectTransaction(): CRoadDefect =
        CRoadDefect(
                id = this.id,
                status = CRoadDefect.Status.valueOf(this.status.value)
        )

fun CRoadDefect.toSTRoadDefectTransaction(): STRoadDefect =
        STRoadDefect().apply {
            status = STRoadDefect.Status.valueOf(this.status.value)
        }


fun STRoadDefectInfo.toCRoadDefectInfoTransaction(): CRoadDefectInfo =
        CRoadDefectInfo(
                timestamp = this.timestamp,
                isStart = this.isStart
        )


fun CRoadDefectInfo.toSTRoadDefectInfoTransaction(): STRoadDefectInfo =
        STRoadDefectInfo(
                id = this.id,
                timestamp = this.timestamp
        )


fun STDefect.toCDefectTransaction(): CDefect =
        CDefect(
                id = this.id,
                timestamp = this.timestamp,
                defectType = this.defectType
        )


fun CDefect.toSTDefectTransaction(): STDefect =
        STDefect(
                timestamp = this.timestamp,
                defectType = this.defectType
        )


fun CDefectLocation.toSTDefectLocationTransaction(): STDefectLocation =
        STDefectLocation(
                timestamp = this.timestamp,
                latitude = this.latitude,
                longitude = this.longitude,
                altitude = this.altitude,
                accuracy = this.accuracy
        )


fun STDefectLocation.toCDefectLocationTransaction(): CDefectLocation =
        CDefectLocation(
                timestamp = this.timestamp,
                latitude = this.latitude,
                longitude = this.longitude,
                altitude = this.altitude,
                accuracy = this.accuracy
        )


fun STRoadDefectEmbedded.toCRoadDefectEmbeddedTransaction(): CRoadDefectEmbedded =
        CRoadDefectEmbedded(
                roadDefect = this.roadDefect.toCRoadDefectTransaction(),
                roadDefectInfos = this.roadDefectInfos.map { it.toCRoadDefectInfoEmbeddedTransaction() },
                defects = this.defects.map { it.toCDefectEmbeddedTransaction() }
        )


fun CAccelerometer.toSTAccelerometerTransaction(): STAccelerometer =
        STAccelerometer(
                x = this.x,
                y = this.y,
                z = this.z,
                timestamp = this.timestamp,
                batchIdFK = this.batch_id,
                mode = this.mode,
                predictionMode = this.predictionMode,
                journeyID = this.journeyID
        )


fun CGyroscope.toSTGyroscopeTransaction(): STGyroscope =
        STGyroscope(
                x = this.x,
                y = this.y,
                z = this.z,
                timestamp = this.timestamp,
                batchIdFK = this.batch_id,
                mode = this.mode,
                predictionMode = this.predictionMode,
                journeyID = this.journeyID
        )


fun CLocation.toSTLocationTransaction(): STLocation =
        STLocation(
                latitude = this.latitude,
                longitude = this.longitude,
                altitude = this.altitude,
                accuracy = this.accuracy,
                isMock = this.isMock,
                timestamp = this.timestamp,
                batchIdFK = this.batch_id,
                mode = this.mode,
                predictionMode = this.predictionMode,
                journeyID = this.journeyID
        )


fun CBarometer.toSTBarometerTransaction(): STBarometer =
        STBarometer(
                pressure = this.pressure,
                timestamp = this.timestamp,
                batchIdFK = this.batch_id,
                mode = this.mode,
                predictionMode = this.predictionMode,
                journeyID = this.journeyID
        )


fun CPedometer.toSTPedometerTransaction(): STPedometer =
        STPedometer(
                steps = this.steps,
                distance = this.distance,
                timestamp = this.timestamp,
                batchIdFK = this.batch_id,
                mode = this.mode,
                predictionMode = this.predictionMode,
                journeyID = this.journeyID
        )


fun CRotationVector.toSTRotationVectorTransaction(): STRotationVector =
        STRotationVector(
                x = this.x,
                y = this.y,
                z = this.z,
                rotationVector = this.rotationVector,
                timestamp = this.timestamp,
                batchIdFK = this.batch_id,
                mode = this.mode,
                predictionMode = this.predictionMode,
                journeyID = this.journeyID
        )


fun CAccelerometerLinear.toSTAccelerometerLinearTransaction(): STAccelerometerLinear =
        STAccelerometerLinear(
                x = this.x,
                y = this.y,
                z = this.z,
                timestamp = this.timestamp,
                batchIdFK = this.batch_id,
                mode = this.mode,
                predictionMode = this.predictionMode,
                journeyID = this.journeyID
        )


fun COrientationAxes.toSTOrientationAxesTransaction(): STOrientationAxes =
        STOrientationAxes(
                x = this.x,
                y = this.y,
                z = this.z,
                timestamp = this.timestamp,
                batchIdFK = this.batch_id,
                mode = this.mode,
                predictionMode = this.predictionMode,
                journeyID = this.journeyID
        )


fun CMagnetometer.toSTMagnetometerTransaction(): STMagnetometer =
        STMagnetometer(
                x = this.x,
                y = this.y,
                z = this.z,
                timestamp = this.timestamp,
                batchIdFK = this.batch_id,
                mode = this.mode,
                predictionMode = this.predictionMode,
                journeyID = this.journeyID
        )


fun STAccelerometer.toCAccelerometerTransaction(): CAccelerometer {
    return CAccelerometer(
            x = this.x,
            y = this.y,
            z = this.z,
            timestamp = this.timestamp
    ).apply {
        mode = this@toCAccelerometerTransaction.mode
        predictionMode = this@toCAccelerometerTransaction.predictionMode
        journeyID = this@toCAccelerometerTransaction.journeyID
        batch_id = batchIdFK
    }
}


fun STGyroscope.toCGyroscopeTransaction(): CGyroscope {
    return CGyroscope(
            x = this.x,
            y = this.y,
            z = this.z,
            timestamp = this.timestamp
    ).apply {
        mode = this@toCGyroscopeTransaction.mode
        predictionMode = this@toCGyroscopeTransaction.predictionMode
        journeyID = this@toCGyroscopeTransaction.journeyID
        batch_id = batchIdFK
    }
}


fun STLocation.toCLocationTransaction(): CLocation {
    return CLocation(
            latitude = this.latitude,
            longitude = this.longitude,
            altitude = this.altitude,
            accuracy = this.accuracy,
            isMock = this.isMock,
            timestamp = this.timestamp
    ).apply {
        mode = this@toCLocationTransaction.mode
        predictionMode = this@toCLocationTransaction.predictionMode
        journeyID = this@toCLocationTransaction.journeyID
        batch_id = batchIdFK
    }
}


fun STBarometer.toCBarometerTransaction(): CBarometer {
    return CBarometer(
            pressure = this.pressure,
            timestamp = this.timestamp
    ).apply {
        mode = this@toCBarometerTransaction.mode
        predictionMode = this@toCBarometerTransaction.predictionMode
        journeyID = this@toCBarometerTransaction.journeyID
        batch_id = batchIdFK
    }
}


fun STPedometer.toCPedometerTransaction(): CPedometer {
    return CPedometer(
            steps = this.steps,
            timestamp = this.timestamp,
            distance = this.distance
    ).apply {
        mode = this@toCPedometerTransaction.mode
        predictionMode = this@toCPedometerTransaction.predictionMode
        journeyID = this@toCPedometerTransaction.journeyID
        batch_id = batchIdFK
    }
}


fun STRotationVector.toCRotationVectorTransaction(): CRotationVector {
    return CRotationVector(
            x = this.x,
            y = this.y,
            z = this.z,
            rotationVector = this.rotationVector,
            timestamp = this.timestamp
    ).apply {
        mode = this@toCRotationVectorTransaction.mode
        predictionMode = this@toCRotationVectorTransaction.predictionMode
        journeyID = this@toCRotationVectorTransaction.journeyID
        batch_id = batchIdFK
    }
}


fun STAccelerometerLinear.toCAccelerometerLinearTransaction(): CAccelerometerLinear {
    return CAccelerometerLinear(
            x = this.x,
            y = this.y,
            z = this.z,
            timestamp = this.timestamp
    ).apply {
        mode = this@toCAccelerometerLinearTransaction.mode
        predictionMode = this@toCAccelerometerLinearTransaction.predictionMode
        journeyID = this@toCAccelerometerLinearTransaction.journeyID
        batch_id = batchIdFK
    }
}


fun STOrientationAxes.toCOrientationAxesTransaction(): COrientationAxes {
    return COrientationAxes(
            x = this.x,
            y = this.y,
            z = this.z,
            timestamp = this.timestamp
    ).apply {
        mode = this@toCOrientationAxesTransaction.mode
        predictionMode = this@toCOrientationAxesTransaction.predictionMode
        journeyID = this@toCOrientationAxesTransaction.journeyID
        batch_id = batchIdFK
    }
}

fun STMagnetometer.toCMagnetometerTransaction(): CMagnetometer {
    return CMagnetometer(
            x = this.x,
            y = this.y,
            z = this.z,
            timestamp = this.timestamp
    ).apply {
        mode = this@toCMagnetometerTransaction.mode
        predictionMode = this@toCMagnetometerTransaction.predictionMode
        journeyID = this@toCMagnetometerTransaction.journeyID
        batch_id = batchIdFK
    }

}


fun CLocation.toCDefectLocationTransaction(): CDefectLocation =
        CDefectLocation(
                timestamp = this.timestamp,
                latitude = this.latitude,
                longitude = this.longitude,
                altitude = this.altitude,
                accuracy = this.accuracy
        )

fun CDefectEmbedded.toSTDefectEmbeddedTransaction(): STDefectEmbedded =
        STDefectEmbedded(
                defect = this.defect.toSTDefectTransaction(),
                locations = this.locations.map { it.toSTDefectLocationTransaction() }
        )


fun STDefectEmbedded.toCDefectEmbeddedTransaction(): CDefectEmbedded =
        CDefectEmbedded(
                defect = this.defect.toCDefectTransaction(),
                locations = this.locations.map { it.toCDefectLocationTransaction() }
        )


fun STRoadDefectInfoEmbedded.toCRoadDefectInfoEmbeddedTransaction(): CRoadDefectInfoEmbedded =
        CRoadDefectInfoEmbedded(
                roadDefectInfo = this.roadDefectInfo.toCRoadDefectInfoTransaction(),
                location = this.location.map { it.toCDefectLocationTransaction() }
        )


fun STBatch.toCBatchTransaction(): CBatch =
        CBatch(
                id = this.id,
                timestamp = this.timestamp,
                status = CBatch.Status.valueOf(this.status.value)
        )

fun STLog.toCLogTransaction(): CLog =
        CLog(
                id = this.id,
                timestamp = this.timestamp,
                type = CLog.TYPE.valueOf(this.type.value),
                key = this.key,
                description = this.description
        )
fun CLog.toSTLogTransaction(): STLog =
        STLog(
                timestamp = this.timestamp,
                key = this.key,
                description = this.description
        ).apply {
            type = STLog.TYPE.valueOf(this.type.value)
        }

fun STBatchEmbedded.toCBatchEmbeddedTransaction(): CBatchEmbedded =
        CBatchEmbedded(
                batch = this.batch.toCBatchTransaction(),
                accelerometer = this.accelerometer.map { it.toCAccelerometerTransaction() },
                accelerometerLinear = this.accelerometerLinear.map { it.toCAccelerometerLinearTransaction() },
                gyroscope = this.gyroscope.map { it.toCGyroscopeTransaction() },
                magnetometer = this.magnetometer.map { it.toCMagnetometerTransaction() },
                location = this.location.map { it.toCLocationTransaction() },
                orientationAxes = this.orientationAxes.map { it.toCOrientationAxesTransaction() },
                rotationVector = this.rotationVector.map { it.toCRotationVectorTransaction() },
                barometer = this.barometer.map { it.toCBarometerTransaction() },
                pedometer = this.pedometer.map { it.toCPedometerTransaction() }
        )


