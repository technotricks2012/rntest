package com.lta.storelib.datastore.accelerometer

import com.lta.datalib.model.SDKConfig
import com.lta.datalib.model.sensor.CAccelerometer
import com.lta.datalib.utils.Response
import com.lta.storelib.datastore.base.DataStore
import io.reactivex.Single

interface AccelerometerDataStore :DataStore{

     fun save(stAccelerometer: CAccelerometer) : Response<CAccelerometer,Throwable>
     fun save(stAccelerometer: List<CAccelerometer>) : Response<List<CAccelerometer>,Throwable>
     fun getList(batchId:Long) : Response<List<CAccelerometer>,Throwable>
     fun getLast() : Response<CAccelerometer,Throwable>


}