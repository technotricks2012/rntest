package com.lta.storelib.implementations

import android.content.Context
import com.lta.datalib.extenstions.applyIOWorkSchedulers
import com.lta.datalib.interfaces.db.ILogDB
import com.lta.datalib.model.db.CLog
import com.lta.datalib.utils.Response
import com.lta.storelib.datastore.log.LogDatabaseDataStore
import io.reactivex.Single

class LogImpl(private val context: Context) : ILogDB {

    override fun save(log: CLog): Single<Response<CLog, Throwable>> =
            Single.fromCallable {
                LogDatabaseDataStore.getInstance(context).save(log)
            }.applyIOWorkSchedulers()

    override fun getList(): Single<Response<List<CLog>, Throwable>> =
            Single.fromCallable {
                LogDatabaseDataStore.getInstance(context).get()
            }.applyIOWorkSchedulers()


}