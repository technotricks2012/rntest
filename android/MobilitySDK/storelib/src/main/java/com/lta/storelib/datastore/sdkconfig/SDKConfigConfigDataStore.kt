package com.lta.storelib.datastore.sdkconfig

import com.lta.datalib.model.SDKConfig
import com.lta.datalib.utils.Response
import com.lta.storelib.datastore.base.DataStore
import io.reactivex.Single

interface SDKConfigConfigDataStore :DataStore{


    fun saveBuildConfig(character : SDKConfig) : Single<Response<SDKConfig,Throwable>>

}