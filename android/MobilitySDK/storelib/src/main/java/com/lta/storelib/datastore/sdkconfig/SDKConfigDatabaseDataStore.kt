package com.lta.storelib.datastore.sdkconfig

import android.content.Context
import com.lta.datalib.extenstions.applyIOWorkSchedulers
import com.lta.datalib.extenstions.resultOrError
import com.lta.datalib.model.SDKConfig
import com.lta.datalib.utils.Response
import com.lta.storelib.Database
import com.lta.storelib.datastore.base.AbstractDataStore
import io.reactivex.Single

//internal class SDKConfigDatabaseDataStore(context : Context)  : AbstractDataStore(context = context), SDKConfigConfigDataStore{
//    override fun saveBuildConfig(sdkConfig: SDKConfig): Single<Response<SDKConfig,Throwable>> {
//        return Single.fromCallable {
//            saveConfig(sdkConfig)
//        }.applyIOWorkSchedulers()
//    }
//
//    private fun saveConfig(sdkConfig : SDKConfig) : Response<SDKConfig,Throwable> = resultOrError {
//        Database.getDatabase(context)
//            .sdkConfigTable()
//            .insert(sdkConfig.toDatabaseSDKConfig())
//        return@resultOrError sdkConfig
//    }
//}