package com.lta.storelib.dao

import android.util.Log
import androidx.room.*
import androidx.sqlite.db.SupportSQLiteQuery
import com.lta.storelib.entity.*

@Dao
interface BatchDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(batch: STBatch): Long

    @RawQuery
    fun getCount(query: SupportSQLiteQuery): Long

    @Update
    fun update(batch: STBatch?): Int

    @Query("UPDATE Batch set status = :status WHERE batchIdPK = :batchId")
    fun updateStatus(batchId: Long, status: String): Int

    @Query("DELETE FROM ${STBatch.Properties.TABLE_NAME} WHERE ${STBatch.Properties.ID} = :ids")
    fun delete(ids: Long)

    @Query("SELECT * FROM ${STBatch.Properties.TABLE_NAME} WHERE ${STBatch.Properties.STATUS} = '${STBatch.Status.Properties.NEW}'")
    fun getNewBatchID(): List<STBatch>


    @Transaction
    fun getNewBatch(singletonBatchCount: Long): Pair<STBatch?, Boolean>? {

        var isCountChange = false
        var batch = getNewBatchID().getOrNull(0)
        batch?.let {
            if (singletonBatchCount >= 100) {//50 * 100
                Log.d("BATCH Count", "Batch Ready State $singletonBatchCount")
                isCountChange = true
                batch?.counts = singletonBatchCount
                batch?.status = STBatch.Status.READY
                update(batch)
            }
        } ?: run {
            val newBatch = STBatch()
            newBatch.id = insert(newBatch)
            batch = newBatch
        }
        return Pair(batch, isCountChange)
    }


    @Query("SELECT * FROM ${STBatch.Properties.TABLE_NAME} WHERE ${STBatch.Properties.STATUS} in ('${STBatch.Status.Properties.FAIL}', '${STBatch.Status.Properties.READY}')")
    fun getBatchEmbedded(): List<STBatchEmbedded>
//
//    @Transaction
//    @Query("""
//SELECT AccCount+AccLCount+MagCount+GyroCount+LocationCount+RotationCount+OrientationCount  as TotalCount From (
//SELECT
//IFNULL(AccCount,0) AS AccCount,
//IFNULL(AccLCount,0) AS AccLCount,
//IFNULL(MagCount,0) AS MagCount,
//IFNULL(GyroCount,0) AS GyroCount,
//IFNULL(LocationCount,0) AS LocationCount,
//IFNULL(RotationCount,0) AS RotationCount,
//IFNULL(OrientationCount,0) AS OrientationCount
//FROM ${STBatch.Properties.TABLE_NAME}
//
//INNER JOIN( SELECT ${STAccelerometer.Properties.BATCH_IDFK}, COUNT(*) AS AccCount FROM ${STAccelerometer.Properties.TABLE_NAME} GROUP BY ${STAccelerometer.Properties.BATCH_IDFK}) AS Acc ON Acc.batchIdFK = ${STBatch.Properties.TABLE_NAME}.${STBatch.Properties.ID}
//LEFT JOIN( SELECT ${STAccelerometerLinear.Properties.BATCH_IDFK}, COUNT(*) AS AccLCount FROM ${STAccelerometerLinear.Properties.TABLE_NAME} GROUP BY ${STAccelerometerLinear.Properties.BATCH_IDFK}) AS AccL ON AccL.batchIdFK = ${STBatch.Properties.TABLE_NAME}.${STBatch.Properties.ID}
//LEFT JOIN( SELECT ${STMagnetometer.Properties.BATCH_IDFK}, COUNT(*) AS MagCount FROM ${STMagnetometer.Properties.TABLE_NAME} GROUP BY ${STMagnetometer.Properties.BATCH_IDFK}) AS Mag ON Mag.batchIdFK = ${STBatch.Properties.TABLE_NAME}.${STBatch.Properties.ID}
//LEFT JOIN( SELECT ${STGyroscope.Properties.BATCH_IDFK}, COUNT(*) AS GyroCount FROM ${STGyroscope.Properties.TABLE_NAME} GROUP BY ${STGyroscope.Properties.BATCH_IDFK}) AS Gyro ON Gyro.batchIdFK = ${STBatch.Properties.TABLE_NAME}.${STBatch.Properties.ID}
//LEFT JOIN( SELECT ${STLocation.Properties.BATCH_IDFK}, COUNT(*) AS LocationCount FROM ${STLocation.Properties.TABLE_NAME} GROUP BY ${STLocation.Properties.BATCH_IDFK}) AS Location ON Location.batchIdFK = ${STBatch.Properties.TABLE_NAME}.${STBatch.Properties.ID}
//LEFT JOIN( SELECT ${STRotationVector.Properties.BATCH_IDFK}, COUNT(*) AS RotationCount FROM ${STRotationVector.Properties.TABLE_NAME} GROUP BY ${STRotationVector.Properties.BATCH_IDFK}) AS Rotation ON Rotation.batchIdFK = ${STBatch.Properties.TABLE_NAME}.${STBatch.Properties.ID}
//LEFT JOIN( SELECT ${STOrientationAxes.Properties.BATCH_IDFK}, COUNT(*) AS OrientationCount FROM ${STOrientationAxes.Properties.TABLE_NAME} GROUP BY ${STOrientationAxes.Properties.BATCH_IDFK}) AS Orientation ON Orientation.batchIdFK = ${STBatch.Properties.TABLE_NAME}.${STBatch.Properties.ID}
//WHERE ${STBatch.Properties.TABLE_NAME}.${STBatch.Properties.ID} = :batchId)
//    """)
//    abstract fun getTotalCount(batchId: Long): Long
}