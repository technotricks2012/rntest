package com.lta.storelib.datastore.orientationAxes

import com.lta.datalib.model.sensor.COrientationAxes
import com.lta.datalib.utils.Response
import com.lta.storelib.datastore.base.DataStore

interface OrientationAxesDataStore :DataStore{
     fun save(stAccelerometer: COrientationAxes) : Response<COrientationAxes,Throwable>
     fun save(stAccelerometer: List<COrientationAxes>) : Response<List<COrientationAxes>,Throwable>
     fun getList(batchId:Long) : Response<List<COrientationAxes>,Throwable>
     fun getLast() : Response<COrientationAxes,Throwable>
}