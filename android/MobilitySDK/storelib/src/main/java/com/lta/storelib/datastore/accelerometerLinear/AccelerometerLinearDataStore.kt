package com.lta.storelib.datastore.accelerometerLinear

import com.lta.datalib.model.sensor.CAccelerometerLinear
import com.lta.datalib.utils.Response
import com.lta.storelib.datastore.base.DataStore

interface AccelerometerLinearDataStore :DataStore{

     fun save(stAccelerometer: CAccelerometerLinear) : Response<CAccelerometerLinear,Throwable>
     fun save(stAccelerometer: List<CAccelerometerLinear>) : Response<List<CAccelerometerLinear>,Throwable>
     fun getList(batchId:Long) : Response<List<CAccelerometerLinear>,Throwable>
     fun getLast() : Response<CAccelerometerLinear,Throwable>


}