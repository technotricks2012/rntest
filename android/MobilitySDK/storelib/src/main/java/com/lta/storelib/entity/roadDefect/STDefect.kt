package com.lta.storelib.entity.roadDefect

import androidx.room.*

@Entity(tableName = STDefect.Properties.TABLE_NAME, foreignKeys = [
    ForeignKey(
            entity = STRoadDefect::class,
            parentColumns = arrayOf(STRoadDefect.Properties.ID),
            childColumns = arrayOf(STDefect.Properties.FK_ID),
            onDelete = ForeignKey.CASCADE
    )]
)

data class STDefect(
        @ColumnInfo(name = Properties.ID)
        @PrimaryKey(autoGenerate = true)
        var id: Long? = null,

        @ColumnInfo(name = Properties.FK_ID)
        var rideID_FK: Long? = null,

        @ColumnInfo(name = Properties.TIMESTAMP)
        val timestamp: Long = -1,

        @ColumnInfo(name = Properties.DEFECT_TYPE)
        val defectType: String

) {
    object Properties {
        const val TABLE_NAME = "Defect"
        const val ID = "defectPK"
        const val FK_ID = "rideIdFK"
        const val TIMESTAMP = "timestamp"
        const val DEFECT_TYPE = "DefectType"
    }
}