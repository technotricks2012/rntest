package com.lta.storelib.entity.roadDefect

import androidx.room.*

class STRoadDefectInfoEmbedded(
        @Embedded
        var roadDefectInfo: STRoadDefectInfo,
        @Relation(parentColumn = STRoadDefectInfo.Properties.ID, entityColumn = STDefectLocation.Properties.ROAD_DEFECT_ID_FK, entity = STDefectLocation::class)
        var location: List<STDefectLocation>)











