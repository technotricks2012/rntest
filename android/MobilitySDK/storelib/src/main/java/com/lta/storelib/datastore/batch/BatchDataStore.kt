package com.lta.storelib.datastore.batch

import com.lta.datalib.utils.Response
import com.lta.storelib.entity.STBatch
import com.lta.storelib.entity.STBatchEmbedded

interface BatchDataStore {
    fun getNewBatch(): Response<STBatch, Throwable>
    fun getReadyOrFailBatch(): Response<List<STBatchEmbedded>, Throwable>
    fun delete(batchId: Long): Response<Boolean, Throwable>
    fun updateStatus(batchId: Long, status: STBatch.Status): Response<Boolean, Throwable>
    fun getCount(): Response<Long, Throwable>

}