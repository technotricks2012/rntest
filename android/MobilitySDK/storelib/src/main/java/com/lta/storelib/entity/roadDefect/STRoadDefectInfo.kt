package com.lta.storelib.entity.roadDefect

import androidx.room.*

@Entity(tableName = STRoadDefectInfo.Properties.TABLE_NAME, foreignKeys = [
    ForeignKey(
            entity = STRoadDefect::class,
            parentColumns = arrayOf(STRoadDefect.Properties.ID),
            childColumns = arrayOf(STRoadDefectInfo.Properties.FK_ID),
            onDelete = ForeignKey.CASCADE,
            onUpdate = ForeignKey.CASCADE
    )])
data class STRoadDefectInfo(
        @ColumnInfo(name = Properties.ID)
        @PrimaryKey(autoGenerate = true)
        var id: Long? = null,

        @ColumnInfo(name = Properties.FK_ID)
        var rideID_FK: Long? = null,

        @ColumnInfo(name = Properties.TYPE)
        val isStart: Boolean? = null,

        @ColumnInfo(name = Properties.TIMESTAMP)
        var timestamp: Long = -1
) {
    object Properties {
        const val TABLE_NAME = "RoadDefectInfo"
        const val ID = "roadDefectInfoPK"
        const val FK_ID = "rideIdFK"
        const val TYPE = "isStart"
        const val TIMESTAMP = "timestamp"
    }
}

