package com.lta.storelib.datastore.log

import com.lta.datalib.model.db.CLog
import com.lta.datalib.utils.Response
import com.lta.storelib.datastore.base.DataStore

interface LogDataStore :DataStore {

     fun save(log: CLog): Response<CLog, Throwable>
     fun get(): Response<List<CLog>, Throwable>
}