package com.lta.storelib.entity.roadDefect

import androidx.room.*

@Entity(tableName = STDefectLocation.Properties.TABLE_NAME, foreignKeys = [
    ForeignKey(
            entity = STDefect::class,
            parentColumns = arrayOf(STDefect.Properties.ID),
            childColumns = arrayOf(STDefectLocation.Properties.FK_ID),
            onDelete = ForeignKey.CASCADE
    ),
    ForeignKey(
            entity = STRoadDefectInfo::class,
            parentColumns = arrayOf(STRoadDefectInfo.Properties.ID),
            childColumns = arrayOf(STDefectLocation.Properties.ROAD_DEFECT_ID_FK),
            onDelete = ForeignKey.CASCADE
    )
//    ,
//    ForeignKey(
//            entity = STRoadDefectInfo::class,
//            parentColumns = arrayOf(STRoadDefectInfo.Properties.ID),
//            childColumns = arrayOf(STDefectLocation.Properties.ROAD_DEFECT_STOP_ID_FK),
//            onDelete = ForeignKey.CASCADE
//    )
])

data class STDefectLocation(
        @ColumnInfo(name = Properties.ID)
        @PrimaryKey(autoGenerate = true)
        val id: Long? = null,
        @ColumnInfo(name = Properties.FK_ID)
        var defectID_FK: Long? = null,

        @ColumnInfo(name = Properties.ROAD_DEFECT_ID_FK)
        var roadDefectInfo_FK: Long? = null,

        @ColumnInfo(name = Properties.TIMESTAMP)
        val timestamp: Long = 0,
        @ColumnInfo(name = Properties.LATITUDE)
        val latitude: Double,
        @ColumnInfo(name = Properties.LONGITUDE)
        var longitude: Double,
        @ColumnInfo(name = Properties.ALTITUDE)
        var altitude: Double,
        @ColumnInfo(name = Properties.ACCURACY)
        var accuracy: Float
) {
    object Properties {
        const val TABLE_NAME = "DefectLocation"
        const val ID = "defectLocationPK"
        const val FK_ID = "defectIDFK"
        const val ROAD_DEFECT_ID_FK = "roadDefectInfoID_FK"
        const val TIMESTAMP = "timestamp"
        const val LATITUDE = "latitude"
        const val LONGITUDE = "longitude"
        const val ALTITUDE = "altitude"
        const val ACCURACY = "accuracy"

    }
}