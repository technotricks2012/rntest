package com.lta.storelib.implementations

import android.content.Context
import android.util.Log
import com.lta.datalib.extenstions.applyIOWorkSchedulers
import com.lta.datalib.interfaces.db.IRotationVectorDB
import com.lta.datalib.model.sensor.CRotationVector
import com.lta.datalib.utils.Response
import com.lta.storelib.datastore.batch.BatchDatabaseDataStore
import com.lta.storelib.datastore.rotationVector.RotationVectorDatabaseDataStore
import io.reactivex.Single

class StoreRotationVectorImpl(private val context: Context) : IRotationVectorDB {
    private val rotationVectorDatabaseDataStore = RotationVectorDatabaseDataStore(context)
//    private val batchDatabaseDataStore = BatchDatabaseDataStore(context)
    private val storeDb = DataStoreImpl(context)


    override fun getList(batchId: Long): Single<Response<List<CRotationVector>, Throwable>> =
            Single.fromCallable {
                rotationVectorDatabaseDataStore.getList(batchId)
            }.applyIOWorkSchedulers()


    override fun getLast(): Single<Response<CRotationVector, Throwable>> =
            Single.fromCallable {
                rotationVectorDatabaseDataStore.getLast()
            }.applyIOWorkSchedulers()

    override fun save(data: CRotationVector): Single<Response<CRotationVector, Throwable>> =
            Single.fromCallable {
//                val batch = batchDatabaseDataStore.getNewBatch()
                val batch = BatchDatabaseDataStore.getInstance(context).getNewBatch()

                val journeyID = storeDb.getJourneyId()
                val mode = storeDb.getMode()
                val predictionMode = storeDb.getPredictionMode()
                if (batch.hasResult) {
                    data.batch_id = batch.result?.id ?: -1
                    data.journeyID = journeyID
                    data.predictionMode = predictionMode
                    data.mode = mode
                } else
                    data.batch_id = -1
                rotationVectorDatabaseDataStore.save(data)
            }.applyIOWorkSchedulers()


    override fun save(data: List<CRotationVector>): Single<Response<List<CRotationVector>, Throwable>> =
            Single.fromCallable {
                Log.d("CRotationVector Axes DB Size", data.size.toString())

//                val batch = batchDatabaseDataStore.getNewBatch()
                val batch = BatchDatabaseDataStore.getInstance(context).getNewBatch()
                if (batch.hasResult) {
                    val batchId = batch.result?.id ?: -1
                    val journeyID = storeDb.getJourneyId()
                    var mode = storeDb.getMode()
                    var predictionMode = storeDb.getPredictionMode()
                    data.map {
                        it.batch_id = batchId
                        it.journeyID = journeyID
                        it.predictionMode = predictionMode
                        it.mode = mode
                    }
                } else
                    data.map {
                        it.batch_id = -1
                    }
                rotationVectorDatabaseDataStore.save(data)
            }.applyIOWorkSchedulers()
}