package com.lta.storelib.datastore.pedometer

import android.content.Context
import com.lta.datalib.extenstions.resultOrError
import com.lta.datalib.model.sensor.CPedometer
import com.lta.datalib.utils.Response
import com.lta.storelib.Database
import com.lta.storelib.datastore.base.AbstractDataStore
import com.lta.storelib.helpers.toCPedometerTransaction
import com.lta.storelib.helpers.toSTPedometerTransaction

internal class PedometerDatabaseDataStore(context: Context?) : AbstractDataStore(context = context), PedometerDataStore {

    override fun save(sensorData: CPedometer): Response<CPedometer, Throwable> = saveItem(sensorData)

    override fun save(sensorDatas: List<CPedometer>): Response<List<CPedometer>, Throwable> = saveItems(sensorDatas)
    override fun getList(batchId: Long): Response<List<CPedometer>, Throwable> = getItems(batchId)
    override fun getLast(): Response<CPedometer, Throwable> =
            resultOrError {
                return@resultOrError Database.getDatabase(context)
                        .pedometerDao()
                        .queryLast().toCPedometerTransaction()
            }

    private fun saveItem(sensorData: CPedometer): Response<CPedometer, Throwable> =
            resultOrError {
                Database.getDatabase(context)
                        .pedometerDao()
                        .insert(sensorData.toSTPedometerTransaction())
                return@resultOrError sensorData
            }

    private fun saveItems(sensorDatas: List<CPedometer>): Response<List<CPedometer>, Throwable> =
            resultOrError {
                Database.getDatabase(context)
                        .pedometerDao()
                        .insert(sensorDatas.map { it.toSTPedometerTransaction() })
                return@resultOrError sensorDatas
            }
    
    private fun getItems(batchId: Long): Response<List<CPedometer>, Throwable> =
            resultOrError {
                return@resultOrError Database.getDatabase(context)
                        .pedometerDao()
                        .queryList(batchId).map {
                            it.toCPedometerTransaction()
                        }
            }
}
