package com.lta.storelib.entity

import androidx.room.*

@Entity(tableName = STBatch.Properties.TABLE_NAME)
data class STBatch(
    @ColumnInfo(name = Properties.ID)
    @PrimaryKey(autoGenerate = true) var id: Long? = null,
    @ColumnInfo(name = Properties.TIMESTAMP) val timestamp: Long = 0,
    @ColumnInfo(name = Properties.BATCH_LIMIT) val batchLimit: Long? = 0,
    @ColumnInfo(name = Properties.LOG_COUNT) var counts: Long? = null
){
    @ColumnInfo(name = Properties.STATUS)
    @TypeConverters(Status.Converter::class)
    var status: Status = Status.NEW

    object Properties {
        const val TABLE_NAME = "Batch"
        const val ID = "batchIdPK"
        const val BATCH_LIMIT = "batchLimit"
        const val LOG_COUNT = "logCounts"
        const val TIMESTAMP = "timestamp"
        const val STATUS = "status"
    }

    enum class Status(val value: String) {
        NEW(Properties.NEW),
        UPLOADING(Properties.UPLOADING),
        UPLOADED(Properties.UPLOADED),
        READY(Properties.READY),
        FAIL(Properties.FAIL),
        UNKNOWN(Properties.UNKNOWN);


        object Properties {
            const val NEW = "NEW"
            const val UPLOADING = "UPLOADING"
            const val UPLOADED = "UPLOADED"
            const val READY = "READY"
            const val FAIL = "FAIL"
            const val UNKNOWN = "UNKNOWN"

        }

        object Converter{
            @TypeConverter
            @JvmStatic
            fun toString(type: Status): String {
                return type.value
            }

            @TypeConverter
            @JvmStatic
            fun toType(value: String): Status {
                return when (value) {
                    NEW.value -> NEW
                    UPLOADED.value -> UPLOADED
                    READY.value -> READY
                    FAIL.value -> FAIL
                    UPLOADING.value -> UPLOADING
                    else -> UNKNOWN

                }
            }
        }
    }
}

