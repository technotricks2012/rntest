package com.lta.storelib.datastore.barometer

import com.lta.datalib.model.sensor.CBarometer
import com.lta.datalib.utils.Response
import com.lta.storelib.datastore.base.DataStore

interface BarometerDataStore : DataStore {
    fun save(stAccelerometer: CBarometer): Response<CBarometer, Throwable>
    fun save(stAccelerometer: List<CBarometer>): Response<List<CBarometer>, Throwable>
    fun getList(batchId: Long): Response<List<CBarometer>, Throwable>
    fun getLast(): Response<CBarometer, Throwable>
}