package com.lta.storelib.datastore.magnetometer

import android.content.Context
import com.lta.datalib.extenstions.resultOrError
import com.lta.datalib.model.sensor.CMagnetometer
import com.lta.datalib.utils.Response
import com.lta.storelib.Database
import com.lta.storelib.datastore.base.AbstractDataStore
import com.lta.storelib.helpers.toCMagnetometerTransaction
import com.lta.storelib.helpers.toSTMagnetometerTransaction

internal class MagnetometerDatabaseDataStore(context: Context?) : AbstractDataStore(context = context), MagnetometerDataStore {


    override  fun save(data: CMagnetometer): Response<CMagnetometer, Throwable> = saveItem(data)

    override  fun save(data: List<CMagnetometer>): Response<List<CMagnetometer>, Throwable> = saveItems(data)
    override  fun getList(batchId: Long): Response<List<CMagnetometer>, Throwable> = getItems(batchId)
    override fun getLast(): Response<CMagnetometer, Throwable> =
            resultOrError {
                return@resultOrError Database.getDatabase(context)
                        .magnetometerDao()
                        .queryLast()
                        .toCMagnetometerTransaction()
            }

    private  fun saveItem(data: CMagnetometer): Response<CMagnetometer, Throwable> =
        resultOrError {
            Database.getDatabase(context)
                    .magnetometerDao()
                    .insert(data.toSTMagnetometerTransaction())
            return@resultOrError data
        }


    private  fun saveItems(cAccelerometer: List<CMagnetometer>): Response<List<CMagnetometer>, Throwable> =
        resultOrError {
            Database.getDatabase(context)
                    .magnetometerDao()
                    .insert(cAccelerometer.map { it.toSTMagnetometerTransaction() })
            return@resultOrError cAccelerometer
        }


    private  fun getItems(batchId: Long): Response<List<CMagnetometer>, Throwable> =
        resultOrError {
            return@resultOrError Database.getDatabase(context)
                .magnetometerDao()
                .queryList(batchId).map {
                        it.toCMagnetometerTransaction()
                    }
        }
    }
