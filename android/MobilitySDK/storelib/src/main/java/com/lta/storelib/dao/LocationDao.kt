package com.lta.storelib.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.lta.storelib.entity.STLocation

@Dao
interface LocationDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(chapter: STLocation)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(chapter: List<STLocation>)

    @Query("SELECT * FROM ${STLocation.Properties.TABLE_NAME} WHERE ${STLocation.Properties.BATCH_IDFK} = :batchId")
    fun queryList(batchId : Long) : List<STLocation>

    @Query("SELECT * FROM ${STLocation.Properties.TABLE_NAME} ORDER BY ${STLocation.Properties.ID} DESC LIMIT 1")
    fun queryLast() : STLocation

}