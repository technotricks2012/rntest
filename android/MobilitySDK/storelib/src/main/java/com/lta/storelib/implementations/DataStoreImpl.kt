package com.lta.storelib.implementations

import android.content.Context
import android.content.SharedPreferences
import android.util.Log
import com.lta.datalib.extenstions.resultOrError
import com.lta.datalib.interfaces.db.IStoreDB
import com.lta.datalib.utils.Response
import io.reactivex.Single

class DataStoreImpl(private val context: Context?) : IStoreDB {


    private var PRIVATE_MODE = 0
    private val PREF = "MobilitySdkStore"

    private val sharedPref by lazy { context?.getSharedPreferences(PREF, PRIVATE_MODE) }


    override fun save(key: String, value: String) {
        val editor = sharedPref?.edit()
        editor?.putString(key, value)
        editor?.apply()
    }

    override fun get(key: String): Response<String, Throwable> {
       return resultOrError {
           val data = sharedPref?.getString(key, null);
           data
       }
    }


    override fun getJourneyId() = get("JOURNEY_ID").result?:""
    override fun getUserId() = get("USER_NAME").result?:""
    override fun getMode() = get("TRAVEL_MODE").result?:""
    override fun getPredictionMode() = get("USER_ACTIVITY_MODE").result?:""
    override fun setIsStart() = save("IS_START","TRUE")
    override fun getIsStart(): Boolean = (get("IS_START").result?:"FALSE").toBoolean()


}