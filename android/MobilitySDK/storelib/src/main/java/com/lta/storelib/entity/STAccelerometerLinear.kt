package com.lta.storelib.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey

@Entity(tableName =  STAccelerometerLinear.Properties.TABLE_NAME, foreignKeys = [
    ForeignKey(
            entity = STBatch::class,
            parentColumns = arrayOf(STBatch.Properties.ID),
            childColumns = arrayOf(STAccelerometerLinear.Properties.BATCH_IDFK),
            onDelete = ForeignKey.CASCADE
    )]
)
data class STAccelerometerLinear(
    @ColumnInfo(name = Properties.ID)
    @PrimaryKey(autoGenerate = true)
    val id: Long? = null,
    @ColumnInfo(name = Properties.TIMESTAMP) val timestamp: Long = 0,
    @ColumnInfo(name = Properties.X) var x: Float,
    @ColumnInfo(name = Properties.Y) var y: Float,
    @ColumnInfo(name = Properties.Z) var z: Float,
    @ColumnInfo(name = Properties.BATCH_IDFK) var batchIdFK: Long,
    @ColumnInfo(name = Properties.MODE) var mode: String,
    @ColumnInfo(name = Properties.PREDICTIONMode) var predictionMode: String,
    @ColumnInfo(name = Properties.JourneyID) var journeyID: String
){
    object Properties {
        const val TABLE_NAME = "AccelerometerLinear"
        const val ID = "accelerometerLinearIdPK"
        const val BATCH_IDFK = "batchIdFK"
        const val TIMESTAMP = "timestamp"
        const val X = "x"
        const val Y = "y"
        const val Z = "z"
        const val MODE = "mode"
        const val PREDICTIONMode = "predictionMode"
        const val JourneyID = "journeyID"
    }
}

