package com.lta.storelib.implementations

import android.content.Context
import android.util.Log
import com.lta.datalib.extenstions.applyIOWorkSchedulers
import com.lta.datalib.interfaces.db.IGyroscopeDB
import com.lta.datalib.model.sensor.CGyroscope
import com.lta.datalib.utils.Response
import com.lta.storelib.datastore.batch.BatchDatabaseDataStore
import com.lta.storelib.datastore.gyroscope.GyroscopeDatabaseDataStore
import io.reactivex.Single

class StoreGyroscopeImpl(private val context: Context) : IGyroscopeDB {
    private val gyroscopeDatabaseDataStore = GyroscopeDatabaseDataStore(context)
//    private val batchDatabaseDataStore = BatchDatabaseDataStore(context)
    private val storeDb = DataStoreImpl(context)



    override fun getList(batchId: Long): Single<Response<List<CGyroscope>, Throwable>> =
            Single.fromCallable {
                gyroscopeDatabaseDataStore.getList(batchId)
            }.applyIOWorkSchedulers()


    override fun getLast(): Single<Response<CGyroscope, Throwable>> =
            Single.fromCallable {
                gyroscopeDatabaseDataStore.getLast()
            }.applyIOWorkSchedulers()

    override fun save(data: CGyroscope): Single<Response<CGyroscope, Throwable>> =
            Single.fromCallable {
//                val batch = batchDatabaseDataStore.getNewBatch()
                val batch = BatchDatabaseDataStore.getInstance(context).getNewBatch()

                val journeyID = storeDb.getJourneyId()
                var mode = storeDb.getMode()
                var predictionMode = storeDb.getPredictionMode()
                if (batch.hasResult) {
                    data.batch_id = batch.result?.id ?: -1
                    data.journeyID = journeyID
                    data.predictionMode = predictionMode
                    data.mode = mode
                } else
                    data.batch_id = -1
                gyroscopeDatabaseDataStore.save(data)
            }.applyIOWorkSchedulers()


    override fun save(data: List<CGyroscope>): Single<Response<List<CGyroscope>, Throwable>> =
            Single.fromCallable {
                Log.d("Gyroscope DB Size", data.size.toString())

//                val batch = batchDatabaseDataStore.getNewBatch()
                val batch = BatchDatabaseDataStore.getInstance(context).getNewBatch()

                if (batch.hasResult) {
                    val batchId = batch.result?.id ?: -1
                    val journeyID = storeDb.getJourneyId()
                    var mode = storeDb.getMode()
                    var predictionMode = storeDb.getPredictionMode()
                    data.map {
                        it.batch_id = batchId
                        it.journeyID = journeyID
                        it.predictionMode = predictionMode
                        it.mode = mode
                    }
                } else
                    data.map {
                        it.batch_id = -1
                    }
                gyroscopeDatabaseDataStore.save(data)
            }.applyIOWorkSchedulers()
}