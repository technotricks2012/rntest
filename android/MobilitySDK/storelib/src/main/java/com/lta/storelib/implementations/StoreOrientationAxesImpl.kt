package com.lta.storelib.implementations

import android.content.Context
import android.util.Log
import com.lta.datalib.extenstions.applyIOWorkSchedulers
import com.lta.datalib.interfaces.db.IOrientationAxesDB
import com.lta.datalib.model.sensor.COrientationAxes
import com.lta.datalib.utils.Response
import com.lta.storelib.datastore.batch.BatchDatabaseDataStore
import com.lta.storelib.datastore.orientationAxes.OrientationAxesDatabaseDataStore
import io.reactivex.Single

class StoreOrientationAxesImpl(private val context: Context) : IOrientationAxesDB {
    private val orientationAxesDatabaseDataStore = OrientationAxesDatabaseDataStore(context)
//    private val batchDatabaseDataStore = BatchDatabaseDataStore(context)
    private val storeDb = DataStoreImpl(context)


    override fun getList(batchId: Long): Single<Response<List<COrientationAxes>, Throwable>> =
            Single.fromCallable {
                orientationAxesDatabaseDataStore.getList(batchId)
            }.applyIOWorkSchedulers()


    override fun getLast(): Single<Response<COrientationAxes, Throwable>> =
            Single.fromCallable {
                orientationAxesDatabaseDataStore.getLast()
            }.applyIOWorkSchedulers()

    override fun save(data: COrientationAxes): Single<Response<COrientationAxes, Throwable>> =
            Single.fromCallable {
//                val batch = batchDatabaseDataStore.getNewBatch()
                val batch = BatchDatabaseDataStore.getInstance(context).getNewBatch()

                val journeyID = storeDb.getJourneyId()
                val mode = storeDb.getMode()
                val predictionMode = storeDb.getPredictionMode()
                if (batch.hasResult) {
                    data.batch_id = batch.result?.id ?: -1
                    data.journeyID = journeyID
                    data.predictionMode = predictionMode
                    data.mode = mode
                } else
                    data.batch_id = -1
                orientationAxesDatabaseDataStore.save(data)
            }.applyIOWorkSchedulers()


    override fun save(data: List<COrientationAxes>): Single<Response<List<COrientationAxes>, Throwable>> =
            Single.fromCallable {
                Log.d("Orientation Axes DB Size", data.size.toString())

//                val batch = batchDatabaseDataStore.getNewBatch()
                val batch = BatchDatabaseDataStore.getInstance(context).getNewBatch()

                if (batch.hasResult) {
                    val batchId = batch.result?.id ?: -1
                    val journeyID = storeDb.getJourneyId()
                    var mode = storeDb.getMode()
                    var predictionMode = storeDb.getPredictionMode()
                    data.map {
                        it.batch_id = batchId
                        it.journeyID = journeyID
                        it.predictionMode = predictionMode
                        it.mode = mode
                    }
                } else
                    data.map {
                        it.batch_id = -1
                    }
                orientationAxesDatabaseDataStore.save(data)
            }.applyIOWorkSchedulers()
}