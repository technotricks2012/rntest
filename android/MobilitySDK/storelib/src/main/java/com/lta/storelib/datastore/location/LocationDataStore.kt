package com.lta.storelib.datastore.location

import com.lta.datalib.model.CLocation
import com.lta.datalib.utils.Response
import com.lta.storelib.datastore.base.DataStore

interface LocationDataStore : DataStore {
    fun save(stAccelerometer: CLocation): Response<CLocation, Throwable>
    fun save(stAccelerometer: List<CLocation>): Response<List<CLocation>, Throwable>
    fun getList(batchId: Long): Response<List<CLocation>, Throwable>
    fun getLast(): Response<CLocation, Throwable>
}