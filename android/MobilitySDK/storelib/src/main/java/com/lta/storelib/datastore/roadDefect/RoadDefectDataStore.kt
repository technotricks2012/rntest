package com.lta.storelib.datastore.roadDefect

import com.lta.datalib.model.roadDefect.CRoadDefectEmbedded
import com.lta.datalib.utils.Response
import com.lta.storelib.datastore.base.DataStore
import com.lta.storelib.entity.roadDefect.STDefect
import com.lta.storelib.entity.roadDefect.STDefectLocation

interface RoadDefectDataStore :DataStore{

     fun insertDefect(stDefect: STDefect,stDefectLocations: List<STDefectLocation>) : Response<List<CRoadDefectEmbedded>,Throwable>
     fun insertStartTimeWithLocation(timestamp: Long,stRoadInfoLocations: List<STDefectLocation>) : Response<List<CRoadDefectEmbedded>,Throwable>
     fun insertStopTimeWithLocation(timestamp: Long,stRoadInfoLocations: List<STDefectLocation>) : Response<List<CRoadDefectEmbedded>,Throwable>

     fun delete(ids: List<Long>): Response<Boolean,Throwable>

     fun getList() : Response<List<CRoadDefectEmbedded>,Throwable>
}