package com.lta.storelib.implementations

import android.content.Context
import android.util.Log
import com.lta.datalib.extenstions.applyIOWorkSchedulers
import com.lta.datalib.interfaces.db.IPedometerDB
import com.lta.datalib.model.sensor.CPedometer
import com.lta.datalib.utils.Response
import com.lta.storelib.datastore.batch.BatchDatabaseDataStore
import com.lta.storelib.datastore.pedometer.PedometerDatabaseDataStore
import io.reactivex.Single

class StorePedometerImpl(private val context: Context) : IPedometerDB {
    private val pedometerDatabaseDataStore = PedometerDatabaseDataStore(context)
//    private val batchDatabaseDataStore = BatchDatabaseDataStore(context)
    private val storeDb = DataStoreImpl(context)

    override fun getList(batchId: Long): Single<Response<List<CPedometer>, Throwable>> =
            Single.fromCallable {
                pedometerDatabaseDataStore.getList(batchId)
            }.applyIOWorkSchedulers()


    override fun getLast(): Single<Response<CPedometer, Throwable>> =
            Single.fromCallable {
                pedometerDatabaseDataStore.getLast()
            }.applyIOWorkSchedulers()

    override fun save(data: CPedometer): Single<Response<CPedometer, Throwable>> =
            Single.fromCallable {
//                val batch = batchDatabaseDataStore.getNewBatch()
                val batch = BatchDatabaseDataStore.getInstance(context).getNewBatch()

                val journeyID = storeDb.getJourneyId()
                var mode = storeDb.getMode()
                var predictionMode = storeDb.getPredictionMode()
                if (batch.hasResult) {
                    data.batch_id = batch.result?.id ?: -1
                    data.journeyID = journeyID
                    data.predictionMode = predictionMode
                    data.mode = mode
                } else
                    data.batch_id = -1
                pedometerDatabaseDataStore.save(data)
            }.applyIOWorkSchedulers()


    override fun save(data: List<CPedometer>): Single<Response<List<CPedometer>, Throwable>> =
            Single.fromCallable {
                Log.d("Pedometer DB Size", data.size.toString())

//                val batch = batchDatabaseDataStore.getNewBatch()
                val batch = BatchDatabaseDataStore.getInstance(context).getNewBatch()
                if (batch.hasResult) {
                    val batchId = batch.result?.id ?: -1
                    val journeyID = storeDb.getJourneyId()
                    var mode = storeDb.getMode()
                    var predictionMode = storeDb.getPredictionMode()
                    data.map {
                        it.batch_id = batchId
                        it.journeyID = journeyID
                        it.predictionMode = predictionMode
                        it.mode = mode
                    }
                } else
                    data.map {
                        it.batch_id = -1
                    }
                pedometerDatabaseDataStore.save(data)
            }.applyIOWorkSchedulers()
}