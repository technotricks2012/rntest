package com.lta.storelib.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.lta.storelib.entity.STAccelerometerLinear

@Dao
interface AccelerometerLinearDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(chapter: STAccelerometerLinear)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(chapter: List<STAccelerometerLinear>)

    @Query("SELECT * FROM ${STAccelerometerLinear.Properties.TABLE_NAME} WHERE ${STAccelerometerLinear.Properties.BATCH_IDFK} = :batchId")
    fun queryList(batchId : Long) : List<STAccelerometerLinear>

    @Query("SELECT * FROM ${STAccelerometerLinear.Properties.TABLE_NAME} ORDER BY ${STAccelerometerLinear.Properties.ID} DESC LIMIT 1")
    fun queryLast() : STAccelerometerLinear

}