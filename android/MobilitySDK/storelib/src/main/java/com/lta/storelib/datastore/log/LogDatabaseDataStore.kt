package com.lta.storelib.datastore.log

import android.content.Context
import com.lta.datalib.extenstions.resultOrError
import com.lta.datalib.helper.SingletonHolder
import com.lta.datalib.model.db.CLog
import com.lta.datalib.utils.Response
import com.lta.storelib.Database
import com.lta.storelib.datastore.base.AbstractDataStore
import com.lta.storelib.helpers.toCLogTransaction
import com.lta.storelib.helpers.toSTLogTransaction

internal class LogDatabaseDataStore(context: Context?) : AbstractDataStore(context = context), LogDataStore {

    companion object : SingletonHolder<LogDatabaseDataStore, Context>(::LogDatabaseDataStore)


    override fun save(log: CLog): Response<CLog, Throwable> =
            resultOrError {
                Database.getDatabase(context)
                        .logDao()
                        .insert(log.toSTLogTransaction())
                return@resultOrError log
            }

    override fun get(): Response<List<CLog>, Throwable> =
            resultOrError {
                Database.getDatabase(context)
                        .logDao()
                        .queryList().map {
                            it.toCLogTransaction()
                        }
            }
}
