package com.lta.storelib.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = SDKConfig.Properties.TABLE_NAME)
class SDKConfig(
    @ColumnInfo(name = Properties.ID)
    @PrimaryKey(autoGenerate = true)
    val id: Long? = 0,
    @ColumnInfo(name = Properties.APP_ID) val appId: String,
    @ColumnInfo(name = Properties.SECRET_KEY) val secret: String
) {
    object Properties {
        const val TABLE_NAME = "SDKConfig"
        const val ID = "id"
        const val APP_ID = "appId"
        const val SECRET_KEY = "secretKey"
    }

}

