package com.lta.storelib.datastore.gyroscope

import com.lta.datalib.model.sensor.CGyroscope
import com.lta.datalib.utils.Response
import com.lta.storelib.datastore.base.DataStore

interface GyroscopeDataStore : DataStore {
    fun save(stAccelerometer: CGyroscope): Response<CGyroscope, Throwable>
    fun save(stAccelerometer: List<CGyroscope>): Response<List<CGyroscope>, Throwable>
    fun getList(batchId: Long): Response<List<CGyroscope>, Throwable>
    fun getLast(): Response<CGyroscope, Throwable>
}