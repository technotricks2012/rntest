package com.lta.storelib.datastore.sdkconfig

import android.content.Context
import com.lta.datalib.extenstions.applyIOWorkSchedulers
import com.lta.datalib.extenstions.resultOrError
import com.lta.datalib.model.SDKConfig
import com.lta.datalib.utils.Response
import com.lta.storelib.Database
import com.lta.storelib.datastore.base.AbstractDataStore
import io.reactivex.Single

internal class SDKConfigNoDataStore(context : Context)  : AbstractDataStore(context = context), SDKConfigConfigDataStore{
    override fun saveBuildConfig(sdkConfig: SDKConfig): Single<Response<SDKConfig,Throwable>> {
        throw UnsupportedOperationException("Character creation on the Server Side is unsupported.")

    }

}