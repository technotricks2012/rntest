package com.lta.storelib.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.lta.storelib.entity.STRotationVector

@Dao
interface RotationVectorDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(chapter: STRotationVector)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(chapter: List<STRotationVector>)

    @Query("SELECT * FROM ${STRotationVector.Properties.TABLE_NAME} WHERE ${STRotationVector.Properties.BATCH_IDFK} = :batchId")
    fun queryList(batchId : Long) : List<STRotationVector>

    @Query("SELECT * FROM ${STRotationVector.Properties.TABLE_NAME} ORDER BY ${STRotationVector.Properties.ID} DESC LIMIT 1")
    fun queryLast() : STRotationVector

}