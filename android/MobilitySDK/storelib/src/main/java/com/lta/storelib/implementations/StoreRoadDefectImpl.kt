package com.lta.storelib.implementations

import android.content.Context
import com.lta.datalib.extenstions.applyIOWorkSchedulers
import com.lta.datalib.interfaces.db.IRoadDefectDB
import com.lta.datalib.model.roadDefect.CDefect
import com.lta.datalib.model.roadDefect.CDefectLocation
import com.lta.datalib.model.roadDefect.CRoadDefect
import com.lta.datalib.model.roadDefect.CRoadDefectEmbedded
import com.lta.datalib.model.sensor.CAccelerometer
import com.lta.datalib.utils.Response
import com.lta.storelib.datastore.roadDefect.RoadDefectDatabaseDataStore
import com.lta.storelib.helpers.toSTDefectLocationTransaction
import com.lta.storelib.helpers.toSTDefectTransaction
import com.lta.storelib.helpers.toSTRoadDefectTransaction
import io.reactivex.Single

class StoreRoadDefectImpl(private val context: Context?) : IRoadDefectDB {
    private val roadDefectDatabaseDataStore = RoadDefectDatabaseDataStore(context)

    override fun insertDefect(cDefect: CDefect,cDefectLocations: List<CDefectLocation>): Single<Response<List<CRoadDefectEmbedded>, Throwable>> {
        return Single.fromCallable {
            roadDefectDatabaseDataStore.insertDefect(stDefect = cDefect.toSTDefectTransaction(),stDefectLocations = cDefectLocations.map { it.toSTDefectLocationTransaction() })
        }.applyIOWorkSchedulers()
    }

    override fun insertStartTimeWithLocation(timestamp: Long, stRoadInfoLocations: List<CDefectLocation>): Single<Response<List<CRoadDefectEmbedded>, Throwable>> {
        return Single.fromCallable {

            roadDefectDatabaseDataStore.insertStartTimeWithLocation(timestamp,stRoadInfoLocations = stRoadInfoLocations.map { it.toSTDefectLocationTransaction() })
        }.applyIOWorkSchedulers()

    }

    override fun insertStopTimeWithLocation(timestamp: Long, stRoadInfoLocations: List<CDefectLocation>): Single<Response<List<CRoadDefectEmbedded>, Throwable>> {
        return Single.fromCallable {
            roadDefectDatabaseDataStore.insertStopTimeWithLocation(timestamp,stRoadInfoLocations = stRoadInfoLocations.map { it.toSTDefectLocationTransaction() })
        }.applyIOWorkSchedulers()
    }

    override fun delete(ids: List<Long>): Single<Response<Boolean, Throwable>> {
        return Single.fromCallable {
            roadDefectDatabaseDataStore.delete(ids)
        }.applyIOWorkSchedulers()
    }

    override fun getList(): Single<Response<List<CRoadDefectEmbedded>, Throwable>> {
        return Single.fromCallable {
            roadDefectDatabaseDataStore.getList()
        }.applyIOWorkSchedulers()
    }

}