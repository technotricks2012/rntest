package com.lta.storelib.datastore.rotationVector

import android.content.Context
import com.lta.datalib.extenstions.resultOrError
import com.lta.datalib.model.sensor.CRotationVector
import com.lta.datalib.utils.Response
import com.lta.storelib.Database
import com.lta.storelib.datastore.base.AbstractDataStore
import com.lta.storelib.helpers.toCRotationVectorTransaction
import com.lta.storelib.helpers.toSTRotationVectorTransaction

internal class RotationVectorDatabaseDataStore(context: Context?) : AbstractDataStore(context = context), RotationVectorDataStore {

    override fun save(sensorData: CRotationVector): Response<CRotationVector, Throwable> = saveItem(sensorData)

    override fun save(sensorDatas: List<CRotationVector>): Response<List<CRotationVector>, Throwable> = saveItems(sensorDatas)
    override fun getList(batchId: Long): Response<List<CRotationVector>, Throwable> = getItems(batchId)
    override fun getLast(): Response<CRotationVector, Throwable> =
            resultOrError {
                return@resultOrError Database.getDatabase(context)
                        .rotationVectorDao()
                        .queryLast().toCRotationVectorTransaction()
            }

    private fun saveItem(sensorData: CRotationVector): Response<CRotationVector, Throwable> =
            resultOrError {
                Database.getDatabase(context)
                        .rotationVectorDao()
                        .insert(sensorData.toSTRotationVectorTransaction())
                return@resultOrError sensorData
            }

    private fun saveItems(sensorDatas: List<CRotationVector>): Response<List<CRotationVector>, Throwable> =
            resultOrError {
                Database.getDatabase(context)
                        .rotationVectorDao()
                        .insert(sensorDatas.map { it.toSTRotationVectorTransaction() })
                return@resultOrError sensorDatas
            }
    
    private fun getItems(batchId: Long): Response<List<CRotationVector>, Throwable> =
            resultOrError {
                return@resultOrError Database.getDatabase(context)
                        .rotationVectorDao()
                        .queryList(batchId).map {
                            it.toCRotationVectorTransaction()
                        }
            }
}
