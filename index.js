import {
  AppRegistry,
  YellowBox,
} from 'react-native';

import {setJSExceptionHandler, setNativeExceptionHandler} from 'react-native-exception-handler';
import BackgroundFetch from "react-native-background-fetch";


import App from './src/App';
import {name as appName} from './app.json';
import React from 'react';
import {Provider} from 'react-redux';
import store from './src/store';
import {uploadAllData} from './src/utils/UploadSensorData';

import {getLocal} from './src/db/DbManager';
import FirebaseDB from './src/firebase/FirebaseDB';
import {getCurrentTime} from './src/utils/Utils';



import crashlytics from '@react-native-firebase/crashlytics';
import { USER_NAME } from './src/constants/IKeyConstants';

YellowBox.ignoreWarnings(['Warning: isMounted(...) is deprecated']);
console.disableYellowBox = true;

import moment from 'moment';
console.log("Time==>"+ new Date().getTime())
console.log("Time==>"+moment().unix())

const AppContainer = () => (
  <Provider store={store}>
    <App />
  </Provider>
);

AppRegistry.registerComponent(appName, () => AppContainer);



let MyHeadlessTask = async () => {
  // console.log('RNBackgroundFetch  uploadAllData');

  // const refID = await FirebaseDB.setData('BGCHECK', {
  //   user: await getLocal(USER_NAME),
  //   time: getCurrentTime()
  // });


  await uploadAllData("15 Min");

  BackgroundFetch.finish();
}




// Register your BackgroundFetch HeadlessTask
BackgroundFetch.registerHeadlessTask(MyHeadlessTask);




const defaultHandler = ErrorUtils.getGlobalHandler()

ErrorUtils.setGlobalHandler((...args) => {

  const error = args[0] || 'Unknown'

  console.log("ExpeptionHandler setGlobalHandler=>"+error)
  //console.log('Crashlytics error sent', error);

  if (error instanceof Error) {
    crashlytics().setAttribute('stack', `${error.stack}`)
    crashlytics().setAttribute('message', `${error.message}`)
    crashlytics().recordError(0, `RN Fatal: ${error.message}`)
  } else {
    // Have never gotten this log so far. Might not be necessary.
    crashlytics().recordError(0, `RN Fatal: ${error}`)
  }
  crashlytics().log(error.message);

  defaultHandler.apply(this, args);
  //force the native crash
  crashlytics().crash();
});

