//
//  MotionKit.swift
//  MobilitySensing
//
//  Created by Kishore Raj on 5/12/19.
//  Copyright © 2019 Facebook. All rights reserved.
//


import Foundation

import RxSwift
import CoreMotion
//import "
//_______________________________________________________________________________________________________________
// this helps retrieve values from the sensors.
protocol MotionKitDelegate :class{
  func retrieveAccelerometerValues (data: String)
  func retrieveGyroscopeValues (data: String)
  func retrieveMagnetometerValues (data: String)
  func retrievePedoMeterValues (data: String)
  func retrieveBaroMeterValues (data: String)
  func retrieveDeviceMotionValues (data: String)
  func retrieveUserMotionValues (data: String)
  
  
  
  //  func retrieveGyroscopeValues     (x: Double, y:Double, z:Double, absoluteValue: Double)
  //  func retrieveDeviceMotionObject  (deviceMotion: CMDeviceMotion)
  //  func retrieveMagnetometerValues  (x: Double, y:Double, z:Double, absoluteValue: Double)
  
  //  func getAccelerationValFromDeviceMotion        (x: Double, y:Double, z:Double)
  //  func getGravityAccelerationValFromDeviceMotion (x: Double, y:Double, z:Double)
  //  func getRotationRateFromDeviceMotion           (x: Double, y:Double, z:Double)
  //  func getMagneticFieldFromDeviceMotion          (x: Double, y:Double, z:Double)
  //  func getAttitudeFromDeviceMotion               (attitude: CMAttitude)
}

enum MotionKitError: Error {
  case ROTATION_SENSOR_NOT_AVAILABLE
  case ACCELETOMETER_SENSOR_NOT_AVAILABLE
  
}
class MotionKit :NSObject{
  
  //  let manager = CMMotionManager()
  let coreMotionManager = CMMotionManager.rx.manager(interval: 0.3)
  let barometerMotionManager = CMAltimeter.rx.manager(interval: 0.3)
  let pedometerManager = CMPedometer.rx.manager(interval: 0.3)
  
  let userMotionManager = CMMotionActivityManager.rx.manager()
  
  let disposeBag = DisposeBag()
  
  
  
  
  var delegate: MotionKitDelegate?
  
  /*
   *  init:void:
   *
   *  Discussion:
   *   Initialises the MotionKit class and throw a Log with a timestamp.
   */
  
  struct Singleton {
    static let sharedInstance = MotionKit()
  }
  
  public override init(){
    NSLog("MotionKit has been initialised successfully")
  }
  
  
  /*
   *  getGyroValues:interval:values:
   *
   *  Discussion:
   *   Starts gyro updates, providing data to the given handler through the given queue.
   *   Note that when the updates are stopped, all operations in the
   *   given NSOperationQueue will be cancelled. You can access the retrieved values either by a
   *   Trailing Closure or through a Delegate.
   */
  func getGyroValues (interval: TimeInterval = 0.1, values: ((_ data: String) -> ())? ){
    
    coreMotionManager
      .flatMapLatest { manager in
        manager.gyroRate ?? Observable.empty()
    }
    .observeOn(MainScheduler.instance)
    .distinctUntilChanged({ (a, b) -> Bool in
      a.timestamp == b.timestamp
    })
      .subscribe(onNext: { (data) in
        
        if values != nil{
          values!(data.toRNFromat)
        }
        
        self.delegate?.retrieveGyroscopeValues(data: data.toRNFromat)
        
      }).disposed(by: disposeBag)
  }
  
  func getAccelerometerValues (interval: TimeInterval = 0.1, values: ((_ data: String) -> ())? ){
    
    coreMotionManager
      .flatMapLatest { manager in
        manager.acceleration ?? Observable.empty()
    }
    .observeOn(MainScheduler.instance)
    .distinctUntilChanged({ (a, b) -> Bool in
      a.timestamp == b.timestamp
    })
      .subscribe(onNext: { (data) in
        
        if values != nil{
          values!(data.toRNFromat)
        }
        
        self.delegate?.retrieveAccelerometerValues(data: data.toRNFromat)
        
      }).disposed(by: disposeBag)
  }
  
  
  func getMagnetoMeterValues (interval: TimeInterval = 0.1, values: ((_ data: String) -> ())? ){
    
    coreMotionManager
      .flatMapLatest { manager in
        manager.magneticField ?? Observable.empty()
    }
      
    .observeOn(MainScheduler.instance)
    .distinctUntilChanged({ (a, b) -> Bool in
      a.timestamp == b.timestamp
    })
      .subscribe(onNext: { (data) in
        
        if values != nil{
          values!(data.toRNFromat)
        }
        
        self.delegate?.retrieveMagnetometerValues(data: data.toRNFromat)
        
      }).disposed(by: disposeBag)
  }
  
  func getDeviceMotionValues (interval: TimeInterval = 0.1, values: ((_ data: String) -> ())? ){
    
    coreMotionManager
      .flatMapLatest { manager in
        manager.deviceMotion ?? Observable.empty()
    }
    .observeOn(MainScheduler.instance)
    .distinctUntilChanged({ (a, b) -> Bool in
      a.timestamp == b.timestamp
    })
      .subscribe(onNext: { (data) in
        
        if values != nil{
          values!(data.toRNFromat)
        }
        
        self.delegate?.retrieveDeviceMotionValues(data: data.toRNFromat)
        
      }).disposed(by: disposeBag)
  }
  
  func getBarometerValues (interval: TimeInterval = 0.1, values: ((_ data: String) -> ())? ){
    
    barometerMotionManager
      .flatMapLatest { manager in
        manager.barometer ?? Observable.empty()
    }
    .observeOn(MainScheduler.instance)
    .distinctUntilChanged({ (a, b) -> Bool in
      a.timestamp == b.timestamp
    })
      .subscribe(onNext: { (data) in
        
        if values != nil{
          values!(data.toRNFromat)
        }
        
        self.delegate?.retrieveBaroMeterValues(data: data.toRNFromat)
        
      }).disposed(by: disposeBag)
  }
  
  func getPedometerValues (interval: TimeInterval = 0.1, values: ((_ data: String) -> ())? ){
    
    pedometerManager
      .flatMapLatest { manager in
        manager.pedometer ?? Observable.empty()
    }
    .observeOn(MainScheduler.instance)
    .distinctUntilChanged({ (a, b) -> Bool in
      a.timestamp == b.timestamp
    })
      .subscribe(onNext: { (data) in
        
        if values != nil{
          values!(data.toRNFromat)
        }
        
        self.delegate?.retrievePedoMeterValues(data: data.toRNFromat)
        
      }).disposed(by: disposeBag)
  }
  
  
  func getUserActivityValues (interval: TimeInterval = 0.1, values: ((_ data: String) -> ())? ){
    
    userMotionManager.flatMap { manager   in
      manager.motionActivity ?? Observable.empty()
    }
    .observeOn(MainScheduler.instance)
    .distinctUntilChanged({ (a, b) -> Bool in
      a.timestamp == b.timestamp
    })
      .subscribe(onNext: { (data) in
        
        
        if values != nil{
          values!(data.toRNFromat)
        }
        self.delegate?.retrieveUserMotionValues(data: data.toRNFromat)
        
      }).disposed(by: disposeBag)
    
    
  }
  
  /*
   *  getSensorObserver:interval:values:
   *
   *  Discussion:
   *   Starts all sensor data , providing data to the given handler through the given queue.
   *   Note that when the updates are stopped, all operations in the
   *   given NSOperationQueue will be cancelled. You can access the retrieved values either by a
   *   Trailing Closure or through a Delgate.
   */
  public func getSensorObserver (interval: TimeInterval = 0.1, values: ((_ sensor: String) -> ())? ) {
    
    
    let gyroRateObserver = coreMotionManager
      .flatMapLatest { manager in
        manager.gyroRate ?? Observable.empty()
    }
    //    .timeout(2, scheduler: MainScheduler.instance)
    //    .buffer(timeSpan: 1, count: 1000, scheduler: MainScheduler.instance)
    
    
    let accelerometerObserver = coreMotionManager
      .flatMapLatest { manager in
        manager.acceleration ?? Observable.empty()
    }
    
    let magneticFieldObserver = coreMotionManager
      .flatMapLatest { manager in
        manager.magneticField ?? Observable.empty()
    }
    
    let deviceMotionObserver = coreMotionManager
      .flatMapLatest { manager in
        manager.deviceMotion ?? Observable.empty()
    }
    
    let barometerObserver = barometerMotionManager
      .flatMapLatest { manager in
        manager.barometer ?? Observable.empty()
    }
    
    let pedometerObserver = pedometerManager
      .flatMapLatest { manager in
        manager.pedometer ?? Observable.empty()
    }
    
    //    .buffer(timeSpan: 1, count: 1000, scheduler: MainScheduler.instance)
    var count = 0
    let allsensor = Observable.zip(gyroRateObserver, accelerometerObserver, magneticFieldObserver,deviceMotionObserver,barometerObserver,pedometerObserver)
    
    allsensor.observeOn(MainScheduler.instance)
      
      //      .retryWhen({ (error) -> Observable<RxError> in
      //
      //        return error
      //      })
      .subscribe(onNext: { (arg0) in
        let (gyroscope, accelerometer, magnetometer, deviceMotion,barometer,pedometer) = arg0
        count += 1
        print("COUNT ==> \(count)")
        print("GYROSCOPE ==> \(gyroscope.jsonDictionary)")
        print("accelerometer ==> \(accelerometer.jsonDictionary)")
        print("magnetometer ==> \(magnetometer.jsonDictionary)")
        print("deviceMotion ==> \(deviceMotion.jsonDictionary.json)")
        print("Barometer ==> \(barometer.jsonDictionary)")
        print("Pedometer  ==> \(pedometer.jsonDictionary)")
        
      })
      .disposed(by: disposeBag)
    
  }
  
}
