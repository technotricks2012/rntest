//
//  RNMobilityKit.swift
//  MobilitySensing
//
//  Created by Kishore Raj on 10/12/19.
//  Copyright © 2019 Facebook. All rights reserved.
//

import Foundation
import CoreLocation

enum RNMobilityKitEventType : String{
  case RNMobilityKitLocation = "RNMobilityKitLocation"
  case RNMobilityKitGyroscope = "RNMobilityKitGyroscope"
  case RNMobilityKitAccelerometer = "RNMobilityKitAccelerometer"
  case RNMobilityKitMagnetoMeter = "RNMobilityKitMagnetoMeter"
  case RNMobilityKitDeviceMotion = "RNMobilityKitDeviceMotion"
  case RNMobilityKitBarometer = "RNMobilityKitBarometer"
  case RNMobilityKitPedometer = "RNMobilityKitPedometer"
  case RNMobilityKitUserActivity = "RNMobilityKitUserActivity"

  case RNMobilityKitServerCall = "RNMobilityKitServerCall"


}

@objc(RNMobilityKit)
class RNMobilityKit: RCTEventEmitter {
  var timer: Timer?

  
  override init() {
    super.init()
    
  }
  
  override func supportedEvents() -> [String]! {
    return [
      RNMobilityKitEventType.RNMobilityKitLocation.rawValue,
      RNMobilityKitEventType.RNMobilityKitGyroscope.rawValue,
      RNMobilityKitEventType.RNMobilityKitAccelerometer.rawValue,
      RNMobilityKitEventType.RNMobilityKitMagnetoMeter.rawValue,
      RNMobilityKitEventType.RNMobilityKitDeviceMotion.rawValue,
      RNMobilityKitEventType.RNMobilityKitBarometer.rawValue,
      RNMobilityKitEventType.RNMobilityKitPedometer.rawValue,
       RNMobilityKitEventType.RNMobilityKitUserActivity.rawValue,
      RNMobilityKitEventType.RNMobilityKitServerCall.rawValue,

    ]
  }
  
  @objc(startService:)
  func startService(interval: Int){
    
    startUpdateLocation(interval: interval)
  }
  
  @objc(startSensor:)
  func startSensor(interval: Int){
    
    MotionKit.Singleton.sharedInstance.delegate = self
    
    MotionKit.Singleton.sharedInstance.getAccelerometerValues { (data) in}

    MotionKit.Singleton.sharedInstance.getGyroValues { (data) in}
    MotionKit.Singleton.sharedInstance.getMagnetoMeterValues { (data) in}
    MotionKit.Singleton.sharedInstance.getBarometerValues { (data) in}
    MotionKit.Singleton.sharedInstance.getPedometerValues { (data) in}
    MotionKit.Singleton.sharedInstance.getDeviceMotionValues { (data) in}
    MotionKit.Singleton.sharedInstance.getUserActivityValues{ (data) in}

    
    self.timer?.invalidate()
    self.timer = nil
    DispatchQueue.main.async(execute: {
      self.timer =  Timer.scheduledTimer(
        timeInterval: TimeInterval(60),
        target: self,
        selector: #selector(self.uploadServerCallTrigger),
        userInfo: nil,
        repeats: true)
    })
    
  }
  
  @objc func uploadServerCallTrigger() {
     self.sendEvent(eventType: RNMobilityKitEventType.RNMobilityKitServerCall,body: "")
   }
  
  @objc(startUpdateLocation:)
  func startUpdateLocation(interval: Int){
    DispatchQueue.main.async {
      LocationKit.Singleton.sharedInstance.delegate = self
      LocationKit.Singleton.sharedInstance.startUpdatingLocation()
    }
    
  }
  
  @objc(stopUpdateLocation)
  func stopUpdateLocation(){
    DispatchQueue.main.async {
      LocationKit.Singleton.sharedInstance.stopUpdatingLocation()
    }
  }
  
  func sendEvent(eventType:RNMobilityKitEventType,body:String){
    sendEvent(withName: eventType.rawValue, body: body)
  }
  
}

extension RNMobilityKit: LocationKitDelegate {
  func locationdidUpdateLocations(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
    guard let mostRecentLocation = locations.last else {
      return
    }
    
    let latitude = mostRecentLocation.coordinate.latitude
    let longitude = mostRecentLocation.coordinate.longitude
    let altitude = mostRecentLocation.altitude
    
    var location = MKLocation(isAvailable: true)
       location.altitude = altitude
       location.latitude=latitude
       location.longitude = longitude
       location.timestamp = Date().toTimeStamp()

          
    
    self.sendEvent(eventType: RNMobilityKitEventType.RNMobilityKitLocation,body: location.toRNFromat)
  }
}



extension RNMobilityKit: MotionKitDelegate {
  func retrieveUserMotionValues(data: String) {

     self.sendEvent(eventType: RNMobilityKitEventType.RNMobilityKitUserActivity,body: data)
  }
  
  func retrieveAccelerometerValues(data: String) {

        self.sendEvent(eventType: RNMobilityKitEventType.RNMobilityKitAccelerometer,body: data)
  }
  
  func retrieveGyroscopeValues(data: String) {

    self.sendEvent(eventType: RNMobilityKitEventType.RNMobilityKitGyroscope,body:data)
    
  }
  
  func retrieveMagnetometerValues(data: String) {

    self.sendEvent(eventType: RNMobilityKitEventType.RNMobilityKitMagnetoMeter,body: data)

  }
  
  func retrievePedoMeterValues(data: String) {

    self.sendEvent(eventType: RNMobilityKitEventType.RNMobilityKitPedometer,body: data)

  }
  
  func retrieveBaroMeterValues(data: String) {

    self.sendEvent(eventType: RNMobilityKitEventType.RNMobilityKitBarometer,body: data)

  }
  
  func retrieveDeviceMotionValues(data: String) {

    self.sendEvent(eventType: RNMobilityKitEventType.RNMobilityKitDeviceMotion,body: data)

  }
  
}
