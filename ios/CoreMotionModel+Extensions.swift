//
//  CoreMotionModelExt.swift
//  MobilitySensing
//
//  Created by Kishore Raj on 12/12/19.
//  Copyright © 2019 Facebook. All rights reserved.
//

import Foundation
import Dispatch
import CoreMotion
import SwiftyJSON

extension CMAttitude  {
  var jsonDictionary:  [String:Any] {
    let json: [String:Any]  = [
      "roll": self.roll,
      "pitch": self.pitch,
      "azimuth": self.yaw,
    ]
    
    return json
  }
}

extension CMRotationRate  {
  var jsonDictionary:  [String:Any] {
    let json: [String:Any]  = [
      "x": self.x,
      "y": self.y,
      "z": self.z,
    ]
    return json
  }
}


extension CMAcceleration  {
  var jsonDictionary:  [String:Any] {
    let json: [String:Any]  = [
      "x": self.x,
      "y": self.y,
      "z": self.z,
    ]
    return json
  }
}


extension CMMagneticField  {
  var jsonDictionary:  [String:Any] {
    let json: [String:Any]  = [
      "x": self.x,
      "y": self.y,
      "z": self.z,
    ]
    return json
  }
}

extension CMMotionActivity  {
  var jsonDictionary:  [String:Any] {
    let json: [String:Any]  = [
      "isAutomotive": self.automotive,
      "isCycling": self.cycling,
      "isRunning": self.running,
      "isStationary": self.stationary,
      "isWalking": self.walking,
      "unknown": self.unknown,
      "startDate": self.startDate,
      "timestamp": self.timestamp,
    ]
    return json
  }
}




extension CMCalibratedMagneticField  {
  var jsonDictionary:  [String:Any] {
    let json: [String:Any]  = [
      "field": self.field.jsonDictionary
    ]
    return json
  }
}

public struct MKDeviceMotion{
  var attitude: CMAttitude?
  var rotationRate: CMRotationRate?
  var gravity: CMAcceleration?
  var userAcceleration: CMAcceleration?
  var magneticField: CMCalibratedMagneticField?
  var isAvailable: Bool? = false
  
  var timestamp: Int64?
  
  init() {}
  
  init(isAvailable:Bool = false) {
    self.isAvailable = isAvailable
  }
  
  var jsonDictionary:  [String:Any] {
    
    
    let json: [String:Any]  = [
      "attitude": self.attitude?.jsonDictionary,
      "rotationRate": self.rotationRate?.jsonDictionary,
      "gravity": self.gravity?.jsonDictionary,
      "userAcceleration": self.userAcceleration?.jsonDictionary,
      "magneticField": self.magneticField?.jsonDictionary,
      "isAvailable": self.isAvailable,
      "timestamp":self.timestamp
    ]
    return json
  }
  
  
  
  var toRNFromat:  String {
    var result:[String:Any]? = [String:Any]()
    
    if(self.isAvailable ?? false){
      result?["SuccessCode"] = "200"
      result?["Data"] = jsonDictionary
    }
    else{
      result?["SuccessCode"] = "500"
    }
    return result?.json ?? ""
  }
}

public struct MKAxisField : Codable{
  var x: Double?
  var y: Double?
  var z: Double?
  var timestamp: Int64?
  
  var isAvailable: Bool? = false
  
  init() {}
  
  init(isAvailable:Bool = false) {
    self.isAvailable = isAvailable
  }
  
  var jsonDictionary:  String {
    
    let json = try! JSONEncoder().encode(self)
    let jsonString = String(data: json, encoding: .utf8)!
    
    return jsonString
    
  }
  
  var toRNFromat:  String {
    var result:[String:Any]? = [String:Any]()
    
    if(self.isAvailable ?? false){
      result?["SuccessCode"] = "200"
      result?["Data"] = jsonDictionary.convertToDictionary()
    }
    else{
      result?["SuccessCode"] = "500"
    }
    return result?.json ?? ""
  }
  
}



public struct MKBarometer : Codable{
  
  var relativeAltitude: Double?
  var pressure: Double?
  var timestamp: Int64?
  
  var isAvailable: Bool? = false
  
  init() {}
  
  init(isAvailable:Bool = false) {
    self.isAvailable = isAvailable
  }
  
  var jsonDictionary:  String {
    
    let json = try! JSONEncoder().encode(self)
    let jsonString = String(data: json, encoding: .utf8)!
    
    return jsonString
    
  }
  
  var toRNFromat:  String {
    var result:[String:Any]? = [String:Any]()
    
    if(self.isAvailable ?? false){
      result?["SuccessCode"] = "200"
      result?["Data"] = jsonDictionary.convertToDictionary()
    }
    else{
      result?["SuccessCode"] = "500"
    }
    return result?.json ?? ""
  }
  
  
}



public struct MKPedometer : Codable{
  
  var startDate: Date?
  var endDate: Date?
  var numberOfSteps: Int?
  var distance: Double?
  var floorsAscended: Int?
  var floorsDescended: Int?
  var timestamp: Int64?
  
  
  var isAvailable: Bool? = false
  
  init() {}
  
  init(isAvailable:Bool = false) {
    self.isAvailable = isAvailable
  }
  
  var jsonDictionary:  String {
    
    let json = try! JSONEncoder().encode(self)
    let jsonString = String(data: json, encoding: .utf8)!
    
    return jsonString
    
  }
  
  var toRNFromat:  String {
    var result:[String:Any]? = [String:Any]()
    
    if(self.isAvailable ?? false){
      result?["SuccessCode"] = "200"
      result?["Data"] = jsonDictionary.convertToDictionary()
    }
    else{
      result?["SuccessCode"] = "500"
    }
    return result?.json ?? ""
  }
  
}


public struct MKLocation : Codable{
  
  var latitude: Double?
  var longitude: Double?
  var altitude: Double?
  
  var timestamp: Int64?
  
  var isAvailable: Bool? = false
  
  init() {}
  
  init(isAvailable:Bool = false) {
    self.isAvailable = isAvailable
  }
  
  var jsonDictionary:  String {
    
    let json = try! JSONEncoder().encode(self)
    let jsonString = String(data: json, encoding: .utf8)!
    return jsonString
    
  }
  
  var toRNFromat:  String {
    var result:[String:Any]? = [String:Any]()
    if(self.isAvailable ?? false){
      result?["SuccessCode"] = "200"
      result?["Data"] = jsonDictionary.convertToDictionary()
    }
    else{
      result?["SuccessCode"] = "500"
    }
    return result?.json ?? ""
  }
  
}



public struct MKMotionActivity : Codable {
  
  var isAutomotive: Bool?
  var isCycling: Bool?
  var isRunning: Bool?
  var isStationary: Bool?
  
  var isWalking: Bool?
  
  var unknown: Bool?
  
  var startDate: Date?
  
  
  var timestamp: Int64?
  var confidence: Int?

  
  
  var isAvailable: Bool? = false
  
  init() {}
  
  init(isAvailable:Bool = false) {
    self.isAvailable = isAvailable
  }
  
  var jsonDictionary:  String {
    
    let json = try! JSONEncoder().encode(self)
    let jsonString = String(data: json, encoding: .utf8)!
    
    return jsonString
    
  }
  
  var toRNFromat:  String {
    var result:[String:Any]? = [String:Any]()
    
    if(self.isAvailable ?? false){
      result?["SuccessCode"] = "200"
      result?["Data"] = jsonDictionary.convertToDictionary()
    }
    else{
      result?["SuccessCode"] = "500"
    }
    return result?.json ?? ""
  }
  
}
