//
//  Counter.m
//  Sensor
//
//  Created by Kishore Raj on 9/10/19.
//  Copyright © 2019 Facebook. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "React/RCTBridgeModule.h"
#import "React/RCTEventEmitter.h"

@interface RCT_EXTERN_MODULE(QuickServiceInterval, RCTEventEmitter)

RCT_EXTERN_METHOD(startTimer:(NSInteger)interval)
RCT_EXTERN_METHOD(stopTimer)
RCT_EXTERN_METHOD(getCount: (RCTResponseSenderBlock)callback)

@end
