//
//  Date+Extension.swift
//  MobilitySensing
//
//  Created by Kishore Raj on 20/12/19.
//  Copyright © 2019 Facebook. All rights reserved.
//

import Foundation


extension Date{
  func toTimeStamp() -> Int64 {
    return Int64(self.timeIntervalSince1970 * 1000)
  }
  
}
