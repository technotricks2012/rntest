//
//  Counter.swift
//  Sensor
//
//  Created by Kishore Raj on 9/10/19.
//  Copyright © 2019 Facebook. All rights reserved.
//

import Foundation
@objc(QuickServiceInterval)
class QuickServiceInterval: RCTEventEmitter {
  
  var timer: Timer?

  
  @objc(startTimer:)
  func startTimer(interval: Int){
//    timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(Counter.update), userInfo: nil, repeats: true)
    print("Native Call- startTimer==>",interval ?? "no")

    DispatchQueue.main.async(execute: {
      self.timer =  Timer.scheduledTimer(
        timeInterval: TimeInterval(interval),
        target: self,
        selector: #selector(self.update),
        userInfo: nil,
        repeats: true)
    })
  
  }
  
  @objc
  func stopTimer() {
    print("Native Call- stopTimer")
    self.timer?.invalidate()
    self.timer = nil
  }
  
  override func supportedEvents() -> [String]! {
    return ["QuickServiceInterval"]
  }

  @objc func update() {
    print("KISHORE 2222")
    sendEvent(withName: "QuickServiceInterval", body: ["Event" : "Start"])
  }
//
//  @objc
//  func getCount(_ callback: RCTResponseSenderBlock) {
//    callback([9])
//  }

}
