//
//  MotionActivityManager.swift
//  MobilitySensing
//
//  Created by Kishore Raj on 12/12/19.
//  Copyright © 2019 Facebook. All rights reserved.
//

import Foundation
import CoreMotion
import RxSwift

extension Reactive where Base: CMMotionActivityManager {
  static public func manager(createActivityManager: @escaping () throws -> CMMotionActivityManager = { CMMotionActivityManager() }) -> Observable<MotionActivityManager> {
    return Observable.create { observer in
      do {
        let activityManager = try createActivityManager()
        observer.on(.next(MotionActivityManager(motionActivityManager: activityManager)))
      }
      catch let e {
        observer.on(.error(e))
      }
      return Disposables.create()
    }.share(replay: 1)
  }
}

var motionActivityKey: UInt8  = 0

extension Reactive where Base: CMMotionActivityManager {
  public var motionActivity: Observable<MKMotionActivity> {
    return memoize(key: &motionActivityKey) {
      Observable.create { observer in
        let activityManager = self.base
        let operationQueue = OperationQueue()
        operationQueue.maxConcurrentOperationCount = 1
        
        activityManager.startActivityUpdates(to: operationQueue, withHandler: { (userActivitydata) in
          guard let userActivitydata = userActivitydata else {
            return
          }
          
          var data = MKMotionActivity(isAvailable: true)
          data.isAutomotive = userActivitydata.automotive
          data.isCycling = userActivitydata.cycling
          data.isRunning = userActivitydata.running
          data.isWalking = userActivitydata.walking
          data.isStationary = userActivitydata.stationary
          data.unknown = userActivitydata.unknown
          data.startDate = userActivitydata.startDate
          data.confidence = userActivitydata.confidence.rawValue
          data.timestamp = Date().toTimeStamp()
          
          
          
          observer.on(.next(data))
          
          
        })
        
        return Disposables.create() {
          activityManager.stopActivityUpdates()
        }
      }.share(replay: 1)
    }
  }
}


extension Reactive where Base: CMMotionActivityManager {
  func memoize<D>(key: UnsafeRawPointer, createLazily: () -> Observable<D>) -> Observable<D> {
    objc_sync_enter(self); defer { objc_sync_exit(self) }
    
    if let sequence = objc_getAssociatedObject(self, key) as? Observable<D> {
      return sequence
    }
    
    let sequence = createLazily()
    objc_setAssociatedObject(self, key, sequence, .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
    
    return sequence
  }
}

// If the current device supports one of the capabilities, observable sequence will not be nil
public struct MotionActivityManager {
  public let motionActivity: Observable<MKMotionActivity>?
  
  public init(motionActivityManager: CMMotionActivityManager) {
    if CMMotionActivityManager.isActivityAvailable() {
      self.motionActivity = motionActivityManager.rx.motionActivity
    }
    else {
      self.motionActivity = Observable.just(MKMotionActivity(isAvailable: false))
    }
  }
}
