//
//  String+Extension.swift
//  MobilitySensing
//
//  Created by Kishore Raj on 16/12/19.
//  Copyright © 2019 Facebook. All rights reserved.
//

import Foundation


extension String {
  
  func convertToDictionary() -> [String: Any]? {
      if let data = self.data(using: .utf8) {
          do {
              return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
          } catch {
              print(error.localizedDescription)
          }
      }
      return nil
  }
}
