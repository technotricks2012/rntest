//
//  RxCoreMotion.swift
//  MobilitySensing
//
//  Created by Kishore Raj on 12/12/19.
//  Copyright © 2019 Facebook. All rights reserved.
//

import Foundation
import CoreMotion
import RxSwift

extension Reactive where Base: CMMotionManager {
  static public func manager(interval:Double,createMotionManager: @escaping () throws -> CMMotionManager = { CMMotionManager() }) -> Observable<MotionManager> {
    return Observable.create { observer in
      do {
        let motionManager = try createMotionManager()
        
        updateInterval = interval
        observer.on(.next(MotionManager(motionManager: motionManager)))
      }
      catch let e {
        observer.on(.error(e))
      }
      return Disposables.create()
    }.share(replay: 1)
  }
}

extension Reactive where Base: CMAltimeter {
  static public func manager(interval:Double,createBarometerManager: @escaping () throws ->
    CMAltimeter = { CMAltimeter() }) -> Observable<BarometerManager> {
    return Observable.create { observer in
      do {
        let barometerManager = try createBarometerManager()
        
        updateInterval = interval
        observer.on(.next(BarometerManager(barometerManager: barometerManager)))
      }
      catch let e {
        observer.on(.error(e))
      }
      return Disposables.create()
    }.share(replay: 1)
  }
}


extension Reactive where Base: CMPedometer {
  static public func manager(interval:Double,createPedometerManager: @escaping () throws ->
    CMPedometer = { CMPedometer() }) -> Observable<PedometerManager> {
    return Observable.create { observer in
      do {
        let pedometerManager = try createPedometerManager()
        
        updateInterval = interval
        observer.on(.next(PedometerManager(pedometerManager: pedometerManager)))
      }
      catch let e {
        observer.on(.error(e))
      }
      return Disposables.create()
    }.share(replay: 1)
  }
}



var accelerationKey: UInt8  = 0
var accelerometerDataKey: UInt8  = 0
var gyroKey: UInt8      = 0
var magneticFieldKey: UInt8 = 0
var deviceMotionKey: UInt8  = 0
var pedometerKey: UInt8  = 0
var barometerKey: UInt8  = 0

var updateInterval:Double = 0
extension Reactive where Base: CMMotionManager {
  public var acceleration: Observable<MKAxisField> {
    return memoize(key: &accelerationKey) {
      Observable.create { observer in
        let motionManager = self.base
        let operationQueue = OperationQueue()
        operationQueue.maxConcurrentOperationCount = 1
        motionManager.accelerometerUpdateInterval = TimeInterval(updateInterval)
        motionManager.startAccelerometerUpdates(to: operationQueue, withHandler: { (accData: CMAccelerometerData?, error: Error?) -> Void in
          
          
          guard let accData = accData else {
            return
          }
          
          var data = MKAxisField(isAvailable: true)
          data.x = accData.acceleration.x
          data.y = accData.acceleration.y
          data.z = accData.acceleration.z
          data.timestamp = Date().toTimeStamp()
          
          observer.on(.next(data))
        })
        
        return Disposables.create() {
          motionManager.stopAccelerometerUpdates()
        }
      }.share(replay: 1)
    }
  }
  
  
  
  public var gyroRate: Observable<MKAxisField> {
    return memoize(key: &gyroKey) {
      Observable.create { observer in
        let motionManager = self.base
        let operationQueue = OperationQueue()
        operationQueue.maxConcurrentOperationCount = 1
        motionManager.gyroUpdateInterval = TimeInterval(updateInterval)
        motionManager.startGyroUpdates(to: operationQueue, withHandler: { (rotationData: CMGyroData?, error: Error?) -> Void in
          guard let rotationData = rotationData else {
            return
          }
          
          var data = MKAxisField(isAvailable: true)
          data.x = rotationData.rotationRate.x
          data.y = rotationData.rotationRate.y
          data.z = rotationData.rotationRate.z
          data.timestamp = Date().toTimeStamp()
          
          observer.on(.next(data))
          
          
        })
        
        return Disposables.create() {
          motionManager.stopGyroUpdates()
        }
      }.share(replay: 1)
    }
  }
  
  public var magneticField: Observable<MKAxisField> {
    return memoize(key: &magneticFieldKey) {
      Observable.create { observer in
        let motionManager = self.base
        let operationQueue = OperationQueue()
        operationQueue.maxConcurrentOperationCount = 1
        motionManager.magnetometerUpdateInterval = TimeInterval(updateInterval)
        motionManager.startMagnetometerUpdates(to: operationQueue, withHandler: { (magneticFieldData: CMMagnetometerData?, error: Error?) -> Void in
          guard let magneticFieldData = magneticFieldData else {
            return
          }
          
          var data = MKAxisField(isAvailable: true)
          data.x = magneticFieldData.magneticField.x
          data.y = magneticFieldData.magneticField.y
          data.z = magneticFieldData.magneticField.z
          data.timestamp = Date().toTimeStamp()
          
          observer.on(.next(data))
          
        })
        
        return Disposables.create() {
          motionManager.stopMagnetometerUpdates()
        }
      }.share(replay: 1)
    }
  }
  
  public var deviceMotion: Observable<MKDeviceMotion> {
    return memoize(key: &deviceMotionKey) {
      Observable.create { observer in
        let motionManager = self.base
        let operationQueue = OperationQueue()
        operationQueue.maxConcurrentOperationCount = 1
        
        motionManager.deviceMotionUpdateInterval = TimeInterval(updateInterval)
        motionManager.startDeviceMotionUpdates(to: operationQueue, withHandler: { (deviceMotion: CMDeviceMotion?, error: Error?) -> Void in
          guard let deviceMotion = deviceMotion else {
            return
          }
          var data = MKDeviceMotion(isAvailable: true)
          data.attitude = deviceMotion.attitude
          data.rotationRate = deviceMotion.rotationRate
          data.gravity = deviceMotion.gravity
          data.userAcceleration = deviceMotion.userAcceleration
          data.magneticField = deviceMotion.magneticField
          data.timestamp = Date().toTimeStamp()

          
          observer.on(.next(data))
        })
        
        return Disposables.create() {
          motionManager.stopDeviceMotionUpdates()
        }
      }.share(replay: 1)
    }
  }
}


extension Reactive where Base: CMMotionManager {
  func memoize<D>(key: UnsafeRawPointer, createLazily: () -> Observable<D>) -> Observable<D> {
    objc_sync_enter(self); defer { objc_sync_exit(self) }
    
    if let sequence = objc_getAssociatedObject(self, key) as? Observable<D> {
      return sequence
    }
    
    let sequence = createLazily()
    objc_setAssociatedObject(self, key, sequence, .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
    
    return sequence
  }
}



extension Reactive where Base: CMAltimeter {
  
  public var barometer: Observable<MKBarometer> {
    return memoize(key: &barometerKey) {
      Observable.create { observer in
        let barometerManager = self.base
        let operationQueue = OperationQueue()
        operationQueue.maxConcurrentOperationCount = 1
        barometerManager.startRelativeAltitudeUpdates(to: operationQueue, withHandler: { (barometerData: CMAltitudeData?, error: Error?) -> Void in
          guard let barometerData = barometerData else {
            return
          }
          
          var data = MKBarometer(isAvailable: true)
          data.timestamp = Date().toTimeStamp()
          data.pressure = Double(exactly: barometerData.pressure)
          data.relativeAltitude = Double(exactly: barometerData.relativeAltitude)
          
          observer.on(.next(data))
        })
        
        return Disposables.create() {
          barometerManager.stopRelativeAltitudeUpdates()
        }
      }.share(replay: 1)
    }
  }
  
  func memoize<D>(key: UnsafeRawPointer, createLazily: () -> Observable<D>) -> Observable<D> {
    objc_sync_enter(self); defer { objc_sync_exit(self) }
    
    if let sequence = objc_getAssociatedObject(self, key) as? Observable<D> {
      return sequence
    }
    
    let sequence = createLazily()
    objc_setAssociatedObject(self, key, sequence, .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
    
    return sequence
  }
}



extension Reactive where Base: CMPedometer {
  public func pedometer(from: Date! = Date()) -> Observable<MKPedometer> {
    return memoize(key: &pedometerKey) {
      Observable.create { observer in
        let pedometer = self.base
        
        pedometer.startUpdates(from: from, withHandler: {(pedometerData, error) in
          guard let pedometerData = pedometerData else {
            return
          }
          
          var data = MKPedometer(isAvailable: true)
          data.startDate = pedometerData.startDate
          data.endDate = pedometerData.endDate
          data.numberOfSteps = Int(exactly: pedometerData.numberOfSteps)
          data.distance = Double(exactly: pedometerData.distance ?? 0.0)
          data.floorsAscended = Int(exactly: pedometerData.floorsAscended ?? 0)
          data.floorsDescended = Int(exactly: pedometerData.floorsDescended ?? 0)
          data.timestamp = Date().toTimeStamp()

          observer.on(.next(data))
        })
        return Disposables.create() {
          pedometer.stopUpdates()
          if #available(iOS 10.0, *) {
            pedometer.stopEventUpdates()
          }
        }
      }.share(replay: 1)
    }
  }
  
  
  public var pedometer: Observable<MKPedometer> {
    return memoize(key: &pedometerKey) {
      Observable.create { observer in
        let pedometer = self.base
        
        pedometer.startUpdates(from: Date(), withHandler: {(pedometerData, error) in
          guard let pedometerData = pedometerData else {
            return
          }
          
          var data = MKPedometer(isAvailable: true)
          data.startDate = pedometerData.startDate
          data.endDate = pedometerData.endDate
          data.numberOfSteps = Int(exactly: pedometerData.numberOfSteps)
          data.distance = Double(exactly: pedometerData.distance ?? 0.0)
          data.floorsAscended = Int(exactly: pedometerData.floorsAscended ?? 0)
          data.floorsDescended = Int(exactly: pedometerData.floorsDescended ?? 0)
          data.timestamp = Date().toTimeStamp()

          observer.on(.next(data))
        })
        return Disposables.create() {
          pedometer.stopUpdates()
          if #available(iOS 10.0, *) {
            pedometer.stopEventUpdates()
          }
        }
      }.share(replay: 1)
    }
  }
  
  func memoize<D>(key: UnsafeRawPointer, createLazily: () -> Observable<D>) -> Observable<D> {
    objc_sync_enter(self); defer { objc_sync_exit(self) }
    
    if let sequence = objc_getAssociatedObject(self, key) as? Observable<D> {
      return sequence
    }
    
    let sequence = createLazily()
    objc_setAssociatedObject(self, key, sequence, .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
    
    return sequence
  }
}


// If the current device supports one of the capabilities, observable sequence will not be nil
public struct MotionManager {
  public let acceleration: Observable<MKAxisField>?
  //  public let accelerometerData: Observable<CMAccelerometerData>?
  public let gyroRate: Observable<MKAxisField>?
  public let magneticField: Observable<MKAxisField>?
  public let deviceMotion: Observable<MKDeviceMotion>?
  
  
  public init(motionManager: CMMotionManager) {
    if motionManager.isAccelerometerAvailable {
      self.acceleration = motionManager.rx.acceleration
      //      self.acceleration = Observable.just(MKAxisField(isAvailable: false))
      //      .retry()
      //Observable.of(MKAxisField(isAvailable: false))
      //      self.accelerometerData = motionManager.rx.accelerometerData
    }
    else {
      self.acceleration = Observable.just(MKAxisField(isAvailable: false))
      //      self.accelerometerData = nil
    }
    
    if motionManager.isGyroAvailable {
      self.gyroRate = motionManager.rx.gyroRate
    }
    else {
      self.gyroRate =  Observable.just(MKAxisField(isAvailable: false))
    }
    
    if motionManager.isMagnetometerAvailable {
      self.magneticField = motionManager.rx.magneticField
    }
    else {
      self.magneticField = Observable.just(MKAxisField(isAvailable: false))
    }
    
    if motionManager.isDeviceMotionAvailable {
      self.deviceMotion = motionManager.rx.deviceMotion
    }
    else {
      self.deviceMotion = Observable.just(MKDeviceMotion(isAvailable: false))
    }
  }
  
  
}

public struct BarometerManager {
  public let barometer: Observable<MKBarometer>?
  
  public init(barometerManager: CMAltimeter) {
    if CMAltimeter.isRelativeAltitudeAvailable() {
      self.barometer = barometerManager.rx.barometer
    }
    else{
      self.barometer = Observable.just(MKBarometer(isAvailable: false))
    }
    
  }
  
}


public struct PedometerManager {
  public let pedometer: Observable<MKPedometer>?
  
  public init(pedometerManager: CMPedometer) {
    if CMPedometer.isStepCountingAvailable() {
      self.pedometer = pedometerManager.rx.pedometer
    }
    else{
      self.pedometer = Observable.just(MKPedometer(isAvailable: false))
    }
    
  }
  
}
