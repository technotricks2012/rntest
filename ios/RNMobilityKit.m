//
//  RNMobilityKit.m
//  MobilitySensing
//
//  Created by Kishore Raj on 10/12/19.
//  Copyright © 2019 Facebook. All rights reserved.
//


#import <Foundation/Foundation.h>
#import "React/RCTBridgeModule.h"
#import "React/RCTEventEmitter.h"

@interface RCT_EXTERN_MODULE(RNMobilityKit, RCTEventEmitter)
RCT_EXTERN_METHOD(startService:(NSInteger)interval)
RCT_EXTERN_METHOD(stopService)

RCT_EXTERN_METHOD(startSensor:(NSInteger)interval)
RCT_EXTERN_METHOD(stopSensor)


RCT_EXTERN_METHOD(startUpdateLocation:(NSInteger)interval)
RCT_EXTERN_METHOD(stopUpdateLocation)
//RCT_EXTERN_METHOD(getCount: (RCTResponseSenderBlock)callback)

@end
