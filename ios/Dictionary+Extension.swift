//
//  Dictionary+Extension.swift
//  MobilitySensing
//
//  Created by Kishore Raj on 13/12/19.
//  Copyright © 2019 Facebook. All rights reserved.
//

import Foundation


extension Dictionary {

    var json: String {
        let invalidJson = "Not a valid JSON"
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: self, options: .prettyPrinted)
            return String(bytes: jsonData, encoding: String.Encoding.utf8) ?? invalidJson
        } catch {
            return invalidJson
        }
    }

}


